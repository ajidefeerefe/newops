webpackJsonp([84],{

/***/ 150:
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(276)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}
var options = null
var ssrIdKey = 'data-vue-ssr-id'

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction, _options) {
  isProduction = _isProduction

  options = _options || {}

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[' + ssrIdKey + '~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }
  if (options.ssrId) {
    styleElement.setAttribute(ssrIdKey, obj.id)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ 276:
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ }),

/***/ 310:
/***/ (function(module, exports) {

module.exports = "/images/rytegate-fav.png?766e05e1b444ff81b63fe2a81392084e";

/***/ }),

/***/ 311:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(312);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(150)("68a6ef11", content, true, {});

/***/ }),

/***/ 312:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(12)(false);
// imports


// module
exports.push([module.i, "input,select{text-transform:capitalize!important}input[type*=email]{text-transform:lowercase!important}", ""]);

// exports


/***/ }),

/***/ 313:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


exports.default = {
    mounted: function mounted() {
        console.log(this.$store.state.currentUser.permissions);
    },

    methods: {
        logout: function logout() {
            this.$store.commit('logout');
            this.$router.push('/login');
        }
    },
    computed: {
        permissions: function permissions() {
            if (this.$store.state.currentUser != null) {
                return this.$store.state.currentUser.permissions;
            } else {
                return {};
            }
        }
    }

};

/***/ }),

/***/ 314:
/***/ (function(module, exports, __webpack_require__) {

var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"wrapper"},[_c('header',{staticClass:"topnavbar-wrapper"},[_c('nav',{staticClass:"navbar topnavbar",attrs:{"role":"navigation"}},[_c('div',{staticClass:"navbar-header"},[_c('router-link',{attrs:{"to":"/"}},[_c('a',{staticClass:"navbar-brand",attrs:{"href":""}},[_c('div',{staticClass:"brand-logo"},[_c('img',{staticClass:"img-responsive",staticStyle:{"height":"50px"},attrs:{"src":__webpack_require__(310),"alt":"Admin Logo"}})]),_vm._v(" "),_c('div',{staticClass:"brand-logo-collapsed"},[_c('img',{staticClass:"img-responsive",attrs:{"src":__webpack_require__(310),"alt":"Admin Logo"}})])])])],1),_vm._v(" "),_vm._m(0)])]),_vm._v(" "),_c('aside',{staticClass:"aside"},[_c('div',{staticClass:"aside-inner"},[_c('nav',{staticClass:"sidebar",attrs:{"data-sidebar-anyclick-close":""}},[_c('ul',{staticClass:"nav menu"},[_vm._m(1),_vm._v(" "),_c('li',[_c('router-link',{attrs:{"to":{ name: 'dashboard' }}},[_c('em',{staticClass:"material-icons"},[_vm._v("dashboard")]),_vm._v(" "),_c('span',[_vm._v("Dashboard")])])],1),_vm._v(" "),(_vm.permissions.system_setup)?_c('li',[_vm._m(2),_vm._v(" "),_c('ul',{staticClass:"nav sidebar-subnav collapse",attrs:{"id":"layout"}},[_c('li',{staticClass:"sidebar-subnav-header"},[_vm._v("System Setup")]),_vm._v(" "),_c('li',[_vm._m(3),_vm._v(" "),_c('ul',{staticClass:"nav sidebar-subnav collapse",attrs:{"id":"finance"}},[_c('li',{staticClass:"sidebar-subnav-header"},[_vm._v("Finance")]),_vm._v(" "),(_vm.permissions.banks)?_c('li',[_c('router-link',{attrs:{"to":{ name: 'banks' }}},[_c('span',[_vm._v("Banks")])])],1):_vm._e(),_vm._v(" "),_c('li',[_c('router-link',{attrs:{"to":{ name: 'earnings' }}},[_c('span',[_vm._v("Earnings")])])],1),_vm._v(" "),_c('li',[_c('router-link',{attrs:{"to":{ name: 'deductions' }}},[_c('span',[_vm._v("Deductions")])])],1),_vm._v(" "),_c('li',[_c('router-link',{attrs:{"to":{ name: 'chartofaccounts' }}},[_c('span',[_vm._v("Chart Of Accounts")])])],1),_vm._v(" "),_c('li',[_c('router-link',{attrs:{"to":{ name: 'payperiods' }}},[_c('span',[_vm._v("Pay Periods")])])],1),_vm._v(" "),(_vm.permissions.imprest_category)?_c('li',[_c('router-link',{attrs:{"to":{ name: 'paymentcategory' }}},[_c('span',[_vm._v("Payment Category")])])],1):_vm._e()])]),_vm._v(" "),_c('li',[_vm._m(4),_vm._v(" "),_c('ul',{staticClass:"nav sidebar-subnav collapse",attrs:{"id":"asset"}},[_c('li',{staticClass:"sidebar-subnav-header"},[_vm._v("Assets")]),_vm._v(" "),(_vm.permissions.color)?_c('li',[_c('router-link',{attrs:{"to":{ name: 'colors' }}},[_c('span',[_vm._v("Colors")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.asset_category)?_c('li',[_c('router-link',{attrs:{"to":{ name: 'assetcategory' }}},[_c('span',[_vm._v("Asset Category")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.asset_manufacturer)?_c('li',[_c('router-link',{attrs:{"to":{ name: 'assetmanufacturer' }}},[_c('span',[_vm._v("Asset Manufacturer")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.asset_model)?_c('li',[_c('router-link',{attrs:{"to":{ name: 'assetmodel' }}},[_c('span',[_vm._v("Asset Model")])])],1):_vm._e()])]),_vm._v(" "),_c('li',[_vm._m(5),_vm._v(" "),_c('ul',{staticClass:"nav sidebar-subnav collapse",attrs:{"id":"user"}},[_c('li',{staticClass:"sidebar-subnav-header"},[_vm._v("User")]),_vm._v(" "),(_vm.permissions.user_role)?_c('li',[_c('router-link',{attrs:{"to":{ name: 'user-roles' }}},[_c('span',[_vm._v("Users Role")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.user_department)?_c('li',[_c('router-link',{attrs:{"to":{ name: 'user-departments' }}},[_c('span',[_vm._v("Users Department")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.user_position)?_c('li',[_c('router-link',{attrs:{"to":{ name: 'user-positions' }}},[_c('span',[_vm._v("Users Position")])])],1):_vm._e()])]),_vm._v(" "),_c('li',[_vm._m(6),_vm._v(" "),_c('ul',{staticClass:"nav sidebar-subnav collapse",attrs:{"id":"project"}},[_c('li',{staticClass:"sidebar-subnav-header"},[_vm._v("Project")]),_vm._v(" "),_c('li',[_c('router-link',{attrs:{"to":{ name: 'material-services-category' }}},[_c('span',[_vm._v("Material & Services Category")])])],1),_vm._v(" "),(_vm.permissions.material_and_labour)?_c('li',[_c('router-link',{attrs:{"to":{ name: 'material-services' }}},[_c('span',[_vm._v("Materials & Services")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.project_phase_setup)?_c('li',[_c('router-link',{attrs:{"to":{ name: 'projectphases' }}},[_c('span',[_vm._v("Project Phase")])])],1):_vm._e(),_vm._v(" "),_c('li',[_c('router-link',{attrs:{"to":{ name: 'projectphaseactivities' }}},[_c('span',[_vm._v("Phase Activity")])])],1),_vm._v(" "),_c('li',[_c('router-link',{attrs:{"to":{ name: 'highlevelprojectphases' }}},[_c('span',[_vm._v("High Level Project")])])],1)])]),_vm._v(" "),(_vm.permissions.inventory_location)?_c('li',[_c('router-link',{attrs:{"to":{ name: 'inventorylocation' }}},[_c('span',[_vm._v("Inventory Location")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.vendor_category)?_c('li',[_c('router-link',{attrs:{"to":{ name: 'vendorcategory' }}},[_c('span',[_vm._v("Vendor Category")])])],1):_vm._e()])]):_vm._e(),_vm._v(" "),(_vm.permissions.user_administration)?_c('li',[_vm._m(7),_vm._v(" "),_c('ul',{staticClass:"nav sidebar-subnav collapse",attrs:{"id":"components"}},[_c('li',{staticClass:"sidebar-subnav-header"},[_vm._v("User Administration")]),_vm._v(" "),(_vm.permissions.user_profile)?_c('li',[_c('router-link',{attrs:{"to":{ name: 'users-profiles' }}},[_c('span',[_vm._v("View/Edit User Profile")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.user_login)?_c('li',[_c('router-link',{attrs:{"to":{ name: 'user-login' }}},[_c('span',[_vm._v("View/Edit User Login")])])],1):_vm._e()])]):_vm._e(),_vm._v(" "),_c('li',[_vm._m(8),_vm._v(" "),_c('ul',{staticClass:"nav sidebar-subnav collapse",attrs:{"id":"hr"}},[_c('li',{staticClass:"sidebar-subnav-header"},[_vm._v("HR Management")]),_vm._v(" "),_c('li',[_c('router-link',{attrs:{"to":{ name: 'users-profiles' }}},[_c('span',[_vm._v("View/Edit User Profile")])])],1),_vm._v(" "),_c('li',[_c('router-link',{attrs:{"to":{ name: 'user-login' }}},[_c('span',[_vm._v("View/Edit User Login")])])],1)])]),_vm._v(" "),_c('li',[_vm._m(9),_vm._v(" "),_c('ul',{staticClass:"nav sidebar-subnav collapse",attrs:{"id":"legal"}},[_c('li',{staticClass:"sidebar-subnav-header"},[_vm._v("Legal And Contracts")]),_vm._v(" "),_c('li',[_c('router-link',{attrs:{"to":{ name: 'users-profiles' }}},[_c('span',[_vm._v("View/Edit User Profile")])])],1),_vm._v(" "),_c('li',[_c('router-link',{attrs:{"to":{ name: 'user-login' }}},[_c('span',[_vm._v("View/Edit User Login")])])],1)])]),_vm._v(" "),(_vm.permissions.client_administration)?_c('li',[_vm._m(10),_vm._v(" "),_c('ul',{staticClass:"nav sidebar-subnav collapse",attrs:{"id":"advanceui"}},[_c('li',{staticClass:"sidebar-subnav-header"},[_vm._v("Client Administration")]),_vm._v(" "),(_vm.permissions.view_client)?_c('li',[_c('router-link',{attrs:{"to":{ name: 'client-profiles' }}},[_c('span',[_vm._v("View/Edit Client Profile")])])],1):_vm._e()])]):_vm._e(),_vm._v(" "),(_vm.permissions.asset_manager)?_c('li',[_vm._m(11),_vm._v(" "),_c('ul',{staticClass:"nav sidebar-subnav collapse",attrs:{"id":"elements"}},[_c('li',{staticClass:"sidebar-subnav-header"},[_vm._v("Asset Manager")]),_vm._v(" "),_c('li',[_c('router-link',{attrs:{"to":{ name: 'liquid-assets' }}},[_c('span',[_vm._v("Liquid Assets")])])],1)])]):_vm._e(),_vm._v(" "),(_vm.permissions.carpool_manager)?_c('li',[_vm._m(12),_vm._v(" "),(_vm.permissions.vehicle_request)?_c('ul',{staticClass:"nav sidebar-subnav collapse",attrs:{"id":"forms"}},[_c('li',{staticClass:"sidebar-subnav-header"},[_vm._v("CarPool Manager")]),_vm._v(" "),_c('li',[_c('router-link',{attrs:{"to":{ name: 'vehicle-request' }}},[_c('span',[_vm._v("Vehicle Requests")])])],1),_vm._v(" "),_c('li',[_c('router-link',{attrs:{"to":{ name: 'vehicle-assignments' }}},[_c('span',[_vm._v("Vehicle Assignments")])])],1)]):_vm._e()]):_vm._e(),_vm._v(" "),(_vm.permissions.finance_administration)?_c('li',[_vm._m(13),_vm._v(" "),_c('ul',{staticClass:"nav sidebar-subnav collapse",attrs:{"id":"charts"}},[_c('li',{staticClass:"sidebar-subnav-header"},[_vm._v("Finance Administration")]),_vm._v(" "),(_vm.permissions.project_costing)?_c('li',[_c('router-link',{attrs:{"to":{ name: 'project-costing' }}},[_c('span',[_vm._v("Project Costing")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.imprest_manager)?_c('li',[_c('router-link',{attrs:{"to":{ name: 'account-receivables' }}},[_c('span',[_vm._v("Account Receivables")])])],1):_vm._e(),_vm._v(" "),_c('li',[_c('router-link',{attrs:{"to":{ name: 'purchase-requisition' }}},[_c('span',[_vm._v("Purchase Requisition")])])],1),_vm._v(" "),(_vm.permissions.employee_imprest)?_c('li',[_c('router-link',{attrs:{"to":{ name: 'personnel-imprest' }}},[_c('span',[_vm._v("Personnel Imprest")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.contracts)?_c('li',[_c('router-link',{attrs:{"to":{ name: 'vendor-contract-categories' }}},[_c('span',[_vm._v("Vendor Contracts")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.inventory_store)?_c('li',[_c('router-link',{attrs:{"to":{ name: 'inventory-store' }}},[_c('span',[_vm._v("Inventory Store")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.payroll)?_c('li',[_c('router-link',{attrs:{"to":{ name: 'tax' }}},[_c('span',[_vm._v("PAYE")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.payroll)?_c('li',[_c('router-link',{attrs:{"to":{ name: 'payroll' }}},[_c('span',[_vm._v("Payroll")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.payroll)?_c('li',[_c('router-link',{attrs:{"to":{ name: 'archive' }}},[_c('span',[_vm._v("Archive")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.view_invoices)?_c('li',[_c('router-link',{attrs:{"to":{ name: 'invoice' }}},[_c('span',[_vm._v("Invoices")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.bank_account)?_c('li',[_c('router-link',{attrs:{"to":{ name: 'bank-accounts' }}},[_c('span',[_vm._v("Bank Accounts")])])],1):_vm._e()])]):_vm._e(),_vm._v(" "),(_vm.permissions.projects)?_c('li',[_vm._m(14),_vm._v(" "),_c('ul',{staticClass:"nav sidebar-subnav collapse",attrs:{"id":"tables"}},[_c('li',{staticClass:"sidebar-subnav-header"},[_vm._v("Project Administration")]),_vm._v(" "),_c('li',[_c('router-link',{attrs:{"to":{name: 'projects'},"title":"Basic Tables"}},[_c('span',[_vm._v("Projects")])])],1)])]):_vm._e(),_vm._v(" "),(_vm.permissions.reports)?_c('li',[_vm._m(15),_vm._v(" "),_c('ul',{staticClass:"nav sidebar-subnav collapse",attrs:{"id":"widgets"}},[_c('li',{staticClass:"sidebar-subnav-header"},[_vm._v("Reports")]),_vm._v(" "),_c('li',[_c('router-link',{attrs:{"to":{name: 'report-by-chart'},"title":"Report By Chart Of Account"}},[_c('span',[_vm._v("Report By Chart Of Account")])])],1),_vm._v(" "),(_vm.permissions.requisition_by_material)?_c('li',[_c('router-link',{attrs:{"to":{name: 'requisitions-by-material-report'},"title":"Requisition By Material"}},[_c('span',[_vm._v("Requisitions By Material")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.requisition_by_date_range)?_c('li',[_c('router-link',{attrs:{"to":{name: 'requisitions-by-date-report'},"title":"Requisition By Date Range"}},[_c('span',[_vm._v("Requisitions By Date Range")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.requisition_by_project)?_c('li',[_c('router-link',{attrs:{"to":{name: 'requisitions-by-project-report'},"title":"Requisition By Project"}},[_c('span',[_vm._v("Requisitions By Project")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.expense_by_category)?_c('li',[_c('router-link',{attrs:{"to":{name: 'expense-by-type-report'},"title":"Expense By Type"}},[_c('span',[_vm._v("Expense By Category")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.expense_by_project_phase)?_c('li',[_c('router-link',{attrs:{"to":{name: 'expense-by-project-phase-report'},"title":"Expense By Project Phase"}},[_c('span',[_vm._v("Expense By Project Phase")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.expense_by_project_phase_cumulative)?_c('li',[_c('router-link',{attrs:{"to":{name: 'expense-by-phase-report'},"title":"Expense By Phase"}},[_c('span',[_vm._v("Expense By Phase")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.report_by_staff_allocation)?_c('li',[_c('router-link',{attrs:{"to":{name: 'staff-allocation-report'},"title":"Staff Allocations"}},[_c('span',[_vm._v("Staff Allocations")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.expense_by_project)?_c('li',[_c('router-link',{attrs:{"to":{name: 'expense-by-project-report'},"title":"Expense By Project"}},[_c('span',[_vm._v("Expense By Project")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.expense_by_date_range)?_c('li',[_c('router-link',{attrs:{"to":{name: 'expense-by-date-range-report'},"title":"Expense By Project"}},[_c('span',[_vm._v("Expense By Date Range")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.contracts_by_date_range)?_c('li',[_c('router-link',{attrs:{"to":{name: 'contract-by-date-report'},"title":"Expense By Project"}},[_c('span',[_vm._v("Contracts By Date Range")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.contract_by_project)?_c('li',[_c('router-link',{attrs:{"to":{name: 'contract-by-project-report'},"title":"Expense By Project"}},[_c('span',[_vm._v("Contracts By Project")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.contracts_by_labour)?_c('li',[_c('router-link',{attrs:{"to":{name: 'contract-by-labour-report'},"title":"Expense By Project"}},[_c('span',[_vm._v("Contracts By Labour")])])],1):_vm._e()])]):_vm._e(),_vm._v(" "),(_vm.permissions.vendor_administration)?_c('li',[_vm._m(16),_vm._v(" "),_c('ul',{staticClass:"nav sidebar-subnav collapse",attrs:{"id":"appview"}},[_c('li',{staticClass:"sidebar-subnav-header"},[_vm._v("Vendors Administration")]),_vm._v(" "),(_vm.permissions.service_vendor)?_c('li',[_c('router-link',{attrs:{"to":{name:'service-vendors'}}},[_c('span',[_vm._v("Service Vendor")])])],1):_vm._e(),_vm._v(" "),(_vm.permissions.vendor_pricing)?_c('li',[_c('router-link',{attrs:{"to":{name:'vendor-pricing'}}},[_c('span',[_vm._v("Vendor Pricing")])])],1):_vm._e()])]):_vm._e()])])])]),_vm._v(" "),_vm._m(17),_vm._v(" "),_c('section',[_c('div',{staticClass:"content-wrapper"},[_c('div',{staticClass:"container-fluid"},[_c('div',[_c('a',{staticClass:"dropdown-item",attrs:{"href":"#"},on:{"click":function($event){if(!('button' in $event)&&_vm._k($event.keyCode,"prvent",undefined,$event.key,undefined)){ return null; }return _vm.logout($event)}}},[_vm._v("Logout")])]),_vm._v(_vm._s(!!0)+"\n                "),(_vm.permissions.system_setup)?_c('div',[_vm._v("I have set up permission")]):_vm._e(),_vm._v(" "),_c('router-view')],1)])]),_vm._v(" "),_vm._m(18)])}
var staticRenderFns = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"nav-wrapper"},[_c('ul',{staticClass:"nav navbar-nav"},[_c('li',[_c('a',{staticClass:"hidden-xs",attrs:{"href":"#","data-trigger-resize":"","data-toggle-state":"aside-collapsed"}},[_c('em',{staticClass:"material-icons"},[_vm._v("menu")])]),_vm._v(" "),_c('a',{staticClass:"visible-xs sidebar-toggle",attrs:{"href":"#","data-toggle-state":"aside-toggled","data-no-persist":"true"}},[_c('em',{staticClass:"material-icons"},[_vm._v("menu")])])])]),_vm._v(" "),_c('ul',{staticClass:"nav navbar-nav navbar-right"},[_c('li',[_c('a',{attrs:{"href":"#","data-search-open":""}},[_c('em',{staticClass:"material-icons"},[_vm._v("search")])])]),_vm._v(" "),_c('li',{staticClass:"visible-lg"},[_c('a',{attrs:{"href":"#","data-toggle-fullscreen":""}},[_c('em',{staticClass:"material-icons"},[_vm._v("fullscreen")])])]),_vm._v(" "),_c('li',{staticClass:"dropdown"},[_c('a',{staticClass:"dropdown-toggle",attrs:{"href":"javascript:void(0);","data-toggle":"dropdown","role":"button"}},[_c('i',{staticClass:"material-icons"},[_vm._v("notifications")]),_vm._v(" "),_c('span',{staticClass:"badge badge-success"},[_vm._v("7")])]),_vm._v(" "),_c('ul',{staticClass:"dropdown-menu"},[_c('li',{staticClass:"header"},[_vm._v("NOTIFICATIONS")]),_vm._v(" "),_c('li',{staticClass:"body"},[_c('ul',{staticClass:"menu"},[_c('li',{staticClass:"media"},[_c('a',{attrs:{"href":"#"}},[_c('div',{staticClass:"media-left"},[_c('div',{staticClass:"icon-circle bg-red"},[_c('i',{staticClass:"material-icons"},[_vm._v("done")])])]),_vm._v(" "),_c('div',{staticClass:"media-body menu-note"},[_c('p',{staticClass:"pull-right"},[_vm._v("Just Now")]),_vm._v(" "),_c('h4',[_vm._v(" Privacy settings changed")])])])]),_vm._v(" "),_c('li',{staticClass:"media"},[_c('a',{attrs:{"href":"#"}},[_c('div',{staticClass:"media-left"},[_c('div',{staticClass:"icon-circle bg-indigo"},[_c('i',{staticClass:"material-icons"},[_vm._v("add")])])]),_vm._v(" "),_c('div',{staticClass:"media-body menu-note"},[_c('p',{staticClass:"pull-right"},[_vm._v("2 mins")]),_vm._v(" "),_c('h4',[_vm._v(" Mike has added you as friend")])])])]),_vm._v(" "),_c('li',{staticClass:"media"},[_c('a',{attrs:{"href":"#"}},[_c('div',{staticClass:"media-left"},[_c('div',{staticClass:"icon-circle bg-blue"},[_c('i',{staticClass:"material-icons"},[_vm._v("alarm")])])]),_vm._v(" "),_c('div',{staticClass:"media-body menu-note"},[_c('p',{staticClass:"pull-right"},[_vm._v("20 mins")]),_vm._v(" "),_c('h4',[_vm._v(" New item added")])])])]),_vm._v(" "),_c('li',{staticClass:"media"},[_c('a',{attrs:{"href":"#"}},[_c('div',{staticClass:"media-left"},[_c('div',{staticClass:"icon-circle bg-orange"},[_c('i',{staticClass:"material-icons"},[_vm._v("thumb_up")])])]),_vm._v(" "),_c('div',{staticClass:"media-body menu-note"},[_c('p',{staticClass:"pull-right"},[_vm._v("2 hrs")]),_vm._v(" "),_c('h4',[_vm._v(" Ketty like your photo")])])])]),_vm._v(" "),_c('li',{staticClass:"media"},[_c('a',{attrs:{"href":"#"}},[_c('div',{staticClass:"media-left"},[_c('div',{staticClass:"icon-circle bg-green"},[_c('i',{staticClass:"material-icons"},[_vm._v("cached")])])]),_vm._v(" "),_c('div',{staticClass:"media-body menu-note"},[_c('p',{staticClass:"pull-right"},[_vm._v("3 days")]),_vm._v(" "),_c('h4',[_vm._v(" Server 10 is not working Properly")])])])]),_vm._v(" "),_c('li',{staticClass:"media"},[_c('a',{attrs:{"href":"#"}},[_c('div',{staticClass:"media-left"},[_c('div',{staticClass:"icon-circle bg-grey"},[_c('i',{staticClass:"material-icons"},[_vm._v("settings")])])]),_vm._v(" "),_c('div',{staticClass:"media-body menu-note"},[_c('p',{staticClass:"pull-right"},[_vm._v("5 days")]),_vm._v(" "),_c('h4',[_vm._v(" Restart your system")])])])])])]),_vm._v(" "),_c('li',{staticClass:"footer"},[_c('a',{attrs:{"href":"javascript:void(0);"}},[_vm._v("View All Notifications")])])])]),_vm._v(" "),_c('a',{staticClass:"dropdown-toggle",attrs:{"href":"javascript:void(0);","data-toggle":"dropdown","role":"button"}},[_c('i',{staticClass:"material-icons"},[_vm._v("mail")]),_vm._v(" "),_c('span',{staticClass:"badge badge-danger"},[_vm._v("7")])]),_vm._v(" "),_c('li',[_c('a',{attrs:{"href":"#","data-toggle-state":"offsidebar-open","data-no-persist":"true"}},[_c('em',{staticClass:"material-icons"},[_vm._v("more_vert")])])])])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('li',{staticClass:"nav-heading "},[_c('span',[_vm._v("MAIN NAVIGATION")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('a',{staticClass:"menu-toggle",attrs:{"href":"#layout","title":"Layouts","data-toggle":"collapse"}},[_c('em',{staticClass:"material-icons"},[_vm._v("settings")]),_vm._v(" "),_c('span',[_vm._v("System Setup")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('a',{staticClass:"menu-toggle",attrs:{"href":"#finance","title":"Level 1","data-toggle":"collapse"}},[_c('span',[_vm._v("Finance")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('a',{staticClass:"menu-toggle",attrs:{"href":"#asset","title":"Level 1","data-toggle":"collapse"}},[_c('span',[_vm._v("Assets")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('a',{staticClass:"menu-toggle",attrs:{"href":"#user","title":"Level 1","data-toggle":"collapse"}},[_c('span',[_vm._v("User")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('a',{staticClass:"menu-toggle",attrs:{"href":"#project","title":"Level 1","data-toggle":"collapse"}},[_c('span',[_vm._v("Project")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('a',{staticClass:"menu-toggle",attrs:{"href":"#components","title":"Components","data-toggle":"collapse"}},[_c('em',{staticClass:"material-icons"},[_vm._v("account_box")]),_vm._v(" "),_c('span',[_vm._v("User Administration")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('a',{staticClass:"menu-toggle",attrs:{"href":"#hr","title":"Components","data-toggle":"collapse"}},[_c('em',{staticClass:"material-icons"},[_vm._v("account_box")]),_vm._v(" "),_c('span',[_vm._v("HR Management")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('a',{staticClass:"menu-toggle",attrs:{"href":"#legal","title":"Components","data-toggle":"collapse"}},[_c('em',{staticClass:"material-icons"},[_vm._v("account_box")]),_vm._v(" "),_c('span',[_vm._v("Legal And Contracts")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('a',{staticClass:"menu-toggle",attrs:{"href":"#advanceui","title":"Advance UI","data-toggle":"collapse"}},[_c('em',{staticClass:"material-icons"},[_vm._v("contacts")]),_vm._v(" "),_c('span',[_vm._v("Client Administration")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('a',{staticClass:"menu-toggle",attrs:{"href":"#elements","title":"Elements","data-toggle":"collapse"}},[_c('em',{staticClass:"material-icons"},[_vm._v("widgets")]),_vm._v(" "),_c('span',[_vm._v("Asset Manager")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('a',{staticClass:"menu-toggle",attrs:{"href":"#forms","title":"Forms","data-toggle":"collapse"}},[_c('em',{staticClass:"material-icons"},[_vm._v("directions_car")]),_vm._v(" "),_c('span',[_vm._v("CarPool Manager")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('a',{staticClass:"menu-toggle",attrs:{"href":"#charts","title":"Charts","data-toggle":"collapse"}},[_c('em',{staticClass:"material-icons"},[_vm._v("attach_money")]),_vm._v(" "),_c('span',[_vm._v("Finance Administration")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('a',{staticClass:"menu-toggle",attrs:{"href":"#tables","title":"Tables","data-toggle":"collapse"}},[_c('em',{staticClass:"material-icons"},[_vm._v("assignment")]),_vm._v(" "),_c('span',[_vm._v("Project Administration")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('a',{staticClass:"menu-toggle",attrs:{"href":"#widgets","title":"Widgets","data-toggle":"collapse"}},[_c('em',{staticClass:"material-icons"},[_vm._v("folder")]),_vm._v(" "),_c('span',[_vm._v("Reports")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('a',{staticClass:"menu-toggle",attrs:{"href":"#appview","title":"App Views","data-toggle":"collapse"}},[_c('em',{staticClass:"material-icons"},[_vm._v("work")]),_vm._v(" "),_c('span',[_vm._v("Vendors Administration")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('aside',{staticClass:"offsidebar hide"},[_c('nav',[_c('div',{attrs:{"role":"tabpanel"}},[_c('ul',{staticClass:"nav nav-tabs nav-tabs-inline nav-justified",attrs:{"role":"tablist"}},[_c('li',{staticClass:"active",attrs:{"role":"presentation"}},[_c('a',{attrs:{"href":"#app-skins","aria-controls":"app-settings","role":"tab","data-toggle":"tab"}},[_c('em',{staticClass:"material-icons"},[_vm._v("color_lens")])])]),_vm._v(" "),_c('li',{attrs:{"role":"presentation"}},[_c('a',{attrs:{"href":"#app-settings","aria-controls":"app-chat","role":"tab","data-toggle":"tab"}},[_c('em',{staticClass:"material-icons"},[_vm._v("settings")])])])]),_vm._v(" "),_c('div',{staticClass:"tab-content"},[_c('div',{staticClass:"tab-pane fade in active",attrs:{"id":"app-skins","role":"tabpanel"}},[_c('ul',{staticClass:"skin_selector",attrs:{"id":"sidebar_clr_selector"}},[_c('h2',[_vm._v("Sidebar Skins")]),_vm._v(" "),_c('li',{staticClass:"active",attrs:{"data-theme":"light"}},[_c('div',{staticClass:"grey"}),_vm._v(" "),_c('span',[_vm._v("Light")])]),_vm._v(" "),_c('li',{attrs:{"data-theme":"dark"}},[_c('div',{staticClass:"black"}),_vm._v(" "),_c('span',[_vm._v("Dark")])])]),_vm._v(" "),_c('ul',{staticClass:"skin_selector",attrs:{"id":"skin_selector"}},[_c('h2',[_vm._v("Navbar Skins")]),_vm._v(" "),_c('li',{attrs:{"data-theme":"red"}},[_c('div',{staticClass:"red"}),_vm._v(" "),_c('span',[_vm._v("Red")])]),_vm._v(" "),_c('li',{attrs:{"data-theme":"pink"}},[_c('div',{staticClass:"pink"}),_vm._v(" "),_c('span',[_vm._v("Pink")])]),_vm._v(" "),_c('li',{attrs:{"data-theme":"purple"}},[_c('div',{staticClass:"purple"}),_vm._v(" "),_c('span',[_vm._v("Purple")])]),_vm._v(" "),_c('li',{attrs:{"data-theme":"indigo"}},[_c('div',{staticClass:"indigo"}),_vm._v(" "),_c('span',[_vm._v("Indigo")])]),_vm._v(" "),_c('li',{staticClass:"active",attrs:{"data-theme":"blue"}},[_c('div',{staticClass:"blue"}),_vm._v(" "),_c('span',[_vm._v("Blue")])]),_vm._v(" "),_c('li',{attrs:{"data-theme":"light-blue"}},[_c('div',{staticClass:"light-blue"}),_vm._v(" "),_c('span',[_vm._v("Light Blue")])]),_vm._v(" "),_c('li',{attrs:{"data-theme":"cyan"}},[_c('div',{staticClass:"cyan"}),_vm._v(" "),_c('span',[_vm._v("Cyan")])]),_vm._v(" "),_c('li',{attrs:{"data-theme":"teal"}},[_c('div',{staticClass:"teal"}),_vm._v(" "),_c('span',[_vm._v("Teal")])]),_vm._v(" "),_c('li',{attrs:{"data-theme":"green"}},[_c('div',{staticClass:"green"}),_vm._v(" "),_c('span',[_vm._v("Green")])]),_vm._v(" "),_c('li',{attrs:{"data-theme":"lime"}},[_c('div',{staticClass:"lime"}),_vm._v(" "),_c('span',[_vm._v("Lime")])]),_vm._v(" "),_c('li',{attrs:{"data-theme":"yellow"}},[_c('div',{staticClass:"yellow"}),_vm._v(" "),_c('span',[_vm._v("Yellow")])]),_vm._v(" "),_c('li',{attrs:{"data-theme":"orange"}},[_c('div',{staticClass:"orange"}),_vm._v(" "),_c('span',[_vm._v("Orange")])]),_vm._v(" "),_c('li',{attrs:{"data-theme":"brown"}},[_c('div',{staticClass:"brown"}),_vm._v(" "),_c('span',[_vm._v("Brown")])]),_vm._v(" "),_c('li',{attrs:{"data-theme":"grey"}},[_c('div',{staticClass:"grey"}),_vm._v(" "),_c('span',[_vm._v("Grey")])])])]),_vm._v(" "),_c('div',{staticClass:"tab-pane fade",attrs:{"id":"app-settings","role":"tabpanel"}},[_c('div',{staticClass:"demo-settings "},[_c('ul',{staticClass:"setting-list"},[_c('h2',[_vm._v("Layout Settings")]),_vm._v(" "),_c('li',[_c('span',[_vm._v("Fixed")]),_vm._v(" "),_c('div',{staticClass:"switch"},[_c('label',[_c('input',{attrs:{"id":"chk-fixed","type":"checkbox","data-toggle-state":"layout-fixed"}}),_c('span',{staticClass:"lever switch-col-grey"})])])]),_vm._v(" "),_c('li',[_c('span',[_vm._v("Boxed")]),_vm._v(" "),_c('div',{staticClass:"switch"},[_c('label',[_c('input',{attrs:{"id":"chk-boxed","type":"checkbox","data-toggle-state":"layout-boxed"}}),_c('span',{staticClass:"lever switch-col-grey"})])])]),_vm._v(" "),_c('li',[_c('span',[_vm._v("Collapsed")]),_vm._v(" "),_c('div',{staticClass:"switch"},[_c('label',[_c('input',{attrs:{"id":"chk-collapsed","type":"checkbox","data-toggle-state":"aside-collapsed"}}),_c('span',{staticClass:"lever switch-col-grey"})])])]),_vm._v(" "),_c('li',[_c('span',[_vm._v("Collapsed Text")]),_vm._v(" "),_c('div',{staticClass:"switch"},[_c('label',[_c('input',{attrs:{"id":"chk-collapsed-text","type":"checkbox","data-toggle-state":"aside-collapsed-text"}}),_c('span',{staticClass:"lever switch-col-grey"})])])]),_vm._v(" "),_c('li',[_c('span',[_vm._v("Float")]),_vm._v(" "),_c('div',{staticClass:"switch"},[_c('label',[_c('input',{attrs:{"id":"chk-float","type":"checkbox","data-toggle-state":"aside-float"}}),_c('span',{staticClass:"lever switch-col-grey"})])])]),_vm._v(" "),_c('li',[_c('span',[_vm._v("Show Scrollbar")]),_vm._v(" "),_c('div',{staticClass:"switch"},[_c('label',[_c('input',{attrs:{"id":"chk-hover","type":"checkbox","data-toggle-state":"show-scrollbar","data-target":".sidebar"}}),_c('span',{staticClass:"lever switch-col-grey"})])])])])])])])])])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('footer',[_c('span',[_vm._v("© 2017 - Eagle Template By "),_c('b',{staticClass:"col-blue"},[_vm._v("Bylancer")])])])}]
module.exports = { render: render, staticRenderFns: staticRenderFns }

/***/ }),

/***/ 61:
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(311)
}
var normalizeComponent = __webpack_require__(11)
/* script */
var __vue_script__ = __webpack_require__(313)
/* template */
var __vue_template__ = __webpack_require__(314)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

module.exports = Component.exports


/***/ })

});