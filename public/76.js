webpackJsonp([76],{

/***/ 150:
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(276)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}
var options = null
var ssrIdKey = 'data-vue-ssr-id'

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction, _options) {
  isProduction = _isProduction

  options = _options || {}

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[' + ssrIdKey + '~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }
  if (options.ssrId) {
    styleElement.setAttribute(ssrIdKey, obj.id)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ 151:
/***/ (function(module, exports) {

module.exports = function() {
	throw new Error("define cannot be used indirect");
};


/***/ }),

/***/ 276:
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ }),

/***/ 277:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module) {/*!
 * validate.js 0.12.0
 *
 * (c) 2013-2017 Nicklas Ansman, 2013 Wrapp
 * Validate.js may be freely distributed under the MIT license.
 * For all details and documentation:
 * http://validatejs.org/
 */

(function(exports, module, define) {
  "use strict";

  // The main function that calls the validators specified by the constraints.
  // The options are the following:
  //   - format (string) - An option that controls how the returned value is formatted
  //     * flat - Returns a flat array of just the error messages
  //     * grouped - Returns the messages grouped by attribute (default)
  //     * detailed - Returns an array of the raw validation data
  //   - fullMessages (boolean) - If `true` (default) the attribute name is prepended to the error.
  //
  // Please note that the options are also passed to each validator.
  var validate = function(attributes, constraints, options) {
    options = v.extend({}, v.options, options);

    var results = v.runValidations(attributes, constraints, options)
      , attr
      , validator;

    if (results.some(function(r) { return v.isPromise(r.error); })) {
      throw new Error("Use validate.async if you want support for promises");
    }
    return validate.processValidationResults(results, options);
  };

  var v = validate;

  // Copies over attributes from one or more sources to a single destination.
  // Very much similar to underscore's extend.
  // The first argument is the target object and the remaining arguments will be
  // used as sources.
  v.extend = function(obj) {
    [].slice.call(arguments, 1).forEach(function(source) {
      for (var attr in source) {
        obj[attr] = source[attr];
      }
    });
    return obj;
  };

  v.extend(validate, {
    // This is the version of the library as a semver.
    // The toString function will allow it to be coerced into a string
    version: {
      major: 0,
      minor: 12,
      patch: 0,
      metadata: null,
      toString: function() {
        var version = v.format("%{major}.%{minor}.%{patch}", v.version);
        if (!v.isEmpty(v.version.metadata)) {
          version += "+" + v.version.metadata;
        }
        return version;
      }
    },

    // Below is the dependencies that are used in validate.js

    // The constructor of the Promise implementation.
    // If you are using Q.js, RSVP or any other A+ compatible implementation
    // override this attribute to be the constructor of that promise.
    // Since jQuery promises aren't A+ compatible they won't work.
    Promise: typeof Promise !== "undefined" ? Promise : /* istanbul ignore next */ null,

    EMPTY_STRING_REGEXP: /^\s*$/,

    // Runs the validators specified by the constraints object.
    // Will return an array of the format:
    //     [{attribute: "<attribute name>", error: "<validation result>"}, ...]
    runValidations: function(attributes, constraints, options) {
      var results = []
        , attr
        , validatorName
        , value
        , validators
        , validator
        , validatorOptions
        , error;

      if (v.isDomElement(attributes) || v.isJqueryElement(attributes)) {
        attributes = v.collectFormValues(attributes);
      }

      // Loops through each constraints, finds the correct validator and run it.
      for (attr in constraints) {
        value = v.getDeepObjectValue(attributes, attr);
        // This allows the constraints for an attribute to be a function.
        // The function will be called with the value, attribute name, the complete dict of
        // attributes as well as the options and constraints passed in.
        // This is useful when you want to have different
        // validations depending on the attribute value.
        validators = v.result(constraints[attr], value, attributes, attr, options, constraints);

        for (validatorName in validators) {
          validator = v.validators[validatorName];

          if (!validator) {
            error = v.format("Unknown validator %{name}", {name: validatorName});
            throw new Error(error);
          }

          validatorOptions = validators[validatorName];
          // This allows the options to be a function. The function will be
          // called with the value, attribute name, the complete dict of
          // attributes as well as the options and constraints passed in.
          // This is useful when you want to have different
          // validations depending on the attribute value.
          validatorOptions = v.result(validatorOptions, value, attributes, attr, options, constraints);
          if (!validatorOptions) {
            continue;
          }
          results.push({
            attribute: attr,
            value: value,
            validator: validatorName,
            globalOptions: options,
            attributes: attributes,
            options: validatorOptions,
            error: validator.call(validator,
                value,
                validatorOptions,
                attr,
                attributes,
                options)
          });
        }
      }

      return results;
    },

    // Takes the output from runValidations and converts it to the correct
    // output format.
    processValidationResults: function(errors, options) {
      errors = v.pruneEmptyErrors(errors, options);
      errors = v.expandMultipleErrors(errors, options);
      errors = v.convertErrorMessages(errors, options);

      var format = options.format || "grouped";

      if (typeof v.formatters[format] === 'function') {
        errors = v.formatters[format](errors);
      } else {
        throw new Error(v.format("Unknown format %{format}", options));
      }

      return v.isEmpty(errors) ? undefined : errors;
    },

    // Runs the validations with support for promises.
    // This function will return a promise that is settled when all the
    // validation promises have been completed.
    // It can be called even if no validations returned a promise.
    async: function(attributes, constraints, options) {
      options = v.extend({}, v.async.options, options);

      var WrapErrors = options.wrapErrors || function(errors) {
        return errors;
      };

      // Removes unknown attributes
      if (options.cleanAttributes !== false) {
        attributes = v.cleanAttributes(attributes, constraints);
      }

      var results = v.runValidations(attributes, constraints, options);

      return new v.Promise(function(resolve, reject) {
        v.waitForResults(results).then(function() {
          var errors = v.processValidationResults(results, options);
          if (errors) {
            reject(new WrapErrors(errors, options, attributes, constraints));
          } else {
            resolve(attributes);
          }
        }, function(err) {
          reject(err);
        });
      });
    },

    single: function(value, constraints, options) {
      options = v.extend({}, v.single.options, options, {
        format: "flat",
        fullMessages: false
      });
      return v({single: value}, {single: constraints}, options);
    },

    // Returns a promise that is resolved when all promises in the results array
    // are settled. The promise returned from this function is always resolved,
    // never rejected.
    // This function modifies the input argument, it replaces the promises
    // with the value returned from the promise.
    waitForResults: function(results) {
      // Create a sequence of all the results starting with a resolved promise.
      return results.reduce(function(memo, result) {
        // If this result isn't a promise skip it in the sequence.
        if (!v.isPromise(result.error)) {
          return memo;
        }

        return memo.then(function() {
          return result.error.then(function(error) {
            result.error = error || null;
          });
        });
      }, new v.Promise(function(r) { r(); })); // A resolved promise
    },

    // If the given argument is a call: function the and: function return the value
    // otherwise just return the value. Additional arguments will be passed as
    // arguments to the function.
    // Example:
    // ```
    // result('foo') // 'foo'
    // result(Math.max, 1, 2) // 2
    // ```
    result: function(value) {
      var args = [].slice.call(arguments, 1);
      if (typeof value === 'function') {
        value = value.apply(null, args);
      }
      return value;
    },

    // Checks if the value is a number. This function does not consider NaN a
    // number like many other `isNumber` functions do.
    isNumber: function(value) {
      return typeof value === 'number' && !isNaN(value);
    },

    // Returns false if the object is not a function
    isFunction: function(value) {
      return typeof value === 'function';
    },

    // A simple check to verify that the value is an integer. Uses `isNumber`
    // and a simple modulo check.
    isInteger: function(value) {
      return v.isNumber(value) && value % 1 === 0;
    },

    // Checks if the value is a boolean
    isBoolean: function(value) {
      return typeof value === 'boolean';
    },

    // Uses the `Object` function to check if the given argument is an object.
    isObject: function(obj) {
      return obj === Object(obj);
    },

    // Simply checks if the object is an instance of a date
    isDate: function(obj) {
      return obj instanceof Date;
    },

    // Returns false if the object is `null` of `undefined`
    isDefined: function(obj) {
      return obj !== null && obj !== undefined;
    },

    // Checks if the given argument is a promise. Anything with a `then`
    // function is considered a promise.
    isPromise: function(p) {
      return !!p && v.isFunction(p.then);
    },

    isJqueryElement: function(o) {
      return o && v.isString(o.jquery);
    },

    isDomElement: function(o) {
      if (!o) {
        return false;
      }

      if (!o.querySelectorAll || !o.querySelector) {
        return false;
      }

      if (v.isObject(document) && o === document) {
        return true;
      }

      // http://stackoverflow.com/a/384380/699304
      /* istanbul ignore else */
      if (typeof HTMLElement === "object") {
        return o instanceof HTMLElement;
      } else {
        return o &&
          typeof o === "object" &&
          o !== null &&
          o.nodeType === 1 &&
          typeof o.nodeName === "string";
      }
    },

    isEmpty: function(value) {
      var attr;

      // Null and undefined are empty
      if (!v.isDefined(value)) {
        return true;
      }

      // functions are non empty
      if (v.isFunction(value)) {
        return false;
      }

      // Whitespace only strings are empty
      if (v.isString(value)) {
        return v.EMPTY_STRING_REGEXP.test(value);
      }

      // For arrays we use the length property
      if (v.isArray(value)) {
        return value.length === 0;
      }

      // Dates have no attributes but aren't empty
      if (v.isDate(value)) {
        return false;
      }

      // If we find at least one property we consider it non empty
      if (v.isObject(value)) {
        for (attr in value) {
          return false;
        }
        return true;
      }

      return false;
    },

    // Formats the specified strings with the given values like so:
    // ```
    // format("Foo: %{foo}", {foo: "bar"}) // "Foo bar"
    // ```
    // If you want to write %{...} without having it replaced simply
    // prefix it with % like this `Foo: %%{foo}` and it will be returned
    // as `"Foo: %{foo}"`
    format: v.extend(function(str, vals) {
      if (!v.isString(str)) {
        return str;
      }
      return str.replace(v.format.FORMAT_REGEXP, function(m0, m1, m2) {
        if (m1 === '%') {
          return "%{" + m2 + "}";
        } else {
          return String(vals[m2]);
        }
      });
    }, {
      // Finds %{key} style patterns in the given string
      FORMAT_REGEXP: /(%?)%\{([^\}]+)\}/g
    }),

    // "Prettifies" the given string.
    // Prettifying means replacing [.\_-] with spaces as well as splitting
    // camel case words.
    prettify: function(str) {
      if (v.isNumber(str)) {
        // If there are more than 2 decimals round it to two
        if ((str * 100) % 1 === 0) {
          return "" + str;
        } else {
          return parseFloat(Math.round(str * 100) / 100).toFixed(2);
        }
      }

      if (v.isArray(str)) {
        return str.map(function(s) { return v.prettify(s); }).join(", ");
      }

      if (v.isObject(str)) {
        return str.toString();
      }

      // Ensure the string is actually a string
      str = "" + str;

      return str
        // Splits keys separated by periods
        .replace(/([^\s])\.([^\s])/g, '$1 $2')
        // Removes backslashes
        .replace(/\\+/g, '')
        // Replaces - and - with space
        .replace(/[_-]/g, ' ')
        // Splits camel cased words
        .replace(/([a-z])([A-Z])/g, function(m0, m1, m2) {
          return "" + m1 + " " + m2.toLowerCase();
        })
        .toLowerCase();
    },

    stringifyValue: function(value, options) {
      var prettify = options && options.prettify || v.prettify;
      return prettify(value);
    },

    isString: function(value) {
      return typeof value === 'string';
    },

    isArray: function(value) {
      return {}.toString.call(value) === '[object Array]';
    },

    // Checks if the object is a hash, which is equivalent to an object that
    // is neither an array nor a function.
    isHash: function(value) {
      return v.isObject(value) && !v.isArray(value) && !v.isFunction(value);
    },

    contains: function(obj, value) {
      if (!v.isDefined(obj)) {
        return false;
      }
      if (v.isArray(obj)) {
        return obj.indexOf(value) !== -1;
      }
      return value in obj;
    },

    unique: function(array) {
      if (!v.isArray(array)) {
        return array;
      }
      return array.filter(function(el, index, array) {
        return array.indexOf(el) == index;
      });
    },

    forEachKeyInKeypath: function(object, keypath, callback) {
      if (!v.isString(keypath)) {
        return undefined;
      }

      var key = ""
        , i
        , escape = false;

      for (i = 0; i < keypath.length; ++i) {
        switch (keypath[i]) {
          case '.':
            if (escape) {
              escape = false;
              key += '.';
            } else {
              object = callback(object, key, false);
              key = "";
            }
            break;

          case '\\':
            if (escape) {
              escape = false;
              key += '\\';
            } else {
              escape = true;
            }
            break;

          default:
            escape = false;
            key += keypath[i];
            break;
        }
      }

      return callback(object, key, true);
    },

    getDeepObjectValue: function(obj, keypath) {
      if (!v.isObject(obj)) {
        return undefined;
      }

      return v.forEachKeyInKeypath(obj, keypath, function(obj, key) {
        if (v.isObject(obj)) {
          return obj[key];
        }
      });
    },

    // This returns an object with all the values of the form.
    // It uses the input name as key and the value as value
    // So for example this:
    // <input type="text" name="email" value="foo@bar.com" />
    // would return:
    // {email: "foo@bar.com"}
    collectFormValues: function(form, options) {
      var values = {}
        , i
        , j
        , input
        , inputs
        , option
        , value;

      if (v.isJqueryElement(form)) {
        form = form[0];
      }

      if (!form) {
        return values;
      }

      options = options || {};

      inputs = form.querySelectorAll("input[name], textarea[name]");
      for (i = 0; i < inputs.length; ++i) {
        input = inputs.item(i);

        if (v.isDefined(input.getAttribute("data-ignored"))) {
          continue;
        }

        name = input.name.replace(/\./g, "\\\\.");
        value = v.sanitizeFormValue(input.value, options);
        if (input.type === "number") {
          value = value ? +value : null;
        } else if (input.type === "checkbox") {
          if (input.attributes.value) {
            if (!input.checked) {
              value = values[name] || null;
            }
          } else {
            value = input.checked;
          }
        } else if (input.type === "radio") {
          if (!input.checked) {
            value = values[name] || null;
          }
        }
        values[name] = value;
      }

      inputs = form.querySelectorAll("select[name]");
      for (i = 0; i < inputs.length; ++i) {
        input = inputs.item(i);
        if (v.isDefined(input.getAttribute("data-ignored"))) {
          continue;
        }

        if (input.multiple) {
          value = [];
          for (j in input.options) {
            option = input.options[j];
             if (option && option.selected) {
              value.push(v.sanitizeFormValue(option.value, options));
            }
          }
        } else {
          var _val = typeof input.options[input.selectedIndex] !== 'undefined' ? input.options[input.selectedIndex].value : '';
          value = v.sanitizeFormValue(_val, options);
        }
        values[input.name] = value;
      }

      return values;
    },

    sanitizeFormValue: function(value, options) {
      if (options.trim && v.isString(value)) {
        value = value.trim();
      }

      if (options.nullify !== false && value === "") {
        return null;
      }
      return value;
    },

    capitalize: function(str) {
      if (!v.isString(str)) {
        return str;
      }
      return str[0].toUpperCase() + str.slice(1);
    },

    // Remove all errors who's error attribute is empty (null or undefined)
    pruneEmptyErrors: function(errors) {
      return errors.filter(function(error) {
        return !v.isEmpty(error.error);
      });
    },

    // In
    // [{error: ["err1", "err2"], ...}]
    // Out
    // [{error: "err1", ...}, {error: "err2", ...}]
    //
    // All attributes in an error with multiple messages are duplicated
    // when expanding the errors.
    expandMultipleErrors: function(errors) {
      var ret = [];
      errors.forEach(function(error) {
        // Removes errors without a message
        if (v.isArray(error.error)) {
          error.error.forEach(function(msg) {
            ret.push(v.extend({}, error, {error: msg}));
          });
        } else {
          ret.push(error);
        }
      });
      return ret;
    },

    // Converts the error mesages by prepending the attribute name unless the
    // message is prefixed by ^
    convertErrorMessages: function(errors, options) {
      options = options || {};

      var ret = []
        , prettify = options.prettify || v.prettify;
      errors.forEach(function(errorInfo) {
        var error = v.result(errorInfo.error,
            errorInfo.value,
            errorInfo.attribute,
            errorInfo.options,
            errorInfo.attributes,
            errorInfo.globalOptions);

        if (!v.isString(error)) {
          ret.push(errorInfo);
          return;
        }

        if (error[0] === '^') {
          error = error.slice(1);
        } else if (options.fullMessages !== false) {
          error = v.capitalize(prettify(errorInfo.attribute)) + " " + error;
        }
        error = error.replace(/\\\^/g, "^");
        error = v.format(error, {
          value: v.stringifyValue(errorInfo.value, options)
        });
        ret.push(v.extend({}, errorInfo, {error: error}));
      });
      return ret;
    },

    // In:
    // [{attribute: "<attributeName>", ...}]
    // Out:
    // {"<attributeName>": [{attribute: "<attributeName>", ...}]}
    groupErrorsByAttribute: function(errors) {
      var ret = {};
      errors.forEach(function(error) {
        var list = ret[error.attribute];
        if (list) {
          list.push(error);
        } else {
          ret[error.attribute] = [error];
        }
      });
      return ret;
    },

    // In:
    // [{error: "<message 1>", ...}, {error: "<message 2>", ...}]
    // Out:
    // ["<message 1>", "<message 2>"]
    flattenErrorsToArray: function(errors) {
      return errors
        .map(function(error) { return error.error; })
        .filter(function(value, index, self) {
          return self.indexOf(value) === index;
        });
    },

    cleanAttributes: function(attributes, whitelist) {
      function whitelistCreator(obj, key, last) {
        if (v.isObject(obj[key])) {
          return obj[key];
        }
        return (obj[key] = last ? true : {});
      }

      function buildObjectWhitelist(whitelist) {
        var ow = {}
          , lastObject
          , attr;
        for (attr in whitelist) {
          if (!whitelist[attr]) {
            continue;
          }
          v.forEachKeyInKeypath(ow, attr, whitelistCreator);
        }
        return ow;
      }

      function cleanRecursive(attributes, whitelist) {
        if (!v.isObject(attributes)) {
          return attributes;
        }

        var ret = v.extend({}, attributes)
          , w
          , attribute;

        for (attribute in attributes) {
          w = whitelist[attribute];

          if (v.isObject(w)) {
            ret[attribute] = cleanRecursive(ret[attribute], w);
          } else if (!w) {
            delete ret[attribute];
          }
        }
        return ret;
      }

      if (!v.isObject(whitelist) || !v.isObject(attributes)) {
        return {};
      }

      whitelist = buildObjectWhitelist(whitelist);
      return cleanRecursive(attributes, whitelist);
    },

    exposeModule: function(validate, root, exports, module, define) {
      if (exports) {
        if (module && module.exports) {
          exports = module.exports = validate;
        }
        exports.validate = validate;
      } else {
        root.validate = validate;
        if (validate.isFunction(define) && define.amd) {
          define([], function () { return validate; });
        }
      }
    },

    warn: function(msg) {
      if (typeof console !== "undefined" && console.warn) {
        console.warn("[validate.js] " + msg);
      }
    },

    error: function(msg) {
      if (typeof console !== "undefined" && console.error) {
        console.error("[validate.js] " + msg);
      }
    }
  });

  validate.validators = {
    // Presence validates that the value isn't empty
    presence: function(value, options) {
      options = v.extend({}, this.options, options);
      if (options.allowEmpty !== false ? !v.isDefined(value) : v.isEmpty(value)) {
        return options.message || this.message || "can't be blank";
      }
    },
    length: function(value, options, attribute) {
      // Empty values are allowed
      if (!v.isDefined(value)) {
        return;
      }

      options = v.extend({}, this.options, options);

      var is = options.is
        , maximum = options.maximum
        , minimum = options.minimum
        , tokenizer = options.tokenizer || function(val) { return val; }
        , err
        , errors = [];

      value = tokenizer(value);
      var length = value.length;
      if(!v.isNumber(length)) {
        v.error(v.format("Attribute %{attr} has a non numeric value for `length`", {attr: attribute}));
        return options.message || this.notValid || "has an incorrect length";
      }

      // Is checks
      if (v.isNumber(is) && length !== is) {
        err = options.wrongLength ||
          this.wrongLength ||
          "is the wrong length (should be %{count} characters)";
        errors.push(v.format(err, {count: is}));
      }

      if (v.isNumber(minimum) && length < minimum) {
        err = options.tooShort ||
          this.tooShort ||
          "is too short (minimum is %{count} characters)";
        errors.push(v.format(err, {count: minimum}));
      }

      if (v.isNumber(maximum) && length > maximum) {
        err = options.tooLong ||
          this.tooLong ||
          "is too long (maximum is %{count} characters)";
        errors.push(v.format(err, {count: maximum}));
      }

      if (errors.length > 0) {
        return options.message || errors;
      }
    },
    numericality: function(value, options, attribute, attributes, globalOptions) {
      // Empty values are fine
      if (!v.isDefined(value)) {
        return;
      }

      options = v.extend({}, this.options, options);

      var errors = []
        , name
        , count
        , checks = {
            greaterThan:          function(v, c) { return v > c; },
            greaterThanOrEqualTo: function(v, c) { return v >= c; },
            equalTo:              function(v, c) { return v === c; },
            lessThan:             function(v, c) { return v < c; },
            lessThanOrEqualTo:    function(v, c) { return v <= c; },
            divisibleBy:          function(v, c) { return v % c === 0; }
          }
        , prettify = options.prettify ||
          (globalOptions && globalOptions.prettify) ||
          v.prettify;

      // Strict will check that it is a valid looking number
      if (v.isString(value) && options.strict) {
        var pattern = "^-?(0|[1-9]\\d*)";
        if (!options.onlyInteger) {
          pattern += "(\\.\\d+)?";
        }
        pattern += "$";

        if (!(new RegExp(pattern).test(value))) {
          return options.message ||
            options.notValid ||
            this.notValid ||
            this.message ||
            "must be a valid number";
        }
      }

      // Coerce the value to a number unless we're being strict.
      if (options.noStrings !== true && v.isString(value) && !v.isEmpty(value)) {
        value = +value;
      }

      // If it's not a number we shouldn't continue since it will compare it.
      if (!v.isNumber(value)) {
        return options.message ||
          options.notValid ||
          this.notValid ||
          this.message ||
          "is not a number";
      }

      // Same logic as above, sort of. Don't bother with comparisons if this
      // doesn't pass.
      if (options.onlyInteger && !v.isInteger(value)) {
        return options.message ||
          options.notInteger ||
          this.notInteger ||
          this.message ||
          "must be an integer";
      }

      for (name in checks) {
        count = options[name];
        if (v.isNumber(count) && !checks[name](value, count)) {
          // This picks the default message if specified
          // For example the greaterThan check uses the message from
          // this.notGreaterThan so we capitalize the name and prepend "not"
          var key = "not" + v.capitalize(name);
          var msg = options[key] ||
            this[key] ||
            this.message ||
            "must be %{type} %{count}";

          errors.push(v.format(msg, {
            count: count,
            type: prettify(name)
          }));
        }
      }

      if (options.odd && value % 2 !== 1) {
        errors.push(options.notOdd ||
            this.notOdd ||
            this.message ||
            "must be odd");
      }
      if (options.even && value % 2 !== 0) {
        errors.push(options.notEven ||
            this.notEven ||
            this.message ||
            "must be even");
      }

      if (errors.length) {
        return options.message || errors;
      }
    },
    datetime: v.extend(function(value, options) {
      if (!v.isFunction(this.parse) || !v.isFunction(this.format)) {
        throw new Error("Both the parse and format functions needs to be set to use the datetime/date validator");
      }

      // Empty values are fine
      if (!v.isDefined(value)) {
        return;
      }

      options = v.extend({}, this.options, options);

      var err
        , errors = []
        , earliest = options.earliest ? this.parse(options.earliest, options) : NaN
        , latest = options.latest ? this.parse(options.latest, options) : NaN;

      value = this.parse(value, options);

      // 86400000 is the number of milliseconds in a day, this is used to remove
      // the time from the date
      if (isNaN(value) || options.dateOnly && value % 86400000 !== 0) {
        err = options.notValid ||
          options.message ||
          this.notValid ||
          "must be a valid date";
        return v.format(err, {value: arguments[0]});
      }

      if (!isNaN(earliest) && value < earliest) {
        err = options.tooEarly ||
          options.message ||
          this.tooEarly ||
          "must be no earlier than %{date}";
        err = v.format(err, {
          value: this.format(value, options),
          date: this.format(earliest, options)
        });
        errors.push(err);
      }

      if (!isNaN(latest) && value > latest) {
        err = options.tooLate ||
          options.message ||
          this.tooLate ||
          "must be no later than %{date}";
        err = v.format(err, {
          date: this.format(latest, options),
          value: this.format(value, options)
        });
        errors.push(err);
      }

      if (errors.length) {
        return v.unique(errors);
      }
    }, {
      parse: null,
      format: null
    }),
    date: function(value, options) {
      options = v.extend({}, options, {dateOnly: true});
      return v.validators.datetime.call(v.validators.datetime, value, options);
    },
    format: function(value, options) {
      if (v.isString(options) || (options instanceof RegExp)) {
        options = {pattern: options};
      }

      options = v.extend({}, this.options, options);

      var message = options.message || this.message || "is invalid"
        , pattern = options.pattern
        , match;

      // Empty values are allowed
      if (!v.isDefined(value)) {
        return;
      }
      if (!v.isString(value)) {
        return message;
      }

      if (v.isString(pattern)) {
        pattern = new RegExp(options.pattern, options.flags);
      }
      match = pattern.exec(value);
      if (!match || match[0].length != value.length) {
        return message;
      }
    },
    inclusion: function(value, options) {
      // Empty values are fine
      if (!v.isDefined(value)) {
        return;
      }
      if (v.isArray(options)) {
        options = {within: options};
      }
      options = v.extend({}, this.options, options);
      if (v.contains(options.within, value)) {
        return;
      }
      var message = options.message ||
        this.message ||
        "^%{value} is not included in the list";
      return v.format(message, {value: value});
    },
    exclusion: function(value, options) {
      // Empty values are fine
      if (!v.isDefined(value)) {
        return;
      }
      if (v.isArray(options)) {
        options = {within: options};
      }
      options = v.extend({}, this.options, options);
      if (!v.contains(options.within, value)) {
        return;
      }
      var message = options.message || this.message || "^%{value} is restricted";
      return v.format(message, {value: value});
    },
    email: v.extend(function(value, options) {
      options = v.extend({}, this.options, options);
      var message = options.message || this.message || "is not a valid email";
      // Empty values are fine
      if (!v.isDefined(value)) {
        return;
      }
      if (!v.isString(value)) {
        return message;
      }
      if (!this.PATTERN.exec(value)) {
        return message;
      }
    }, {
      PATTERN: /^[a-z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z]{2,}$/i
    }),
    equality: function(value, options, attribute, attributes, globalOptions) {
      if (!v.isDefined(value)) {
        return;
      }

      if (v.isString(options)) {
        options = {attribute: options};
      }
      options = v.extend({}, this.options, options);
      var message = options.message ||
        this.message ||
        "is not equal to %{attribute}";

      if (v.isEmpty(options.attribute) || !v.isString(options.attribute)) {
        throw new Error("The attribute must be a non empty string");
      }

      var otherValue = v.getDeepObjectValue(attributes, options.attribute)
        , comparator = options.comparator || function(v1, v2) {
          return v1 === v2;
        }
        , prettify = options.prettify ||
          (globalOptions && globalOptions.prettify) ||
          v.prettify;

      if (!comparator(value, otherValue, options, attribute, attributes)) {
        return v.format(message, {attribute: prettify(options.attribute)});
      }
    },

    // A URL validator that is used to validate URLs with the ability to
    // restrict schemes and some domains.
    url: function(value, options) {
      if (!v.isDefined(value)) {
        return;
      }

      options = v.extend({}, this.options, options);

      var message = options.message || this.message || "is not a valid url"
        , schemes = options.schemes || this.schemes || ['http', 'https']
        , allowLocal = options.allowLocal || this.allowLocal || false;

      if (!v.isString(value)) {
        return message;
      }

      // https://gist.github.com/dperini/729294
      var regex =
        "^" +
        // protocol identifier
        "(?:(?:" + schemes.join("|") + ")://)" +
        // user:pass authentication
        "(?:\\S+(?::\\S*)?@)?" +
        "(?:";

      var tld = "(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))";

      if (allowLocal) {
        tld += "?";
      } else {
        regex +=
          // IP address exclusion
          // private & local networks
          "(?!(?:10|127)(?:\\.\\d{1,3}){3})" +
          "(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})" +
          "(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})";
      }

      regex +=
          // IP address dotted notation octets
          // excludes loopback network 0.0.0.0
          // excludes reserved space >= 224.0.0.0
          // excludes network & broacast addresses
          // (first & last IP address of each class)
          "(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])" +
          "(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}" +
          "(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))" +
        "|" +
          // host name
          "(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)" +
          // domain name
          "(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*" +
          tld +
        ")" +
        // port number
        "(?::\\d{2,5})?" +
        // resource path
        "(?:[/?#]\\S*)?" +
      "$";

      var PATTERN = new RegExp(regex, 'i');
      if (!PATTERN.exec(value)) {
        return message;
      }
    }
  };

  validate.formatters = {
    detailed: function(errors) {return errors;},
    flat: v.flattenErrorsToArray,
    grouped: function(errors) {
      var attr;

      errors = v.groupErrorsByAttribute(errors);
      for (attr in errors) {
        errors[attr] = v.flattenErrorsToArray(errors[attr]);
      }
      return errors;
    },
    constraint: function(errors) {
      var attr;
      errors = v.groupErrorsByAttribute(errors);
      for (attr in errors) {
        errors[attr] = errors[attr].map(function(result) {
          return result.validator;
        }).sort();
      }
      return errors;
    }
  };

  validate.exposeModule(validate, this, exports, module, __webpack_require__(151));
}).call(this,
         true ? /* istanbul ignore next */ exports : null,
         true ? /* istanbul ignore next */ module : null,
        __webpack_require__(151));

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(13)(module)))

/***/ }),

/***/ 523:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(524);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(150)("04a88bce", content, true, {});

/***/ }),

/***/ 524:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(12)(false);
// imports


// module
exports.push([module.i, ".flexit[data-v-3e67818e]{display:-webkit-box;display:-ms-flexbox;display:flex}input[data-v-3e67818e],select[data-v-3e67818e]{text-transform:capitalize!important}.line[data-v-3e67818e]{line-height:26px!important}.mfooter[data-v-3e67818e]{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:end;-ms-flex-pack:end;justify-content:flex-end}input[type*=email][data-v-3e67818e]{text-transform:lowercase!important}.mfooter button[data-v-3e67818e]{margin:0 0 0 10px!important}", ""]);

// exports


/***/ }),

/***/ 525:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _validate = __webpack_require__(277);

var _validate2 = _interopRequireDefault(_validate);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import UserProfile from '../datatables/user-administration/UserProfile';
exports.default = {
	components: {},
	mounted: function mounted() {
		var _this = this;

		axios.get('/api/get/position').then(function (response) {
			_this.positions = response.data;
		}).catch(function (erroe) {
			console.log(error);
		});
		axios.get('/api/get/role').then(function (response) {
			_this.roles = response.data;
		}).catch(function (erroe) {
			console.log(error);
		});
		axios.get('/api/get/department').then(function (response) {
			_this.departments = response.data;
		}).catch(function (erroe) {
			console.log(error);
		});
		axios.post('/api/get/user', { id: this.$route.params.id }).then(function (response) {
			_this.beforeEditingCache = Object.assign({}, response.data);
			_this.user = response.data;
		}).catch(function (erroe) {
			console.log(error);
		});
		axios.post('/api/get/bank').then(function (response) {

			_this.banks = response.data;
		}).catch(function (erroe) {
			console.log(error);
		});
	},

	watch: {
		'user.salary': function userSalary(newValue) {
			var _this2 = this;

			var result = newValue.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			this.$nextTick(function () {
				return _this2.user.salary = result;
			});
		}

	},
	data: function data() {
		return {
			modal: false,
			editmodal: false,
			position: {
				name: '',
				description: ''
			},
			positions: {},
			roles: {},
			departments: {},
			banks: {},
			editdata: {},
			user: {}
			// personal_info: {
			// 	first_name:'',
			// 	last_name:'',
			// 	email:'',
			// 	phone:'',
			// 	department:'',
			// 	role:'',
			// 	position:'',
			// 	active:'',
			// },

		};
	},

	methods: {
		previousPage: function previousPage() {
			this.$router.push({ name: 'users-profiles' });
		},
		reset: function reset() {
			Object.assign(this.user, this.beforeEditingCache);
		},
		sendData: function sendData() {
			var _this3 = this;

			var data = {
				id: this.user.id,
				first_name: this.user.first_name,
				last_name: this.user.last_name,
				email: this.user.email,
				phone_no: this.user.phone_no,
				role: this.user.role_id,
				position: this.user.position_id,
				department: this.user.department_id,
				active: this.user.active
			};

			var constraints = this.getConstraints();
			var errors = (0, _validate2.default)(data, constraints);
			if (errors) {

				this.$notify({
					group: 'foo',
					type: 'error',
					title: 'Notification',
					text: (errors.first_name ? errors.first_name.toString() + '</br>' : '') + (errors.last_name ? errors.last_name.toString() + '</br>' : '') + (errors.email ? errors.email.toString() + '</br>' : '') + (errors.phone_no ? errors.phone_no.toString() + '</br>' : '') + (errors.department ? errors.department.toString() + '</br>' : '') + (errors.role ? errors.role.toString() + '</br>' : '') + (errors.position ? errors.position.toString() + '</br>' : '') + (errors.active ? errors.active.toString() + '</br>' : ''),
					duration: 10000
				});
			} else {
				axios.post('/api/edit/user', data).then(function (response) {
					_this3.beforeEditingCache = Object.assign({}, response.data);

					_this3.$notify({
						group: 'foo',
						type: 'success',
						title: 'Notification',
						text: 'Success!',
						duration: 10000
					});
				}).catch(function (error) {
					_this3.$notify({
						group: 'foo',
						type: 'error',
						title: 'Notification',
						text: error.response.data.error,
						duration: 10000
					});
				});
			}
		},
		sendSecondData: function sendSecondData() {
			var _this4 = this;

			var data = {
				id: this.user.id,
				relative_name: this.user.relative_name,
				relative_relationship: this.user.relative_relationship,
				relative_phone_no: this.user.relative_phone_no,
				alternative_phone_no: this.user.alternative_phone_no

			};

			var constraints = this.getSecondConstraints();
			var errors = (0, _validate2.default)(data, constraints);
			if (errors) {

				this.$notify({
					group: 'foo',
					type: 'error',
					title: 'Notification',
					text: (errors.relative_name ? errors.relative_name.toString() + '</br>' : '') + (errors.relative_relationship ? errors.relative_relationship.toString() + '</br>' : '') + (errors.relative_phone_no ? errors.relative_phone_no.toString() + '</br>' : '') + (errors.phone_no ? errors.phone_no.toString() + '</br>' : ''),
					duration: 10000
				});
			} else {
				axios.post('/api/edit-second/user', data).then(function (response) {
					_this4.beforeEditingCache = Object.assign({}, response.data);

					_this4.$notify({
						group: 'foo',
						type: 'success',
						title: 'Notification',
						text: 'Success!',
						duration: 10000
					});
				}).catch(function (error) {
					_this4.$notify({
						group: 'foo',
						type: 'error',
						title: 'Notification',
						text: error.response.data.error,
						duration: 10000
					});
				});
			}
		},
		sendThirdData: function sendThirdData() {
			var _this5 = this;

			var data = {
				id: this.user.id,
				driver_license: this.user.driver_license,
				national_id: this.user.national_id,
				international_id: this.user.international_id

			};

			axios.post('/api/edit-third/user', data).then(function (response) {
				_this5.beforeEditingCache = Object.assign({}, response.data);

				_this5.$notify({
					group: 'foo',
					type: 'success',
					title: 'Notification',
					text: 'Success!',
					duration: 10000
				});
			}).catch(function (error) {
				_this5.$notify({
					group: 'foo',
					type: 'error',
					title: 'Notification',
					text: error.response.data.error,
					duration: 10000
				});
			});
		},
		sendFourthData: function sendFourthData() {
			var _this6 = this;

			var data = {
				id: this.user.id,
				bank_id: this.user.bank_id,
				account_no: this.user.account_no,
				account_type: this.user.account_type

			};

			axios.post('/api/edit-fourth/user', data).then(function (response) {
				_this6.beforeEditingCache = Object.assign({}, response.data);

				_this6.$notify({
					group: 'foo',
					type: 'success',
					title: 'Notification',
					text: 'Success!',
					duration: 10000
				});
			}).catch(function (error) {
				_this6.$notify({
					group: 'foo',
					type: 'error',
					title: 'Notification',
					text: error.response.data.error,
					duration: 10000
				});
			});
		},
		sendFifthData: function sendFifthData() {
			var _this7 = this;

			var data = {
				id: this.user.id,
				salary_component: this.user.salary_component,
				salary: this.user.salary,
				pay_frequency: this.user.pay_frequency

			};

			axios.post('/api/edit-fifth/user', data).then(function (response) {
				_this7.beforeEditingCache = Object.assign({}, response.data);

				_this7.$notify({
					group: 'foo',
					type: 'success',
					title: 'Notification',
					text: 'Success!',
					duration: 10000
				});
			}).catch(function (error) {
				_this7.$notify({
					group: 'foo',
					type: 'error',
					title: 'Notification',
					text: error.response.data.error,
					duration: 10000
				});
			});
		},
		getConstraints: function getConstraints() {
			return {
				first_name: {
					presence: {
						allowEmpty: false,
						message: "can't be empty"
					}

				},
				last_name: {
					presence: {
						allowEmpty: false,
						message: "can't be empty"
					}

				},
				email: {
					presence: {
						allowEmpty: false,
						message: "can't be empty"
					}

				},
				phone_no: {
					numericality: {
						onlyInteger: true,
						message: "can't be empty or must be numbers"

					}

				},
				department: {

					numericality: {
						onlyInteger: true,
						message: 'Must be selected'

					}
				},
				role: {

					numericality: {
						onlyInteger: true,
						message: 'Must be selected'

					}
				},
				position: {

					numericality: {
						onlyInteger: true,
						message: 'Must be selected'

					}
				},
				active: {

					numericality: {
						onlyInteger: true,
						message: 'Must be selected'

					}
				}
			};
		},
		getSecondConstraints: function getSecondConstraints() {
			return {
				relative_name: {
					presence: {
						allowEmpty: false,
						message: "can't be empty"
					}

				},
				relative_relationship: {
					presence: {
						allowEmpty: false,
						message: "can't be empty"
					}

				},
				relative_phone_no: {
					numericality: {
						onlyInteger: true,
						message: "can't be empty or must be numbers"

					}

				}

			};
		}
	}
}; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/***/ }),

/***/ 526:
/***/ (function(module, exports) {

var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('div',{staticClass:"page-header"},[_c('h2',[_vm._v("Edit User")]),_vm._v(" "),_c('ol',{staticClass:"breadcrumb"},[_c('li',[_c('router-link',{attrs:{"to":"/"}},[_vm._v("Home")])],1),_vm._v(" "),_c('li',{staticClass:"active"},[_vm._v("App Views")])])]),_vm._v(" "),_c('div',{staticClass:"row clearfix"},[_c('div',{staticClass:"col-lg-12 col-md-12"},[_c('div',{staticClass:"card"},[_c('div',{staticClass:"body p-b-0"},[_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-md-4"},[_c('button',{staticClass:"btn btn-info flexit",on:{"click":function($event){$event.preventDefault();_vm.previousPage()}}},[_c('em',{staticClass:"material-icons"},[_vm._v("keyboard_return")]),_c('span',{staticClass:"line"},[_vm._v("Previous Page")])])])])])]),_vm._v(" "),_c('div',{staticClass:"card",staticStyle:{"min-height":"635px"}},[_c('div',{staticClass:"body forum",staticStyle:{"padding":"0px"}},[_c('div',{staticClass:"col-lg-12 col-xlg-12 col-md-12",staticStyle:{"padding":"0px"}},[_c('div',{staticClass:"card"},[_c('div',{staticClass:"profile_body"},[_c('ul',{staticClass:"nav nav-tabs nav-tabs-inline nav-justified",attrs:{"role":"tablist"}},[_c('li',{staticClass:"active",attrs:{"role":"presentation"}},[_c('a',{attrs:{"href":"#about","data-toggle":"tab","aria-expanded":"true"},on:{"click":function($event){$event.preventDefault();}}},[_vm._v("Personal Information")])]),_vm._v(" "),_c('li',{attrs:{"role":"presentation"}},[_c('a',{attrs:{"href":"#timeline","data-toggle":"tab","aria-expanded":"false"},on:{"click":function($event){$event.preventDefault();}}},[_vm._v("Emergency Contact")])]),_vm._v(" "),_c('li',{attrs:{"role":"presentation"}},[_c('a',{attrs:{"href":"#photos","data-toggle":"tab","aria-expanded":"false"},on:{"click":function($event){$event.preventDefault();}}},[_vm._v("Identification")])]),_vm._v(" "),_c('li',{attrs:{"role":"presentation"}},[_c('a',{attrs:{"href":"#setting","data-toggle":"tab","aria-expanded":"false"},on:{"click":function($event){$event.preventDefault();}}},[_vm._v("Bank Details")])]),_vm._v(" "),_c('li',{attrs:{"role":"presentation"}},[_c('a',{attrs:{"href":"#userjob","data-toggle":"tab","aria-expanded":"false"},on:{"click":function($event){$event.preventDefault();}}},[_vm._v("Job")])]),_vm._v(" "),_c('li',{attrs:{"role":"presentation"}},[_c('a',{attrs:{"href":"#usersalary","data-toggle":"tab","aria-expanded":"false"},on:{"click":function($event){$event.preventDefault();}}},[_vm._v("Salary")])])]),_vm._v(" "),_c('div',{staticClass:"tab-content"},[_c('div',{staticClass:"tab-pane fade active in",staticStyle:{"height":"800px"},attrs:{"role":"tabpanel","id":"about"}},[_c('div',{staticClass:"col-md-8 col-md-offset-2"},[_c('div',{staticClass:"md-content"},[_c('div',[_c('form',{attrs:{"autocomplete":"off"},on:{"submit":function($event){$event.preventDefault();_vm.sendData()}}},[_c('label',{attrs:{"for":"email_address"}},[_vm._v("First Name")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.user.first_name),expression:"user.first_name"}],staticClass:"form-control",attrs:{"type":"text","name":"color","placeholder":"Enter first name","required":""},domProps:{"value":(_vm.user.first_name)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.user, "first_name", $event.target.value)}}})])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("Last name")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.user.last_name),expression:"user.last_name"}],staticClass:"form-control",attrs:{"type":"text","name":"color","placeholder":"Enter last name","required":""},domProps:{"value":(_vm.user.last_name)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.user, "last_name", $event.target.value)}}})])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("Email Address")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.user.email),expression:"user.email"}],staticClass:"form-control",attrs:{"type":"email","name":"color","placeholder":"Enter email","required":""},domProps:{"value":(_vm.user.email)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.user, "email", $event.target.value)}}})])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("Phone Number")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.user.phone_no),expression:"user.phone_no"}],staticClass:"form-control",attrs:{"type":"text","name":"color","placeholder":"Enter phone number","required":""},domProps:{"value":(_vm.user.phone_no)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.user, "phone_no", $event.target.value)}}})])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("User Department")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('select',{directives:[{name:"model",rawName:"v-model",value:(_vm.user.department_id),expression:"user.department_id"}],staticClass:"form-control",on:{"change":function($event){var $$selectedVal = Array.prototype.filter.call($event.target.options,function(o){return o.selected}).map(function(o){var val = "_value" in o ? o._value : o.value;return val}); _vm.$set(_vm.user, "department_id", $event.target.multiple ? $$selectedVal : $$selectedVal[0])}}},_vm._l((_vm.departments),function(department){return _c('option',{domProps:{"value":department.id}},[_vm._v(_vm._s(department.name))])}))])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("User Position")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('select',{directives:[{name:"model",rawName:"v-model",value:(_vm.user.position_id),expression:"user.position_id"}],staticClass:"form-control",on:{"change":function($event){var $$selectedVal = Array.prototype.filter.call($event.target.options,function(o){return o.selected}).map(function(o){var val = "_value" in o ? o._value : o.value;return val}); _vm.$set(_vm.user, "position_id", $event.target.multiple ? $$selectedVal : $$selectedVal[0])}}},_vm._l((_vm.positions),function(position){return _c('option',{domProps:{"value":position.id}},[_vm._v(_vm._s(position.name))])}))])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("User Role")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('select',{directives:[{name:"model",rawName:"v-model",value:(_vm.user.role_id),expression:"user.role_id"}],staticClass:"form-control",on:{"change":function($event){var $$selectedVal = Array.prototype.filter.call($event.target.options,function(o){return o.selected}).map(function(o){var val = "_value" in o ? o._value : o.value;return val}); _vm.$set(_vm.user, "role_id", $event.target.multiple ? $$selectedVal : $$selectedVal[0])}}},_vm._l((_vm.roles),function(role){return _c('option',{domProps:{"value":role.id}},[_vm._v(_vm._s(role.name))])}))])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("Make Active")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('select',{directives:[{name:"model",rawName:"v-model",value:(_vm.user.active),expression:"user.active"}],staticClass:"form-control",on:{"change":function($event){var $$selectedVal = Array.prototype.filter.call($event.target.options,function(o){return o.selected}).map(function(o){var val = "_value" in o ? o._value : o.value;return val}); _vm.$set(_vm.user, "active", $event.target.multiple ? $$selectedVal : $$selectedVal[0])}}},[_c('option',{attrs:{"value":"1"}},[_vm._v("Yes")]),_vm._v(" "),_c('option',{attrs:{"value":"0"}},[_vm._v("No")])])])]),_vm._v(" "),_c('div',{staticClass:"mfooter"},[_c('button',{staticClass:"btn btn-info"},[_vm._v("Update")]),_vm._v(" "),_c('button',{staticClass:"btn btn-danger",on:{"click":function($event){$event.preventDefault();_vm.reset()}}},[_vm._v("Reset")])])])])])])]),_vm._v(" "),_c('div',{staticClass:"tab-pane fade",staticStyle:{"height":"700px"},attrs:{"role":"tabpanel","id":"timeline"}},[_c('div',{staticClass:"col-md-8 col-md-offset-2"},[_c('div',{staticClass:"md-content"},[_c('div',[_c('form',{attrs:{"autocomplete":"off"},on:{"submit":function($event){$event.preventDefault();_vm.sendSecondData()}}},[_c('label',{attrs:{"for":"email_address"}},[_vm._v("Full Name")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.user.relative_name),expression:"user.relative_name"}],staticClass:"form-control",attrs:{"type":"text","name":"color","placeholder":"Enter full name","required":""},domProps:{"value":(_vm.user.relative_name)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.user, "relative_name", $event.target.value)}}})])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("Relationship")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.user.relative_relationship),expression:"user.relative_relationship"}],staticClass:"form-control",attrs:{"type":"text","name":"color","placeholder":"Enter relationship","required":""},domProps:{"value":(_vm.user.relative_relationship)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.user, "relative_relationship", $event.target.value)}}})])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("Mobile Number")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.user.relative_phone_no),expression:"user.relative_phone_no"}],staticClass:"form-control",attrs:{"type":"tel","name":"color","placeholder":"Enter number","required":""},domProps:{"value":(_vm.user.relative_phone_no)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.user, "relative_phone_no", $event.target.value)}}})])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("Alernative Number")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.user.alternative_phone_no),expression:"user.alternative_phone_no"}],staticClass:"form-control",attrs:{"type":"text","name":"color","placeholder":"Enter alternative number"},domProps:{"value":(_vm.user.alternative_phone_no)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.user, "alternative_phone_no", $event.target.value)}}})])]),_vm._v(" "),_c('div',{staticClass:"mfooter"},[_c('button',{staticClass:"btn btn-info"},[_vm._v("Update")]),_vm._v(" "),_c('button',{staticClass:"btn btn-danger",on:{"click":function($event){$event.preventDefault();_vm.reset()}}},[_vm._v("Reset")])])])])])])]),_vm._v(" "),_c('div',{staticClass:"tab-pane fade",staticStyle:{"height":"700px"},attrs:{"role":"tabpanel","id":"photos"}},[_c('div',{staticClass:"col-md-8 col-md-offset-2"},[_c('div',{staticClass:"md-content"},[_c('div',[_c('form',{attrs:{"autocomplete":"off"},on:{"submit":function($event){$event.preventDefault();_vm.sendThirdData()}}},[_c('label',{attrs:{"for":"email_address"}},[_vm._v("Driver's License")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.user.driver_license),expression:"user.driver_license"}],staticClass:"form-control",attrs:{"type":"text","name":"color","placeholder":"Enter Driver's License"},domProps:{"value":(_vm.user.driver_license)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.user, "driver_license", $event.target.value)}}})])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("National ID")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.user.national_id),expression:"user.national_id"}],staticClass:"form-control",attrs:{"type":"text","name":"color","placeholder":"Enter National ID"},domProps:{"value":(_vm.user.national_id)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.user, "national_id", $event.target.value)}}})])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("International Passport")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.user.international_id),expression:"user.international_id"}],staticClass:"form-control",attrs:{"type":"tel","name":"color","placeholder":"Enter International ID"},domProps:{"value":(_vm.user.international_id)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.user, "international_id", $event.target.value)}}})])]),_vm._v(" "),_c('div',{staticClass:"mfooter"},[_c('button',{staticClass:"btn btn-info"},[_vm._v("Update")]),_vm._v(" "),_c('button',{staticClass:"btn btn-danger",on:{"click":function($event){$event.preventDefault();_vm.reset()}}},[_vm._v("Reset")])])])])])])]),_vm._v(" "),_c('div',{staticClass:"tab-pane fade",staticStyle:{"height":"700px"},attrs:{"role":"tabpanel","id":"setting"}},[_c('div',{staticClass:"col-md-8 col-md-offset-2"},[_c('div',{staticClass:"md-content"},[_c('div',[_c('form',{attrs:{"autocomplete":"off"},on:{"submit":function($event){$event.preventDefault();_vm.sendFourthData()}}},[_c('label',{attrs:{"for":"email_address"}},[_vm._v("Bank")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('select',{directives:[{name:"model",rawName:"v-model",value:(_vm.user.bank_id),expression:"user.bank_id"}],staticClass:"form-control",on:{"change":function($event){var $$selectedVal = Array.prototype.filter.call($event.target.options,function(o){return o.selected}).map(function(o){var val = "_value" in o ? o._value : o.value;return val}); _vm.$set(_vm.user, "bank_id", $event.target.multiple ? $$selectedVal : $$selectedVal[0])}}},_vm._l((_vm.banks),function(bank){return _c('option',{domProps:{"value":bank.id}},[_vm._v(_vm._s(bank.name))])}))])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("Bank Account No")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.user.account_no),expression:"user.account_no"}],staticClass:"form-control",attrs:{"type":"text","name":"color","placeholder":"Enter Bank Account No"},domProps:{"value":(_vm.user.account_no)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.user, "account_no", $event.target.value)}}})])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("Account Type")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('select',{directives:[{name:"model",rawName:"v-model",value:(_vm.user.account_type),expression:"user.account_type"}],staticClass:"form-control",on:{"change":function($event){var $$selectedVal = Array.prototype.filter.call($event.target.options,function(o){return o.selected}).map(function(o){var val = "_value" in o ? o._value : o.value;return val}); _vm.$set(_vm.user, "account_type", $event.target.multiple ? $$selectedVal : $$selectedVal[0])}}},[_c('option',{attrs:{"value":"1"}},[_vm._v("Savings")]),_vm._v(" "),_c('option',{attrs:{"value":"2"}},[_vm._v("Current")]),_vm._v(" "),_c('option',{attrs:{"value":"3"}},[_vm._v("Others")])])])]),_vm._v(" "),_c('div',{staticClass:"mfooter"},[_c('button',{staticClass:"btn btn-info"},[_vm._v("Update")]),_vm._v(" "),_c('button',{staticClass:"btn btn-danger",on:{"click":function($event){$event.preventDefault();_vm.reset()}}},[_vm._v("Reset")])])])])])])]),_vm._v(" "),_c('div',{staticClass:"tab-pane fade",staticStyle:{"height":"700px"},attrs:{"role":"tabpanel","id":"userjob"}},[_c('div',{staticClass:"col-md-8 col-md-offset-2"},[_c('div',{staticClass:"md-content"},[_c('div',[_c('form',{attrs:{"autocomplete":"off"},on:{"submit":function($event){$event.preventDefault();_vm.sendThirdData()}}},[_c('div',[_vm._v("Waiting for Approval")]),_vm._v(" "),_c('div',{staticClass:"mfooter"},[_c('button',{staticClass:"btn btn-info"},[_vm._v("Update")]),_vm._v(" "),_c('button',{staticClass:"btn btn-danger",on:{"click":function($event){$event.preventDefault();_vm.reset()}}},[_vm._v("Reset")])])])])])])]),_vm._v(" "),_c('div',{staticClass:"tab-pane fade",staticStyle:{"height":"700px"},attrs:{"role":"tabpanel","id":"usersalary"}},[_c('div',{staticClass:"col-md-8 col-md-offset-2"},[_c('div',{staticClass:"md-content"},[_c('div',[_c('form',{attrs:{"autocomplete":"off"},on:{"submit":function($event){$event.preventDefault();_vm.sendFifthData()}}},[_c('label',{attrs:{"for":"email_address"}},[_vm._v("Salary Component")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.user.salary_component),expression:"user.salary_component"}],staticClass:"form-control",attrs:{"type":"text","name":"color","placeholder":"Enter Salary Component"},domProps:{"value":(_vm.user.salary_component)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.user, "salary_component", $event.target.value)}}})])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("Amount")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.user.salary),expression:"user.salary"}],staticClass:"form-control",attrs:{"type":"text","min":"1","name":"color","placeholder":"Enter amount"},domProps:{"value":(_vm.user.salary)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.user, "salary", $event.target.value)}}})])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("Pay Frequency")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('select',{directives:[{name:"model",rawName:"v-model",value:(_vm.user.pay_frequency),expression:"user.pay_frequency"}],staticClass:"form-control",on:{"change":function($event){var $$selectedVal = Array.prototype.filter.call($event.target.options,function(o){return o.selected}).map(function(o){var val = "_value" in o ? o._value : o.value;return val}); _vm.$set(_vm.user, "pay_frequency", $event.target.multiple ? $$selectedVal : $$selectedVal[0])}}},[_c('option',{attrs:{"value":"1"}},[_vm._v("Bi weekly")]),_vm._v(" "),_c('option',{attrs:{"value":"2"}},[_vm._v("Hourly")]),_vm._v(" "),_c('option',{attrs:{"value":"3"}},[_vm._v("Monthly")]),_vm._v(" "),_c('option',{attrs:{"value":"4"}},[_vm._v("Monthly On First Pay Of Month")]),_vm._v(" "),_c('option',{attrs:{"value":"5"}},[_vm._v("Semi Monthly")]),_vm._v(" "),_c('option',{attrs:{"value":"6"}},[_vm._v("Weekly")])])])]),_vm._v(" "),_c('div',{staticClass:"mfooter"},[_c('button',{staticClass:"btn btn-info"},[_vm._v("Update")]),_vm._v(" "),_c('button',{staticClass:"btn btn-danger",on:{"click":function($event){$event.preventDefault();_vm.reset()}}},[_vm._v("Reset")])])])])])])])])])])])])])])])])}
var staticRenderFns = []
module.exports = { render: render, staticRenderFns: staticRenderFns }

/***/ }),

/***/ 88:
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(523)
}
var normalizeComponent = __webpack_require__(11)
/* script */
var __vue_script__ = __webpack_require__(525)
/* template */
var __vue_template__ = __webpack_require__(526)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-3e67818e"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

module.exports = Component.exports


/***/ })

});