webpackJsonp([87],{

/***/ 320:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    methods: {
        logout: function logout() {
            this.$store.commit('logout');
            this.$router.push('/login');
        }
    },
    computed: {
        currentUser: function currentUser() {
            return this.$store.getters.currentUser;
        }
    }
};

/***/ }),

/***/ 321:
/***/ (function(module, exports) {

var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _vm._m(0)}
var staticRenderFns = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('div',{staticClass:"page-header"},[_c('h2',[_vm._v("DASHBOARD")])]),_vm._v(" "),_c('div',{staticClass:"row clearfix"},[_c('div',{staticClass:"col-lg-3"},[_c('div',{staticClass:"widget bg-indigo"},[_c('div',{staticClass:"col-xs-4 widget-icon"},[_c('div',{staticClass:"chart-bar"},[_vm._v("7,8,9,4,6,10,6,7,8,9,11")])]),_vm._v(" "),_c('div',{staticClass:"col-xs-8 widget-body text-right"},[_c('span',[_vm._v(" Sale ")]),_vm._v(" "),_c('h2',{staticClass:"num"},[_vm._v("350")])])])]),_vm._v(" "),_c('div',{staticClass:"col-lg-3"},[_c('div',{staticClass:"widget bg-red"},[_c('div',{staticClass:"col-xs-4 widget-icon"},[_c('div',{staticClass:"chart-pie"},[_vm._v("40, 20, 21, 19")])]),_vm._v(" "),_c('div',{staticClass:"col-xs-8 widget-body text-right"},[_c('span',[_vm._v(" CPU Usages ")]),_vm._v(" "),_c('h2',{staticClass:"num"},[_vm._v("60%")])])])]),_vm._v(" "),_c('div',{staticClass:"col-lg-3"},[_c('div',{staticClass:"widget bg-cyan"},[_c('div',{staticClass:"col-xs-4 widget-icon"},[_c('canvas',{staticClass:"m-t-15",attrs:{"id":"partly_cloudy_day_2","width":"50","height":"50"}})]),_vm._v(" "),_c('div',{staticClass:"col-xs-8 widget-body text-right"},[_c('span',[_vm._v(" Paris ")]),_vm._v(" "),_c('h2',{staticClass:"num"},[_vm._v("25°C")])])])]),_vm._v(" "),_c('div',{staticClass:"col-lg-3"},[_c('div',{staticClass:"widget bg-green"},[_c('div',{staticClass:"col-xs-4 widget-icon"},[_c('canvas',{staticClass:"m-t-15",attrs:{"id":"snow_2","width":"50","height":"50"}})]),_vm._v(" "),_c('div',{staticClass:"col-xs-8 widget-body text-right"},[_c('span',[_vm._v(" Shimla ")]),_vm._v(" "),_c('h2',{staticClass:"num"},[_vm._v("-5°C")])])])])])])}]
module.exports = { render: render, staticRenderFns: staticRenderFns }

/***/ }),

/***/ 63:
/***/ (function(module, exports, __webpack_require__) {

var normalizeComponent = __webpack_require__(11)
/* script */
var __vue_script__ = __webpack_require__(320)
/* template */
var __vue_template__ = __webpack_require__(321)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

module.exports = Component.exports


/***/ })

});