webpackJsonp([86],{

/***/ 150:
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(276)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}
var options = null
var ssrIdKey = 'data-vue-ssr-id'

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction, _options) {
  isProduction = _isProduction

  options = _options || {}

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[' + ssrIdKey + '~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }
  if (options.ssrId) {
    styleElement.setAttribute(ssrIdKey, obj.id)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ 276:
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ }),

/***/ 371:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(372);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(150)("11678afc", content, true, {});

/***/ }),

/***/ 372:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(12)(false);
// imports


// module
exports.push([module.i, ".flexit[data-v-b2dfa226]{display:-webkit-box;display:-ms-flexbox;display:flex}.line[data-v-b2dfa226]{line-height:26px!important}.mfooter[data-v-b2dfa226]{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:end;-ms-flex-pack:end;justify-content:flex-end}.mfooter button[data-v-b2dfa226]{margin:0 0 0 10px!important}", ""]);

// exports


/***/ }),

/***/ 373:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


exports.default = {
	filters: {
		capitalize: function capitalize(str) {
			var value = str.replace(/[^A-Z0-9]/ig, " ");
			if (!value) return '';
			value = value.toString();
			return value.charAt(0).toUpperCase() + value.slice(1);
		}
	},
	mounted: function mounted() {
		var _this = this;

		axios.post('/api/get/role-notifications', { role_id: this.$route.params.id }).then(function (response) {
			var array = response.data.role.name;
			var array2 = response.data;
			delete array2.role;

			_this.user_notifications = array2;
			_this.role_name = array;
		}).catch(function (error) {});
	},


	components: {},
	data: function data() {
		return {
			user_notifications: {},
			role_name: ''

		};
	},

	methods: {
		previousPage: function previousPage() {
			this.$router.push({ name: 'user-roles' });
		},
		closeModal: function closeModal() {
			this.modal = false;
		},
		sendData: function sendData() {
			var _this2 = this;

			axios.post('/api/edit/role-notifications', { data: this.user_notifications, role_id: this.$route.params.id }).then(function (response) {
				console.log(response.data);
				_this2.$notify({
					group: 'foo',
					type: 'success',
					title: 'Notification',
					text: 'Success!',
					duration: 10000
				});
			}).catch(function (error) {
				_this2.$notify({
					group: 'foo',
					type: 'error',
					title: 'Notification',
					text: error.response.data.error,
					duration: 10000
				});
			});
		}
	},
	computed: {}
};

/***/ }),

/***/ 374:
/***/ (function(module, exports) {

var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('div',{staticClass:"page-header"},[_c('h2',[_vm._v("Notifications Subscription for "),_c('span',{domProps:{"textContent":_vm._s(_vm.role_name)}}),_vm._v(" Role")]),_vm._v(" "),_c('ol',{staticClass:"breadcrumb"},[_c('li',[_c('router-link',{attrs:{"to":"/"}},[_vm._v("Home")])],1),_vm._v(" "),_c('li',{staticClass:"active"},[_vm._v("App Views")])])]),_vm._v(" "),_c('div',{staticClass:"row clearfix"},[_c('div',{staticClass:"col-lg-12 col-md-12"},[_c('div',{staticClass:"card"},[_c('div',{staticClass:"body p-b-0"},[_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-md-8"},[_c('button',{staticClass:"btn btn-info flexit",on:{"click":function($event){$event.preventDefault();_vm.previousPage()}}},[_c('em',{staticClass:"material-icons"},[_vm._v("keyboard_return")]),_c('span',{staticClass:"line"},[_vm._v("Previous Page")])])])])])]),_vm._v(" "),_c('div',{staticClass:"card",staticStyle:{"min-height":"700px"}},[_c('div',{staticClass:"body forum"},[_vm._l((_vm.user_notifications),function(permission,i){return _c('div',{staticClass:"col-md-3 col-sm-6 col-xs-12"},[_c('div',{staticClass:"card card-bordered"},[_c('div',{staticClass:"body",staticStyle:{"padding":"10px","text-transform":"capitalize"}},[_c('label',{staticClass:"eagle-check custom-checkbox"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.user_notifications[i]),expression:"user_notifications[i]"}],staticClass:"eagle-check-input",attrs:{"type":"checkbox","true-value":"1","false-value":"0"},domProps:{"checked":Array.isArray(_vm.user_notifications[i])?_vm._i(_vm.user_notifications[i],null)>-1:_vm._q(_vm.user_notifications[i],"1")},on:{"change":function($event){var $$a=_vm.user_notifications[i],$$el=$event.target,$$c=$$el.checked?("1"):("0");if(Array.isArray($$a)){var $$v=null,$$i=_vm._i($$a,$$v);if($$el.checked){$$i<0&&(_vm.$set(_vm.user_notifications, i, $$a.concat([$$v])))}else{$$i>-1&&(_vm.$set(_vm.user_notifications, i, $$a.slice(0,$$i).concat($$a.slice($$i+1))))}}else{_vm.$set(_vm.user_notifications, i, $$c)}}}}),_vm._v(" "),_c('span',{staticClass:"eagle-check-indicator"}),_vm._v(" "),_c('span',{staticClass:"eagle-check-description"},[_vm._v(_vm._s(_vm._f("capitalize")(i)))])])])])])}),_vm._v(" "),_c('button',{staticClass:"btn btn-info btn-lg",on:{"click":function($event){$event.preventDefault();_vm.sendData()}}},[_vm._v("UpDate")])],2)])])])])}
var staticRenderFns = []
module.exports = { render: render, staticRenderFns: staticRenderFns }

/***/ }),

/***/ 70:
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(371)
}
var normalizeComponent = __webpack_require__(11)
/* script */
var __vue_script__ = __webpack_require__(373)
/* template */
var __vue_template__ = __webpack_require__(374)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-b2dfa226"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

module.exports = Component.exports


/***/ })

});