webpackJsonp([80],{

/***/ 132:
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(886)
}
var normalizeComponent = __webpack_require__(11)
/* script */
var __vue_script__ = __webpack_require__(888)
/* template */
var __vue_template__ = __webpack_require__(889)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-712049d4"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

module.exports = Component.exports


/***/ }),

/***/ 150:
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(276)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}
var options = null
var ssrIdKey = 'data-vue-ssr-id'

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction, _options) {
  isProduction = _isProduction

  options = _options || {}

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[' + ssrIdKey + '~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }
  if (options.ssrId) {
    styleElement.setAttribute(ssrIdKey, obj.id)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ 151:
/***/ (function(module, exports) {

module.exports = function() {
	throw new Error("define cannot be used indirect");
};


/***/ }),

/***/ 276:
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ }),

/***/ 277:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module) {/*!
 * validate.js 0.12.0
 *
 * (c) 2013-2017 Nicklas Ansman, 2013 Wrapp
 * Validate.js may be freely distributed under the MIT license.
 * For all details and documentation:
 * http://validatejs.org/
 */

(function(exports, module, define) {
  "use strict";

  // The main function that calls the validators specified by the constraints.
  // The options are the following:
  //   - format (string) - An option that controls how the returned value is formatted
  //     * flat - Returns a flat array of just the error messages
  //     * grouped - Returns the messages grouped by attribute (default)
  //     * detailed - Returns an array of the raw validation data
  //   - fullMessages (boolean) - If `true` (default) the attribute name is prepended to the error.
  //
  // Please note that the options are also passed to each validator.
  var validate = function(attributes, constraints, options) {
    options = v.extend({}, v.options, options);

    var results = v.runValidations(attributes, constraints, options)
      , attr
      , validator;

    if (results.some(function(r) { return v.isPromise(r.error); })) {
      throw new Error("Use validate.async if you want support for promises");
    }
    return validate.processValidationResults(results, options);
  };

  var v = validate;

  // Copies over attributes from one or more sources to a single destination.
  // Very much similar to underscore's extend.
  // The first argument is the target object and the remaining arguments will be
  // used as sources.
  v.extend = function(obj) {
    [].slice.call(arguments, 1).forEach(function(source) {
      for (var attr in source) {
        obj[attr] = source[attr];
      }
    });
    return obj;
  };

  v.extend(validate, {
    // This is the version of the library as a semver.
    // The toString function will allow it to be coerced into a string
    version: {
      major: 0,
      minor: 12,
      patch: 0,
      metadata: null,
      toString: function() {
        var version = v.format("%{major}.%{minor}.%{patch}", v.version);
        if (!v.isEmpty(v.version.metadata)) {
          version += "+" + v.version.metadata;
        }
        return version;
      }
    },

    // Below is the dependencies that are used in validate.js

    // The constructor of the Promise implementation.
    // If you are using Q.js, RSVP or any other A+ compatible implementation
    // override this attribute to be the constructor of that promise.
    // Since jQuery promises aren't A+ compatible they won't work.
    Promise: typeof Promise !== "undefined" ? Promise : /* istanbul ignore next */ null,

    EMPTY_STRING_REGEXP: /^\s*$/,

    // Runs the validators specified by the constraints object.
    // Will return an array of the format:
    //     [{attribute: "<attribute name>", error: "<validation result>"}, ...]
    runValidations: function(attributes, constraints, options) {
      var results = []
        , attr
        , validatorName
        , value
        , validators
        , validator
        , validatorOptions
        , error;

      if (v.isDomElement(attributes) || v.isJqueryElement(attributes)) {
        attributes = v.collectFormValues(attributes);
      }

      // Loops through each constraints, finds the correct validator and run it.
      for (attr in constraints) {
        value = v.getDeepObjectValue(attributes, attr);
        // This allows the constraints for an attribute to be a function.
        // The function will be called with the value, attribute name, the complete dict of
        // attributes as well as the options and constraints passed in.
        // This is useful when you want to have different
        // validations depending on the attribute value.
        validators = v.result(constraints[attr], value, attributes, attr, options, constraints);

        for (validatorName in validators) {
          validator = v.validators[validatorName];

          if (!validator) {
            error = v.format("Unknown validator %{name}", {name: validatorName});
            throw new Error(error);
          }

          validatorOptions = validators[validatorName];
          // This allows the options to be a function. The function will be
          // called with the value, attribute name, the complete dict of
          // attributes as well as the options and constraints passed in.
          // This is useful when you want to have different
          // validations depending on the attribute value.
          validatorOptions = v.result(validatorOptions, value, attributes, attr, options, constraints);
          if (!validatorOptions) {
            continue;
          }
          results.push({
            attribute: attr,
            value: value,
            validator: validatorName,
            globalOptions: options,
            attributes: attributes,
            options: validatorOptions,
            error: validator.call(validator,
                value,
                validatorOptions,
                attr,
                attributes,
                options)
          });
        }
      }

      return results;
    },

    // Takes the output from runValidations and converts it to the correct
    // output format.
    processValidationResults: function(errors, options) {
      errors = v.pruneEmptyErrors(errors, options);
      errors = v.expandMultipleErrors(errors, options);
      errors = v.convertErrorMessages(errors, options);

      var format = options.format || "grouped";

      if (typeof v.formatters[format] === 'function') {
        errors = v.formatters[format](errors);
      } else {
        throw new Error(v.format("Unknown format %{format}", options));
      }

      return v.isEmpty(errors) ? undefined : errors;
    },

    // Runs the validations with support for promises.
    // This function will return a promise that is settled when all the
    // validation promises have been completed.
    // It can be called even if no validations returned a promise.
    async: function(attributes, constraints, options) {
      options = v.extend({}, v.async.options, options);

      var WrapErrors = options.wrapErrors || function(errors) {
        return errors;
      };

      // Removes unknown attributes
      if (options.cleanAttributes !== false) {
        attributes = v.cleanAttributes(attributes, constraints);
      }

      var results = v.runValidations(attributes, constraints, options);

      return new v.Promise(function(resolve, reject) {
        v.waitForResults(results).then(function() {
          var errors = v.processValidationResults(results, options);
          if (errors) {
            reject(new WrapErrors(errors, options, attributes, constraints));
          } else {
            resolve(attributes);
          }
        }, function(err) {
          reject(err);
        });
      });
    },

    single: function(value, constraints, options) {
      options = v.extend({}, v.single.options, options, {
        format: "flat",
        fullMessages: false
      });
      return v({single: value}, {single: constraints}, options);
    },

    // Returns a promise that is resolved when all promises in the results array
    // are settled. The promise returned from this function is always resolved,
    // never rejected.
    // This function modifies the input argument, it replaces the promises
    // with the value returned from the promise.
    waitForResults: function(results) {
      // Create a sequence of all the results starting with a resolved promise.
      return results.reduce(function(memo, result) {
        // If this result isn't a promise skip it in the sequence.
        if (!v.isPromise(result.error)) {
          return memo;
        }

        return memo.then(function() {
          return result.error.then(function(error) {
            result.error = error || null;
          });
        });
      }, new v.Promise(function(r) { r(); })); // A resolved promise
    },

    // If the given argument is a call: function the and: function return the value
    // otherwise just return the value. Additional arguments will be passed as
    // arguments to the function.
    // Example:
    // ```
    // result('foo') // 'foo'
    // result(Math.max, 1, 2) // 2
    // ```
    result: function(value) {
      var args = [].slice.call(arguments, 1);
      if (typeof value === 'function') {
        value = value.apply(null, args);
      }
      return value;
    },

    // Checks if the value is a number. This function does not consider NaN a
    // number like many other `isNumber` functions do.
    isNumber: function(value) {
      return typeof value === 'number' && !isNaN(value);
    },

    // Returns false if the object is not a function
    isFunction: function(value) {
      return typeof value === 'function';
    },

    // A simple check to verify that the value is an integer. Uses `isNumber`
    // and a simple modulo check.
    isInteger: function(value) {
      return v.isNumber(value) && value % 1 === 0;
    },

    // Checks if the value is a boolean
    isBoolean: function(value) {
      return typeof value === 'boolean';
    },

    // Uses the `Object` function to check if the given argument is an object.
    isObject: function(obj) {
      return obj === Object(obj);
    },

    // Simply checks if the object is an instance of a date
    isDate: function(obj) {
      return obj instanceof Date;
    },

    // Returns false if the object is `null` of `undefined`
    isDefined: function(obj) {
      return obj !== null && obj !== undefined;
    },

    // Checks if the given argument is a promise. Anything with a `then`
    // function is considered a promise.
    isPromise: function(p) {
      return !!p && v.isFunction(p.then);
    },

    isJqueryElement: function(o) {
      return o && v.isString(o.jquery);
    },

    isDomElement: function(o) {
      if (!o) {
        return false;
      }

      if (!o.querySelectorAll || !o.querySelector) {
        return false;
      }

      if (v.isObject(document) && o === document) {
        return true;
      }

      // http://stackoverflow.com/a/384380/699304
      /* istanbul ignore else */
      if (typeof HTMLElement === "object") {
        return o instanceof HTMLElement;
      } else {
        return o &&
          typeof o === "object" &&
          o !== null &&
          o.nodeType === 1 &&
          typeof o.nodeName === "string";
      }
    },

    isEmpty: function(value) {
      var attr;

      // Null and undefined are empty
      if (!v.isDefined(value)) {
        return true;
      }

      // functions are non empty
      if (v.isFunction(value)) {
        return false;
      }

      // Whitespace only strings are empty
      if (v.isString(value)) {
        return v.EMPTY_STRING_REGEXP.test(value);
      }

      // For arrays we use the length property
      if (v.isArray(value)) {
        return value.length === 0;
      }

      // Dates have no attributes but aren't empty
      if (v.isDate(value)) {
        return false;
      }

      // If we find at least one property we consider it non empty
      if (v.isObject(value)) {
        for (attr in value) {
          return false;
        }
        return true;
      }

      return false;
    },

    // Formats the specified strings with the given values like so:
    // ```
    // format("Foo: %{foo}", {foo: "bar"}) // "Foo bar"
    // ```
    // If you want to write %{...} without having it replaced simply
    // prefix it with % like this `Foo: %%{foo}` and it will be returned
    // as `"Foo: %{foo}"`
    format: v.extend(function(str, vals) {
      if (!v.isString(str)) {
        return str;
      }
      return str.replace(v.format.FORMAT_REGEXP, function(m0, m1, m2) {
        if (m1 === '%') {
          return "%{" + m2 + "}";
        } else {
          return String(vals[m2]);
        }
      });
    }, {
      // Finds %{key} style patterns in the given string
      FORMAT_REGEXP: /(%?)%\{([^\}]+)\}/g
    }),

    // "Prettifies" the given string.
    // Prettifying means replacing [.\_-] with spaces as well as splitting
    // camel case words.
    prettify: function(str) {
      if (v.isNumber(str)) {
        // If there are more than 2 decimals round it to two
        if ((str * 100) % 1 === 0) {
          return "" + str;
        } else {
          return parseFloat(Math.round(str * 100) / 100).toFixed(2);
        }
      }

      if (v.isArray(str)) {
        return str.map(function(s) { return v.prettify(s); }).join(", ");
      }

      if (v.isObject(str)) {
        return str.toString();
      }

      // Ensure the string is actually a string
      str = "" + str;

      return str
        // Splits keys separated by periods
        .replace(/([^\s])\.([^\s])/g, '$1 $2')
        // Removes backslashes
        .replace(/\\+/g, '')
        // Replaces - and - with space
        .replace(/[_-]/g, ' ')
        // Splits camel cased words
        .replace(/([a-z])([A-Z])/g, function(m0, m1, m2) {
          return "" + m1 + " " + m2.toLowerCase();
        })
        .toLowerCase();
    },

    stringifyValue: function(value, options) {
      var prettify = options && options.prettify || v.prettify;
      return prettify(value);
    },

    isString: function(value) {
      return typeof value === 'string';
    },

    isArray: function(value) {
      return {}.toString.call(value) === '[object Array]';
    },

    // Checks if the object is a hash, which is equivalent to an object that
    // is neither an array nor a function.
    isHash: function(value) {
      return v.isObject(value) && !v.isArray(value) && !v.isFunction(value);
    },

    contains: function(obj, value) {
      if (!v.isDefined(obj)) {
        return false;
      }
      if (v.isArray(obj)) {
        return obj.indexOf(value) !== -1;
      }
      return value in obj;
    },

    unique: function(array) {
      if (!v.isArray(array)) {
        return array;
      }
      return array.filter(function(el, index, array) {
        return array.indexOf(el) == index;
      });
    },

    forEachKeyInKeypath: function(object, keypath, callback) {
      if (!v.isString(keypath)) {
        return undefined;
      }

      var key = ""
        , i
        , escape = false;

      for (i = 0; i < keypath.length; ++i) {
        switch (keypath[i]) {
          case '.':
            if (escape) {
              escape = false;
              key += '.';
            } else {
              object = callback(object, key, false);
              key = "";
            }
            break;

          case '\\':
            if (escape) {
              escape = false;
              key += '\\';
            } else {
              escape = true;
            }
            break;

          default:
            escape = false;
            key += keypath[i];
            break;
        }
      }

      return callback(object, key, true);
    },

    getDeepObjectValue: function(obj, keypath) {
      if (!v.isObject(obj)) {
        return undefined;
      }

      return v.forEachKeyInKeypath(obj, keypath, function(obj, key) {
        if (v.isObject(obj)) {
          return obj[key];
        }
      });
    },

    // This returns an object with all the values of the form.
    // It uses the input name as key and the value as value
    // So for example this:
    // <input type="text" name="email" value="foo@bar.com" />
    // would return:
    // {email: "foo@bar.com"}
    collectFormValues: function(form, options) {
      var values = {}
        , i
        , j
        , input
        , inputs
        , option
        , value;

      if (v.isJqueryElement(form)) {
        form = form[0];
      }

      if (!form) {
        return values;
      }

      options = options || {};

      inputs = form.querySelectorAll("input[name], textarea[name]");
      for (i = 0; i < inputs.length; ++i) {
        input = inputs.item(i);

        if (v.isDefined(input.getAttribute("data-ignored"))) {
          continue;
        }

        name = input.name.replace(/\./g, "\\\\.");
        value = v.sanitizeFormValue(input.value, options);
        if (input.type === "number") {
          value = value ? +value : null;
        } else if (input.type === "checkbox") {
          if (input.attributes.value) {
            if (!input.checked) {
              value = values[name] || null;
            }
          } else {
            value = input.checked;
          }
        } else if (input.type === "radio") {
          if (!input.checked) {
            value = values[name] || null;
          }
        }
        values[name] = value;
      }

      inputs = form.querySelectorAll("select[name]");
      for (i = 0; i < inputs.length; ++i) {
        input = inputs.item(i);
        if (v.isDefined(input.getAttribute("data-ignored"))) {
          continue;
        }

        if (input.multiple) {
          value = [];
          for (j in input.options) {
            option = input.options[j];
             if (option && option.selected) {
              value.push(v.sanitizeFormValue(option.value, options));
            }
          }
        } else {
          var _val = typeof input.options[input.selectedIndex] !== 'undefined' ? input.options[input.selectedIndex].value : '';
          value = v.sanitizeFormValue(_val, options);
        }
        values[input.name] = value;
      }

      return values;
    },

    sanitizeFormValue: function(value, options) {
      if (options.trim && v.isString(value)) {
        value = value.trim();
      }

      if (options.nullify !== false && value === "") {
        return null;
      }
      return value;
    },

    capitalize: function(str) {
      if (!v.isString(str)) {
        return str;
      }
      return str[0].toUpperCase() + str.slice(1);
    },

    // Remove all errors who's error attribute is empty (null or undefined)
    pruneEmptyErrors: function(errors) {
      return errors.filter(function(error) {
        return !v.isEmpty(error.error);
      });
    },

    // In
    // [{error: ["err1", "err2"], ...}]
    // Out
    // [{error: "err1", ...}, {error: "err2", ...}]
    //
    // All attributes in an error with multiple messages are duplicated
    // when expanding the errors.
    expandMultipleErrors: function(errors) {
      var ret = [];
      errors.forEach(function(error) {
        // Removes errors without a message
        if (v.isArray(error.error)) {
          error.error.forEach(function(msg) {
            ret.push(v.extend({}, error, {error: msg}));
          });
        } else {
          ret.push(error);
        }
      });
      return ret;
    },

    // Converts the error mesages by prepending the attribute name unless the
    // message is prefixed by ^
    convertErrorMessages: function(errors, options) {
      options = options || {};

      var ret = []
        , prettify = options.prettify || v.prettify;
      errors.forEach(function(errorInfo) {
        var error = v.result(errorInfo.error,
            errorInfo.value,
            errorInfo.attribute,
            errorInfo.options,
            errorInfo.attributes,
            errorInfo.globalOptions);

        if (!v.isString(error)) {
          ret.push(errorInfo);
          return;
        }

        if (error[0] === '^') {
          error = error.slice(1);
        } else if (options.fullMessages !== false) {
          error = v.capitalize(prettify(errorInfo.attribute)) + " " + error;
        }
        error = error.replace(/\\\^/g, "^");
        error = v.format(error, {
          value: v.stringifyValue(errorInfo.value, options)
        });
        ret.push(v.extend({}, errorInfo, {error: error}));
      });
      return ret;
    },

    // In:
    // [{attribute: "<attributeName>", ...}]
    // Out:
    // {"<attributeName>": [{attribute: "<attributeName>", ...}]}
    groupErrorsByAttribute: function(errors) {
      var ret = {};
      errors.forEach(function(error) {
        var list = ret[error.attribute];
        if (list) {
          list.push(error);
        } else {
          ret[error.attribute] = [error];
        }
      });
      return ret;
    },

    // In:
    // [{error: "<message 1>", ...}, {error: "<message 2>", ...}]
    // Out:
    // ["<message 1>", "<message 2>"]
    flattenErrorsToArray: function(errors) {
      return errors
        .map(function(error) { return error.error; })
        .filter(function(value, index, self) {
          return self.indexOf(value) === index;
        });
    },

    cleanAttributes: function(attributes, whitelist) {
      function whitelistCreator(obj, key, last) {
        if (v.isObject(obj[key])) {
          return obj[key];
        }
        return (obj[key] = last ? true : {});
      }

      function buildObjectWhitelist(whitelist) {
        var ow = {}
          , lastObject
          , attr;
        for (attr in whitelist) {
          if (!whitelist[attr]) {
            continue;
          }
          v.forEachKeyInKeypath(ow, attr, whitelistCreator);
        }
        return ow;
      }

      function cleanRecursive(attributes, whitelist) {
        if (!v.isObject(attributes)) {
          return attributes;
        }

        var ret = v.extend({}, attributes)
          , w
          , attribute;

        for (attribute in attributes) {
          w = whitelist[attribute];

          if (v.isObject(w)) {
            ret[attribute] = cleanRecursive(ret[attribute], w);
          } else if (!w) {
            delete ret[attribute];
          }
        }
        return ret;
      }

      if (!v.isObject(whitelist) || !v.isObject(attributes)) {
        return {};
      }

      whitelist = buildObjectWhitelist(whitelist);
      return cleanRecursive(attributes, whitelist);
    },

    exposeModule: function(validate, root, exports, module, define) {
      if (exports) {
        if (module && module.exports) {
          exports = module.exports = validate;
        }
        exports.validate = validate;
      } else {
        root.validate = validate;
        if (validate.isFunction(define) && define.amd) {
          define([], function () { return validate; });
        }
      }
    },

    warn: function(msg) {
      if (typeof console !== "undefined" && console.warn) {
        console.warn("[validate.js] " + msg);
      }
    },

    error: function(msg) {
      if (typeof console !== "undefined" && console.error) {
        console.error("[validate.js] " + msg);
      }
    }
  });

  validate.validators = {
    // Presence validates that the value isn't empty
    presence: function(value, options) {
      options = v.extend({}, this.options, options);
      if (options.allowEmpty !== false ? !v.isDefined(value) : v.isEmpty(value)) {
        return options.message || this.message || "can't be blank";
      }
    },
    length: function(value, options, attribute) {
      // Empty values are allowed
      if (!v.isDefined(value)) {
        return;
      }

      options = v.extend({}, this.options, options);

      var is = options.is
        , maximum = options.maximum
        , minimum = options.minimum
        , tokenizer = options.tokenizer || function(val) { return val; }
        , err
        , errors = [];

      value = tokenizer(value);
      var length = value.length;
      if(!v.isNumber(length)) {
        v.error(v.format("Attribute %{attr} has a non numeric value for `length`", {attr: attribute}));
        return options.message || this.notValid || "has an incorrect length";
      }

      // Is checks
      if (v.isNumber(is) && length !== is) {
        err = options.wrongLength ||
          this.wrongLength ||
          "is the wrong length (should be %{count} characters)";
        errors.push(v.format(err, {count: is}));
      }

      if (v.isNumber(minimum) && length < minimum) {
        err = options.tooShort ||
          this.tooShort ||
          "is too short (minimum is %{count} characters)";
        errors.push(v.format(err, {count: minimum}));
      }

      if (v.isNumber(maximum) && length > maximum) {
        err = options.tooLong ||
          this.tooLong ||
          "is too long (maximum is %{count} characters)";
        errors.push(v.format(err, {count: maximum}));
      }

      if (errors.length > 0) {
        return options.message || errors;
      }
    },
    numericality: function(value, options, attribute, attributes, globalOptions) {
      // Empty values are fine
      if (!v.isDefined(value)) {
        return;
      }

      options = v.extend({}, this.options, options);

      var errors = []
        , name
        , count
        , checks = {
            greaterThan:          function(v, c) { return v > c; },
            greaterThanOrEqualTo: function(v, c) { return v >= c; },
            equalTo:              function(v, c) { return v === c; },
            lessThan:             function(v, c) { return v < c; },
            lessThanOrEqualTo:    function(v, c) { return v <= c; },
            divisibleBy:          function(v, c) { return v % c === 0; }
          }
        , prettify = options.prettify ||
          (globalOptions && globalOptions.prettify) ||
          v.prettify;

      // Strict will check that it is a valid looking number
      if (v.isString(value) && options.strict) {
        var pattern = "^-?(0|[1-9]\\d*)";
        if (!options.onlyInteger) {
          pattern += "(\\.\\d+)?";
        }
        pattern += "$";

        if (!(new RegExp(pattern).test(value))) {
          return options.message ||
            options.notValid ||
            this.notValid ||
            this.message ||
            "must be a valid number";
        }
      }

      // Coerce the value to a number unless we're being strict.
      if (options.noStrings !== true && v.isString(value) && !v.isEmpty(value)) {
        value = +value;
      }

      // If it's not a number we shouldn't continue since it will compare it.
      if (!v.isNumber(value)) {
        return options.message ||
          options.notValid ||
          this.notValid ||
          this.message ||
          "is not a number";
      }

      // Same logic as above, sort of. Don't bother with comparisons if this
      // doesn't pass.
      if (options.onlyInteger && !v.isInteger(value)) {
        return options.message ||
          options.notInteger ||
          this.notInteger ||
          this.message ||
          "must be an integer";
      }

      for (name in checks) {
        count = options[name];
        if (v.isNumber(count) && !checks[name](value, count)) {
          // This picks the default message if specified
          // For example the greaterThan check uses the message from
          // this.notGreaterThan so we capitalize the name and prepend "not"
          var key = "not" + v.capitalize(name);
          var msg = options[key] ||
            this[key] ||
            this.message ||
            "must be %{type} %{count}";

          errors.push(v.format(msg, {
            count: count,
            type: prettify(name)
          }));
        }
      }

      if (options.odd && value % 2 !== 1) {
        errors.push(options.notOdd ||
            this.notOdd ||
            this.message ||
            "must be odd");
      }
      if (options.even && value % 2 !== 0) {
        errors.push(options.notEven ||
            this.notEven ||
            this.message ||
            "must be even");
      }

      if (errors.length) {
        return options.message || errors;
      }
    },
    datetime: v.extend(function(value, options) {
      if (!v.isFunction(this.parse) || !v.isFunction(this.format)) {
        throw new Error("Both the parse and format functions needs to be set to use the datetime/date validator");
      }

      // Empty values are fine
      if (!v.isDefined(value)) {
        return;
      }

      options = v.extend({}, this.options, options);

      var err
        , errors = []
        , earliest = options.earliest ? this.parse(options.earliest, options) : NaN
        , latest = options.latest ? this.parse(options.latest, options) : NaN;

      value = this.parse(value, options);

      // 86400000 is the number of milliseconds in a day, this is used to remove
      // the time from the date
      if (isNaN(value) || options.dateOnly && value % 86400000 !== 0) {
        err = options.notValid ||
          options.message ||
          this.notValid ||
          "must be a valid date";
        return v.format(err, {value: arguments[0]});
      }

      if (!isNaN(earliest) && value < earliest) {
        err = options.tooEarly ||
          options.message ||
          this.tooEarly ||
          "must be no earlier than %{date}";
        err = v.format(err, {
          value: this.format(value, options),
          date: this.format(earliest, options)
        });
        errors.push(err);
      }

      if (!isNaN(latest) && value > latest) {
        err = options.tooLate ||
          options.message ||
          this.tooLate ||
          "must be no later than %{date}";
        err = v.format(err, {
          date: this.format(latest, options),
          value: this.format(value, options)
        });
        errors.push(err);
      }

      if (errors.length) {
        return v.unique(errors);
      }
    }, {
      parse: null,
      format: null
    }),
    date: function(value, options) {
      options = v.extend({}, options, {dateOnly: true});
      return v.validators.datetime.call(v.validators.datetime, value, options);
    },
    format: function(value, options) {
      if (v.isString(options) || (options instanceof RegExp)) {
        options = {pattern: options};
      }

      options = v.extend({}, this.options, options);

      var message = options.message || this.message || "is invalid"
        , pattern = options.pattern
        , match;

      // Empty values are allowed
      if (!v.isDefined(value)) {
        return;
      }
      if (!v.isString(value)) {
        return message;
      }

      if (v.isString(pattern)) {
        pattern = new RegExp(options.pattern, options.flags);
      }
      match = pattern.exec(value);
      if (!match || match[0].length != value.length) {
        return message;
      }
    },
    inclusion: function(value, options) {
      // Empty values are fine
      if (!v.isDefined(value)) {
        return;
      }
      if (v.isArray(options)) {
        options = {within: options};
      }
      options = v.extend({}, this.options, options);
      if (v.contains(options.within, value)) {
        return;
      }
      var message = options.message ||
        this.message ||
        "^%{value} is not included in the list";
      return v.format(message, {value: value});
    },
    exclusion: function(value, options) {
      // Empty values are fine
      if (!v.isDefined(value)) {
        return;
      }
      if (v.isArray(options)) {
        options = {within: options};
      }
      options = v.extend({}, this.options, options);
      if (!v.contains(options.within, value)) {
        return;
      }
      var message = options.message || this.message || "^%{value} is restricted";
      return v.format(message, {value: value});
    },
    email: v.extend(function(value, options) {
      options = v.extend({}, this.options, options);
      var message = options.message || this.message || "is not a valid email";
      // Empty values are fine
      if (!v.isDefined(value)) {
        return;
      }
      if (!v.isString(value)) {
        return message;
      }
      if (!this.PATTERN.exec(value)) {
        return message;
      }
    }, {
      PATTERN: /^[a-z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z]{2,}$/i
    }),
    equality: function(value, options, attribute, attributes, globalOptions) {
      if (!v.isDefined(value)) {
        return;
      }

      if (v.isString(options)) {
        options = {attribute: options};
      }
      options = v.extend({}, this.options, options);
      var message = options.message ||
        this.message ||
        "is not equal to %{attribute}";

      if (v.isEmpty(options.attribute) || !v.isString(options.attribute)) {
        throw new Error("The attribute must be a non empty string");
      }

      var otherValue = v.getDeepObjectValue(attributes, options.attribute)
        , comparator = options.comparator || function(v1, v2) {
          return v1 === v2;
        }
        , prettify = options.prettify ||
          (globalOptions && globalOptions.prettify) ||
          v.prettify;

      if (!comparator(value, otherValue, options, attribute, attributes)) {
        return v.format(message, {attribute: prettify(options.attribute)});
      }
    },

    // A URL validator that is used to validate URLs with the ability to
    // restrict schemes and some domains.
    url: function(value, options) {
      if (!v.isDefined(value)) {
        return;
      }

      options = v.extend({}, this.options, options);

      var message = options.message || this.message || "is not a valid url"
        , schemes = options.schemes || this.schemes || ['http', 'https']
        , allowLocal = options.allowLocal || this.allowLocal || false;

      if (!v.isString(value)) {
        return message;
      }

      // https://gist.github.com/dperini/729294
      var regex =
        "^" +
        // protocol identifier
        "(?:(?:" + schemes.join("|") + ")://)" +
        // user:pass authentication
        "(?:\\S+(?::\\S*)?@)?" +
        "(?:";

      var tld = "(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))";

      if (allowLocal) {
        tld += "?";
      } else {
        regex +=
          // IP address exclusion
          // private & local networks
          "(?!(?:10|127)(?:\\.\\d{1,3}){3})" +
          "(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})" +
          "(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})";
      }

      regex +=
          // IP address dotted notation octets
          // excludes loopback network 0.0.0.0
          // excludes reserved space >= 224.0.0.0
          // excludes network & broacast addresses
          // (first & last IP address of each class)
          "(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])" +
          "(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}" +
          "(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))" +
        "|" +
          // host name
          "(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)" +
          // domain name
          "(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*" +
          tld +
        ")" +
        // port number
        "(?::\\d{2,5})?" +
        // resource path
        "(?:[/?#]\\S*)?" +
      "$";

      var PATTERN = new RegExp(regex, 'i');
      if (!PATTERN.exec(value)) {
        return message;
      }
    }
  };

  validate.formatters = {
    detailed: function(errors) {return errors;},
    flat: v.flattenErrorsToArray,
    grouped: function(errors) {
      var attr;

      errors = v.groupErrorsByAttribute(errors);
      for (attr in errors) {
        errors[attr] = v.flattenErrorsToArray(errors[attr]);
      }
      return errors;
    },
    constraint: function(errors) {
      var attr;
      errors = v.groupErrorsByAttribute(errors);
      for (attr in errors) {
        errors[attr] = errors[attr].map(function(result) {
          return result.validator;
        }).sort();
      }
      return errors;
    }
  };

  validate.exposeModule(validate, this, exports, module, __webpack_require__(151));
}).call(this,
         true ? /* istanbul ignore next */ exports : null,
         true ? /* istanbul ignore next */ module : null,
        __webpack_require__(151));

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(13)(module)))

/***/ }),

/***/ 886:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(887);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(150)("01f8a08d", content, true, {});

/***/ }),

/***/ 887:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(12)(false);
// imports


// module
exports.push([module.i, ".flexit[data-v-712049d4]{display:-webkit-box;display:-ms-flexbox;display:flex;float:left;margin-bottom:10px}.line[data-v-712049d4]{line-height:26px!important}.mfooter[data-v-712049d4]{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:end;-ms-flex-pack:end;justify-content:flex-end}.mfooter button[data-v-712049d4]{margin:0 0 0 10px!important}.ul_search[data-v-712049d4]{background:#00b0e4;color:#fff}.ul_search li[data-v-712049d4]{cursor:pointer;list-style:none;color:#fff;border-bottom:1px solid #fff;text-align:center}.md-content>div ul[data-v-712049d4]{margin:0;padding:0 0 26px!important}", ""]);

// exports


/***/ }),

/***/ 888:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _validate = __webpack_require__(277);

var _validate2 = _interopRequireDefault(_validate);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import UserProfile from '../datatables/user-administration/UserProfile';
exports.default = {
	components: {},
	mounted: function mounted() {
		var _this = this;

		axios.post('/api/get/singleuser/salary', { id: this.$route.params.id }).then(function (response) {
			console.log(response.data);
			_this.user = response.data[0];

			_this.bigCal();
		}).catch(function (error) {
			console.log(error);
		});

		axios.get('/api/bankaccounts').then(function (response) {
			_this.bankaccounts = response.data;
		}).catch(function (error) {
			console.log(error);
		});

		axios.get('/api/get/chartofaccount').then(function (response) {
			_this.chart_of_accounts = response.data;
		}).catch(function (error) {
			console.log(error);
		});
		// axios.get('/api/get/role')
		// .then(response=>{
		// 	this.roles = response.data
		// })
		// .catch(erroe=>{
		// 	console.log(error);
		// });
		// axios.get('/api/get/department')
		// .then(response=>{
		// 	this.departments = response.data
		// })
		// .catch(erroe=>{
		// 	console.log(error);
		// });
	},
	data: function data() {
		return {
			modal: false,
			editmodal: false,
			user: {
				salary: 20000
			},
			paye: {},
			userPayment: {
				basic: '',
				housing: '',
				transport: '',
				medical: '',
				pension: '',
				meal: '',
				utilities: '',
				leave: '',
				dressing: '',
				entertainment: '',
				net_pay: '',
				paye: '',
				bank: '',
				project: '',
				project_phase: '',
				chart_of_account: ''
				// transport: this.transport(this.user.salary),

			},
			earnings: [],
			deductions: [],
			bankaccounts: {},
			chart_of_accounts: {},
			showProjectSearch: false,
			projects: [],
			project_phases: []
			// positions:{},
			// roles:{},
			// departments:{},


		};
	},

	methods: {
		selectProject: function selectProject(object) {
			var _this2 = this;

			this.userPayment.project_name = object.name;
			this.userPayment.project = object.id;
			this.projects = [];
			this.showProjectSearch = false;

			axios.post('/api/get/project/invividualprojectphases', { data: object.id }).then(function (response) {
				if (response.data.length > 0) {

					response.data.forEach(function (arr) {
						_this2.project_phases.push(arr.projectphase);
					});
					// this.project_phases = response.data.projectphase;
					// console.log(this.project_phases);

				} else {
					_this2.project_phases = [];
				}
			}).catch(function (error) {
				console.log(error);
			});
		},
		sortProjects: function sortProjects(value) {
			var _this3 = this;

			if (value.length > 0) {

				axios.post('/api/search/projects', { data: value }).then(function (response) {

					_this3.projects = response.data;
					if (_this3.projects.length > 0) {
						_this3.showProjectSearch = true;
					}
				}).catch(function (error) {
					console.log(error);
				});
			} else {
				this.showProjectSearch = false;
			}
		},
		bigCal: function bigCal() {
			var _this4 = this;

			this.userPayment.basic = this.addCommas(this.cal(11, this.user.salary));
			this.userPayment.housing = this.addCommas(this.cal(9, this.user.salary));
			this.userPayment.transport = this.addCommas(this.cal(9.6, this.user.salary));
			this.userPayment.medical = this.addCommas(this.cal(11.8, this.user.salary));
			this.userPayment.pension = this.addCommas(this.pencal(29.6, this.user.salary)); //basic + housing + transport
			this.userPayment.meal = this.addCommas(this.cal(11.9, this.user.salary));
			this.userPayment.utilities = this.addCommas(this.cal(11.8, this.user.salary));
			this.userPayment.leave = this.addCommas(this.cal(11.9, this.user.salary));
			this.userPayment.dressing = this.addCommas(this.cal(11.5, this.user.salary));
			this.userPayment.entertainment = this.addCommas(this.cal(11.5, this.user.salary));
			this.userPayment.net_pay = this.addCommas(this.net(this.user));
			this.userPayment.paye = this.addCommas(this.user.tax.length > 0 ? parseFloat(this.user.tax[0].amount) : 0);
			this.userPayment.bank = this.user.bank != null ? this.user.bank.name : '';

			if (this.user.earnings.length) {
				this.user.earnings.forEach(function (earning) {
					if (earning.payroll_id == _this4.$route.params.payperiod) {
						_this4.earnings.push(earning);
					}
				});
			}

			if (this.user.deductions.length) {
				this.user.deductions.forEach(function (deduction) {
					if (deduction.payroll_id == _this4.$route.params.payperiod) {
						_this4.deductions.push(deduction);
					}
				});
			}

			//console.log(earnings);
			// console.log(deductions);
		},
		addCommas: function addCommas(x) {
			x = (Math.round(x * 100) / 100).toFixed(2);
			return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		},
		cal: function cal(value, salary) {
			var result = parseFloat(value * salary / 100);
			return result;
		},
		pencal: function pencal(value, salary) {
			var result = parseFloat(value * salary / 100);
			result = parseFloat(8 * result / 100);
			return result;
		},
		net: function net() {
			var result = parseFloat(this.user.salary) + this.getEarnsOrDeducts(this.user.earnings) - this.getEarnsOrDeducts(this.user.deductions) - this.pencal(29.6, this.user.salary) - (this.user.tax.length > 0 ? parseFloat(this.user.tax[0].amount) : 0);

			return result;
		},
		getEarnsOrDeducts: function getEarnsOrDeducts(arrays) {
			var _this5 = this;

			var sum = 0;

			arrays.forEach(function (array) {
				if (array.payroll_id == _this5.$route.params.payperiod) {
					sum += arrays.length > 0 ? parseFloat(array.amount) : 0;
				}
			});

			return sum;
		},
		previousPage: function previousPage() {
			this.$router.go(-1);
		},
		closeModal: function closeModal() {
			this.modal = false;
		},
		closeEditModal: function closeEditModal() {
			Object.assign(this.editdata, this.beforeEditingCache);
			this.editmodal = false;
		},
		edit: function edit(value) {
			this.editdata = value;
			this.editmodal = true;
			this.beforeEditingCache = Object.assign({}, this.editdata);
		},
		sendData: function sendData() {
			var _this6 = this;

			var constraints = this.getConstraints();
			var errors = (0, _validate2.default)(this.userPayment, constraints);
			if (errors) {

				this.$notify({
					group: 'foo',
					type: 'error',
					title: 'Notification',
					text: (errors.project ? errors.project.toString() + '</br>' : '') + (errors.project_phase ? errors.project_phase.toString() + '</br>' : '') + (errors.chart_of_account ? errors.chart_of_account.toString() + '</br>' : ''),
					duration: 10000
				});
			} else {
				this.userPayment.payperiod = this.$route.params.payperiod;
				this.userPayment.user_id = this.user.id;
				axios.post('/api/add/payment', this.userPayment).then(function (response) {

					_this6.$notify({
						group: 'foo',
						type: 'success',
						title: 'Notification',
						text: 'Success!',
						duration: 10000
					});

					// this.user={};

					_this6.$router.go(-1);
				}).catch(function (error) {
					console.log(error);
					_this6.$notify({
						group: 'foo',
						type: 'error',
						title: 'Notification',
						text: error.response.data.error,
						duration: 10000
					});
				});
			}
		},
		sendEditData: function sendEditData() {
			var _this7 = this;

			var constraints = this.getConstraints();
			var errors = (0, _validate2.default)(this.editdata, constraints);
			// const errors = null;
			if (errors) {

				this.$notify({
					group: 'foo',
					type: 'error',
					title: 'Notification',
					text: (errors.name ? errors.name.toString() + '</br>' : '') + (errors.description ? errors.description.toString() + '</br>' : ''),
					duration: 10000
				});
			} else {
				axios.post('/api/edit/position', this.editdata).then(function (response) {
					_this7.$notify({
						group: 'foo',
						type: 'success',
						title: 'Notification',
						text: 'Success!',
						duration: 10000
					});
					_this7.editmodal = false;
					_this7.editdata = {};
				}).catch(function (error) {
					_this7.$notify({
						group: 'foo',
						type: 'error',
						title: 'Notification',
						text: error.response.data.error,
						duration: 10000
					});
				});
			}
		},
		getConstraints: function getConstraints() {
			return {

				project: {

					numericality: {
						onlyInteger: true,
						message: 'must be selected'

					}
				},
				project_phase: {

					numericality: {
						onlyInteger: true,
						message: 'ust be selected'

					}
				},
				chart_of_accounts: {

					numericality: {
						onlyInteger: true,
						message: 'must be selected'

					}
				}

			};
		}
	}
}; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/***/ }),

/***/ 889:
/***/ (function(module, exports) {

var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('div',{staticClass:"page-header"},[_c('h2',[_vm._v("Make Payment To "+_vm._s(_vm.user.last_name)+" "+_vm._s(_vm.user.first_name))]),_vm._v(" "),_c('ol',{staticClass:"breadcrumb"},[_c('li',[_c('router-link',{attrs:{"to":"/"}},[_vm._v("Home")])],1),_vm._v(" "),_c('li',{staticClass:"active"},[_vm._v("App Views")])])]),_vm._v(" "),_c('div',{staticClass:"row clearfix"},[_c('div',{staticClass:"col-lg-12 col-md-12"},[_c('div',{staticClass:"card"},[_c('div',{staticClass:"body p-b-0"},[_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-md-4"},[_c('button',{staticClass:"btn btn-info flexit",on:{"click":function($event){$event.preventDefault();_vm.previousPage()}}},[_c('em',{staticClass:"material-icons"},[_vm._v("keyboard_return")]),_c('span',{staticClass:"line"},[_vm._v("Previous Page")])])])])])]),_vm._v(" "),_c('div',{staticClass:"card",staticStyle:{"min-height":"1320px"}},[_c('div',{staticClass:"body forum"},[_c('div',{staticClass:"col-md-8 col-md-offset-2"},[_c('div',{staticClass:"md-content"},[_c('h3',[_vm._v("Make Payment")]),_vm._v(" "),_c('div',[_c('form',{attrs:{"autocomplete":"off"},on:{"submit":function($event){$event.preventDefault();_vm.sendData()}}},[_c('div',{staticStyle:{"margin-bottom":"10px"}},[_c('div',{staticClass:"col-md-6"},[_c('label',{attrs:{"for":"email_address"}},[_vm._v("Basic")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.userPayment.basic),expression:"userPayment.basic"}],staticClass:"form-control",attrs:{"type":"text","name":"color","placeholder":"Enter first name","required":"","readonly":""},domProps:{"value":(_vm.userPayment.basic)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.userPayment, "basic", $event.target.value)}}})])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("Transport")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.userPayment.transport),expression:"userPayment.transport"}],staticClass:"form-control",attrs:{"type":"text","name":"color","placeholder":"Enter last name","required":"","readonly":""},domProps:{"value":(_vm.userPayment.transport)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.userPayment, "transport", $event.target.value)}}})])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("Pension")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.userPayment.pension),expression:"userPayment.pension"}],staticClass:"form-control",attrs:{"type":"email","name":"color","placeholder":"Enter email","required":"","readonly":""},domProps:{"value":(_vm.userPayment.pension)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.userPayment, "pension", $event.target.value)}}})])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("Utilities")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.userPayment.utilities),expression:"userPayment.utilities"}],staticClass:"form-control",attrs:{"type":"text","name":"color","placeholder":"Enter phone number","required":"","readonly":""},domProps:{"value":(_vm.userPayment.utilities)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.userPayment, "utilities", $event.target.value)}}})])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("Dressing Allowance")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.userPayment.dressing),expression:"userPayment.dressing"}],staticClass:"form-control",attrs:{"type":"text","name":"color","placeholder":"Enter phone number","required":"","readonly":""},domProps:{"value":(_vm.userPayment.dressing)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.userPayment, "dressing", $event.target.value)}}})])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("PAYE")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.userPayment.paye),expression:"userPayment.paye"}],staticClass:"form-control",attrs:{"type":"text","name":"color","placeholder":"Enter phone number","required":"","readonly":""},domProps:{"value":(_vm.userPayment.paye)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.userPayment, "paye", $event.target.value)}}})])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("Bank Name")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.userPayment.bank),expression:"userPayment.bank"}],staticClass:"form-control",attrs:{"type":"text","name":"color","placeholder":"Enter phone number","required":"","readonly":""},domProps:{"value":(_vm.userPayment.bank)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.userPayment, "bank", $event.target.value)}}})])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("Bank Account")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('select',{directives:[{name:"model",rawName:"v-model",value:(_vm.userPayment.bank_account),expression:"userPayment.bank_account"}],staticClass:"form-control",on:{"change":function($event){var $$selectedVal = Array.prototype.filter.call($event.target.options,function(o){return o.selected}).map(function(o){var val = "_value" in o ? o._value : o.value;return val}); _vm.$set(_vm.userPayment, "bank_account", $event.target.multiple ? $$selectedVal : $$selectedVal[0])}}},_vm._l((_vm.bankaccounts),function(bankaccount){return _c('option',{domProps:{"value":bankaccount.id}},[_vm._v(_vm._s(bankaccount.account_name)+" "+_vm._s(bankaccount.account_number))])}))])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("Chart Of Account")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('select',{directives:[{name:"model",rawName:"v-model",value:(_vm.userPayment.chart_of_account),expression:"userPayment.chart_of_account"}],staticClass:"form-control",on:{"change":function($event){var $$selectedVal = Array.prototype.filter.call($event.target.options,function(o){return o.selected}).map(function(o){var val = "_value" in o ? o._value : o.value;return val}); _vm.$set(_vm.userPayment, "chart_of_account", $event.target.multiple ? $$selectedVal : $$selectedVal[0])}}},_vm._l((_vm.chart_of_accounts),function(chart_of_account){return _c('option',{domProps:{"value":chart_of_account.id}},[_vm._v(_vm._s(chart_of_account.name))])}))])]),_vm._v(" "),_c('h4',[_vm._v("Earnings")]),_vm._v(" "),_vm._l((_vm.earnings),function(earning){return _c('div',[_c('label',{attrs:{"for":"email_address"}},[_vm._v(_vm._s(earning.earns.name))]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(earning.amount),expression:"earning.amount"}],staticClass:"form-control",attrs:{"type":"text","name":"color","placeholder":"Enter phone number","required":"","readonly":""},domProps:{"value":(earning.amount)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(earning, "amount", $event.target.value)}}})])])])})],2),_vm._v(" "),_c('div',{staticClass:"col-md-6"},[_c('label',{attrs:{"for":"email_address"}},[_vm._v("Housing")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.userPayment.housing),expression:"userPayment.housing"}],staticClass:"form-control",attrs:{"type":"text","name":"color","placeholder":"Enter phone number","required":"","readonly":""},domProps:{"value":(_vm.userPayment.housing)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.userPayment, "housing", $event.target.value)}}})])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("Medical")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.userPayment.medical),expression:"userPayment.medical"}],staticClass:"form-control",attrs:{"type":"text","name":"color","placeholder":"Enter phone number","required":"","readonly":""},domProps:{"value":(_vm.userPayment.medical)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.userPayment, "medical", $event.target.value)}}})])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("Meal Allowance")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.userPayment.meal),expression:"userPayment.meal"}],staticClass:"form-control",attrs:{"type":"text","name":"color","placeholder":"Enter phone number","required":"","readonly":""},domProps:{"value":(_vm.userPayment.meal)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.userPayment, "meal", $event.target.value)}}})])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("Leave Allowance")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.userPayment.leave),expression:"userPayment.leave"}],staticClass:"form-control",attrs:{"type":"text","name":"color","placeholder":"Enter phone number","required":"","readonly":""},domProps:{"value":(_vm.userPayment.leave)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.userPayment, "leave", $event.target.value)}}})])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("Entertainment")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.userPayment.entertainment),expression:"userPayment.entertainment"}],staticClass:"form-control",attrs:{"type":"text","name":"color","placeholder":"Enter phone number","required":"","readonly":""},domProps:{"value":(_vm.userPayment.entertainment)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.userPayment, "entertainment", $event.target.value)}}})])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("Net Pay")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.userPayment.net_pay),expression:"userPayment.net_pay"}],staticClass:"form-control",attrs:{"type":"text","name":"color","placeholder":"Enter phone number","required":"","readonly":""},domProps:{"value":(_vm.userPayment.net_pay)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.userPayment, "net_pay", $event.target.value)}}})])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("Account Number")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.user.account_no),expression:"user.account_no"}],staticClass:"form-control",attrs:{"type":"text","name":"color","placeholder":"Enter phone number","required":"","readonly":""},domProps:{"value":(_vm.user.account_no)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.user, "account_no", $event.target.value)}}})])]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("Project")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.userPayment.project_name),expression:"userPayment.project_name"}],staticClass:"form-control",attrs:{"type":"","name":"","placeholder":"Type In Project"},domProps:{"value":(_vm.userPayment.project_name)},on:{"keyup":function($event){_vm.sortProjects(_vm.userPayment.project_name)},"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.userPayment, "project_name", $event.target.value)}}})]),_vm._v(" "),(_vm.showProjectSearch)?_c('div',{staticStyle:{"position":"absolute","width":"87%","z-index":"40000"}},[_c('ul',{staticClass:"list-unstyled ul_search"},_vm._l((_vm.projects),function(project){return _c('li',{on:{"click":function($event){_vm.selectProject(project)}}},[_vm._v(_vm._s(project.name)+" ")])}))]):_vm._e()]),_vm._v(" "),_c('label',{attrs:{"for":"email_address"}},[_vm._v("Project Phase")]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('select',{directives:[{name:"model",rawName:"v-model",value:(_vm.userPayment.project_phase),expression:"userPayment.project_phase"}],staticClass:"form-control",on:{"change":function($event){var $$selectedVal = Array.prototype.filter.call($event.target.options,function(o){return o.selected}).map(function(o){var val = "_value" in o ? o._value : o.value;return val}); _vm.$set(_vm.userPayment, "project_phase", $event.target.multiple ? $$selectedVal : $$selectedVal[0])}}},_vm._l((_vm.project_phases),function(project_phase){return _c('option',{domProps:{"value":project_phase.id}},[_vm._v(_vm._s(project_phase.name))])}))])]),_vm._v(" "),_c('h4',[_vm._v("Deductions")]),_vm._v(" "),_vm._l((_vm.deductions),function(deduction){return _c('div',[_c('label',{attrs:{"for":"email_address"}},[_vm._v(_vm._s(deduction.deducts.name))]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"form-line"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(deduction.amount),expression:"deduction.amount"}],staticClass:"form-control",attrs:{"type":"text","name":"color","placeholder":"Enter phone number","required":"","readonly":""},domProps:{"value":(deduction.amount)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(deduction, "amount", $event.target.value)}}})])])])})],2)]),_vm._v(" "),_vm._m(0)])])])])])])])])])}
var staticRenderFns = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"mfooter",staticStyle:{"float":"right"}},[_c('button',{staticClass:"btn btn-info"},[_vm._v("Submit")])])}]
module.exports = { render: render, staticRenderFns: staticRenderFns }

/***/ })

});