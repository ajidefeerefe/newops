webpackJsonp([73],{

/***/ 124:
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(809)
}
var normalizeComponent = __webpack_require__(11)
/* script */
var __vue_script__ = __webpack_require__(811)
/* template */
var __vue_template__ = __webpack_require__(812)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-6fd3620e"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

module.exports = Component.exports


/***/ }),

/***/ 150:
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(276)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}
var options = null
var ssrIdKey = 'data-vue-ssr-id'

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction, _options) {
  isProduction = _isProduction

  options = _options || {}

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[' + ssrIdKey + '~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }
  if (options.ssrId) {
    styleElement.setAttribute(ssrIdKey, obj.id)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ 151:
/***/ (function(module, exports) {

module.exports = function() {
	throw new Error("define cannot be used indirect");
};


/***/ }),

/***/ 276:
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ }),

/***/ 277:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module) {/*!
 * validate.js 0.12.0
 *
 * (c) 2013-2017 Nicklas Ansman, 2013 Wrapp
 * Validate.js may be freely distributed under the MIT license.
 * For all details and documentation:
 * http://validatejs.org/
 */

(function(exports, module, define) {
  "use strict";

  // The main function that calls the validators specified by the constraints.
  // The options are the following:
  //   - format (string) - An option that controls how the returned value is formatted
  //     * flat - Returns a flat array of just the error messages
  //     * grouped - Returns the messages grouped by attribute (default)
  //     * detailed - Returns an array of the raw validation data
  //   - fullMessages (boolean) - If `true` (default) the attribute name is prepended to the error.
  //
  // Please note that the options are also passed to each validator.
  var validate = function(attributes, constraints, options) {
    options = v.extend({}, v.options, options);

    var results = v.runValidations(attributes, constraints, options)
      , attr
      , validator;

    if (results.some(function(r) { return v.isPromise(r.error); })) {
      throw new Error("Use validate.async if you want support for promises");
    }
    return validate.processValidationResults(results, options);
  };

  var v = validate;

  // Copies over attributes from one or more sources to a single destination.
  // Very much similar to underscore's extend.
  // The first argument is the target object and the remaining arguments will be
  // used as sources.
  v.extend = function(obj) {
    [].slice.call(arguments, 1).forEach(function(source) {
      for (var attr in source) {
        obj[attr] = source[attr];
      }
    });
    return obj;
  };

  v.extend(validate, {
    // This is the version of the library as a semver.
    // The toString function will allow it to be coerced into a string
    version: {
      major: 0,
      minor: 12,
      patch: 0,
      metadata: null,
      toString: function() {
        var version = v.format("%{major}.%{minor}.%{patch}", v.version);
        if (!v.isEmpty(v.version.metadata)) {
          version += "+" + v.version.metadata;
        }
        return version;
      }
    },

    // Below is the dependencies that are used in validate.js

    // The constructor of the Promise implementation.
    // If you are using Q.js, RSVP or any other A+ compatible implementation
    // override this attribute to be the constructor of that promise.
    // Since jQuery promises aren't A+ compatible they won't work.
    Promise: typeof Promise !== "undefined" ? Promise : /* istanbul ignore next */ null,

    EMPTY_STRING_REGEXP: /^\s*$/,

    // Runs the validators specified by the constraints object.
    // Will return an array of the format:
    //     [{attribute: "<attribute name>", error: "<validation result>"}, ...]
    runValidations: function(attributes, constraints, options) {
      var results = []
        , attr
        , validatorName
        , value
        , validators
        , validator
        , validatorOptions
        , error;

      if (v.isDomElement(attributes) || v.isJqueryElement(attributes)) {
        attributes = v.collectFormValues(attributes);
      }

      // Loops through each constraints, finds the correct validator and run it.
      for (attr in constraints) {
        value = v.getDeepObjectValue(attributes, attr);
        // This allows the constraints for an attribute to be a function.
        // The function will be called with the value, attribute name, the complete dict of
        // attributes as well as the options and constraints passed in.
        // This is useful when you want to have different
        // validations depending on the attribute value.
        validators = v.result(constraints[attr], value, attributes, attr, options, constraints);

        for (validatorName in validators) {
          validator = v.validators[validatorName];

          if (!validator) {
            error = v.format("Unknown validator %{name}", {name: validatorName});
            throw new Error(error);
          }

          validatorOptions = validators[validatorName];
          // This allows the options to be a function. The function will be
          // called with the value, attribute name, the complete dict of
          // attributes as well as the options and constraints passed in.
          // This is useful when you want to have different
          // validations depending on the attribute value.
          validatorOptions = v.result(validatorOptions, value, attributes, attr, options, constraints);
          if (!validatorOptions) {
            continue;
          }
          results.push({
            attribute: attr,
            value: value,
            validator: validatorName,
            globalOptions: options,
            attributes: attributes,
            options: validatorOptions,
            error: validator.call(validator,
                value,
                validatorOptions,
                attr,
                attributes,
                options)
          });
        }
      }

      return results;
    },

    // Takes the output from runValidations and converts it to the correct
    // output format.
    processValidationResults: function(errors, options) {
      errors = v.pruneEmptyErrors(errors, options);
      errors = v.expandMultipleErrors(errors, options);
      errors = v.convertErrorMessages(errors, options);

      var format = options.format || "grouped";

      if (typeof v.formatters[format] === 'function') {
        errors = v.formatters[format](errors);
      } else {
        throw new Error(v.format("Unknown format %{format}", options));
      }

      return v.isEmpty(errors) ? undefined : errors;
    },

    // Runs the validations with support for promises.
    // This function will return a promise that is settled when all the
    // validation promises have been completed.
    // It can be called even if no validations returned a promise.
    async: function(attributes, constraints, options) {
      options = v.extend({}, v.async.options, options);

      var WrapErrors = options.wrapErrors || function(errors) {
        return errors;
      };

      // Removes unknown attributes
      if (options.cleanAttributes !== false) {
        attributes = v.cleanAttributes(attributes, constraints);
      }

      var results = v.runValidations(attributes, constraints, options);

      return new v.Promise(function(resolve, reject) {
        v.waitForResults(results).then(function() {
          var errors = v.processValidationResults(results, options);
          if (errors) {
            reject(new WrapErrors(errors, options, attributes, constraints));
          } else {
            resolve(attributes);
          }
        }, function(err) {
          reject(err);
        });
      });
    },

    single: function(value, constraints, options) {
      options = v.extend({}, v.single.options, options, {
        format: "flat",
        fullMessages: false
      });
      return v({single: value}, {single: constraints}, options);
    },

    // Returns a promise that is resolved when all promises in the results array
    // are settled. The promise returned from this function is always resolved,
    // never rejected.
    // This function modifies the input argument, it replaces the promises
    // with the value returned from the promise.
    waitForResults: function(results) {
      // Create a sequence of all the results starting with a resolved promise.
      return results.reduce(function(memo, result) {
        // If this result isn't a promise skip it in the sequence.
        if (!v.isPromise(result.error)) {
          return memo;
        }

        return memo.then(function() {
          return result.error.then(function(error) {
            result.error = error || null;
          });
        });
      }, new v.Promise(function(r) { r(); })); // A resolved promise
    },

    // If the given argument is a call: function the and: function return the value
    // otherwise just return the value. Additional arguments will be passed as
    // arguments to the function.
    // Example:
    // ```
    // result('foo') // 'foo'
    // result(Math.max, 1, 2) // 2
    // ```
    result: function(value) {
      var args = [].slice.call(arguments, 1);
      if (typeof value === 'function') {
        value = value.apply(null, args);
      }
      return value;
    },

    // Checks if the value is a number. This function does not consider NaN a
    // number like many other `isNumber` functions do.
    isNumber: function(value) {
      return typeof value === 'number' && !isNaN(value);
    },

    // Returns false if the object is not a function
    isFunction: function(value) {
      return typeof value === 'function';
    },

    // A simple check to verify that the value is an integer. Uses `isNumber`
    // and a simple modulo check.
    isInteger: function(value) {
      return v.isNumber(value) && value % 1 === 0;
    },

    // Checks if the value is a boolean
    isBoolean: function(value) {
      return typeof value === 'boolean';
    },

    // Uses the `Object` function to check if the given argument is an object.
    isObject: function(obj) {
      return obj === Object(obj);
    },

    // Simply checks if the object is an instance of a date
    isDate: function(obj) {
      return obj instanceof Date;
    },

    // Returns false if the object is `null` of `undefined`
    isDefined: function(obj) {
      return obj !== null && obj !== undefined;
    },

    // Checks if the given argument is a promise. Anything with a `then`
    // function is considered a promise.
    isPromise: function(p) {
      return !!p && v.isFunction(p.then);
    },

    isJqueryElement: function(o) {
      return o && v.isString(o.jquery);
    },

    isDomElement: function(o) {
      if (!o) {
        return false;
      }

      if (!o.querySelectorAll || !o.querySelector) {
        return false;
      }

      if (v.isObject(document) && o === document) {
        return true;
      }

      // http://stackoverflow.com/a/384380/699304
      /* istanbul ignore else */
      if (typeof HTMLElement === "object") {
        return o instanceof HTMLElement;
      } else {
        return o &&
          typeof o === "object" &&
          o !== null &&
          o.nodeType === 1 &&
          typeof o.nodeName === "string";
      }
    },

    isEmpty: function(value) {
      var attr;

      // Null and undefined are empty
      if (!v.isDefined(value)) {
        return true;
      }

      // functions are non empty
      if (v.isFunction(value)) {
        return false;
      }

      // Whitespace only strings are empty
      if (v.isString(value)) {
        return v.EMPTY_STRING_REGEXP.test(value);
      }

      // For arrays we use the length property
      if (v.isArray(value)) {
        return value.length === 0;
      }

      // Dates have no attributes but aren't empty
      if (v.isDate(value)) {
        return false;
      }

      // If we find at least one property we consider it non empty
      if (v.isObject(value)) {
        for (attr in value) {
          return false;
        }
        return true;
      }

      return false;
    },

    // Formats the specified strings with the given values like so:
    // ```
    // format("Foo: %{foo}", {foo: "bar"}) // "Foo bar"
    // ```
    // If you want to write %{...} without having it replaced simply
    // prefix it with % like this `Foo: %%{foo}` and it will be returned
    // as `"Foo: %{foo}"`
    format: v.extend(function(str, vals) {
      if (!v.isString(str)) {
        return str;
      }
      return str.replace(v.format.FORMAT_REGEXP, function(m0, m1, m2) {
        if (m1 === '%') {
          return "%{" + m2 + "}";
        } else {
          return String(vals[m2]);
        }
      });
    }, {
      // Finds %{key} style patterns in the given string
      FORMAT_REGEXP: /(%?)%\{([^\}]+)\}/g
    }),

    // "Prettifies" the given string.
    // Prettifying means replacing [.\_-] with spaces as well as splitting
    // camel case words.
    prettify: function(str) {
      if (v.isNumber(str)) {
        // If there are more than 2 decimals round it to two
        if ((str * 100) % 1 === 0) {
          return "" + str;
        } else {
          return parseFloat(Math.round(str * 100) / 100).toFixed(2);
        }
      }

      if (v.isArray(str)) {
        return str.map(function(s) { return v.prettify(s); }).join(", ");
      }

      if (v.isObject(str)) {
        return str.toString();
      }

      // Ensure the string is actually a string
      str = "" + str;

      return str
        // Splits keys separated by periods
        .replace(/([^\s])\.([^\s])/g, '$1 $2')
        // Removes backslashes
        .replace(/\\+/g, '')
        // Replaces - and - with space
        .replace(/[_-]/g, ' ')
        // Splits camel cased words
        .replace(/([a-z])([A-Z])/g, function(m0, m1, m2) {
          return "" + m1 + " " + m2.toLowerCase();
        })
        .toLowerCase();
    },

    stringifyValue: function(value, options) {
      var prettify = options && options.prettify || v.prettify;
      return prettify(value);
    },

    isString: function(value) {
      return typeof value === 'string';
    },

    isArray: function(value) {
      return {}.toString.call(value) === '[object Array]';
    },

    // Checks if the object is a hash, which is equivalent to an object that
    // is neither an array nor a function.
    isHash: function(value) {
      return v.isObject(value) && !v.isArray(value) && !v.isFunction(value);
    },

    contains: function(obj, value) {
      if (!v.isDefined(obj)) {
        return false;
      }
      if (v.isArray(obj)) {
        return obj.indexOf(value) !== -1;
      }
      return value in obj;
    },

    unique: function(array) {
      if (!v.isArray(array)) {
        return array;
      }
      return array.filter(function(el, index, array) {
        return array.indexOf(el) == index;
      });
    },

    forEachKeyInKeypath: function(object, keypath, callback) {
      if (!v.isString(keypath)) {
        return undefined;
      }

      var key = ""
        , i
        , escape = false;

      for (i = 0; i < keypath.length; ++i) {
        switch (keypath[i]) {
          case '.':
            if (escape) {
              escape = false;
              key += '.';
            } else {
              object = callback(object, key, false);
              key = "";
            }
            break;

          case '\\':
            if (escape) {
              escape = false;
              key += '\\';
            } else {
              escape = true;
            }
            break;

          default:
            escape = false;
            key += keypath[i];
            break;
        }
      }

      return callback(object, key, true);
    },

    getDeepObjectValue: function(obj, keypath) {
      if (!v.isObject(obj)) {
        return undefined;
      }

      return v.forEachKeyInKeypath(obj, keypath, function(obj, key) {
        if (v.isObject(obj)) {
          return obj[key];
        }
      });
    },

    // This returns an object with all the values of the form.
    // It uses the input name as key and the value as value
    // So for example this:
    // <input type="text" name="email" value="foo@bar.com" />
    // would return:
    // {email: "foo@bar.com"}
    collectFormValues: function(form, options) {
      var values = {}
        , i
        , j
        , input
        , inputs
        , option
        , value;

      if (v.isJqueryElement(form)) {
        form = form[0];
      }

      if (!form) {
        return values;
      }

      options = options || {};

      inputs = form.querySelectorAll("input[name], textarea[name]");
      for (i = 0; i < inputs.length; ++i) {
        input = inputs.item(i);

        if (v.isDefined(input.getAttribute("data-ignored"))) {
          continue;
        }

        name = input.name.replace(/\./g, "\\\\.");
        value = v.sanitizeFormValue(input.value, options);
        if (input.type === "number") {
          value = value ? +value : null;
        } else if (input.type === "checkbox") {
          if (input.attributes.value) {
            if (!input.checked) {
              value = values[name] || null;
            }
          } else {
            value = input.checked;
          }
        } else if (input.type === "radio") {
          if (!input.checked) {
            value = values[name] || null;
          }
        }
        values[name] = value;
      }

      inputs = form.querySelectorAll("select[name]");
      for (i = 0; i < inputs.length; ++i) {
        input = inputs.item(i);
        if (v.isDefined(input.getAttribute("data-ignored"))) {
          continue;
        }

        if (input.multiple) {
          value = [];
          for (j in input.options) {
            option = input.options[j];
             if (option && option.selected) {
              value.push(v.sanitizeFormValue(option.value, options));
            }
          }
        } else {
          var _val = typeof input.options[input.selectedIndex] !== 'undefined' ? input.options[input.selectedIndex].value : '';
          value = v.sanitizeFormValue(_val, options);
        }
        values[input.name] = value;
      }

      return values;
    },

    sanitizeFormValue: function(value, options) {
      if (options.trim && v.isString(value)) {
        value = value.trim();
      }

      if (options.nullify !== false && value === "") {
        return null;
      }
      return value;
    },

    capitalize: function(str) {
      if (!v.isString(str)) {
        return str;
      }
      return str[0].toUpperCase() + str.slice(1);
    },

    // Remove all errors who's error attribute is empty (null or undefined)
    pruneEmptyErrors: function(errors) {
      return errors.filter(function(error) {
        return !v.isEmpty(error.error);
      });
    },

    // In
    // [{error: ["err1", "err2"], ...}]
    // Out
    // [{error: "err1", ...}, {error: "err2", ...}]
    //
    // All attributes in an error with multiple messages are duplicated
    // when expanding the errors.
    expandMultipleErrors: function(errors) {
      var ret = [];
      errors.forEach(function(error) {
        // Removes errors without a message
        if (v.isArray(error.error)) {
          error.error.forEach(function(msg) {
            ret.push(v.extend({}, error, {error: msg}));
          });
        } else {
          ret.push(error);
        }
      });
      return ret;
    },

    // Converts the error mesages by prepending the attribute name unless the
    // message is prefixed by ^
    convertErrorMessages: function(errors, options) {
      options = options || {};

      var ret = []
        , prettify = options.prettify || v.prettify;
      errors.forEach(function(errorInfo) {
        var error = v.result(errorInfo.error,
            errorInfo.value,
            errorInfo.attribute,
            errorInfo.options,
            errorInfo.attributes,
            errorInfo.globalOptions);

        if (!v.isString(error)) {
          ret.push(errorInfo);
          return;
        }

        if (error[0] === '^') {
          error = error.slice(1);
        } else if (options.fullMessages !== false) {
          error = v.capitalize(prettify(errorInfo.attribute)) + " " + error;
        }
        error = error.replace(/\\\^/g, "^");
        error = v.format(error, {
          value: v.stringifyValue(errorInfo.value, options)
        });
        ret.push(v.extend({}, errorInfo, {error: error}));
      });
      return ret;
    },

    // In:
    // [{attribute: "<attributeName>", ...}]
    // Out:
    // {"<attributeName>": [{attribute: "<attributeName>", ...}]}
    groupErrorsByAttribute: function(errors) {
      var ret = {};
      errors.forEach(function(error) {
        var list = ret[error.attribute];
        if (list) {
          list.push(error);
        } else {
          ret[error.attribute] = [error];
        }
      });
      return ret;
    },

    // In:
    // [{error: "<message 1>", ...}, {error: "<message 2>", ...}]
    // Out:
    // ["<message 1>", "<message 2>"]
    flattenErrorsToArray: function(errors) {
      return errors
        .map(function(error) { return error.error; })
        .filter(function(value, index, self) {
          return self.indexOf(value) === index;
        });
    },

    cleanAttributes: function(attributes, whitelist) {
      function whitelistCreator(obj, key, last) {
        if (v.isObject(obj[key])) {
          return obj[key];
        }
        return (obj[key] = last ? true : {});
      }

      function buildObjectWhitelist(whitelist) {
        var ow = {}
          , lastObject
          , attr;
        for (attr in whitelist) {
          if (!whitelist[attr]) {
            continue;
          }
          v.forEachKeyInKeypath(ow, attr, whitelistCreator);
        }
        return ow;
      }

      function cleanRecursive(attributes, whitelist) {
        if (!v.isObject(attributes)) {
          return attributes;
        }

        var ret = v.extend({}, attributes)
          , w
          , attribute;

        for (attribute in attributes) {
          w = whitelist[attribute];

          if (v.isObject(w)) {
            ret[attribute] = cleanRecursive(ret[attribute], w);
          } else if (!w) {
            delete ret[attribute];
          }
        }
        return ret;
      }

      if (!v.isObject(whitelist) || !v.isObject(attributes)) {
        return {};
      }

      whitelist = buildObjectWhitelist(whitelist);
      return cleanRecursive(attributes, whitelist);
    },

    exposeModule: function(validate, root, exports, module, define) {
      if (exports) {
        if (module && module.exports) {
          exports = module.exports = validate;
        }
        exports.validate = validate;
      } else {
        root.validate = validate;
        if (validate.isFunction(define) && define.amd) {
          define([], function () { return validate; });
        }
      }
    },

    warn: function(msg) {
      if (typeof console !== "undefined" && console.warn) {
        console.warn("[validate.js] " + msg);
      }
    },

    error: function(msg) {
      if (typeof console !== "undefined" && console.error) {
        console.error("[validate.js] " + msg);
      }
    }
  });

  validate.validators = {
    // Presence validates that the value isn't empty
    presence: function(value, options) {
      options = v.extend({}, this.options, options);
      if (options.allowEmpty !== false ? !v.isDefined(value) : v.isEmpty(value)) {
        return options.message || this.message || "can't be blank";
      }
    },
    length: function(value, options, attribute) {
      // Empty values are allowed
      if (!v.isDefined(value)) {
        return;
      }

      options = v.extend({}, this.options, options);

      var is = options.is
        , maximum = options.maximum
        , minimum = options.minimum
        , tokenizer = options.tokenizer || function(val) { return val; }
        , err
        , errors = [];

      value = tokenizer(value);
      var length = value.length;
      if(!v.isNumber(length)) {
        v.error(v.format("Attribute %{attr} has a non numeric value for `length`", {attr: attribute}));
        return options.message || this.notValid || "has an incorrect length";
      }

      // Is checks
      if (v.isNumber(is) && length !== is) {
        err = options.wrongLength ||
          this.wrongLength ||
          "is the wrong length (should be %{count} characters)";
        errors.push(v.format(err, {count: is}));
      }

      if (v.isNumber(minimum) && length < minimum) {
        err = options.tooShort ||
          this.tooShort ||
          "is too short (minimum is %{count} characters)";
        errors.push(v.format(err, {count: minimum}));
      }

      if (v.isNumber(maximum) && length > maximum) {
        err = options.tooLong ||
          this.tooLong ||
          "is too long (maximum is %{count} characters)";
        errors.push(v.format(err, {count: maximum}));
      }

      if (errors.length > 0) {
        return options.message || errors;
      }
    },
    numericality: function(value, options, attribute, attributes, globalOptions) {
      // Empty values are fine
      if (!v.isDefined(value)) {
        return;
      }

      options = v.extend({}, this.options, options);

      var errors = []
        , name
        , count
        , checks = {
            greaterThan:          function(v, c) { return v > c; },
            greaterThanOrEqualTo: function(v, c) { return v >= c; },
            equalTo:              function(v, c) { return v === c; },
            lessThan:             function(v, c) { return v < c; },
            lessThanOrEqualTo:    function(v, c) { return v <= c; },
            divisibleBy:          function(v, c) { return v % c === 0; }
          }
        , prettify = options.prettify ||
          (globalOptions && globalOptions.prettify) ||
          v.prettify;

      // Strict will check that it is a valid looking number
      if (v.isString(value) && options.strict) {
        var pattern = "^-?(0|[1-9]\\d*)";
        if (!options.onlyInteger) {
          pattern += "(\\.\\d+)?";
        }
        pattern += "$";

        if (!(new RegExp(pattern).test(value))) {
          return options.message ||
            options.notValid ||
            this.notValid ||
            this.message ||
            "must be a valid number";
        }
      }

      // Coerce the value to a number unless we're being strict.
      if (options.noStrings !== true && v.isString(value) && !v.isEmpty(value)) {
        value = +value;
      }

      // If it's not a number we shouldn't continue since it will compare it.
      if (!v.isNumber(value)) {
        return options.message ||
          options.notValid ||
          this.notValid ||
          this.message ||
          "is not a number";
      }

      // Same logic as above, sort of. Don't bother with comparisons if this
      // doesn't pass.
      if (options.onlyInteger && !v.isInteger(value)) {
        return options.message ||
          options.notInteger ||
          this.notInteger ||
          this.message ||
          "must be an integer";
      }

      for (name in checks) {
        count = options[name];
        if (v.isNumber(count) && !checks[name](value, count)) {
          // This picks the default message if specified
          // For example the greaterThan check uses the message from
          // this.notGreaterThan so we capitalize the name and prepend "not"
          var key = "not" + v.capitalize(name);
          var msg = options[key] ||
            this[key] ||
            this.message ||
            "must be %{type} %{count}";

          errors.push(v.format(msg, {
            count: count,
            type: prettify(name)
          }));
        }
      }

      if (options.odd && value % 2 !== 1) {
        errors.push(options.notOdd ||
            this.notOdd ||
            this.message ||
            "must be odd");
      }
      if (options.even && value % 2 !== 0) {
        errors.push(options.notEven ||
            this.notEven ||
            this.message ||
            "must be even");
      }

      if (errors.length) {
        return options.message || errors;
      }
    },
    datetime: v.extend(function(value, options) {
      if (!v.isFunction(this.parse) || !v.isFunction(this.format)) {
        throw new Error("Both the parse and format functions needs to be set to use the datetime/date validator");
      }

      // Empty values are fine
      if (!v.isDefined(value)) {
        return;
      }

      options = v.extend({}, this.options, options);

      var err
        , errors = []
        , earliest = options.earliest ? this.parse(options.earliest, options) : NaN
        , latest = options.latest ? this.parse(options.latest, options) : NaN;

      value = this.parse(value, options);

      // 86400000 is the number of milliseconds in a day, this is used to remove
      // the time from the date
      if (isNaN(value) || options.dateOnly && value % 86400000 !== 0) {
        err = options.notValid ||
          options.message ||
          this.notValid ||
          "must be a valid date";
        return v.format(err, {value: arguments[0]});
      }

      if (!isNaN(earliest) && value < earliest) {
        err = options.tooEarly ||
          options.message ||
          this.tooEarly ||
          "must be no earlier than %{date}";
        err = v.format(err, {
          value: this.format(value, options),
          date: this.format(earliest, options)
        });
        errors.push(err);
      }

      if (!isNaN(latest) && value > latest) {
        err = options.tooLate ||
          options.message ||
          this.tooLate ||
          "must be no later than %{date}";
        err = v.format(err, {
          date: this.format(latest, options),
          value: this.format(value, options)
        });
        errors.push(err);
      }

      if (errors.length) {
        return v.unique(errors);
      }
    }, {
      parse: null,
      format: null
    }),
    date: function(value, options) {
      options = v.extend({}, options, {dateOnly: true});
      return v.validators.datetime.call(v.validators.datetime, value, options);
    },
    format: function(value, options) {
      if (v.isString(options) || (options instanceof RegExp)) {
        options = {pattern: options};
      }

      options = v.extend({}, this.options, options);

      var message = options.message || this.message || "is invalid"
        , pattern = options.pattern
        , match;

      // Empty values are allowed
      if (!v.isDefined(value)) {
        return;
      }
      if (!v.isString(value)) {
        return message;
      }

      if (v.isString(pattern)) {
        pattern = new RegExp(options.pattern, options.flags);
      }
      match = pattern.exec(value);
      if (!match || match[0].length != value.length) {
        return message;
      }
    },
    inclusion: function(value, options) {
      // Empty values are fine
      if (!v.isDefined(value)) {
        return;
      }
      if (v.isArray(options)) {
        options = {within: options};
      }
      options = v.extend({}, this.options, options);
      if (v.contains(options.within, value)) {
        return;
      }
      var message = options.message ||
        this.message ||
        "^%{value} is not included in the list";
      return v.format(message, {value: value});
    },
    exclusion: function(value, options) {
      // Empty values are fine
      if (!v.isDefined(value)) {
        return;
      }
      if (v.isArray(options)) {
        options = {within: options};
      }
      options = v.extend({}, this.options, options);
      if (!v.contains(options.within, value)) {
        return;
      }
      var message = options.message || this.message || "^%{value} is restricted";
      return v.format(message, {value: value});
    },
    email: v.extend(function(value, options) {
      options = v.extend({}, this.options, options);
      var message = options.message || this.message || "is not a valid email";
      // Empty values are fine
      if (!v.isDefined(value)) {
        return;
      }
      if (!v.isString(value)) {
        return message;
      }
      if (!this.PATTERN.exec(value)) {
        return message;
      }
    }, {
      PATTERN: /^[a-z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z]{2,}$/i
    }),
    equality: function(value, options, attribute, attributes, globalOptions) {
      if (!v.isDefined(value)) {
        return;
      }

      if (v.isString(options)) {
        options = {attribute: options};
      }
      options = v.extend({}, this.options, options);
      var message = options.message ||
        this.message ||
        "is not equal to %{attribute}";

      if (v.isEmpty(options.attribute) || !v.isString(options.attribute)) {
        throw new Error("The attribute must be a non empty string");
      }

      var otherValue = v.getDeepObjectValue(attributes, options.attribute)
        , comparator = options.comparator || function(v1, v2) {
          return v1 === v2;
        }
        , prettify = options.prettify ||
          (globalOptions && globalOptions.prettify) ||
          v.prettify;

      if (!comparator(value, otherValue, options, attribute, attributes)) {
        return v.format(message, {attribute: prettify(options.attribute)});
      }
    },

    // A URL validator that is used to validate URLs with the ability to
    // restrict schemes and some domains.
    url: function(value, options) {
      if (!v.isDefined(value)) {
        return;
      }

      options = v.extend({}, this.options, options);

      var message = options.message || this.message || "is not a valid url"
        , schemes = options.schemes || this.schemes || ['http', 'https']
        , allowLocal = options.allowLocal || this.allowLocal || false;

      if (!v.isString(value)) {
        return message;
      }

      // https://gist.github.com/dperini/729294
      var regex =
        "^" +
        // protocol identifier
        "(?:(?:" + schemes.join("|") + ")://)" +
        // user:pass authentication
        "(?:\\S+(?::\\S*)?@)?" +
        "(?:";

      var tld = "(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))";

      if (allowLocal) {
        tld += "?";
      } else {
        regex +=
          // IP address exclusion
          // private & local networks
          "(?!(?:10|127)(?:\\.\\d{1,3}){3})" +
          "(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})" +
          "(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})";
      }

      regex +=
          // IP address dotted notation octets
          // excludes loopback network 0.0.0.0
          // excludes reserved space >= 224.0.0.0
          // excludes network & broacast addresses
          // (first & last IP address of each class)
          "(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])" +
          "(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}" +
          "(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))" +
        "|" +
          // host name
          "(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)" +
          // domain name
          "(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*" +
          tld +
        ")" +
        // port number
        "(?::\\d{2,5})?" +
        // resource path
        "(?:[/?#]\\S*)?" +
      "$";

      var PATTERN = new RegExp(regex, 'i');
      if (!PATTERN.exec(value)) {
        return message;
      }
    }
  };

  validate.formatters = {
    detailed: function(errors) {return errors;},
    flat: v.flattenErrorsToArray,
    grouped: function(errors) {
      var attr;

      errors = v.groupErrorsByAttribute(errors);
      for (attr in errors) {
        errors[attr] = v.flattenErrorsToArray(errors[attr]);
      }
      return errors;
    },
    constraint: function(errors) {
      var attr;
      errors = v.groupErrorsByAttribute(errors);
      for (attr in errors) {
        errors[attr] = errors[attr].map(function(result) {
          return result.validator;
        }).sort();
      }
      return errors;
    }
  };

  validate.exposeModule(validate, this, exports, module, __webpack_require__(151));
}).call(this,
         true ? /* istanbul ignore next */ exports : null,
         true ? /* istanbul ignore next */ module : null,
        __webpack_require__(151));

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(13)(module)))

/***/ }),

/***/ 308:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
function createStyle(doc, cssText) {
    var style = doc.createElement('style');
    style.type = 'text/css';
    style.appendChild(window.document.createTextNode(cssText));
    return style;
}
exports.createStyle = createStyle;
function createIFrame(parent) {
    if (parent === void 0) { parent = window.document.body; }
    var el = window.document.createElement('iframe');
    var css = 'visibility:hidden;width:0;height:0;position:absolute;z-index:-9999;bottom:0;';
    el.setAttribute('src', 'about:blank');
    el.setAttribute('style', css);
    el.setAttribute('width', '0');
    el.setAttribute('height', '0');
    el.setAttribute('wmode', 'opaque');
    parent.appendChild(el);
    return el;
}
exports.createIFrame = createIFrame;
var Printd = /** @class */ (function () {
    function Printd(parent) {
        if (parent === void 0) { parent = window.document.body; }
        this.node = null;
        this.parent = parent;
        this.iframe = createIFrame(this.parent);
    }
    Printd.prototype.getIFrame = function () {
        return this.iframe;
    };
    Printd.prototype.print = function (el, cssText, callback) {
        this.node = el.cloneNode(true);
        var _a = this.iframe, contentDocument = _a.contentDocument, contentWindow = _a.contentWindow;
        if (contentDocument && contentWindow) {
            if (cssText) {
                contentDocument.head.appendChild(createStyle(contentDocument, cssText));
            }
            if (this.node) {
                contentDocument.body.innerHTML = '';
                contentDocument.body.appendChild(this.node);
                if (callback) {
                    callback(contentWindow, contentDocument, this.node, this.launchPrint);
                }
                else {
                    this.launchPrint(contentWindow);
                }
            }
        }
    };
    Printd.prototype.launchPrint = function (contentWindow) {
        var result = contentWindow.document.execCommand('print', false, null);
        if (!result) {
            contentWindow.print();
        }
    };
    return Printd;
}());
exports.Printd = Printd;
exports.default = Printd;


/***/ }),

/***/ 309:
/***/ (function(module, exports) {

module.exports = "/images/logo.png?2a2e2d4b4a36e63b2d7e15c07278399d";

/***/ }),

/***/ 809:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(810);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(150)("049b5201", content, true, {});

/***/ }),

/***/ 810:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(12)(false);
// imports


// module
exports.push([module.i, ".flexit[data-v-6fd3620e]{display:-webkit-box;display:-ms-flexbox;display:flex;float:left;margin-bottom:10px}.line[data-v-6fd3620e]{line-height:26px!important}.mfooter[data-v-6fd3620e]{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:end;-ms-flex-pack:end;justify-content:flex-end}.mfooter button[data-v-6fd3620e]{margin:0 0 0 10px!important}.ul_search[data-v-6fd3620e]{background:#00b0e4;color:#fff}.ul_search li[data-v-6fd3620e]{cursor:pointer;list-style:none;color:#fff;border-bottom:1px solid #fff;text-align:center}.md-content>div ul[data-v-6fd3620e]{margin:0;padding:0 0 26px!important}", ""]);

// exports


/***/ }),

/***/ 811:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _validate = __webpack_require__(277);

var _validate2 = _interopRequireDefault(_validate);

var _printd = __webpack_require__(308);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import UserProfile from '../datatables/user-administration/UserProfile';
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
	components: {},
	mounted: function mounted() {
		var _this = this;

		axios.post('/api/get/singleuser/salary', { id: this.$route.params.id }).then(function (response) {
			// console.log(response.data);
			_this.user = response.data[0];

			_this.bigCal();
		}).catch(function (error) {
			console.log(error);
		});

		axios.post('/api/get/payperiod', { id: this.$route.params.payperiod }).then(function (response) {
			console.log(response.data);
			_this.payperiods.push(response.data);
		}).catch(function (error) {
			console.log(error);
		});

		this.d = new _printd.Printd();
	},
	data: function data() {
		return {
			modal: false,
			editmodal: false,
			user: {
				salary: 20000
			},
			paye: {},
			userPayment: {
				basic: '',
				housing: '',
				transport: '',
				medical: '',
				pension: '',
				meal: '',
				utilities: '',
				leave: '',
				dressing: '',
				entertainment: '',
				net_pay: '',
				paye: '',
				bank: '',
				project: '',
				project_phase: '',
				chart_of_account: '',
				cssText: '\n                  .box {\n                    font-family: sans-serif;\n                    width: 500px;\n                    border: solid 1px #ccc;\n                    text-align: center;\n                    padding: 1em;\n                    margin: 2em auto;\n                  }\n\n                  button {\n                    background-color: #f0f0f0;\n                    border: solid 1px #ccc;\n                    padding: 5px 10px;\n                    font-size: 12px;\n                  }\n\n                  pre {\n                    color: #c7254e;\n                  }\n                '
				// transport: this.transport(this.user.salary),

			},
			earnings: [],
			deductions: [],
			bankaccounts: {},
			chart_of_accounts: {},
			showProjectSearch: false,
			projects: [],
			project_phases: [],
			payperiods: []
			// positions:{},
			// roles:{},
			// departments:{},


		};
	},

	methods: {
		print: function print() {
			this.d.print(document.getElementById('printable'), this.cssText);
		},
		selectProject: function selectProject(object) {
			var _this2 = this;

			this.userPayment.project_name = object.name;
			this.userPayment.project = object.id;
			this.projects = [];
			this.showProjectSearch = false;

			axios.post('/api/get/project/invividualprojectphases', { data: object.id }).then(function (response) {
				if (response.data.length > 0) {

					response.data.forEach(function (arr) {
						_this2.project_phases.push(arr.projectphase);
					});
					// this.project_phases = response.data.projectphase;
					// console.log(this.project_phases);

				} else {
					_this2.project_phases = [];
				}
			}).catch(function (error) {
				console.log(error);
			});
		},
		sortProjects: function sortProjects(value) {
			var _this3 = this;

			if (value.length > 0) {

				axios.post('/api/search/projects', { data: value }).then(function (response) {

					_this3.projects = response.data;
					if (_this3.projects.length > 0) {
						_this3.showProjectSearch = true;
					}
				}).catch(function (error) {
					console.log(error);
				});
			} else {
				this.showProjectSearch = false;
			}
		},
		bigCal: function bigCal() {
			var _this4 = this;

			this.userPayment.basic = this.addCommas(this.cal(11, this.user.salary));
			this.userPayment.housing = this.addCommas(this.cal(9, this.user.salary));
			this.userPayment.transport = this.addCommas(this.cal(9.6, this.user.salary));
			this.userPayment.medical = this.addCommas(this.cal(11.8, this.user.salary));
			this.userPayment.pension = this.addCommas(this.pencal(29.6, this.user.salary)); //basic + housing + transport
			this.userPayment.meal = this.addCommas(this.cal(11.9, this.user.salary));
			this.userPayment.utilities = this.addCommas(this.cal(11.8, this.user.salary));
			this.userPayment.leave = this.addCommas(this.cal(11.9, this.user.salary));
			this.userPayment.dressing = this.addCommas(this.cal(11.5, this.user.salary));
			this.userPayment.entertainment = this.addCommas(this.cal(11.5, this.user.salary));
			this.userPayment.net_pay = this.addCommas(this.net(this.user));
			this.userPayment.paye = this.addCommas(this.user.tax.length > 0 ? parseFloat(this.user.tax[0].amount) : 0);
			this.userPayment.bank = this.user.bank != null ? this.user.bank.name : '';

			if (this.user.earnings.length) {
				this.user.earnings.forEach(function (earning) {
					if (earning.payroll_id == _this4.$route.params.payperiod) {
						_this4.earnings.push(earning);
					}
				});
			}

			if (this.user.deductions.length) {
				this.user.deductions.forEach(function (deduction) {
					if (deduction.payroll_id == _this4.$route.params.payperiod) {
						_this4.deductions.push(deduction);
					}
				});
			}

			//console.log(earnings);
			// console.log(deductions);
		},
		addCommas: function addCommas(x) {
			x = (Math.round(x * 100) / 100).toFixed(2);
			return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		},
		cal: function cal(value, salary) {
			var result = parseFloat(value * salary / 100);
			return result;
		},
		pencal: function pencal(value, salary) {
			var result = parseFloat(value * salary / 100);
			result = parseFloat(8 * result / 100);
			return result;
		},
		net: function net() {
			var result = parseFloat(this.user.salary) + this.getEarnsOrDeducts(this.user.earnings) - this.getEarnsOrDeducts(this.user.deductions) - this.pencal(29.6, this.user.salary) - (this.user.tax.length > 0 ? parseFloat(this.user.tax[0].amount) : 0);

			return result;
		},
		getEarnsOrDeducts: function getEarnsOrDeducts(arrays) {
			var _this5 = this;

			// console.log(arrays);
			// console.log('arrays');
			var sum = 0;
			if (arrays != null) {
				arrays.forEach(function (array) {
					if (array.payroll_id == _this5.$route.params.payperiod) {
						sum += arrays.length > 0 ? parseFloat(array.amount) : 0;
					}
				});
			}

			return sum;
		},
		allDeductions: function allDeductions() {
			var result = this.getEarnsOrDeducts(this.user.deductions) + this.pencal(29.6, this.user.salary) + (this.user.tax != null ? parseFloat(this.user.tax[0].amount) : 0);
			return result;
		},
		previousPage: function previousPage() {
			this.$router.go(-1);
		},
		closeModal: function closeModal() {
			this.modal = false;
		},
		closeEditModal: function closeEditModal() {
			Object.assign(this.editdata, this.beforeEditingCache);
			this.editmodal = false;
		},
		edit: function edit(value) {
			this.editdata = value;
			this.editmodal = true;
			this.beforeEditingCache = Object.assign({}, this.editdata);
		},
		getConstraints: function getConstraints() {
			return {

				project: {

					numericality: {
						onlyInteger: true,
						message: 'must be selected'

					}
				},
				project_phase: {

					numericality: {
						onlyInteger: true,
						message: 'ust be selected'

					}
				},
				chart_of_accounts: {

					numericality: {
						onlyInteger: true,
						message: 'must be selected'

					}
				}

			};
		}
	}
};

/***/ }),

/***/ 812:
/***/ (function(module, exports, __webpack_require__) {

var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('div',{staticClass:"page-header"},[_c('h2',[_vm._v("Print Payment Slip For "+_vm._s(_vm.user.last_name)+" "+_vm._s(_vm.user.first_name))]),_vm._v(" "),_c('ol',{staticClass:"breadcrumb"},[_c('li',[_c('router-link',{attrs:{"to":"/"}},[_vm._v("Home")])],1),_vm._v(" "),_c('li',{staticClass:"active"},[_vm._v("App Views")])])]),_vm._v(" "),_c('div',{staticClass:"row clearfix"},[_c('div',{staticClass:"col-lg-12 col-md-12"},[_c('div',{staticClass:"card"},[_c('div',{staticClass:"body p-b-0"},[_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-md-4"},[_c('button',{staticClass:"btn btn-info flexit",on:{"click":function($event){$event.preventDefault();_vm.previousPage()}}},[_c('em',{staticClass:"material-icons"},[_vm._v("keyboard_return")]),_c('span',{staticClass:"line"},[_vm._v("Previous Page")])])])])])]),_vm._v(" "),_c('div',{staticClass:"card",staticStyle:{"min-height":"1320px"}},[_c('div',{staticClass:"body forum"},[_c('div',{staticClass:"modal-body",staticStyle:{"padding":"5%"},attrs:{"id":"printable"}},[_vm._m(0),_vm._v(" "),_c('div',{staticClass:"row"},[_vm._m(1),_vm._v(" "),_c('div',{staticClass:"col-xs-3"},[_c('span',{staticClass:"pull-left",attrs:{"id":"cemployee_name"}},[_vm._v(_vm._s(_vm.user.last_name)+" "+_vm._s(_vm.user.first_name))])])]),_vm._v(" "),_c('div',{staticClass:"row"},[_vm._m(2),_vm._v(" "),_c('div',{staticClass:"col-xs-3"},[_c('span',{attrs:{"id":"emp_no"}},[_vm._v(_vm._s(_vm.user.id))])])]),_vm._v(" "),_c('div',{staticClass:"row",staticStyle:{"margin-bottom":"2%"}},[_vm._m(3),_vm._v(" "),_vm._l((_vm.payperiods),function(payperiod){return _c('div',{staticClass:"col-xs-3"},[_c('span',{staticClass:"pull-left",attrs:{"id":"employee_name"}},[_vm._v(_vm._s(payperiod.month.name)+", "+_vm._s(payperiod.year)+".")])])})],2),_vm._v(" "),_c('div',{staticStyle:{"border":"1px solid black"}},[_vm._m(4),_vm._v(" "),_c('hr'),_vm._v(" "),_c('div',{staticClass:"row m",staticStyle:{"margin-left":"1%"}},[_c('div',{staticClass:"col-xs-4",staticStyle:{"border-right":"1px solid black"}},[_c('div',{staticClass:"row"},[_vm._m(5),_vm._v(" "),_c('div',{staticClass:"col-xs-3"},[_c('span',{attrs:{"id":"stub_basic"}},[_vm._v(_vm._s(_vm.userPayment.basic))])])]),_vm._v(" "),_c('div',{staticClass:"row"},[_vm._m(6),_vm._v(" "),_c('div',{staticClass:"col-xs-3"},[_c('span',{attrs:{"id":"stub_housing"}},[_vm._v(_vm._s(_vm.userPayment.housing))])])]),_vm._v(" "),_c('div',{staticClass:"row"},[_vm._m(7),_vm._v(" "),_c('div',{staticClass:"col-xs-3"},[_c('span',{attrs:{"id":"stub_transport"}},[_vm._v(" "+_vm._s(_vm.userPayment.transport))])])]),_vm._v(" "),_c('div',{staticClass:"row"},[_vm._m(8),_vm._v(" "),_c('div',{staticClass:"col-xs-3"},[_c('span',{attrs:{"id":"stub_medical"}},[_vm._v(_vm._s(_vm.userPayment.medical)+" ")])])]),_vm._v(" "),_c('div',{staticClass:"row"},[_vm._m(9),_vm._v(" "),_c('div',{staticClass:"col-xs-3"},[_c('span',{attrs:{"id":"smeal"}},[_vm._v(" "+_vm._s(_vm.userPayment.meal))])])]),_vm._v(" "),_c('div',{staticClass:"row"},[_vm._m(10),_vm._v(" "),_c('div',{staticClass:"col-xs-3"},[_c('span',{attrs:{"id":"sutilities"}},[_vm._v(_vm._s(_vm.userPayment.utilities)+" ")])])]),_vm._v(" "),_c('div',{staticClass:"row"},[_vm._m(11),_vm._v(" "),_c('div',{staticClass:"col-xs-3"},[_c('span',{attrs:{"id":"sleave"}},[_vm._v(" "+_vm._s(_vm.userPayment.leave))])])]),_vm._v(" "),_c('div',{staticClass:"row"},[_vm._m(12),_vm._v(" "),_c('div',{staticClass:"col-xs-3"},[_c('span',{attrs:{"id":"sdress"}},[_vm._v(" "+_vm._s(_vm.userPayment.dressing))])])]),_vm._v(" "),_c('div',{staticClass:"row"},[_vm._m(13),_vm._v(" "),_c('div',{staticClass:"col-xs-3"},[_c('span',{attrs:{"id":"sentertainment"}},[_vm._v(" "+_vm._s(_vm.userPayment.entertainment))])])])]),_vm._v(" "),_c('div',{staticClass:"col-xs-4",staticStyle:{"height":"100px","border-right":"1px solid black"}},_vm._l((_vm.earnings),function(earning){return _c('div',{staticClass:"row"},[_c('div',{staticClass:"col-xs-6"},[_c('label',{staticClass:"control-label bold",attrs:{"for":""}},[_vm._v(_vm._s(earning.earns.name)+": ")])]),_vm._v(" "),_c('div',{staticClass:"col-xs-3"},[_c('span',{attrs:{"id":"stub_paye"}},[_vm._v(" "+_vm._s(earning.amount))])])])})),_vm._v(" "),_c('div',{staticClass:"col-xs-4"},[_c('div',{staticClass:"row"},[_vm._m(14),_vm._v(" "),_c('div',{staticClass:"col-xs-3"},[_c('span',{attrs:{"id":"stub_pension"}},[_vm._v(" "+_vm._s(_vm.userPayment.pension))])])]),_vm._v(" "),_c('div',{staticClass:"row"},[_vm._m(15),_vm._v(" "),_c('div',{staticClass:"col-xs-3"},[_c('span',{attrs:{"id":"stub_paye"}},[_vm._v(" "+_vm._s(_vm.userPayment.paye))])])]),_vm._v(" "),_vm._l((_vm.deductions),function(deduction){return _c('div',{staticClass:"row"},[_c('div',{staticClass:"col-xs-6"},[_c('label',{staticClass:"control-label bold",staticStyle:{"text-transform":"capitalize"},attrs:{"for":""}},[_vm._v(_vm._s(deduction.deducts.name)+": ")])]),_vm._v(" "),_c('div',{staticClass:"col-xs-3"},[_c('span',{attrs:{"id":"stub_paye"}},[_vm._v(" "+_vm._s(deduction.amount))])])])})],2)]),_vm._v(" "),_c('hr'),_vm._v(" "),_c('div',{staticClass:"row",staticStyle:{"margin-top":"2%","margin-left":"1%"}},[_c('div',{staticClass:"col-xs-4",staticStyle:{"border-right":"1px solid black"}},[_c('div',{staticClass:"row"},[_vm._m(16),_vm._v(" "),_c('div',{staticClass:"col-xs-3"},[_c('span',{attrs:{"id":"stub_gross"}},[_vm._v(" "+_vm._s(_vm.user.salary))])])])]),_vm._v(" "),_c('div',{staticClass:"col-xs-4",staticStyle:{"border-right":"1px solid black"}},[_c('div',{staticClass:"row"},[_vm._m(17),_vm._v(" "),_c('div',{staticClass:"col-xs-3"},[_c('span',{attrs:{"id":"totalA"}},[_vm._v(" "+_vm._s(_vm.addCommas(_vm.getEarnsOrDeducts(_vm.user.earnings))))])])])]),_vm._v(" "),_c('div',{staticClass:"col-xs-4"},[_c('div',{staticClass:"row"},[_vm._m(18),_vm._v(" "),_c('div',{staticClass:"col-xs-3"},[_c('span',{attrs:{"id":"totalD"}},[_vm._v(" "+_vm._s(_vm.addCommas(_vm.allDeductions())))])])])])]),_vm._v(" "),_c('hr'),_vm._v(" "),_c('div',{staticClass:"row",staticStyle:{"margin-top":"2%","margin-left":"1%","margin-bottom":"1%"}},[_c('div',{staticClass:"col-xs-4"},[_c('div',{staticClass:"row"},[_vm._m(19),_vm._v(" "),_c('div',{staticClass:"col-xs-3"},[_c('span',{staticClass:"pull-left",attrs:{"id":"stub_total2"}},[_vm._v(_vm._s(_vm.userPayment.net_pay))])])])])])])]),_vm._v(" "),_c('div',{staticClass:"modal-footer ",staticStyle:{"border":"none"}},[_c('button',{staticClass:"btn btn-primary",attrs:{"type":"button"},on:{"click":function($event){$event.preventDefault();_vm.print()}}},[_c('span',{staticClass:"fa fa-print"}),_vm._v(" Print")])])])])])])])}
var staticRenderFns = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"row",staticStyle:{"margin-bottom":"3%"}},[_c('div',{staticClass:"col-sm-3"},[_c('img',{staticClass:"img-responsive",staticStyle:{"height":"40px"},attrs:{"src":__webpack_require__(309)}})])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"col-xs-2"},[_c('label',{staticClass:"control-label bold",attrs:{"for":""}},[_vm._v("Employee Name: ")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"col-xs-2"},[_c('label',{staticClass:"control-label bold",attrs:{"for":""}},[_vm._v("Employee Number: ")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"col-xs-2"},[_c('label',{staticClass:"control-label bold",attrs:{"for":""}},[_vm._v("Pay Period: ")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"row m",staticStyle:{"margin-top":"2%","margin-left":"1%"}},[_c('div',{staticClass:"col-xs-4 bold"},[_vm._v("Gross Pay")]),_vm._v(" "),_c('div',{staticClass:"col-xs-4 bold"},[_vm._v("Other Earnings")]),_vm._v(" "),_c('div',{staticClass:"col-xs-4 bold"},[_vm._v("Deductions")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"col-xs-6"},[_c('label',{staticClass:"control-label bold",attrs:{"for":""}},[_vm._v("Basic: ")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"col-xs-6"},[_c('label',{staticClass:"control-label bold",attrs:{"for":""}},[_vm._v("Housing: ")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"col-xs-6"},[_c('label',{staticClass:"control-label bold",attrs:{"for":""}},[_vm._v("Transport: ")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"col-xs-6"},[_c('label',{staticClass:"control-label bold",attrs:{"for":""}},[_vm._v("Medical: ")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"col-xs-6"},[_c('label',{staticClass:"control-label bold",attrs:{"for":""}},[_vm._v("Meal Allowance: ")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"col-xs-6"},[_c('label',{staticClass:"control-label bold",attrs:{"for":""}},[_vm._v("Utilities: ")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"col-xs-6"},[_c('label',{staticClass:"control-label bold",attrs:{"for":""}},[_vm._v("Leave Allowance: ")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"col-xs-6"},[_c('label',{staticClass:"control-label bold",attrs:{"for":""}},[_vm._v("Dressing Allowance: ")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"col-xs-6"},[_c('label',{staticClass:"control-label bold",attrs:{"for":""}},[_vm._v("Entertainment: ")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"col-xs-6"},[_c('label',{staticClass:"control-label bold",attrs:{"for":""}},[_vm._v("Pension: ")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"col-xs-6"},[_c('label',{staticClass:"control-label bold",attrs:{"for":""}},[_vm._v("PAYE: ")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"col-xs-6"},[_c('label',{staticClass:"control-label bold",attrs:{"for":""}},[_vm._v("Total Gross: ")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"col-xs-6"},[_c('label',{staticClass:"control-label bold",attrs:{"for":""}},[_vm._v("Total Other Earnings: ")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"col-xs-6"},[_c('label',{staticClass:"control-label bold",attrs:{"for":""}},[_vm._v("Total Deductions: ")])])},function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"col-xs-6"},[_c('label',{staticClass:"control-label bold",attrs:{"for":""}},[_vm._v("Net Pay: ")])])}]
module.exports = { render: render, staticRenderFns: staticRenderFns }

/***/ })

});