let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');


mix.scripts([
    'public/js/jquery.min.js',
    'public/js/jquery.slimscroll.js',
    'public/js/jquery.storageapi.js',
    'public/js/modernizr.custom.js',
    'public/js/bootstrap-notify.js',
    'public/js/classie.js',
    'public/js/modalEffects.js',
    'public/js/modals.js',
    'public/js/waves.js',
    'public/js/demo.js',
     'public/js/layout.js'
     ], 'public/js/all.js');

// mix.options({
//     uglifyOptions: {
//       compress: false
//     }
//   });