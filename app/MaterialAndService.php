<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaterialAndService extends Model
{
    public function type()
    {
    	return $this->belongsTo('App\MaterialAndServiceType','service_type_id');
       
    }
    public function category()
    {
    	return $this->belongsTo('App\MaterialAndServiceCategory','material_and_services_category_id');
       
    }

    public function projectmaterials(){
        return $this->hasMany('App\ProjectMaterial');
    }

    public function projectlabours(){
        return $this->hasMany('App\ProjectLabour');
    }
    public function expense_records()
    {
        return $this->hasMany('App\ExpenseRecord');
       
    }
}
