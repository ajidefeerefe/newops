<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IndividualProjectPhases extends Model
{
    protected $guarded = [];
    public function project(){
        return $this->belongsTo('App\Project');
    }

    public function projectphase(){
        return $this->belongsTo('App\ProjectPhase','project_phase_id');
    }

    public function expense_record(){
        return $this->hasMany('App\ExpenseRecord')
                    ->selectRaw('individual_project_phases_id, SUM(amount) as amount')
                    ->groupBy('individual_project_phases_id');
    }

    // William
    public function expense_records(){
        return $this->hasMany('App\ExpenseRecord');
    }

    public function phase_material_amount(){

        return $this->hasMany('App\ProjectMaterial')
                    ->selectRaw('individual_project_phases_id, SUM(amount) as amount')
                    ->groupBy('individual_project_phases_id');

    }

    public function phase_material(){
        
        return $this->hasMany('App\ProjectMaterial');

    }

    public function phase_labour_amount(){
        
        return $this->hasMany('App\ProjectLabour')
                    ->selectRaw('individual_project_phases_id, SUM(amount) as amount')
                    ->groupBy('individual_project_phases_id');

    }

    public function phase_labour(){
        
        return $this->hasMany('App\ProjectLabour');

    }
    
    public function costingweek(){
        return $this->hasMany('App\CostingWeek');
    }

    public function purchase_requisition(){
        return $this->hasMany('App\PurchaseRequisition');
    }

}
