<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    // public function role()
    // {
    //     // return $this->hasOne('App\Role');
    //     return $this->belongsToMany('App\Role', 'user__roles', 'user_id', 'role_id');
    // } 
    public function role()
    {
        // return $this->hasOne('App\Role');
        return $this->belongsTo('App\Role','role_id');
    }

     public function position()
    {
        return $this->belongsTo('App\UserPosition','position_id');
       
    }

    public function department()
    {
        return $this->belongsTo('App\Department','department_id');
       
    } 

    public function deductions()
    {
        return $this->hasMany('App\Deduction','user_id');
       
    } 

    public function earnings()
    {
        return $this->hasMany('App\Earning','user_id');
       
    }
    public function tax()
    {
        return $this->hasMany('App\Tax','user_id')
                    ->selectRaw('user_id, SUM(amount) as amount')
                    ->groupBy('user_id');
       
    } 
    public function bank()
    {
       return $this->belongsTo('App\Bank','bank_id');
       
    }
    public function purchase_requisition(){
        return $this->hasOne('App\PurchaseRequisition');
    }

     public function expense_records()
    {
        return $this->hasMany('App\ExpenseRecord','created_by');
       
    }
}
