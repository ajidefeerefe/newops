<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectPhase extends Model
{
    protected $guarded=['id'];

    function individualprojectphase(){
    	return $this->hasMany('App\IndividualProjectPhases');
    }

    function projectfiles(){
    	return $this->hasMany('App\ProjectFile');
    }
}
