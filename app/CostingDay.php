<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostingDay extends Model
{
    public function project_material(){
    	return $this->hasMany('App\ProjectMaterial');
    }

    public function project_labour(){
    	return $this->hasMany('App\ProjectLabour');
    }

    public function costing_week(){
    	return $this->belongsTo('App\CostingWeek');
    }
}
