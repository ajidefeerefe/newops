<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $guarded = ['id'];

    public function topup()
    {
    	return $this->hasMany('App\Inventorytopup','inventory_id');
    				
       
    }

    public function material()
    {
    	return $this->belongsTo('App\MaterialAndService','materrial_and_service_id');
       
    }

    public function location()
    {
        return $this->belongsTo('App\InventoryLocation','inventory_location_id');
       
    } 

    
}
