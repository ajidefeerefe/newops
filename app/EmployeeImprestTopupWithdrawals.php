<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeImprestTopupWithdrawals extends Model
{
    public function imprest()
    {
    	return $this->belongsTo('App\EmployeeImprest','employee_imprest_id');
       
    } 

     public function project()
    {
    	return $this->belongsTo('App\Project','project_id');
       
    }   

    public function projectPhase()
    {
    	return $this->belongsTo('App\ProjectPhase','project_phase_id');
       
    }  

    public function material()
    {
        return $this->belongsTo('App\MaterialAndService','material_and_service_id');
       
    } 
    public function asset()
    {
    	return $this->belongsTo('App\AssetModel','asset_model_id');
       
    } 
}
