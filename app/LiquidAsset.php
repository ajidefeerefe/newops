<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiquidAsset extends Model
{
     protected $guarded = ['id'];

    public function assetmodel()
    {
    	return $this->belongsTo('App\AssetModel','asset_model_id');
       
    }
    public function assetmanufacturer()
    {
    	return $this->belongsTo('App\AssetManufacturer','asset_manufacturer_id');
       
    }  
    public function assetcategory()
    {
    	return $this->belongsTo('App\AssetCategory','asset_category_id');
       
    }

     public function assetcolor()
    {
    	return $this->belongsTo('App\Color','color_id');
       
    } 
    public function assignedto()
    {
    	return $this->belongsTo('App\LiquidAssetAssignment','assignment_id');
       
    }
}
