<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $fillable = ['name'];

    public function bank_account(){
        return $this->hasMany('App\BankAccount');
    }
}
