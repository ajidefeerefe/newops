<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
	protected $garded = ['id','role_id'];

    public function role()
    {
        return $this->belongsTo('App\Role')->select(['id','name']);
    }

 
}
