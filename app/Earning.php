<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Earning extends Model
{
    protected $guarded = ['id'];

    public function earns()
    {
    	return $this->belongsTo('App\Earn','earn_id');
    				
       
    } 
}
