<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectPhaseActivity extends Model
{
    protected $guarded = ['id'];

    public function project_phase()
    {
    	return $this->belongsTo('App\ProjectPhase','project_phase_id');
       
    }
    public function material_labour()
    {
    	return $this->belongsTo('App\MaterialAndService','material_and_service_id')->where('service_type_id',2);
       
    }
}
