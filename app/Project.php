<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
	protected $guarded =[];
    public function department(){
    	return $this->belongsTo('App\Department','department_id');
    }

    public function individualprojectphases(){

    	return $this->hasMany('App\IndividualProjectPhases');

    }

    public function projectfiles(){
    	return $this->hasMany('App\ProjectFile');
    }

    public function projectstaffallocations(){
        return $this->hasMany('App\ProjectStaffAllocation');
    }

    public function projectmaterials(){
        return $this->hasMany('App\ProjectMaterial');
    }

    public function projectlabours(){
        return $this->hasMany('App\ProjectLabour');
    }

    public function client_payments(){
        return $this->hasMany('App\ClientPayment')
                    ->selectRaw('project_id, SUM(amount) as amount')
                    ->groupBy('project_id');
    }

     public function vendorContracts(){
        return $this->hasMany('App\VendorContract','project_id');
    }

    public function purchase_requisition(){
        return $this->hasMany('App\PurchaseRequisition');
    }

    public function expense_records(){
        return $this->hasMany('App\ExpenseRecord');
    }

    public function expense_records_amount(){
        
        return $this->hasMany('App\ExpenseRecord')
                    ->selectRaw('project_id, SUM(amount) as amount')
                    ->groupBy('project_id');

    }

     public function individual_project_phase_activities(){
        return $this->hasMany('App\IndividualProjectPhaseActivity','project_id');
    }

   
}
