<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpenseRecord extends Model
{
	 protected $guarded = ['id'];
    public function individual_project_phase(){
    	return $this->belongsTo('App\IndividualProjectPhases','individual_project_phases_id');
    }

     public function bank_account(){
        return $this->belongsTo('App\BankAccount');
    }
     public function user(){
        return $this->belongsTo('App\User','created_by');
    } 
    public function vendor_guarantor(){
    	return $this->belongsTo('App\User','vendor_guarantor');
    }

    public function material_and_service(){
        return $this->belongsTo('App\MaterialAndService');
    } 

    public function vendor(){
        return $this->belongsTo('App\Vendor','vendor_id');
    }
    //William
    public function purchase_requisition_material(){
        return $this->belongsTo('App\PurchaseRequisitionMaterial');
    }

    public function project(){
        return $this->belongsTo('App\Project','project_id');
    }

    public function vendor_contract(){
        return $this->belongsTo('App\VendorContract','vendor_contract_id');
    }
}
