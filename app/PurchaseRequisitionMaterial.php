<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseRequisitionMaterial extends Model
{
    public function purchase_requisition(){
    	return $this->belongsTo('App\PurchaseRequisition');
    }

    public function project_material(){
    	return $this->belongsTo('App\ProjectMaterial');
    }

    public function vendor(){
    	return $this->belongsTo('App\Vendor');
    }

    public function expense_records(){
    	return $this->hasMany('App\ExpenseRecord');
    }

    public function expense_records_amount(){
    	
    	return $this->hasMany('App\ExpenseRecord')
    				->selectRaw('purchase_requisition_material_id, SUM(amount) as amount')
    				->groupBy('purchase_requisition_material_id');

    }

    public function chart_of_account(){
        return $this->belongsTo('App\ChartOfAccount');
    }

    
}
