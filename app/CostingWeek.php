<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostingWeek extends Model
{
	protected $guarded = [];
    public function individualprojectphase(){
    	return $this->belongsTo('App\IndividualProjectPhases','individual_project_phases_id');
    }

    public function department(){
    	return $this->belongsTo('App\Department','department_id');
    }

    public function costing_day(){
    	return $this->hasMany('App\CostingDay');
    }
}
