<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryTopupWithdrawal extends Model
{
    protected $guarded = ['id'];
}
