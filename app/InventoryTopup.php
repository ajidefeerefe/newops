<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryTopup extends Model
{
     protected $guarded = ['id'];

    public function withdraws()
    {
    	return $this->hasMany('App\InventoryTopupWithdrawal','inventory_topup_id');
       
    }
    public function vendor()
    {
    	return $this->belongsTo('App\Vendor','vendor_id');
       
    }

     public function inventory()
    {
    	return $this->belongsTo('App\Inventory','inventory_id');
    				
       
    }
}
