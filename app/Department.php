<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = ['name','active'];

    public function project(){
    	return $this->hasMany('App\Project');
    }

    public function costingweek(){
    	return $this->hasMany('App\CostingWeek');
    }
}

