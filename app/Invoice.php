<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
	protected $guarded = ['id'];
	
    public function client_payments(){
        return $this->hasMany('App\ClientPayment');
    } 

    public function details(){
        return $this->hasMany('App\InvoiceDetail','invoice_id');
    }
    
    public function project()
    {
    	return $this->belongsTo('App\Project','project_id');
       
    } 

    public function client()
    {
        return $this->belongsTo('App\Client','client_id');
       
    }  
    
    public function payment()
    {
    	return $this->hasMany('App\ClientPayment','invoice_id')
                    ->selectRaw('invoice_id, SUM(amount) as amount')
                    ->groupBy('invoice_id');
       
    } 
}
