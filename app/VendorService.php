<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorService extends Model
{
    public function material_and_service(){

    	return $this->belongsTo('App\MaterialAndService','material_and_service_id');

    }

    public function vendor(){
    	return $this->belongsTo('App\Vendor');
    }
}
