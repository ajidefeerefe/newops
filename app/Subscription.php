<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    public function role()
    {
        return $this->belongsTo('App\Role')->select(['id','name']);
    }
}
