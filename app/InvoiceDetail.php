<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetail extends Model
{
    protected $guarded = ['id'];

      public function materials(){
        return $this->belongsTo('App\MaterialAndService','material_and_service_id');
    }
}
