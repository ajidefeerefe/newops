<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeImprestTopup extends Model
{
    public function imprest()
    {
        return $this->belongsTo('App\EmployeeImprest','employee_imprest_id');
       
    }  

    public function topup_by()
    {
    	return $this->belongsTo('App\User','topup_by');
       
    } 
    // public function withdraws()
    // {
    // 	return $this->hasMany('App\EmployeeImprestTopupWithdrawals','employee_imprest_topup_id')
    // 				->selectRaw('employee_imprest_topup_id, SUM(amount) as amount')
    // 				->groupBy('employee_imprest_topup_id');
       
    // } 
}
