<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectMaterial extends Model
{
    protected $guarded = ['id'];
    public function project(){
    	return $this->belongsTo('App\Project','project_id');
    }

    public function material_and_service(){
    	return $this->belongsTo('App\MaterialAndService','material_and_services_id');
    }

    public function individualprojectphase(){
    	return $this->belongsTo('App\IndividualProjectPhases','individual_project_phases_id');
    }

    public function costing_day(){
    	return $this->belongsTo('App\CostingDay');
    }

    public function purchase_requisition_materials(){
        return $this->hasMany('App\PurchaseRequisitionMaterial');
    }
}
