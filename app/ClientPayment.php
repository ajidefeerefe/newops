<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientPayment extends Model
{
    protected $guarded = [];

    public function project(){
        return $this->belongsTo('App\Project', 'project_id');
    }

    public function invoice(){
        return $this->belongsTo('App\Invoice');
    }

    public function payment_category(){
        return $this->belongsTo('App\PaymentCategory');
    }

    public function bank_account(){
        return $this->belongsTo('App\BankAccount');
    }
}
