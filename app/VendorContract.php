<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorContract extends Model
{
     protected $guarded = ['id'];

      public function expenseRecords(){
    	return $this->hasMany('App\ExpenseRecord','vendor_contract_id')
    					->selectRaw('vendor_contract_id, SUM(amount) as amount')
                    	->groupBy('vendor_contract_id');
    } 
    public function vendor(){
    	return $this->belongsTo('App\Vendor','vendor_id');
    					
    }

    public function material(){
        return $this->belongsTo('App\MaterialAndService','material_and_service_id');
                        
    }
    public function asset(){
    	return $this->belongsTo('App\LiquidAsset','liquid_asset_id');
    					
    }
    public function project(){
    	return $this->belongsTo('App\Project','project_id');					
    } 
    public function projectphase(){
        return $this->belongsTo('App\ProjectPhase','project_phase_id');                 
    }
    public function projectlabour(){
    	return $this->belongsTo('App\ProjectLabour','project_labour_id');					
    }
    public function individualprojectphase(){
        return $this->belongsTo('App\IndividualProjectPhases','individual_project_phase_id');                   
    } 

    public function contractor(){
        return $this->belongsTo('App\User','contractor');                   
    } 

    public function agreements(){
        return $this->hasMany('App\ContractAgreement','vendor_contract_id');                    
    } 

    public function milestones(){
        return $this->hasMany('App\ContractMilestone','vendor_contract_id');                    
    }
    public function dates(){
    	return $this->hasMany('App\ContractDate','vendor_contract_id');					
    }
}
