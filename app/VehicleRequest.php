<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleRequest extends Model
{
    protected $guarded=['id'];

    public function assetmodel()
    {
    	return $this->belongsTo('App\AssetModel','asset_model_id');
       
    }

    public function user()
    {
    	return $this->belongsTo('App\User','created_by');
    }
}
