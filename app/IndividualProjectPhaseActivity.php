<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IndividualProjectPhaseActivity extends Model
{
    protected $guarded = [];
  
    public function project_labour()
    {
    	return $this->belongsTo('App\ProjectLabour','project_labour_id');
       
    }
}
