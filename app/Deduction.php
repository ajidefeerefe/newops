<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deduction extends Model
{
    protected $guarded = ['id'];

    public function deducts()
    {
    	return $this->belongsTo('App\Deduct','deduct_id');
    				
       
    } 
}
