<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PayPeriod;
use App\Month;

class PayPeriodController extends Controller
{
    public function add(Request $r)
    {

    	$r->validate([
            'year' => 'required',
    	    'month' => 'required',
    	    
    	]);

    	
        if (PayPeriod::where('year', '=', $r->year)->where('month_id', '=', $r->month)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
           
           $payperiod = new PayPeriod;
           $payperiod->year = $r->year;
           $payperiod->month_id = $r->month;
           $payperiod->save();


        }
    	return $payperiod->where('id',$payperiod->id)->with('month')->get()->makeHidden(['created_at','updated_at'])->toArray();
    }

    public function show(Request $request)
    {

        $columns = ['id','year'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;

        $query = PayPeriod::select('id', 'year','month_id')->with(['month' => function($query) {
            $query->select('id','name');
        }])->where('active',1)->orderBy($columns[$column], $dir);


        if($searchValue){
            $query->where('year', 'like', '%' . $searchValue . '%')
                    ->orWhere('id', 'like', '%' .$searchValue . '%')
                    ->orWhereHas('month', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->get();
           
        }


        $payperiods = $query->paginate($length);

        return ['data' => $payperiods, 'draw' => $request->draw];
    }

         public function showSaved(Request $request)
            {

            	$columns = ['id','year'];

            	$length = $request->length;
            	$column = $request->column;
            	$dir = $request->dir;
            	$searchValue = $request->search;

            	$query = PayPeriod::select('id', 'year','month_id')->with(['month' => function($query) {
            	    $query->select('id','name');
            	}])->where('active',0)->orderBy($columns[$column], $dir);


            	if($searchValue){
                    $query->where('year', 'like', '%' . $searchValue . '%')
                            ->orWhere('id', 'like', '%' .$searchValue . '%')
                            ->orWhereHas('month', function($q) use ($searchValue){
                                $q->where('name',  'like', '%' . $searchValue . '%');
                            })->get();
                   
            	}


            	$payperiods = $query->paginate($length);

            	return ['data' => $payperiods, 'draw' => $request->draw];
            }



    public function edit(Request $request)
    {
    	
        if (PayPeriod::where('year', '=', $request->year)->where('month_id', '=', $request->month['id'])->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
           
        $payperiod = PayPeriod::find($request->id);
        $payperiod->year = $request->year;
        $payperiod->month_id = $request->month['id'];
        $payperiod->save();


        }
        
        
        $month = Month::find($request->month['id']);

        return $month->makeHidden(['created_at','updated_at'])->toArray();
    }

    public function get(Request $request){
        return PayPeriod::select('id', 'year','month_id')->with('month:id,name')->where('id',$request->id)->first();
    }

    public function save(Request $request){
        $pp = PayPeriod::find($request->id);
        $pp->active = 0;
        $pp->save();
    }
}
