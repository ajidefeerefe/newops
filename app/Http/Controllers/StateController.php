<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\State;

class StateController extends Controller
{
    public function get(){
        return State::all()->makeHidden(['created_at','updated_at'])->toArray();
    }
}
