<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Month;

class MonthController extends Controller
{
    public function show()
    {
    	$months = Month::all();

    	return $months->makeHidden(['created_at','updated_at'])->toArray();
    }
}
