<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tax;
use App\User;

class TaxController extends Controller
{
     public function add(Request $r)
    {
    	$r->validate([
    	    'user' => 'required',
    	    'amount' => 'required',
    	    
    	]);

    	// $earn = Earn::firstOrCreate(['name' => $r->name]);
        if (Tax::where('user_id', '=', $r->user)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
        	$amount = str_replace(",", "",$r->amount);
           $bank = new Tax;
           $bank->user_id = $r->user;
           $bank->amount = $amount;
           $bank->save();

           $user = User::find($r->user);
           $user->paye = 1;
           $user->save();

        }
    	return $bank->where('id',$bank->id)->with('user:id,last_name,first_name,salary')->get()->makeHidden(['created_at','updated_at'])->toArray();
    }

    public function show(Request $request)
    {

    	$columns = ['id','user_id','amount'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

    	$query = Tax::select('id', 'user_id','amount')->with('user:id,last_name,first_name,salary')->orderBy($columns[$column], $dir);


    	if($searchValue){
    	    $query->where(function($query) use ($searchValue) {
    	        $query->where('amount', 'like', '%' . $searchValue . '%')
    	        ->orWhere('id', 'like', '%' .$searchValue . '%')
    	        ->orWhereHas('user', function($q) use ($searchValue){
    	            	$q->where('first_name',  'like', '%' . $searchValue . '%')
    	            	->orWhere('last_name',  'like', '%' . $searchValue . '%')
    	            	->orWhere('salary',  'like', '%' . $searchValue . '%');
    	        });
    	    });
    	}


    	$banks = $query->paginate($length);

    	return ['data' => $banks, 'draw' => $request->draw];
    }

    public function edit(Request $request)
    {
        
        $tax = Tax::find($request->id);
        $tax->amount = $request->amount;
        $tax->save();
        return $tax;
        
    }
}
