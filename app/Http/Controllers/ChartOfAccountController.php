<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ChartOfAccount;

class ChartOfAccountController extends Controller
{
     public function add(Request $r)
    {
    	$r->validate([
    	    'name' => 'required',
    	    
    	]);

        if (ChartOfAccount::where('name', '=', $r->name)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
           $chart_of_account = ChartOfAccount::create($r->only(['name']));
        }
    	return $chart_of_account->makeHidden(['created_at','updated_at'])->toArray();
    }

    public function show(Request $request)
    {

    	$columns = ['id','name'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

    	$query = ChartOfAccount::select('id', 'name')->orderBy($columns[$column], $dir);


    	if($searchValue){
    	    $query->where(function($query) use ($searchValue) {
    	        $query->where('name', 'like', '%' . $searchValue . '%')
    	        ->orWhere('id', 'like', '%' .$searchValue . '%');
    	    });
    	}


    	$chart_of_accounts = $query->paginate($length);

    	return ['data' => $chart_of_accounts, 'draw' => $request->draw];
    }



    public function edit(Request $request)
    {
        if (ChartOfAccount::where('name', '=', $request->name)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
            $chart_of_account = ChartOfAccount::find($request->id);
            $chart_of_account->name = $request->name;
            $chart_of_account->save();
        }
        
        return $chart_of_account;
    }

    public function get()
    {
        return ChartOfAccount::all()->makeHidden(['created_at','updated_at'])->toArray();
    }
}
