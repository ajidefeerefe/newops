<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProjectLabour;
use App\IndividualProjectPhases;
use App\CostingWeek;

class ProjectLabourController extends Controller
{
	public function update(Request $r){
		// dd($r->all());
		if($r->labours == []){//Check if the data is null
          return response(['error'=>"Nothing to update"], 404);
        }

		foreach ($r->labours as $key => $value) 
        {

         
          if($value['material_and_services_id']=='' or $value['quantity']=='' or $value['amount']==''){//Check if any phase field was left blank
          return response(['error'=>"No field can be empty"], 404);
          }
        }
	    ProjectLabour::where('individual_project_phases_id', '=', $value['individual_project_phases_id'])->delete();//Clear all the record before reinserting
		$individual_project_phases_id = $r->labours[0]['individual_project_phases_id'];
	    foreach ($r->labours as $key => $value)
	    {

	       	$p_labour = new ProjectLabour;
        	$amount = str_replace(',', '', $value['amount']);
          	$p_labour->id = $value['id'];
            $p_labour->individual_project_phases_id = $value['individual_project_phases_id'];
	       	$p_labour->material_and_services_id = $value['material_and_services_id'];
	       	$p_labour->quantity = $value['quantity'];
	       	$p_labour->amount = $amount;
	       	$p_labour->save();
	            
	    }
	    // return "Done";
        IndividualProjectPhases::where('id', $individual_project_phases_id)->update(['status' => 5]); //Set the project as needing review 

	    return ProjectLabour::with('material_and_service:id,name')->where('individual_project_phases_id', '=', $value['individual_project_phases_id'])->get()->makeHidden(['created_at','updated_at'])->toArray();
	}

	public function add(Request $r){
		// dd($r);

		foreach ($r->labours as $key => $value) 
        {
          if($value['material_and_services_id']=='' or $value['quantity']=='' or $value['amount']==''){//Check if any phase field was left blank
          return response(['error'=>"No field can be empty"], 404);
          }
          if (ProjectLabour::where('individual_project_phases_id', '=', $value['individual_project_phases_id'])->where('material_and_services_id',$value['material_and_services_id'])->exists()) {
              return response(['error'=>'Duplicates Not Permitted'], 404);
          }
        }

		$individual_project_phases_id = $r->labours[0]['individual_project_phases_id'];
        foreach ($r->labours as $key => $value) {

        	$amount = str_replace(',', '', $value['amount']);
        	$p_labour = new ProjectLabour;
	       	$p_labour->material_and_services_id = $value['material_and_services_id'];
          	$p_labour->individual_project_phases_id = $value['individual_project_phases_id'];
	       	$p_labour->quantity = $value['quantity'];
	       	$p_labour->amount = $amount;
	       	$p_labour->save();   

	       }
        IndividualProjectPhases::where('id', $individual_project_phases_id)->update(['status' => 5]); //Set the project as needing review 

	    return ProjectLabour::with('material_and_service:id,name')->where('individual_project_phases_id', '=', $value['individual_project_phases_id'])->get()->makeHidden(['created_at','updated_at'])->toArray();


	}

	public function delete(Request $r){
        // dd($r->all());
		// $individual_project_phases_id = $r->labours[0]['individual_project_phases_id'];

        ProjectLabour::where('id',$r->id)->delete();
        IndividualProjectPhases::where('id', $r->individual_project_phases_id)->update(['status' => 5]); //Set the project as needing review 

	    return ProjectLabour::with('material_and_service:id,name')->where('individual_project_phases_id',$r->individual_project_phases_id)->get()->makeHidden(['created_at','updated_at'])->toArray();

    }


    public function updateForOperationalProjects(Request $r){

      // dd($r->all());

      
		if($r->labours == [])
		{//Check if the data is null
	      return response(['error'=>"Nothing to update"], 404);
	    }

      	foreach ($r->labours as $key => $value) 
        {
          if($value['material_and_services_id'] =='' or $value['costing_day_id'] =='' or $value['quantity']=='' or $value['amount']==''){//Check if any phase field was left blank
          return response(['error'=>"No field can be empty"], 404);
          }
        }
       
      	$costing_week_id = $r->costing_week_id;
      	$individual_project_phases_id = $r->labours[0]['individual_project_phases_id'];
      
      	ProjectLabour::where('individual_project_phases_id', '=', $individual_project_phases_id)->delete();//Clear all the record before reinserting

      	foreach ($r->labours as $key => $value) {
          $p_labour = new ProjectLabour;
          $amount = str_replace(',', '', $value['amount']);//Remove the commas before inserting ;)
          $p_labour->id = $value['id'];
          $p_labour->costing_day_id = $value['costing_day_id'];
          $p_labour->individual_project_phases_id = $value['individual_project_phases_id'];
          $p_labour->material_and_services_id = $value['material_and_services_id'];
          $p_labour->quantity = $value['quantity'];
          $p_labour->amount = $amount;
          $p_labour->save(); 
          };

      	CostingWeek::where('id', $costing_week_id)->update(['status' => 5]); //Set the project as needing review 

      return CostingWeek::with('costing_day.project_material','costing_day.project_labour')->where('id',$costing_week_id)->get()->makeHidden(['created_at','updated_at'])->toArray();
      
      // return ProjectMaterial::with('projectphase:id,name')->get()->makeHidden(['created_at','updated_at'])->toArray();
  }


    public function addForOperationalProjects(Request $r){
    // dd($r);

    foreach ($r->labours as $key => $value) 
    {
      if($value['costing_day_id']=='' or $value['material_and_services_id']=='' or $value['quantity']=='' or $value['amount']==''){//Check if any phase field was left blank
      return response(['error'=>"No field can be empty"], 404);
      }
      if (ProjectLabour::where('costing_day_id', '=', $value['costing_day_id'])->where('material_and_services_id',$value['material_and_services_id'])->exists()) {
          return response(['error'=>'Duplicates Not Permitted'], 404);
      }
    }

    $costing_week_id = $r->labours[0]['costing_week_id'];
    foreach ($r->labours as $key => $value) {

      $amount = str_replace(',', '', $value['amount']);//Remove the commas before inserting
      $p_labour = new ProjectLabour;
      $p_labour->costing_day_id = $value['costing_day_id'];
      $p_labour->individual_project_phases_id = $value['individual_project_phases_id'];
      $p_labour->material_and_services_id = $value['material_and_services_id'];
      $p_labour->quantity = $value['quantity'];
      $p_labour->amount = $amount;
      $p_labour->save();        

     }

        CostingWeek::where('id', $costing_week_id)->update(['status' => 5]); //Set the project as needing review 

       return CostingWeek::with('individualprojectphase.phase_labour','costing_day.project_material','costing_day.project_labour')->where('id',$costing_week_id)->get()->makeHidden(['created_at','updated_at'])->toArray();

    }

    public function deleteForOperationalProjects(Request $r){
        // dd($r->all());

      ProjectLabour::where('id',$r->id)->delete();

      CostingWeek::where('id', $r->costing_day_id)->update(['status' => 5]); //Set the project as needing review 

      return 'Okay';

    }

    public function getProjectLabour(Request $r){
      // return $r->all();
      if($r->project_type == 1){
         return ProjectLabour::with('material_and_service:id,name')->where('individual_project_phases_id',$r->id)->get()->makeHidden(['created_at','updated_at'])->toArray();
      }else{
         return ProjectLabour::with('material_and_service:id,name')->where('individual_project_phases_id',$r->id)->where('costing_day_id',$r->day_id)->get()->makeHidden(['created_at','updated_at'])->toArray();
      }
     
    } 

    public function getAllProjectLabour(Request $r){
         // return $r->all();
     
         return ProjectLabour::with('material_and_service:id,name')->where('individual_project_phases_id',$r->id)->get()->makeHidden(['created_at','updated_at'])->toArray();
      
    }

    public function updateActivity(Request $r){
      $pl = ProjectLabour::find($r->id);
      $pl->start_date = $r->start_date;
      $pl->end_date = $r->end_date;
      $pl->progress = $r->progress;
      $pl->save();
    }
    	
}
