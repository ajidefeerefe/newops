<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CostingDay;

class CostingDayController extends Controller
{
   public function getDays(Request $r){
   	return CostingDay::where('costing_week_id', $r->id)->get();

   }

   public function getDay(Request $r){
   	return CostingDay::find($r->id);

   }
}
