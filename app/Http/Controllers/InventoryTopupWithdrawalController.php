<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InventoryTopupWithdrawal;
use App\ExpenseRecord;
use App\Invoice;
use App\InvoiceDetail;
use Carbon\Carbon;

class InventoryTopupWithdrawalController extends Controller
{
     public function add(Request $r)
    {
      
       if($r->initial_quantity < (int)(str_replace(",", "",$r->quantity))) {
            return response(['error'=>'Requested quantity not available'], 404);
        }else{
            
           $withdraw = new InventoryTopupWithdrawal;
           $withdraw->inventory_topup_id = $r->topup_id;
           $withdraw->quantity = str_replace(",", "",$r->quantity);
           $withdraw->save();


           $expense = new ExpenseRecord;
           $expense->project_id = $r->project;
           $expense->vendor_id = $r->vendor_id;
           $expense->chart_of_account_id = $r->chart_of_account;
           $expense->material_and_service_id = $r->materrial_and_service_id;
           $expense->bank_account_id = null;
           $expense->individual_project_phases_id = $r->project_phase;
           // $expense->purchase_requisition_id =  null;
           $expense->amount = (int)(str_replace(",", "",$r->quantity)) * (int)(str_replace(",", "",$r->unit_price));
           $expense->date = Carbon::now();
           $expense->vendor_guarantor = null;
           $expense->created_by = \Auth::user()->id;
           $expense->notes = $r->notes;
           $expense->approved = 1;
           $expense->pay_period_id = null;
           $expense->save();

            return $withdraw->where('id',$withdraw->id)->get()->makeHidden(['created_at','updated_at'])->toArray();
    }
        
     
    }  


    public function addInvoice(Request $r)
    {
      
       if($r->initial_quantity < (int)(str_replace(",", "",$r->quantity))){
            return response(['error'=>'Requested quantity not available'], 404);
        }else{

          $invoice = new Invoice;
           $invoice->client_id = $r->client;
           $invoice->project_id = 7;
           $invoice->created_by = \Auth::user()->id;
           $invoice->date = Carbon::now();
           $invoice->due_date = $r->due_date;
           $invoice->save();

           $invoice_detail = new InvoiceDetail;
            $invoice_detail->invoice_id = $invoice->id;
            $invoice_detail->material_and_service_id = $r->materrial_and_service_id;
            $invoice_detail->quantity = str_replace(",", "",$r->quantity);
            $invoice_detail->price = str_replace(",", "",$r->unit_price);
            $invoice_detail->save();

            
           $withdraw = new InventoryTopupWithdrawal;
           $withdraw->inventory_topup_id = $r->topup_id;
           $withdraw->quantity = str_replace(",", "",$r->quantity);
           $withdraw->save();

          return $withdraw->where('id',$withdraw->id)->get()->makeHidden(['created_at','updated_at'])->toArray();
    }
        
    }  

    public function addToInvoice(Request $r)
    {
    	
    	 if($r->initial_quantity < (int)(str_replace(",", "",$r->quantity))){
            return response(['error'=>'Requested quantity not available'], 404);
        }else{

          // $invoice = new Invoice;
          //  $invoice->client_id = $r->client;
          //  $invoice->project_id = 7;
          //  $invoice->created_by = \Auth::user()->id;
          //  $invoice->date = Carbon::now();
          //  $invoice->due_date = $r->due_date;
          //  $invoice->save();

           $invoice_detail = new InvoiceDetail;
            $invoice_detail->invoice_id = $r->invoice;
            $invoice_detail->material_and_service_id = $r->materrial_and_service_id;
            $invoice_detail->quantity = str_replace(",", "",$r->quantity);
            $invoice_detail->price = str_replace(",", "",$r->unit_price);
            $invoice_detail->save();

            
           $withdraw = new InventoryTopupWithdrawal;
           $withdraw->inventory_topup_id = $r->topup_id;
           $withdraw->quantity = str_replace(",", "",$r->quantity);
           $withdraw->save();

          return $withdraw->where('id',$withdraw->id)->get()->makeHidden(['created_at','updated_at'])->toArray();
		}
        
    }

    public function addNewInvoiceItem(Request $r)
    {
      
          $invoice_detail = new InvoiceDetail;
          $invoice_detail->invoice_id = $r->invoice_id;
          $invoice_detail->material_and_service_id = $r->material_or_service;
          $invoice_detail->quantity = str_replace(",", "",$r->quantity);
          $invoice_detail->price = str_replace(",", "",$r->unit_price);
          $invoice_detail->save();

          return $invoice_detail->where('id',$invoice_detail->id)->get()->makeHidden(['created_at','updated_at'])->toArray();
    
        
    }
}
