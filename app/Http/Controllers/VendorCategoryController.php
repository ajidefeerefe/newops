<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VendorCategory;

class VendorCategoryController extends Controller
{
     public function add(Request $r)
    {
    	$r->validate([
    	    'name' => 'required',
    	    
    	]);

    	// $earn = Earn::firstOrCreate(['name' => $r->name]);
        if (VendorCategory::where('name', '=', $r->name)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
           $VendorCategory = VendorCategory::create($r->only(['name']));
        }
    	return $VendorCategory->makeHidden(['created_at','updated_at'])->toArray();
    }

    public function show(Request $request)
    {

    	$columns = ['id','name'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

    	$query = VendorCategory::select('id', 'name')->orderBy($columns[$column], $dir);


    	if($searchValue){
    	    $query->where(function($query) use ($searchValue) {
    	        $query->where('name', 'like', '%' . $searchValue . '%')
    	        ->orWhere('id', 'like', '%' .$searchValue . '%');
    	    });
    	}


    	$VendorCategory = $query->paginate($length);

    	return ['data' => $VendorCategory, 'draw' => $request->draw];
    }

    public function edit(Request $request)
    {
        if (VendorCategory::where('name', '=', $request->name)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
        $VendorCategory = VendorCategory::find($request->id);
        $VendorCategory->name = $request->name;
        $VendorCategory->save();
        return $VendorCategory;
        }
    }

    public function get(Request $request){
        return VendorCategory::all()->makeHidden(['created_at','updated_at'])->toArray();
    }
}
