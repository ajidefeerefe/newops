<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;

class DepartmentController extends Controller
{
     public function add(Request $r)
    {
    	$r->validate([
            'name' => 'required',
    	    'active' => 'required',
    	    
    	]);

    	
        if (Department::where('name', '=', $r->name)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
           
           $department = new Department;
           $department->name = $r->name;
           $department->active = $r->active;
           $department->save();


        }
    	return $department->makeHidden(['created_at','updated_at'])->toArray();
    }

    public function show(Request $request)
    {

    	$columns = ['id','name','active'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

    	$query = Department::select('id', 'name','active')->orderBy($columns[$column], $dir);


    	if($searchValue){
    	    $query->where(function($query) use ($searchValue) {
    	        $query->where('name', 'like', '%' . $searchValue . '%')
    	        ->orWhere('id', 'like', '%' .$searchValue . '%');
    	    });
    	}


    	$departments = $query->paginate($length);

    	return ['data' => $departments, 'draw' => $request->draw];
    }



    public function edit(Request $request)
    {
        if (Department::where('name', '=', $request->name)->where('active', '=', $request->active)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
           
           $department = Department::find($request->id);
            $department->name = $request->name;
            $department->active = $request->active;
            $department->save();


        }
        
        return $department;
    }
     public function get(){
        return Department::all()->makeHidden(['created_at','updated_at'])->toArray();
    }
}
