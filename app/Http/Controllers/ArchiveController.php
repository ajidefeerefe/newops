<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Archive;
use App\ExpenseRecord;
use Carbon\Carbon;

class ArchiveController extends Controller
{
     public function add(Request $r)
    {

    	// return $r->all();
    	// $r->validate([
    	//     'name' => 'required',
    	    
    	// ]);

    	// $earn = Earn::firstOrCreate(['name' => $r->name]);
        if (Archive::where('payroll_id', $r->payperiod)->where('user_id',$r->user_id)->exists()) {
            return response(['error'=>'Payment already made'], 404);
        } else {
           $ar = new Archive();
           $ar->payroll_id = $r->payperiod;
           $ar->user_id = $r->user_id;
           $ar->total = str_replace(",", "",$r->net_pay);
           $ar->save();

           $ex = new ExpenseRecord();
           $ex->project_id = $r->project;
           $ex->individual_project_phases_id = $r->project_phase;
           $ex->chart_of_account_id = $r->chart_of_account;
           $ex->bank_account_id = $r->bank_account;
           $ex->amount = str_replace(",", "",$r->net_pay);
           $ex->date = date('Y-m-d');
           $ex->created_by = \Auth::user()->id;
           $ex->notes = 'salary';
           $ex->approved = 1;
           $ex->pay_period_id = $r->payperiod;
           $ex->save();


        }



    	return 'success';
    }
}
