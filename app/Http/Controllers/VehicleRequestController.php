<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VehicleRequest;
use App\AssetModel;

class VehicleRequestController extends Controller
{
     public function add(Request $r)
    {
    	
    	$r->validate([
            'departure_date' => 'required',
    	    'return_date' => 'required',
    	    'destination_address' => 'required',
    	    'destination_city' => 'required',
    	    'notes' => 'required',

    	    
    	]);

    	
        
            
           $v_request = new VehicleRequest;
           $v_request->departure_date = $r->departure_date;
           $v_request->return_date = $r->return_date;
           $v_request->destination_address = $r->destination_address;
           $v_request->destination_city = $r->destination_city;
           $v_request->notes = $r->notes;
           $v_request->created_by = \Auth::user()->id;;
           $v_request->save();

        
    	return $v_request->where('id',$v_request->id)->with('assetmodel:id,name','user:id,first_name,last_name')->get()->makeHidden(['user_id','created_at','updated_at'])->toArray();
    }

    public function show(Request $request)
    {

    	$columns = ['id','departure_date','return_date','destination_address','destination_city','request_status','return_status','created_by'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

  

    	$query = VehicleRequest::select('id', 'departure_date','return_date','destination_address','destination_city','request_status','return_status','created_by','notes','asset_model_id')->with('assetmodel:id,name','user:id,first_name,last_name')->orderBy($columns[$column], $dir);




    	if($searchValue){

    	    $query->where('id', 'like', '%' . $searchValue . '%')
    	    		->orWhere('departure_date', 'like', '%' .$searchValue . '%')
    	    		->orWhere('return_date', 'like', '%' .$searchValue . '%')
    	    		->orWhere('destination_address', 'like', '%' .$searchValue . '%')
    	    		->orWhere('destination_city', 'like', '%' .$searchValue . '%')
    	    		->orWhereHas('assetmodel', function($q) use ($searchValue){
    	            	$q->where('name',  'like', '%' . $searchValue . '%');
    	        	})
    	        	->orWhereHas('user', function($q) use ($searchValue){
    	            	$q->where('first_name',  'like', '%' . $searchValue . '%');
    	            	$q->orWhere('last_name',  'like', '%' . $searchValue . '%');
    	        	})->get();

    	    
    	}



    	$vehicle_requests = $query->paginate($length);

    	return ['data' => $vehicle_requests, 'draw' => $request->draw];
    }



    public function edit(Request $request)
    {
    	
      
        $v_request = VehicleRequest::find($request->id);
        $v_request->departure_date = $request->departure_date;
       $v_request->return_date = $request->return_date;
       $v_request->destination_address = $request->destination_address;
       $v_request->destination_city = $request->destination_city;
       $v_request->notes = $request->notes;
        $v_request->save();

        
        return $v_request;
       
        
    } 


    public function approve(Request $request)
    {
    	if($request->action != 1){
    		$v_request = VehicleRequest::find($request->id);
	        $v_request->request_status = $request->action;
	       $v_request->return_status = 0;
	       $v_request->asset_model_id = null;
	       $v_request->approve_note = $request->notes;
	        $v_request->save();
    	}else{
    		$v_request = VehicleRequest::find($request->id);
        $v_request->request_status = $request->action;
       $v_request->return_status = $request->action;
       $v_request->asset_model_id = $request->vehicle;
       $v_request->approve_note = $request->notes;
        $v_request->save();


    	}
    	
      
        
        
        return $v_request->where('id',$v_request->id)->with('assetmodel:id,name','user:id,first_name,last_name')->get()->makeHidden(['user_id','created_at','updated_at'])->toArray();;
       
        
    }


    public function return(Request $request)
    {
        
      
        $v_request = VehicleRequest::find($request->id);
        $v_request->return_status = 2;
        $v_request->save();

        
        return $v_request;
       
        
    } 

    public function search(Request $request)
    {
        
        $query = AssetModel::select('id', 'name')->where('name', 'like', '%' .$request->data . '%')->get();

        
        return $query;
       
        
    }

    public function getAssignedVehicles(Request $request)
    {
    	
       $columns = ['id','departure_date','return_date','created_by'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;

  

        $query = VehicleRequest::select('id', 'departure_date','return_date','destination_address','created_by','notes','asset_model_id')->with('user:id,first_name,last_name')->orderBy($columns[$column], $dir)->where('asset_model_id',$request->id)->where('departure_date','>=',$request->departure_date)->where('request_status',1)->where('return_status',1);




        if($searchValue){

            $query->where('id', 'like', '%' . $searchValue . '%')
                    ->orWhere('departure_date', 'like', '%' .$searchValue . '%')
                    ->orWhere('return_date', 'like', '%' .$searchValue . '%')
                    ->orWhere('destination_address', 'like', '%' .$searchValue . '%')
                    ->orWhereHas('user', function($q) use ($searchValue){
                        $q->where('first_name',  'like', '%' . $searchValue . '%');
                        $q->orWhere('last_name',  'like', '%' . $searchValue . '%');
                    })->get();

            
        }



        $vehicle_requests = $query->paginate($length);

        return ['data' => $vehicle_requests, 'draw' => $request->draw];
       
        
    } 


    public function checkVehicleAvil(Request $r)
    {
        // return $r->all();
        return VehicleRequest::where('departure_date', 'like', '%' .substr($r->date, 0, 10). '%')->where('asset_model_id',$r->model)->with('user:id,last_name,first_name')->get();
    }
}
