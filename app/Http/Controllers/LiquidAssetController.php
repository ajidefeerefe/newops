<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LiquidAsset;

class LiquidAssetController extends Controller
{
     public function add(Request $r)
    {

    		
        if (LiquidAsset::where('asset_model_id', '=', $r->model)->where('asset_manufacturer_id', '=', $r->assetmanufacturer)->where('serial_no', '=', $r->serial_number)->where('asset_category_id', '=', $r->assetcategory)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
           $pa = new LiquidAsset;
           $pa->asset_model_id = $r->model;
           $pa->asset_manufacturer_id = $r->assetmanufacturer;
           $pa->serial_no = $r->serial_number;
           $pa->asset_category_id = $r->assetcategory;
           $pa->color_id = $r->color;
           $pa->save();
           

        }
       
     return $pa->where('id',$pa->id)->with('assetmodel:id,name','assetmanufacturer:id,name','assetcategory:id,name','assignedto.user:id,last_name,first_name','assignedto.vendor:id,name','assignedto.client:id,lastname,firstname,company')->get();
    }

    public function show(Request $request)
    {

    	$columns = ['id','asset_manufacturer_id','asset_model_id','asset_category_id','serial_no','assignment_id'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

    	$query = LiquidAsset::select('id','asset_model_id','asset_manufacturer_id','asset_category_id','serial_no','assignment_id','active')->with('assetmodel:id,name','assetmanufacturer:id,name','assetcategory:id,name','assignedto:id,user_type,assigned_to','assignedto.user:id,last_name,first_name','assignedto.vendor:id,name','assignedto.client:id,lastname,firstname,company')->orderBy($columns[$column], $dir);

    	if($searchValue){
    	    $query->where('id', 'like', '%' . $searchValue . '%')
    	    		->orWhereHas('assetmodel', function($q) use ($searchValue){
    	            	$q->where('name',  'like', '%' . $searchValue . '%');
    	        	})->orWhereHas('assetmanufacturer', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->orWhereHas('assetcategory', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->get();
    	}


    	$LiquidAssets = $query->paginate($length);

    	return ['data' => $LiquidAssets, 'draw' => $request->draw];
    }



    public function edit(Request $request)
    {
        
        
            $la = LiquidAsset::find($request->id);
            $la->asset_category_id = $request->asset_category;
            $la->asset_model_id = $request->asset_model;
            $la->asset_manufacturer_id = $request->asset_manufacturer;
            $la->color_id = $request->color_id;
            $la->location = $request->location;
            $la->serial_no = $request->serial_no;
            $la->save();
        

         $data = LiquidAsset::where('id',$la->id)->with('assetmodel:id,name','assetmanufacturer:id,name','assetcategory:id,name','assignedto.user:id,last_name,first_name')->get();
        return $data;
        
       
    } 

    public function edit_second(Request $request)
    {
        
        
            $la = LiquidAsset::find($request->id);
            $la->maintenance = $request->maintenance;
            $la->active = $request->active;
            $la->retired = $request->retired;
            $la->save();
        

         $data = LiquidAsset::where('id',$la->id)->with('assetmodel:id,name','assetmanufacturer:id,name','assetcategory:id,name','assignedto.user:id,last_name,first_name')->get();
        return $data;
        
       
    }

    public function edit_third(Request $request)
    {
    	
        
            $la = LiquidAsset::find($request->id);
            $la->purchase_amount = $request->purchase_amount;
            $la->purchase_date = $request->purchase_date;
            $la->vendor_id = $request->vendor;
            $la->save();
        

         $data = LiquidAsset::where('id',$la->id)->with('assetmodel:id,name','assetmanufacturer:id,name','assetcategory:id,name','assignedto.user:id,last_name,first_name')->get();
        return $data;
        
       
    }


    public function editactive(Request $request){
    	$data = LiquidAsset::find($request->id);
    	$data->active = 0;
    	$data->save();

    	return $data;
    }

    public function get(Request $request)
    {

        $data = LiquidAsset::where('id',$request->id)->with('assetmodel:id,name','assetmanufacturer:id,name','assetcategory:id,name','assignedto.user:id,last_name,first_name')->get();
        return $data;
    }

    
}
