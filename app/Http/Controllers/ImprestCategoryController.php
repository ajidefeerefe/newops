<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ImprestCategory;

class ImprestCategoryController extends Controller
{
     public function add(Request $r)
    {
    	$r->validate([
    	    'name' => 'required',
    	    'description' => 'required',
    	]);

    	// $earn = Earn::firstOrCreate(['name' => $r->name]);
        if (ImprestCategory::where('name', '=', $r->name)->exists()) {
            return response(['error'=>'This Imprest Category already exists'], 404);
        } else {
           $imprestcategory = ImprestCategory::create($r->only(['name','description']));
        }
    	return $imprestcategory->makeHidden(['created_at','updated_at'])->toArray();
    }

    public function show(Request $request)
    {

    	$columns = ['id','name','description'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

    	$query = ImprestCategory::select('id', 'name','description')->orderBy($columns[$column], $dir);


    	if($searchValue){
    	    $query->where(function($query) use ($searchValue) {
    	        $query->where('name', 'like', '%' . $searchValue . '%')
    	        ->orWhere('id', 'like', '%' .$searchValue . '%')
    	        ->orWhere('description', 'like', '%' .$searchValue . '%');
    	    });
    	}


    	$colors = $query->paginate($length);

    	return ['data' => $colors, 'draw' => $request->draw];
    }



    public function edit(Request $request)
    {
        if (ImprestCategory::where('name', '=', $request->name)->exists()) {
            return response(['error'=>'Imprest Category already exist'], 404);
        } else {
           $color = ImprestCategory::find($request->id);
            $color->name = $request->name;
            $color->description = $request->description;
            $color->save();
        }
        
        return $color;
    }
}
