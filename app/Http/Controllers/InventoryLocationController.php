<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InventoryLocation;

class InventoryLocationController extends Controller
{
    public function add(Request $r)
    {
    	$r->validate([
    	    'name' => 'required',
    	]);

    	// $earn = Earn::firstOrCreate(['name' => $r->name]);
        if (InventoryLocation::where('name', '=', $r->name)->exists()) {
            return response(['error'=>'Inventory Location already exists'], 404);
        } else {
           $inventorylocation = InventoryLocation::create($r->only(['name']));
        }
    	return $inventorylocation->makeHidden(['created_at','updated_at'])->toArray();
    }

    public function show(Request $request)
    {

    	$columns = ['id','name'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

    	$query = InventoryLocation::select('id', 'name')->orderBy($columns[$column], $dir);


    	if($searchValue){
    	    $query->where(function($query) use ($searchValue) {
    	        $query->where('name', 'like', '%' . $searchValue . '%')
    	        ->orWhere('id', 'like', '%' .$searchValue . '%');
    	    });
    	}


    	$colors = $query->paginate($length);

    	return ['data' => $colors, 'draw' => $request->draw];
    }



    public function edit(Request $request)
    {
        if (InventoryLocation::where('name', '=', $request->name)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
           $color = InventoryLocation::find($request->id);
            $color->name = $request->name;
            $color->save();
        }
        
        return $color;
    }

    public function get()
    {
        $locations = InventoryLocation::all()->makeHidden(['created_at','updated_at'])->toArray();
        return $locations;
    }
}
