<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProjectPhase;

class ProjectPhaseController extends Controller
{
    public function add(Request $r)
    {
  
    	$r->validate([
    	    'name' => 'required',
    	    'description' => 'required',
    	    
    	]);

    	// $earn = Earn::firstOrCreate(['name' => $r->name]);
        if (ProjectPhase::where('name', '=', $r->name)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
           $ProjectPhase = ProjectPhase::create($r->only(['name','description']));
        }
    	return $ProjectPhase->makeHidden(['created_at','updated_at'])->toArray();
    }

    public function show(Request $request)
    {

    	$columns = ['id','name','description'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

    	$query = ProjectPhase::select('id', 'name','description')->orderBy($columns[$column], $dir);


    	if($searchValue){
    	    $query->where(function($query) use ($searchValue) {
    	        $query->where('name', 'like', '%' . $searchValue . '%')
    	        ->orWhere('id', 'like', '%' .$searchValue . '%')
    	        ->orWhere('description', 'like', '%' .$searchValue . '%');
    	    });
    	}


    	$colors = $query->paginate($length);

    	return ['data' => $colors, 'draw' => $request->draw];
    }



    public function edit(Request $request)
    {
        if (ProjectPhase::where('name', '=', $request->name)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
           $color = ProjectPhase::find($request->id);
            $color->name = $request->name;
            $color->description = $request->description;
            $color->save();
        }
        
        return $color;
    }


    public function get(){
        return ProjectPhase::all()->makeHidden(['created_at','updated_at'])->toArray();
    }

    public function search(Request $request){
        $query = ProjectPhase::select('id', 'name')->where('name', 'like', '%' .$request->data . '%')->get();
        return $query;
    }
}
