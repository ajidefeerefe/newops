<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Color;

class ColorController extends Controller
{
    public function add(Request $r)
    {
    	$r->validate([
    	    'name' => 'required',
    	    
    	]);

    	// $earn = Earn::firstOrCreate(['name' => $r->name]);
        if (Color::where('name', '=', $r->name)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
           $color = Color::create($r->only(['name']));
        }
    	return $color->makeHidden(['created_at','updated_at'])->toArray();
    }

    public function show(Request $request)
    {

    	$columns = ['id','name'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

    	$query = Color::select('id', 'name')->orderBy($columns[$column], $dir);


    	if($searchValue){
    	    $query->where(function($query) use ($searchValue) {
    	        $query->where('name', 'like', '%' . $searchValue . '%')
    	        ->orWhere('id', 'like', '%' .$searchValue . '%');
    	    });
    	}


    	$colors = $query->paginate($length);

    	return ['data' => $colors, 'draw' => $request->draw];
    }



    public function edit(Request $request)
    {
        if (Color::where('name', '=', $request->name)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
           $color = Color::find($request->id);
            $color->name = $request->name;
            $color->save();
        }
        
        return $color;
    }

    public function get()
    {
        return Color::all()->makeHidden(['created_at','updated_at'])->toArray();
    }
}
