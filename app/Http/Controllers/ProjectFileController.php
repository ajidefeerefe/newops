<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\ProjectFile;
use Illuminate\Support\Facades\Storage;

class ProjectFileController extends Controller
{
    public function add(Request $r){
    	// dd($r->all());
        if(!count($r->images) || !$r->project_phase_id){
        	return response(['error'=>"No field can be empty"], 404);
        }else{
    		foreach ($r->images as $image) {
    			$underscored_name = str_replace(' ', '_', $image->getClientOriginalName());
    			$image_new_name = time().$underscored_name;
    			$image->storeAs('public/project_uploads',$image_new_name);//Save the image inside the project_uploads folder in the storage folder
    									//This has to be pre-defined in the config/filesysystems.php file
        		// $image->move('assets/uploads/', $image_new_name);
    			$path = Storage::url($image_new_name);
          $path = str_replace('/storage/', '/storage/project_uploads/', $path);
    			$file = new ProjectFile;
               	$file->project_id = $r->project_id;
               	$file->project_phase_id = $r->project_phase_id;
               	$file->path = $path;
               	$file->name = $image_new_name;
               	
               	$file->save();
    		}
    		return ProjectFile::with('projectphase:id,name')->where('project_files.id',$file->id)->get()->makeHidden(['created_at','updated_at'])->toArray();
        // 

    	}

    	
    }
}
