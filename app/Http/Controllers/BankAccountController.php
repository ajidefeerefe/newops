<?php
/*
deposits come from the client payment and bank account tupups tables
expenses come from the expense records table 
*/
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BankAccount;
use App\ExpenseRecord;

class BankAccountController extends Controller
{
    public function getAll(){
    	return BankAccount::with('bank:id,name')->get()->makeHidden(['created_at','updated_at'])->toArray();
    }

    public function add(Request $r){
    	// dd($r->all());
    	if(BankAccount::where('account_number', '=', $r->number)->exists()){
    		return response(['error'=>'Bank Account Already Registered'], 404);
    	}
    	$b_a = new BankAccount;
    	$b_a->bank_id = $r->bank;
    	$b_a->account_number = $r->number;
    	$b_a->account_name = $r->name;
    	$b_a->branch = $r->branch;
    	$b_a->save();
    	return $b_a::with('bank:id,name','client_payments_amount','bank_account_topups_amount','expense_records_amount')->where('id', $b_a->id)->get()->makeHidden(['created_at','updated_at'])->toArray();;
    }

    public function show(Request $request){
    	$columns = ['id','account_name','account_number','expenses','deposits','balance','bank','branch'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

    	$query = BankAccount::with('bank:id,name','client_payments_amount','bank_account_topups_amount','expense_records_amount')->orderBy($columns[$column], $dir);


    	if($searchValue){
    	    $query->where(function($query) use ($searchValue) {
    	        $query->where('account_name', 'like', '%' . $searchValue . '%')
    	        ->orWhere('id', 'like', '%' .$searchValue . '%')
    	        ->orWhere('account_number', 'like', '%' .$searchValue . '%')
    	        ->orWhere('branch', 'like', '%' .$searchValue . '%');
    	    })->orWhereHas('bank', function($q) use ($searchValue){
                  $q->where('name',  'like', '%' . $searchValue . '%');
             })->get();
    	}


    	$banks = $query->paginate($length);

    	return ['data' => $banks, 'draw' => $request->draw];
    }

    public function edit(Request $request)
    {
        if (BankAccount::where('account_name', '=', $request->account_name)->where('account_number', '=', $request->account_number)->where('branch', '=', $request->branch)->exists()) {
            return response(['error'=>'No Changes Made.'], 404);
        } else {
        $bank_account = BankAccount::find($request->id);
        $bank_account->account_name = $request->account_name;
        $bank_account->account_number = $request->account_number;
        $bank_account->branch= $request->branch;
        $bank_account->save();
        return $bank_account;
        }
    }
    public function viewExpenses(Request $request){
        // dd($request->all());
        $columns = ['id','amount','date','x','y','z','notes'];//What matters are the columsn that have sortable as true

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;

        // William
        $query = ExpenseRecord::with('user:id,first_name,last_name','material_and_service.type','bank_account:id,account_number')->where('bank_account_id',$request->id)->orderBy($columns[$column], $dir);


        if($searchValue){
            $query->where(function($query) use ($searchValue) {
                $query->where('amount', 'like', '%' . $searchValue . '%')
                ->orWhere('id', 'like', '%' .$searchValue . '%')
                ->orWhere('date', 'like', '%' .$searchValue . '%');
            })->orWhereHas('user', function($q) use ($searchValue){
                  $q->where('first_name',  'like', '%' . $searchValue . '%');
                  $q->orWhere('last_name',  'like', '%' . $searchValue . '%');
             })->orWhereHas('material_and_service', function($q) use ($searchValue){
                  $q->where('name',  'like', '%' . $searchValue . '%');
                  $q->orWhereHas('type', function($q) use ($searchValue){
                      $q->where('name',  'like', '%' . $searchValue . '%');
                 });
             })->get();
        }


        $expenses = $query->paginate($length);

        return ['data' => $expenses, 'draw' => $request->draw];

    }

    //William
    public function withdraw(Request $r){
        // dd($r->all());
        $withdraw = new ExpenseRecord;
        $withdraw->amount = $r->usedAmount;
        $withdraw->bank_account_id = $r->id;
        $withdraw->date = date('Y-m-j');
        $withdraw->created_by = \Auth::user()->id;
        $withdraw->notes = 'Direct Withdrawal';
        $withdraw->save();
        return 'ok';
    }

    

}
