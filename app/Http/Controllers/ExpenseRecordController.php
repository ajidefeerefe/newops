<?php
/*Gets data from 
payroll (Salary)
Vendor contracts
PR
Direct withrawals from bank accounts
*/
namespace App\Http\Controllers;

use App\ExpenseRecord;
use Illuminate\Http\Request;

class ExpenseRecordController extends Controller
{
    public function ProjectExpenses(Request $request)//William
    {
      
       $columns = ['id','date','amount'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;

  
        //Costing week isnt useful atm cos v1 doesnt clearly represent the range
        $query = ExpenseRecord::with('project:id,name,type','vendor:id,name','purchase_requisition_material.vendor:id,name','purchase_requisition_material.project_material.costing_day.costing_week:id,start_date,end_date','material_and_service:id,name','vendor_contract.projectlabour.costing_day:id,date','purchase_requisition_material.project_material.material_and_service:id,name','individual_project_phase.phase_material.costing_day:id,date')->orderBy($columns[$column], $dir)->where('date','>=',date('Y-m-d',strtotime($request->start_date)))->where('date','<=',date('Y-m-d',strtotime($request->end_date)))
                                ->where('project_id', $request->id);
                                    



        if($searchValue){

            $expenses = ExpenseRecord::with('project:id,name,type','vendor:id,name','purchase_requisition_material.vendor:id,name','purchase_requisition_material.project_material.costing_day.costing_week:id,start_date,end_date','material_and_service:id,name','vendor_contract.projectlabour.costing_day:id,date','purchase_requisition_material.project_material.material_and_service:id,name','individual_project_phase.phase_material.costing_day:id,date')->where('date','>=',date('Y-m-d',strtotime($request->start_date)))->where('date','<=',date('Y-m-d',strtotime($request->end_date)))
                ->where('project_id', $request->id)
                ->where(function ($q) use ($searchValue)
                {
                    $q->where('id', 'like', '%' . $searchValue . '%')
                    ->orWhere('date', 'like', '%' .$searchValue . '%')
                    ->orWhere('amount', 'like', '%' .$searchValue . '%')
                    ->orWhereHas('material_and_service', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })
                    ->orWhereHas('project', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->orWhereHas('vendor', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->orWhereHas('purchase_requisition_material', function($q) use ($searchValue){
                        $q->whereHas('project_material', function($q) use ($searchValue){
                            $q->whereHas('material_and_service', function($q) use ($searchValue){
                                $q->where('name',  'like', '%' . $searchValue . '%');
                            });
                        });
                        $q->orWhereHas('vendor', function($q) use ($searchValue){
                            $q->where('name',  'like', '%' . $searchValue . '%');
                        });
                    });
                })
                ->orderBy('id','DESC');
            $response = $expenses->paginate($length);
              
        }else{
            $response = $query->paginate($length);
        }


        return ['data' => $response, 'draw' => $request->draw];
        
    }

    public function PhaseExpenses(Request $request)//William
    {
        // dd(date('Y-m-d',strtotime($request->start_date)));
       $columns = ['id','date','amount'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;

  
        $query = ExpenseRecord::with('project:id,name,type','vendor:id,name','purchase_requisition_material.vendor:id,name','purchase_requisition_material.project_material.costing_day.costing_week:id,start_date,end_date','material_and_service:id,name','vendor_contract.projectlabour.costing_day:id,date','purchase_requisition_material.project_material.material_and_service:id,name','individual_project_phase.projectphase:id,name','individual_project_phase.phase_material.costing_day:id,date')->orderBy($columns[$column], $dir)->where('date','>=',date('Y-m-d',strtotime($request->start_date)))->where('date','<=',date('Y-m-d',strtotime($request->end_date)))->whereHas('individual_project_phase', function($q) use ($request){
                            $q->whereHas('projectphase', function($q) use ($request){
                                $q->where('id', $request->id);
                            });
                        });
                                    



        if($searchValue){

            $expenses = ExpenseRecord::with('project:id,name,type','vendor:id,name','purchase_requisition_material.vendor:id,name','purchase_requisition_material.project_material.costing_day.costing_week:id,start_date,end_date','material_and_service:id,name','vendor_contract.projectlabour.costing_day:id,date','purchase_requisition_material.project_material.material_and_service:id,name','individual_project_phase.projectphase:id,name','individual_project_phase.phase_material.costing_day:id,date')->where('date','>=',date('Y-m-d',strtotime($request->start_date)))->where('date','<=',date('Y-m-d',strtotime($request->end_date)))
                ->whereHas('individual_project_phase', function($q) use ($request){
                    $q->whereHas('projectphase', function($q) use ($request){
                        $q->where('id', $request->id);
                    });
                })->where(function ($q) use ($searchValue)
                {
                    $q->where('id', 'like', '%' . $searchValue . '%')
                    ->orWhere('date', 'like', '%' .$searchValue . '%')
                    ->orWhere('amount', 'like', '%' .$searchValue . '%')
                    ->orWhereHas('material_and_service', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })
                    ->orWhereHas('project', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->orWhereHas('vendor', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->orWhereHas('purchase_requisition_material', function($q) use ($searchValue){
                        $q->whereHas('project_material', function($q) use ($searchValue){
                            $q->whereHas('material_and_service', function($q) use ($searchValue){
                                $q->where('name',  'like', '%' . $searchValue . '%');
                            });
                        });
                        $q->orWhereHas('vendor', function($q) use ($searchValue){
                            $q->where('name',  'like', '%' . $searchValue . '%');
                        });
                    })->orWhereHas('individual_project_phase', function($q) use ($searchValue){
                        $q->whereHas('projectphase', function($q) use ($searchValue){
                            $q->where('name', 'like', '%' . $searchValue . '%');
                        });
                    });
                })
                ->orderBy('id','DESC');
            $response = $expenses->paginate($length);
              
        }else{
            $response = $query->paginate($length);
        }


        return ['data' => $response, 'draw' => $request->draw];
        
    } 

    public function IndividualPhaseExpenses(Request $request)//William
    {
        // dd(date('Y-m-d',strtotime($request->start_date)));
       $columns = ['id','date','amount'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;

  
        $query = ExpenseRecord::with('project:id,name,type','vendor:id,name','purchase_requisition_material.vendor:id,name','purchase_requisition_material.project_material.costing_day.costing_week:id,start_date,end_date','material_and_service:id,name','vendor_contract.projectlabour.costing_day:id,date','purchase_requisition_material.project_material.material_and_service:id,name','individual_project_phase.projectphase:id,name','individual_project_phase.phase_material.costing_day:id,date')->orderBy($columns[$column], $dir)->where('date','>=',date('Y-m-d',strtotime($request->start_date)))->where('date','<=',date('Y-m-d',strtotime($request->end_date)))->where('individual_project_phases_id', $request->id);
                                    



        if($searchValue){

            $expenses = ExpenseRecord::with('project:id,name,type','vendor:id,name','purchase_requisition_material.vendor:id,name','purchase_requisition_material.project_material.costing_day.costing_week:id,start_date,end_date','material_and_service:id,name','vendor_contract.projectlabour.costing_day:id,date','purchase_requisition_material.project_material.material_and_service:id,name','individual_project_phase.projectphase:id,name','individual_project_phase.phase_material.costing_day:id,date')->where('date','>=',date('Y-m-d',strtotime($request->start_date)))->where('date','<=',date('Y-m-d',strtotime($request->end_date)))
                ->where('individual_project_phases_id', $request->id)
                ->where(function ($q) use ($searchValue)
                {
                    $q->where('id', 'like', '%' . $searchValue . '%')
                    ->orWhere('date', 'like', '%' .$searchValue . '%')
                    ->orWhere('amount', 'like', '%' .$searchValue . '%')
                    ->orWhereHas('material_and_service', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })
                    ->orWhereHas('project', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->orWhereHas('vendor', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->orWhereHas('purchase_requisition_material', function($q) use ($searchValue){
                        $q->whereHas('project_material', function($q) use ($searchValue){
                            $q->whereHas('material_and_service', function($q) use ($searchValue){
                                $q->where('name',  'like', '%' . $searchValue . '%');
                            });
                        });
                        $q->orWhereHas('vendor', function($q) use ($searchValue){
                            $q->where('name',  'like', '%' . $searchValue . '%');
                        });
                    })->orWhereHas('individual_project_phase', function($q) use ($searchValue){
                        $q->whereHas('projectphase', function($q) use ($searchValue){
                            $q->where('name', 'like', '%' . $searchValue . '%');
                        });
                    });
                })
                ->orderBy('id','DESC');
            $response = $expenses->paginate($length);
              
        }else{
            $response = $query->paginate($length);
        }


        return ['data' => $response, 'draw' => $request->draw];
        
    }

    public function DateRangeExpenses(Request $request)//William
    {
        // dd(date('Y-m-d',strtotime($request->start_date)));
       $columns = ['id','date','amount'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;

  
        $query = ExpenseRecord::with('project:id,name,type','vendor:id,name','purchase_requisition_material.vendor:id,name','purchase_requisition_material.project_material.costing_day.costing_week:id,start_date,end_date','material_and_service:id,name','vendor_contract.projectlabour.costing_day:id,date','purchase_requisition_material.project_material.material_and_service:id,name','individual_project_phase.projectphase:id,name','individual_project_phase.phase_material.costing_day:id,date')->orderBy($columns[$column], $dir)->where('date','>=',date('Y-m-d',strtotime($request->start_date)))->where('date','<=',date('Y-m-d',strtotime($request->end_date)));
                                    



        if($searchValue){

            $expenses = ExpenseRecord::with('project:id,name,type','vendor:id,name','purchase_requisition_material.vendor:id,name','purchase_requisition_material.project_material.costing_day.costing_week:id,start_date,end_date','material_and_service:id,name','vendor_contract.projectlabour.costing_day:id,date','purchase_requisition_material.project_material.material_and_service:id,name','individual_project_phase.projectphase:id,name','individual_project_phase.phase_material.costing_day:id,date')->where('date','>=',date('Y-m-d',strtotime($request->start_date)))->where('date','<=',date('Y-m-d',strtotime($request->end_date)))
                ->where(function ($q) use ($searchValue)
                {
                    $q->where('id', 'like', '%' . $searchValue . '%')
                    ->orWhere('date', 'like', '%' .$searchValue . '%')
                    ->orWhere('amount', 'like', '%' .$searchValue . '%')
                    ->orWhereHas('material_and_service', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })
                    ->orWhereHas('project', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->orWhereHas('vendor', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->orWhereHas('purchase_requisition_material', function($q) use ($searchValue){
                        $q->whereHas('project_material', function($q) use ($searchValue){
                            $q->whereHas('material_and_service', function($q) use ($searchValue){
                                $q->where('name',  'like', '%' . $searchValue . '%');
                            });
                        });
                        $q->orWhereHas('vendor', function($q) use ($searchValue){
                            $q->where('name',  'like', '%' . $searchValue . '%');
                        });
                    })->orWhereHas('individual_project_phase', function($q) use ($searchValue){
                        $q->whereHas('projectphase', function($q) use ($searchValue){
                            $q->where('name', 'like', '%' . $searchValue . '%');
                        });
                    });
                })
                ->orderBy('id','DESC');
            $response = $expenses->paginate($length);
              
        }else{
            $response = $query->paginate($length);
        }


        return ['data' => $response, 'draw' => $request->draw];
        
    }

    public function TypeExpenses(Request $request)//William
    {
        // dd(date('Y-m-d',strtotime($request->start_date)));
       $columns = ['id','date','amount'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;
        $db_column = '';
        if($request->type == 1){
            $db_column = 'purchase_requisition_material_id';
        }
        elseif($request->type == 2){
            $db_column = 'vendor_contract_id';
        }
        elseif($request->type == 3){
            $db_column = 'pay_period_id';
        }else{
            $db_column = 'id';
        }

  
        $query = ExpenseRecord::with('project:id,name,type','vendor:id,name','purchase_requisition_material.vendor:id,name','purchase_requisition_material.project_material.costing_day.costing_week:id,start_date,end_date','material_and_service:id,name','vendor_contract.projectlabour.costing_day:id,date','purchase_requisition_material.project_material.material_and_service:id,name','individual_project_phase.projectphase:id,name','individual_project_phase.phase_material.costing_day:id,date')->orderBy($columns[$column], $dir)->where('date','>=',date('Y-m-d',strtotime($request->start_date)))->where('date','<=',date('Y-m-d',strtotime($request->end_date)))->where($db_column,'!=',0);

        if($searchValue){

            $expenses = ExpenseRecord::with('project:id,name,type','vendor:id,name','purchase_requisition_material.vendor:id,name','purchase_requisition_material.project_material.costing_day.costing_week:id,start_date,end_date','material_and_service:id,name','vendor_contract.projectlabour.costing_day:id,date','purchase_requisition_material.project_material.material_and_service:id,name','individual_project_phase.projectphase:id,name','individual_project_phase.phase_material.costing_day:id,date')->where('date','>=',date('Y-m-d',strtotime($request->start_date)))->where('date','<=',date('Y-m-d',strtotime($request->end_date)))
                ->where($db_column,'!=',0)
                ->where(function ($q) use ($searchValue)
                {
                    $q->where('id', 'like', '%' . $searchValue . '%')
                    ->orWhere('date', 'like', '%' .$searchValue . '%')
                    ->orWhere('amount', 'like', '%' .$searchValue . '%')
                    ->orWhereHas('material_and_service', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })
                    ->orWhereHas('project', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->orWhereHas('vendor', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->orWhereHas('purchase_requisition_material', function($q) use ($searchValue){
                        $q->whereHas('project_material', function($q) use ($searchValue){
                            $q->whereHas('material_and_service', function($q) use ($searchValue){
                                $q->where('name',  'like', '%' . $searchValue . '%');
                            });
                        });
                        $q->orWhereHas('vendor', function($q) use ($searchValue){
                            $q->where('name',  'like', '%' . $searchValue . '%');
                        });
                    })->orWhereHas('individual_project_phase', function($q) use ($searchValue){
                        $q->whereHas('projectphase', function($q) use ($searchValue){
                            $q->where('name', 'like', '%' . $searchValue . '%');
                        });
                    });
                })
                ->orderBy('id','DESC');
            $response = $expenses->paginate($length);
              
        }else{
            $response = $query->paginate($length);
        }


        return ['data' => $response, 'draw' => $request->draw];
        
    }


    public function ChartOfAccount(Request $request)//William
    {
        // return $request->type;
        // dd(date('Y-m-d',strtotime($request->start_date)));
       $columns = ['id','date','amount'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;
        // $db_column = '';
        // if($request->type == 1){
        //     $db_column = 'purchase_requisition_material_id';
        // }
        // elseif($request->type == 2){
        //     $db_column = 'vendor_contract_id';
        // }
        // elseif($request->type == 3){
        //     $db_column = 'pay_period_id';
        // }else{
        //     $db_column = 'id';
        // }

  
        $query = ExpenseRecord::with('project:id,name,type','vendor:id,name','purchase_requisition_material.vendor:id,name','purchase_requisition_material.project_material.costing_day.costing_week:id,start_date,end_date','material_and_service:id,name','vendor_contract.projectlabour.costing_day:id,date','purchase_requisition_material.project_material.material_and_service:id,name','individual_project_phase.projectphase:id,name','individual_project_phase.phase_material.costing_day:id,date')->orderBy($columns[$column], $dir)->where('date','>=',date('Y-m-d',strtotime($request->start_date)))->where('date','<=',date('Y-m-d',strtotime($request->end_date)))->where('chart_of_account_id',$request->type);

        if($searchValue){

            $expenses = ExpenseRecord::with('project:id,name,type','vendor:id,name','purchase_requisition_material.vendor:id,name','purchase_requisition_material.project_material.costing_day.costing_week:id,start_date,end_date','material_and_service:id,name','vendor_contract.projectlabour.costing_day:id,date','purchase_requisition_material.project_material.material_and_service:id,name','individual_project_phase.projectphase:id,name','individual_project_phase.phase_material.costing_day:id,date')->where('date','>=',date('Y-m-d',strtotime($request->start_date)))->where('date','<=',date('Y-m-d',strtotime($request->end_date)))
                ->where('chart_of_account_id',$request->type)
                ->where(function ($q) use ($searchValue)
                {
                    $q->where('id', 'like', '%' . $searchValue . '%')
                    ->orWhere('date', 'like', '%' .$searchValue . '%')
                    ->orWhere('amount', 'like', '%' .$searchValue . '%')
                    ->orWhereHas('material_and_service', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })
                    ->orWhereHas('project', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->orWhereHas('vendor', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->orWhereHas('purchase_requisition_material', function($q) use ($searchValue){
                        $q->whereHas('project_material', function($q) use ($searchValue){
                            $q->whereHas('material_and_service', function($q) use ($searchValue){
                                $q->where('name',  'like', '%' . $searchValue . '%');
                            });
                        });
                        $q->orWhereHas('vendor', function($q) use ($searchValue){
                            $q->where('name',  'like', '%' . $searchValue . '%');
                        });
                    })->orWhereHas('individual_project_phase', function($q) use ($searchValue){
                        $q->whereHas('projectphase', function($q) use ($searchValue){
                            $q->where('name', 'like', '%' . $searchValue . '%');
                        });
                    });
                })
                ->orderBy('id','DESC');
            $response = $expenses->paginate($length);
              
        }else{
            $response = $query->paginate($length);
        }


        return ['data' => $response, 'draw' => $request->draw];
        
    }
}