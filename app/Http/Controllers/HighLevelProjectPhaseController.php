<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HighLevelProjectPhase;
use App\Department;

class HighLevelProjectPhaseController extends Controller
{
    public function add(Request $r)
    {

    	$r->validate([
            'name' => 'required',
    	    'department' => 'required'
    	    
    	]);

    	
        if (HighLevelProjectPhase::where('name', '=', $r->name)->exists()) {
            return response(['error'=>'Project already exist'], 404);
        } else {
           $input =[

           		"name" => $r->name,
           		"department_id" => $r->department

           ];
           $id = HighLevelProjectPhase::create($input)->id;

        }
        $project_phase = new HighLevelProjectPhase;
    	return $project_phase->with('department:id,name')->where('high_level_project_phases.id',$id)->first()->makeHidden(['created_at','updated_at'])->toArray();
    }

    public function show(Request $request)
    {

    	$columns = ['id','name','department_id'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

  

    	$query = HighLevelProjectPhase::select('id', 'name','department_id')->with('department:id,name')->orderBy($columns[$column], $dir);




    	if($searchValue){

    	    $query->where('name', 'like', '%' . $searchValue . '%')
    	    		->orWhere('id', 'like', '%' .$searchValue . '%')
    	    		->orWhereHas('department', function($q) use ($searchValue){//Used for tables having r/ships
    	            	$q->where('name',  'like', '%' . $searchValue . '%');
    	        	})->get();

    	    
    	}



    	$project_phases = $query->paginate($length);

    	return ['data' => $project_phases, 'draw' => $request->draw];
    }



    public function edit(Request $request)
    {
    	
      if (HighLevelProjectPhase::where('name', '=', $request->name)->where('department_id', '=', $request->department['id'])->exists()) {
            return response(['error'=>'Project Phase Already Exist'], 404);
        } else {
        $project_phase = HighLevelProjectPhase::find($request->id);
        $project_phase->name = $request->name;
        $project_phase->department_id = $request->department['id'];
        $project_phase->save();

        }

		// $department = Department::find($request->department['id'])->makeHidden(['created_at','updated_at'])->toArray();

      $department = Department::find($request->department['id'])->makeHidden(['created_at','updated_at'])->toArray();

      

        return ['department'=>$department];
       
        
    }
}
