<?php

namespace App\Http\Controllers;

use App\VendorService;
use App\Vendor;
use Illuminate\Http\Request;

class VendorServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VendorService  $vendorService
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        // dd($request->all());
        $columns = ['id','material_and_service_id','cost','material_and_service_id','vendor_id','vendor_id','vendor_id'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;

        
        $query = VendorService::select('id', 'cost','vendor_id','material_and_service_id')->with('material_and_service:id,name,service_type_id','vendor.city')->orderBy($columns[$column], $dir);


        if($searchValue){
            $query->where('id', 'like', '%' . $searchValue . '%')
                    ->orWhere('cost', 'like', '%' . $searchValue . '%')
                    ->orWhereHas('material_and_service', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                        $q->orWhereHas('type', function($qry) use ($searchValue){
                            $qry->where('name',  'like', '%' . $searchValue . '%');
                        });
                    })->orWhereHas('vendor', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                        $q->orWhere('phone',  'like', '%' . $searchValue . '%');
                        $q->orWhereHas('city', function($qry) use ($searchValue){
                            $qry->where('name',  'like', '%' . $searchValue . '%');
                        });
                    })->get();
        }


        $vendors = $query->paginate($length);

        return ['data' => $vendors, 'draw' => $request->draw];
    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VendorService  $vendorService
     * @return \Illuminate\Http\Response
     */
    public function edit(VendorService $vendorService)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VendorService  $vendorService
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VendorService $vendorService)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VendorService  $vendorService
     * @return \Illuminate\Http\Response
     */
    public function destroy(VendorService $vendorService)
    {
        //
    }
}
