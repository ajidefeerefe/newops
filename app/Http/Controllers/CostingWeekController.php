<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CostingWeek;
use App\CostingDay;

class CostingWeekController extends Controller
{
    public function get(Request $request){
    	// dd($request->all());
        $columns = ['id','start_date','end_date','status'];

        $length = $request->params['length'];
        $column = $request->params['column'];
        $dir = $request->params['dir'];
        $searchValue = $request->params['search'];

        
        $query = CostingWeek::with('department:id,name','individualprojectphase.projectphase:id,name','individualprojectphase.phase_material','individualprojectphase.phase_labour','costing_day.project_material',
            'costing_day.project_labour')->where('individual_project_phases_id',$request->id)->orderBy($columns[$column], $dir);


        if($searchValue)
        {
            $query->where('id', 'like', '%' . $searchValue . '%')
                  ->orWhereHas('department', function($q) use ($searchValue){
                      $q->where('name',  'like', '%' . $searchValue . '%');
                  })->get();
        }


        $projects = $query->paginate($length);
        // dd($projects);
        return ['data' => $projects, 'draw' => $request->params['draw']];
    }

    public function add(Request $r){

        $date1 = new \DateTime($r->start_date);
        $date2 = new \DateTime($r->end_date);
        $difference = date_diff($date2,$date1);
        // {{dd($difference->format("%a"));}}
        $days_elapsed = $difference->format("%a");

        // dd($days_elapsed);

        // $earn = Earn::firstOrCreate(['name' => $r->name]);
        $start_date_test = date('Y-m-d', strtotime($r->start_date));
        $end_date_test = date('Y-m-d', strtotime($r->end_date));

        if($date2  <= $date1){

            return response(['error'=>'Please enter a valid date range'], 404);

        }elseif (CostingWeek::where('start_date', '<=', $end_date_test)->where('end_date', '>=', $start_date_test)->where('individual_project_phases_id',$r->individual_project_phase_id)->where('department_id',$r->department)->exists()) {
            return response(['error'=>'A costing already exists for the specified dates'], 404);
        } else {
            $input = [
                'start_date' => $r->start_date,
                'end_date' => $r->end_date,
                'priority' => $r->priority,
                'department_id' => $r->department,
                'individual_project_phases_id' => $r->individual_project_phase_id,
                'created_by' => \Auth::user()->id,
                'status' => 0,//Default to unbuilt
            ];
            $week_id = CostingWeek::create($input)->id;
            //Insert the days based on the week's interval into the costing day's table
            $counter = 0;
            while ( $counter <= $days_elapsed) {
                $day = new CostingDay;
                $day->date = date('Y-m-d H:i:s', strtotime("+".$counter." day", strtotime($r->start_date)));
                $day->costing_week_id = $week_id;
                $day->created_by = \Auth::user()->id;
                $day->save();
                $counter++;
            }

        }
        return 'Okay';
    }

    public function review(Request $r){
        // dd($r->all());
        CostingWeek::where('id', $r->id)->update(['status' => 1,'review_submitter'=> \Auth::user()->id]); 
        return "okay";
    }

    public function approve(Request $r){
        // dd($r->all());
        CostingWeek::where('id', $r->id)->update(['status' => 2,'approved_by'=> \Auth::user()->id]); //Approve Project Phase
        return "okay";
    }

    public function decline(Request $r){
        // dd($r->all());
        CostingWeek::where('id', $r->id)->update(['status' => 3,'declined_by'=> \Auth::user()->id]); //decline Project Phase
        return "okay";
    }

    public function cancel(Request $r){
        // dd($r->all());
        CostingWeek::where('id', $r->id)->update(['status' => 4,'canceled_by'=> \Auth::user()->id]); //cancel Project Phase
        return "okay";
    }

    public function getWeeks(Request $r){
         return CostingWeek::where('individual_project_phases_id', $r->id)->get();
    } 

    public function getWeek(Request $r){
         return CostingWeek::find($r->id);
    }


}
