<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permission;

class PermissionController extends Controller
{
    public function get(Request $request){
    	$permissions = Permission::where('role_id', $request->role_id)->with('role')->first();

    	return $permissions->makeHidden(['id','role_id','created_at','updated_at'])->toArray();
    	
    }

    public function edit(Request $request){
    	// return $request->data;

    	
    	Permission::where('role_id', '=', $request->role_id)
			->update($request->data);

			return "success";
    }
}
