<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Earn;

class EarnController extends Controller
{
    public function add(Request $r)
    {
    	$r->validate([
    	    'name' => 'required',
    	    
    	]);

    	// $earn = Earn::firstOrCreate(['name' => $r->name]);
        if (Earn::where('name', '=', $r->name)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
           $earn = Earn::create($r->only(['name']));
        }
    	return $earn;
    }

    public function show(Request $request)
    {

    	$columns = ['id','name'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

    	$query = Earn::select('id', 'name')->orderBy($columns[$column], $dir);


    	if($searchValue){
    	    $query->where(function($query) use ($searchValue) {
    	        $query->where('name', 'like', '%' . $searchValue . '%')
    	        ->orWhere('id', 'like', '%' .$searchValue . '%');
    	    });
    	}


    	$earnings = $query->paginate($length);

    	return ['data' => $earnings, 'draw' => $request->draw];
    }



    public function edit(Request $request)
    {
        if (Earn::where('name', '=', $request->name)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
          $earn = Earn::find($request->id);
        $earn->name = $request->name;
        $earn->save();
        }
        
        return $earn;
    }

    public function getAll(Request $request){
        return Earn::select('id','name')->get();
    } 
}
