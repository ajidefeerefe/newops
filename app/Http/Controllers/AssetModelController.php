<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AssetModel;
use App\AssetCategory;
use App\AssetManufacturer;

class AssetModelController extends Controller
{
     public function add(Request $r)
    {
    	
    	$r->validate([
            'model' => 'required',
    	    'active' => 'required',
    	    'assetcategory' => 'required',
    	    'assetmanufacturer' => 'required',

    	    
    	]);

    	
        if (AssetModel::where('name', '=', $r->model)->where('asset_manufacturer_id',$r->assetmanufacturer)->where('asset_category_id',$r->assetcategory)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
            
           $asset_model = new AssetModel;
           $asset_model->name = $r->model;
           $asset_model->active = $r->active;
           $asset_model->user_id = \Auth::user()->id;
           $asset_model->asset_category_id = $r->assetcategory;
           $asset_model->asset_manufacturer_id = $r->assetmanufacturer;
           
           $asset_model->save();

        }
    	return $asset_model->where('id',$asset_model->id)->with('assetcategory:id,name','assetmanufacturer:id,name')->get()->makeHidden(['user_id','created_at','updated_at'])->toArray();
    }

    public function show(Request $request)
    {

    	$columns = ['id','name','asset_category_id','asset_manufacturer_id'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

  

    	$query = AssetModel::select('id', 'name','asset_category_id','asset_manufacturer_id','active')->with('assetcategory:id,name','assetmanufacturer:id,name')->orderBy($columns[$column], $dir);




    	if($searchValue){

    	    $query->where('name', 'like', '%' . $searchValue . '%')
    	    		->orWhere('id', 'like', '%' .$searchValue . '%')
    	    		->orWhereHas('assetcategory', function($q) use ($searchValue){
    	            	$q->where('name',  'like', '%' . $searchValue . '%');
    	        	})->get();

    	    
    	}



    	$asset_categories = $query->paginate($length);

    	return ['data' => $asset_categories, 'draw' => $request->draw];
    }



    public function edit(Request $request)
    {
    	
      if (AssetModel::where('name', '=', $request->name)->where('active', '=', $request->active)->where('asset_category_id', '=', $request->assetcategory['id'])->where('asset_manufacturer_id', '=', $request->assetmanufacturer['id'])->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
        $asset_category = AssetModel::find($request->id);
        $asset_category->name = $request->name;
        $asset_category->active = $request->active;
        $asset_category->asset_category_id = $request->assetcategory['id'];
        $asset_category->asset_manufacturer_id = $request->assetmanufacturer['id'];
        $asset_category->save();

        }

		$assetcategory = AssetCategory::find($request->assetcategory['id'])->makeHidden(['created_at','updated_at','user_id'])->toArray();
		$assetmanufacturer = AssetManufacturer::find($request->assetmanufacturer['id'])->makeHidden(['created_at','updated_at','user_id'])->toArray();

      

        return ['assetcategory'=>$assetcategory,'assetmanufacturer'=>$assetmanufacturer];
       
        
    }


    public function get(){
       return AssetModel::where('active',1)->get()->makeHidden(['user_id','created_at','updated_at'])->toArray();
    }

    public function getVehicleModels(){
       $assetmodel = AssetModel::where('asset_category_id',4)->get()->makeHidden(['user_id','created_at','updated_at'])->toArray();

       return $assetmodel;
    }

    public function getAsset(Request $request){
        $search = $request->data;
    $query = AssetModel::select('id', 'name','asset_manufacturer_id')->with('assetmanufacturer:id,name')->where('name', 'like', '%' .$search. '%')->orWhereHas('assetmanufacturer',function($q) use ($search){
                        $q->where('name',  'like', '%' . $search  . '%');
                    })->get();

    return $query;
         // return MaterialAndServiceCategory::where('material_and_services_category_id',1)->makeHidden(['created_at','updated_at'])->toArray();
    }  
}
