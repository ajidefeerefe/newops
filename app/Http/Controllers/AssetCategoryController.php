<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AssetCategory;

class AssetCategoryController extends Controller
{
    public function add(Request $r)
    {
    	$r->validate([
            'name' => 'required',
    	    'active' => 'required',
    	    
    	]);

    	
        if (AssetCategory::where('name', '=', $r->name)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
            
           $asset_category = new AssetCategory;
           $asset_category->name = $r->name;
           $asset_category->active = $r->active;
           $asset_category->user_id = \Auth::user()->id;
           
           $asset_category->save();

        }
    	return $asset_category->makeHidden(['user_id','created_at','updated_at'])->toArray();
    }

    public function show(Request $request)
    {

    	$columns = ['id','name'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

    	$query = AssetCategory::select('id', 'name','active')->orderBy($columns[$column], $dir);


    	if($searchValue){
    	    $query->where(function($query) use ($searchValue) {
    	        $query->where('name', 'like', '%' . $searchValue . '%')
    	        ->orWhere('id', 'like', '%' .$searchValue . '%');
    	    });
    	}


    	$asset_categories = $query->paginate($length);

    	return ['data' => $asset_categories, 'draw' => $request->draw];
    }



    public function edit(Request $request)
    {

      if (AssetCategory::where('name', '=', $request->name)->where('active', '=', $request->active)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
        $asset_category = AssetCategory::find($request->id);
        $asset_category->name = $request->name;
        $asset_category->active = $request->active;
        $asset_category->save();

        }
       
        return $asset_category;
    }

    public function get()
    {
        return AssetCategory::where('active',1)->get()->makeHidden(['user_id','created_at','updated_at'])->toArray();
    }
}
