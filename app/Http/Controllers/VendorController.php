<?php

namespace App\Http\Controllers;

use App\Vendor;
use App\VendorService;
use App\PurchaseRequisitionMaterial;
use Illuminate\Http\Request;

class VendorController extends Controller
{
    public function add(Request $r)
    {
        // dd($r->services[0]);
        $r->validate([
            'vendor.name' => 'required',
            'vendor.category' => 'required',
            'vendor.phone' => 'required',
            'vendor.email' => 'required',
            'vendor.specialty' => 'required',
            'vendor.address' => 'required',
            'vendor.address2' => 'required',
            'vendor.country' => 'required',
            'vendor.state' => 'required',
            'vendor.city' => 'required',
            'vendor.bank' => 'required',
            'vendor.note' => 'required',
            'vendor.rating' => 'required',
            'vendor.account_number' => 'required',
            'vendor.account_name' => 'required',
            
        ]);

        // $earn = Earn::firstOrCreate(['name' => $r->name]);
        if (Vendor::where('phone', '=', $r->vendor['phone'])->where('email', '=', $r->vendor['email'])->exists()) {
            return response(['error'=>'Vendor with email and phone already exist'], 404);
        } else {
            $input =[

                "name" => $r->vendor['name'],
                "category_id" => $r->vendor['category'],
                "specialty" => $r->vendor['specialty'],
                "email" => $r->vendor['email'],
                "phone" => $r->vendor['phone'],
                "address" => $r->vendor['address'],
                "address2" => $r->vendor['address2'],
                "country_id" => $r->vendor['country'],
                "state_id" => $r->vendor['state'],
                "city_id" => $r->vendor['city'],
                "bank_id" => $r->vendor['bank'],
                "account_number" => $r->vendor['account_number'],
                "account_name" => $r->vendor['account_name'],
                "note" => $r->vendor['note'],
                "rating" => $r->vendor['rating'],
                "created_by" => \Auth::user()->id

           ];
           $id = Vendor::create($input)->id;

            foreach ($r->services as $key => $value) {

               if($value['service'] == '' or $value['cost']==''){//Check if the user added a mat_servID
                return response(['error'=>"Rendered Service fields can't be empty"], 404);
                    }
                    
                if (VendorService::where('material_and_service_id', '=', $value['service'])->where('vendor_id', '=', $id)->exists()) {
                    return response(['error'=>'This vendor already has this material'], 404);
                } else {
                   $vs = new VendorService;
                   $vs->vendor_id = $id;
                   $vs->material_and_service_id = $value['service'];
                   $vs->cost = $value['cost'];
                   $vs->save();
                }

            }

            if(isset($r->material_id)){//If you are creating this vendor from the requisition material page
                PurchaseRequisitionMaterial::where('id', $r->material_id)->update(['vendor_id' => $id, 'current_vendor_price' => $r->services[0]['cost']]); //Assign a vendor
                //Also create a mail to relevant persons to avtivate this vendor
                return "okay";
            }else
            {
                $vendor = new Vendor;

                return $vendor->with('country:id,name','state:id,name','city:id,name','bank:id,name','category:id,name','vendorservice.material_and_service:id,name')->where('vendors.id',$id)->get()->makeHidden(['created_at','updated_at'])->toArray();
            }
        }
    }

    public function show(Request $request)
    {
        // dd($request->all());
        $columns = ['id','name','category_id','phone','city_id','rating','status','flagged'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;

        //william
        $query = Vendor::select('id', 'name','category_id','phone','city_id','rating','flagged')->with('category:id,name','city:id,name')->where('vendors.status',1)->orderBy($columns[$column], $dir);


        if($searchValue){
            $query->where('id', 'like', '%' . $searchValue . '%')
                    ->orWhere('name', 'like', '%' . $searchValue . '%')
                    ->orWhere('phone', 'like', '%' . $searchValue . '%')
                    ->orWhereHas('category', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->orWhereHas('city', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->get();
        }


        $vendors = $query->paginate($length);

        return ['data' => $vendors, 'draw' => $request->draw];
    }


    //william
    public function edit(Request $r)
    {
        // dd($r->vendor['id']);
        $input =[

                "name" => $r->vendor['name'],
                "category_id" => $r->vendor['category'],
                "specialty" => $r->vendor['specialty'],
                "email" => $r->vendor['email'],
                "phone" => $r->vendor['phone'],
                "address" => $r->vendor['address'],
                "address2" => $r->vendor['address2'],
                "country_id" => $r->vendor['country'],
                "state_id" => $r->vendor['state'],
                "city_id" => $r->vendor['city'],
                "bank_id" => $r->vendor['bank'],
                "account_number" => $r->vendor['account_number'],
                "account_name" => $r->vendor['account_name'],
                "note" => $r->vendor['note'],
                "rating" => $r->vendor['rating'],
                "flagged" => $r->vendor['flagged'],
                "updated_by" => \Auth::user()->id

           ];
           $id = Vendor::where('id',$r->vendor['id'])->update($input);
           //Delete every because i couldnt use the loop to cater for updates when new items are added to the array
           VendorService::where('vendor_id',$r->vendor['id'])->delete();

           foreach ($r->services as $key => $value) {

               if($value['material_and_service_id'] == '' or $value['cost']==''){//Check if the user added a mat_servID
                return response(['error'=>"Rendered Service fields can't be empty"], 404);
                    }
                
                   $vs = new VendorService;
                   $vs->vendor_id = $r->vendor['id'];
                   $vs->material_and_service_id = $value['material_and_service_id'];
                   $vs->cost = $value['cost'];
                   $vs->save();

            }
        $vendor = new Vendor;
        return $vendor->with('vendorservice.material_and_service:id,name')->where('vendors.id',$r->vendor['id'])->first()->makeHidden(['created_at','updated_at'])->toArray();
        
    }



    public function get(Request $request){
        // return Vendor::find($request->id)->with('vendorservice')->first()->makeHidden(['created_at','updated_at'])->toArray();

        return Vendor::with('vendorservice.material_and_service:id,name')->where('id',$request->id)->where('flagged',0)->first()->makeHidden(['created_at','updated_at'])->toArray();
    }

    public function delete(Request $r){
        // dd($r->all());
        $data = ["status" => 0,"deleted_by" =>\Auth::user()->id];
        Vendor::where('id',$r->vendorId)->update($data);
    }

    public function flag(Request $r){
        // dd($r->all());
        $data = ["flagged" => 1,"flagged_by" =>\Auth::user()->id];
        Vendor::where('id',$r->vendorId)->update($data);
    }

    public function unflag(Request $r){
        // dd($r->all());
        $data = ["flagged" => 0,"unflagged_by" =>\Auth::user()->id];
        Vendor::where('id',$r->vendorId)->update($data);
    }

    public function getAll(){
        return Vendor::with('vendorservice')->where('flagged',0)->get()->makeHidden(['created_at','updated_at'])->toArray();
    }

     public function vendorImprest(Request $request){
        return Vendor::select('id','name')->where('flagged',0)->where('employee_imprest',0)->get();
    }
  
}
