<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LiquidAsset;
use App\LiquidAssetAssignment;

class LiquidAssetAssignmentController extends Controller
{
      public function add(Request $r)
    {



    	
           $pa = new LiquidAssetAssignment;
           $pa->liquid_asset_id = $r->id;
           $pa->assigned_to = $r->user;
           $pa->user_type = $r->user_type;
           $pa->assignment_status = 1;
           $pa->assignment_start = $r->start_time;
           $pa->assignment_end = $r->end_time;
           $pa->assignment_note = $r->notes;
           $pa->save();

           $la = LiquidAsset::find($r->id);
           $la->assignment_id = $pa->id;
           $la->save();

          
           

       
     $result = $pa->where('id',$pa->id)->with('user:id,first_name,last_name','vendor:id,name','client:id,lastname,firstname,company')->get();

     return json_encode($result);
    }

    public function show(Request $request)
    {

    	$columns = ['id','assignment_start','assignment_end'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

    	$query = LiquidAssetAssignment::where('liquid_asset_id',$request->id)->select('id','assignment_start','assignment_end','assignment_status','user_type','assigned_to')->with('user:id,last_name,first_name','vendor:id,name','client:id,lastname,firstname,company')->orderBy($columns[$column], $dir);

    	if($searchValue){
    	    $query->where('id', 'like', '%' . $searchValue . '%')
    	   		 ->orWhere('assignment_start', 'like', '%' . $searchValue . '%')
    	   		 ->orWhere('assignment_end', 'like', '%' . $searchValue . '%')
    	    	 ->get();
    	}


    	$LiquidAssetAssignmentss = $query->paginate($length);

    	return ['data' => $LiquidAssetAssignmentss, 'draw' => $request->draw];
    }



    public function edit(Request $request)
    {
    	
        $la = LiquidAsset::find($request->id);
        $la->assignment_id = null;
        $la->save();


        $laa = LiquidAssetAssignment::find($request->assignment_id);
        $laa->assignment_status = 0;
        $laa->actual_return_date = $request->unassign_date;
        $laa->save();

        return "success";

       
    }
}
