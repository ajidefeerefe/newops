<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Invoice;
use App\ClientPayment;

class InvoiceController extends Controller
{
     public function add(Request $r)
    {
    	
    	// $r->validate([
     //        'model' => 'required',
    	//     'active' => 'required',
    	//     'assetcategory' => 'required',
    	//     'assetmanufacturer' => 'required',

    	    
    	// ]);

    	
        if (Invoice::where('client_id', '=', $r->client)->where('project_id',$r->project)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
            
           $invoice = new Invoice;
           $invoice->client_id = $r->client;
           $invoice->project_id = $r->project;
           $invoice->created_by = \Auth::user()->id;
           $invoice->date = $r->start_date;
           $invoice->due_date = $r->due_date;
           
           $invoice->save();

        }
    	return $invoice->where('id',$invoice->id)->with('project:id,name','client:id,lastname,firstname,company','project.individualprojectphases.phase_material','project.individualprojectphases.phase_labour','payment','details.materials')->get()->makeHidden(['created_by','created_at','updated_at'])->toArray();
    }

    public function show(Request $request)
    {

    	$columns = ['id','name','asset_category_id','asset_manufacturer_id'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

  

    	$query = Invoice::select('id', 'client_id','project_id','date','due_date')->with('project:id,name','client:id,lastname,firstname,company','project.individualprojectphases.phase_material','project.individualprojectphases.phase_labour','payment','details.materials')->orderBy($columns[$column], $dir);




    	if($searchValue){

    	    $query->where('id', 'like', '%' . $searchValue . '%')
    	    		->orWhere('date', 'like', '%' .$searchValue . '%')
    	    		->orWhere('due_date', 'like', '%' .$searchValue . '%')
    	    		->orWhereHas('project', function($q) use ($searchValue){
    	            	$q->where('name',  'like', '%' . $searchValue . '%');
    	        	})->orWhereHas('client', function($q) use ($searchValue){
    	            	$q->where('firstname',  'like', '%' . $searchValue . '%')
    	            	->orWhere('lastname',  'like', '%' . $searchValue . '%');
    	        	})->get();

    	    
    	}



    	$asset_categories = $query->paginate($length);

    	return ['data' => $asset_categories, 'draw' => $request->draw];
    }



    public function edit(Request $request)
    {
    	
      if (AssetModel::where('name', '=', $request->name)->where('active', '=', $request->active)->where('asset_category_id', '=', $request->assetcategory['id'])->where('asset_manufacturer_id', '=', $request->assetmanufacturer['id'])->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
        $asset_category = AssetModel::find($request->id);
        $asset_category->name = $request->name;
        $asset_category->active = $request->active;
        $asset_category->asset_category_id = $request->assetcategory['id'];
        $asset_category->asset_manufacturer_id = $request->assetmanufacturer['id'];
        $asset_category->save();

        }

		$assetcategory = AssetCategory::find($request->assetcategory['id'])->makeHidden(['created_at','updated_at','user_id'])->toArray();
		$assetmanufacturer = AssetManufacturer::find($request->assetmanufacturer['id'])->makeHidden(['created_at','updated_at','user_id'])->toArray();

      

        return ['assetcategory'=>$assetcategory,'assetmanufacturer'=>$assetmanufacturer];
       
        
    }


    public function get(){
    	return Invoice::all()->makeHidden(['created_at','updated_at','created_by','date','due_date'])->toArray();

    }


    public function addRecievable(Request $request)
    {

    	 $amount = str_replace(",", "",$request->amount);

    	$payment = new ClientPayment;
    	$payment->date = $request->date;
    	$payment->amount = $amount;
    	$payment->project_id = $request->project_id;
    	$payment->invoice_id = $request->invoice_id;
    	$payment->payment_category_id = $request->paymentcategory;
    	$payment->payment_method = $request->payment_method;
    	$payment->bank_account_id = $request->bank_account;
    	$payment->receiver = $request->recieved_by;
    	$payment->created_by = \Auth::user()->id;
    	$payment->notes =  $request->notes;
    	$payment->save();

    	return $payment->where('id',$payment->id)->get()->makeHidden(['created_by','created_at','updated_at'])->toArray();
    }

    public function print(Request $request)
    {
        $invoice = Invoice::where('id',$request->invoice_id)->with('project:id,name','client:id,lastname,firstname,company,address,city_id,state_id,country_id','client.city','client.state','client.country','project.individualprojectphases.phase_material.material_and_service','project.individualprojectphases.phase_labour.material_and_service','payment')->get()->makeHidden(['created_by','created_at','updated_at'])->toArray();

        return $invoice;
    }

    public function getCount(){
        return Invoice::max('id');
    }

}
