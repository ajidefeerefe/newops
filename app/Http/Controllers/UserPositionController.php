<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserPosition;

class UserPositionController extends Controller
{
    public function add(Request $r)
    {
  
    	$r->validate([
    	    'name' => 'required',
    	    'description' => 'required',
    	    
    	]);

    	// $earn = Earn::firstOrCreate(['name' => $r->name]);
        if (UserPosition::where('name', '=', $r->name)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
           $position = UserPosition::create($r->only(['name','description']));
        }
    	return $position->makeHidden(['created_at','updated_at'])->toArray();
    }

    public function show(Request $request)
    {

    	$columns = ['id','name','description'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

    	$query = UserPosition::select('id', 'name','description')->orderBy($columns[$column], $dir);


    	if($searchValue){
    	    $query->where(function($query) use ($searchValue) {
    	        $query->where('name', 'like', '%' . $searchValue . '%')
    	        ->orWhere('id', 'like', '%' .$searchValue . '%')
    	        ->orWhere('description', 'like', '%' .$searchValue . '%');
    	    });
    	}


    	$positions = $query->paginate($length);

    	return ['data' => $positions, 'draw' => $request->draw];
    }



    public function edit(Request $request)
    {
        if (UserPosition::where('name', '=', $request->name)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
           $position = UserPosition::find($request->id);
            $position->name = $request->name;
            $position->description = $request->description;
            $position->save();
        }
        
        return $position;
    }


    public function get(){
        return UserPosition::all()->makeHidden(['created_at','updated_at'])->toArray();
    }
}
