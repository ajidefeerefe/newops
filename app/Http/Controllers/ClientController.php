<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
        public function add(Request $r)
    {
  
        $r->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'company' => 'required',
            'address' => 'required',
            'address2' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            
        ]);

        // $earn = Earn::firstOrCreate(['name' => $r->name]);
        if (Client::where('email', '=', $r->email)->exists()) {
            return response(['error'=>'Client with email already exist'], 404);
        } else {
            /*$client = new Client;
            $client->firstname = $r->firstname;
            $client->lastname = $r->lastname;
            $client->phone = $r->phone;
            $client->email = $r->email;
            $client->company = $r->company;
            $client->address = $r->address;
            $client->address2 = $r->address2;
            $client->country_id = $r->country;
            $client->state_id = $r->state;
            $client->city_id = $r->city;

            $client->save();
        }
        return $client->find($client->id)->with('country:id,name','state:id,name','city:id,name')->get()->makeHidden(['created_at','updated_at'])->toArray();*/
        $input =[

                "firstname" => $r->firstname,
                "lastname" => $r->lastname,
                "phone" => $r->phone,
                "email" => $r->email,
                "company" => $r->company,
                "address" => $r->address,
                "address2" => $r->address2,
                "country_id" => $r->country,
                "state_id" => $r->state,
                "city_id" => $r->city,

           ];
           $id = Client::create($input)->id;
        }
        $client = new Client;
        return $client->with('country:id,name','state:id,name','city:id,name')->where('clients.id',$id)->first()->makeHidden(['created_at','updated_at'])->toArray();
    }

    public function show(Request $request)
    {
        // dd($request->all());
        $columns = ['id','firstname','lastname','company','country_id','state_id','city_id'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;

        
        $query = Client::select('id', 'firstname','lastname','company','country_id','state_id','city_id')->with('country:id,name','state:id,name','city:id,name')->orderBy($columns[$column], $dir);


        if($searchValue){
            $query->where('id', 'like', '%' . $searchValue . '%')
                    ->orWhere('firstname', 'like', '%' . $searchValue . '%')
                    ->orWhere('lastname', 'like', '%' . $searchValue . '%')
                    ->orWhere('company', 'like', '%' . $searchValue . '%')
                    ->orWhereHas('country', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->orWhereHas('state', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->orWhereHas('city', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->get();
        }


        $users = $query->paginate($length);

        return ['data' => $users, 'draw' => $request->draw];
    }



    public function edit(Request $r)
    {
            // dd($r->all());
           $client = Client::find($r->id);
           $client->firstname = $r->firstname;
           $client->lastname = $r->lastname;
           $client->company = $r->company;
           $client->address = $r->address;
           $client->address2 = $r->address2;
           $client->country_id = $r->country;
           $client->state_id = $r->state;
           $client->city_id = $r->city;
           $client->email = $r->email;
           $client->phone = $r->phone;
           $client->save();
        
        
        return $client;
    }



    public function get(Request $request){
        return Client::find($request->id)->makeHidden(['created_at','updated_at'])->toArray();
    }

    public function getAll(){
        return Client::all();
    }
}
