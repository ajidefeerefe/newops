<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmployeeImprestTopup;

class EmployeeImprestTopupController extends Controller
{
      public function add(Request $r)
    {
    	
    	$r->validate([
            'id' => 'required',
    	    'bank_id' => 'required',
    	    'date' => 'required',
    	    'amount' => 'required',

    	    
    	]);

    	 $amount = str_replace(",", "",$r->amount);
            
           $topup = new EmployeeImprestTopup;
           $topup->employee_imprest_id = $r->id;
           $topup->topup_date = $r->date;
           $topup->topup_by = \Auth::user()->id;
           $topup->bank_id = $r->bank_id;
           $topup->amount = $amount;
           $topup->save();

        
    	return $topup->where('id',$topup->id)->get()->makeHidden(['topup_by','created_at','updated_at'])->toArray();
    }

    public function show(Request $request)
    {

    	$columns = ['id','employee_id','topup_by','amount','topup_date'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

  

    	$query = EmployeeImprestTopup::where('employee_imprest_id',$request->id)->select('id','employee_imprest_id','topup_by','topup_date','amount')->with('topup_by:id,last_name,first_name')->orderBy($columns[$column], $dir);
	// $query = EmployeeImprest::select('id','employee_id','user_type','created_by')->with('user:id,last_name,first_name','vendor:id,name','topups:id,amount','topup.imprest.withdraws:id,amount')->orderBy($columns[$column], $dir);




    	if($searchValue){

    	    $query->where('name', 'like', '%' . $searchValue . '%')
    	    		->orWhere('id', 'like', '%' .$searchValue . '%')
    	    		->orWhereHas('assetcategory', function($q) use ($searchValue){
    	            	$q->where('name',  'like', '%' . $searchValue . '%');
    	        	})->get();

    	    
    	}



    	$asset_categories = $query->paginate($length);

    	return ['data' => $asset_categories, 'draw' => $request->draw];
    }



    public function edit(Request $request)
    {
    	
      if (EmployeeImprest::where('name', '=', $request->name)->where('active', '=', $request->active)->where('asset_category_id', '=', $request->assetcategory['id'])->where('asset_manufacturer_id', '=', $request->assetmanufacturer['id'])->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
        $asset_category = EmployeeImprest::find($request->id);
        $asset_category->name = $request->name;
        $asset_category->active = $request->active;
        $asset_category->asset_category_id = $request->assetcategory['id'];
        $asset_category->asset_manufacturer_id = $request->assetmanufacturer['id'];
        $asset_category->save();

        }

		$assetcategory = EmployeeImprest::find($request->assetcategory['id'])->makeHidden(['created_at','updated_at','user_id'])->toArray();
		$assetmanufacturer = EmployeeImprest::find($request->assetmanufacturer['id'])->makeHidden(['created_at','updated_at','user_id'])->toArray();

      

        return ['assetcategory'=>$assetcategory,'assetmanufacturer'=>$assetmanufacturer];
       
        
    }

    public function imprestUser(Request $request){
        return EmployeeImprestTopup::where('employee_imprest_id',$request->id)->select('id','employee_imprest_id')->with('imprest','imprest.user','imprest.vendor')->first();
    }

}
