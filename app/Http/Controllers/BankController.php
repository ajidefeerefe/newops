<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bank;

class BankController extends Controller
{
	 public function add(Request $r)
    {
    	$r->validate([
    	    'name' => 'required',
    	    
    	]);

    	// $earn = Earn::firstOrCreate(['name' => $r->name]);
        if (Bank::where('name', '=', $r->name)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
           $bank = Bank::create($r->only(['name']));
        }
    	return $bank->makeHidden(['created_at','updated_at'])->toArray();
    }

    public function show(Request $request)
    {

    	$columns = ['id','name'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

    	$query = Bank::select('id', 'name')->orderBy($columns[$column], $dir);


    	if($searchValue){
    	    $query->where(function($query) use ($searchValue) {
    	        $query->where('name', 'like', '%' . $searchValue . '%')
    	        ->orWhere('id', 'like', '%' .$searchValue . '%');
    	    });
    	}


    	$banks = $query->paginate($length);

    	return ['data' => $banks, 'draw' => $request->draw];
    }

    public function edit(Request $request)
    {
        if (Bank::where('name', '=', $request->name)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
        $bank = Bank::find($request->id);
        $bank->name = $request->name;
        $bank->save();
        return $bank;
        }
    }

    public function get(Request $request){
        return Bank::all()->makeHidden(['created_at','updated_at'])->toArray();
    }
}
