<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Deduct;

class DeductController extends Controller
{
    public function add(Request $r)
    {
    	$r->validate([
    	    'name' => 'required',
    	    
    	]);

    	
        if (Deduct::where('name', '=', $r->name)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
           $earn = Deduct::create($r->only(['name']));
        }
    	return $earn;
    }

    public function show(Request $request)
    {

    	$columns = ['id','name'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

    	$query = Deduct::select('id', 'name')->orderBy($columns[$column], $dir);


    	if($searchValue){
    	    $query->where(function($query) use ($searchValue) {
    	        $query->where('name', 'like', '%' . $searchValue . '%')
    	        ->orWhere('id', 'like', '%' .$searchValue . '%');
    	    });
    	}


    	$earnings = $query->paginate($length);

    	return ['data' => $earnings, 'draw' => $request->draw];
    }



    public function edit(Request $request)
    {
         if (Deduct::where('name', '=', $request->name)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
           $earn = Deduct::find($request->id);
            $earn->name = $request->name;
            $earn->save();
        }
        
        return $earn;
    }

    public function getAll(Request $request){
        return Deduct::select('id','name')->get();
    } 
}
