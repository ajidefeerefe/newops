<?php

namespace App\Http\Controllers;

use App\Project;
use App\Subscription;
use App\Mail\newProjectCosting;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailer;

class ProjectController extends Controller
{
    
    public function add(Request $r)
    {
        $r->validate([
            'name' => 'required',
        ]);

        // $earn = Earn::firstOrCreate(['name' => $r->name]);
        if (Project::where('name', '=', $r->name)->where('department_id', '=', $r->department)->exists()) {
            return response(['error'=>'This project already exists for this department'], 404);
        } else {
            $input = [
                'name' => $r->name,
                'project_date' => $r->date,
                'department_id' => $r->department,
                'created_by' => \Auth::user()->id,
                'cost' => $r->usedcost,//Use the cost where comma has been removed
                'type' => $r->type,//Use the cost where comma has been removed
                'active' => $r->active

            ];
            $id = Project::create($input)->id;
        }
        return Project::with('department:id,name')->where('id',$id)->first()->makeHidden(['created_at','updated_at'])->toArray();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        // dd($request->all());
        $columns = ['id','name','department_id','project_date','cost','active'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;

        
        $query = Project::select(['id','name','department_id','project_date','cost','active','project_costing'])->with('department:id,name')->orderBy($columns[$column], $dir);


        if($searchValue){
            $query->where('id', 'like', '%' . $searchValue . '%')
                    ->orWhere('name', 'like', '%' . $searchValue . '%')
                    ->orWhere('project_date', 'like', '%' . $searchValue . '%')
                    ->orWhere('cost', 'like', '%' . $searchValue . '%')
                    ->orWhereHas('department', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->get();
        }


        $projects = $query->paginate($length);
        // dd($projects);
        return ['data' => $projects, 'draw' => $request->draw];
    }

    public function showCosting(Request $request)
    {
        // dd($request->all());
        $columns = ['id','name','department_id','project_date','type','active'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;

        
        $query = Project::select(['id','name','department_id','project_date','type','active','project_costing'])->with('department:id,name')->orderBy($columns[$column], $dir)->where('projects.project_costing',1);


        if($searchValue){
            $query->where('id', 'like', '%' . $searchValue . '%')
                    ->orWhere('name', 'like', '%' . $searchValue . '%')
                    ->orWhere('project_date', 'like', '%' . $searchValue . '%')
                    ->orWhere('cost', 'like', '%' . $searchValue . '%')
                    ->orWhereHas('department', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->get();
        }


        $projects = $query->paginate($length);
        // dd($projects);
        return ['data' => $projects, 'draw' => $request->draw];
    }

    public function showContract(Request $request)
    {
        // dd($request->all());
        $columns = ['id','name','department_id','project_date','type','active'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;

        
        $query = Project::select(['id','name'])->with('vendorContracts','vendorContracts.expenseRecords')->orderBy($columns[$column], $dir)->where('vendor_contract',1)->where('active',1);


        if($searchValue){
            $query->where('id', 'like', '%' . $searchValue . '%')
                    ->orWhere('name', 'like', '%' . $searchValue . '%')
                    ->orWhere('project_date', 'like', '%' . $searchValue . '%')
                    ->orWhere('cost', 'like', '%' . $searchValue . '%')
                    ->orWhereHas('department', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->get();
        }


        $projects = $query->paginate($length);
        // dd($projects);
        return ['data' => $projects, 'draw' => $request->draw];
    }



    public function edit(Request $r)
    {
            // dd($r->all());
           $project = Project::find($r->id);
           $project->name = $r->name;
           $project->coordinator = $r->coordinator;
           $project->project_date = $r->date;
           $project->department_id = $r->department;
           $project->cost = $r->usedcost;
           $project->active = $r->active;
           $project->save();
        
        
        return $project;
    }

    public function get(Request $request){

        // return Project::with('department:id,name','individualprojectphases.projectphase:id,name,description','projectfiles.projectphase:id,name','projectstaffallocations.user:id,first_name,last_name')->where('projects.id',$request->id)->get()->makeHidden(['created_at','updated_at'])->toArray();
        return Project::with('department:id,name','individualprojectphases.projectphase:id,name,description','individualprojectphases.phase_material.material_and_service:id,name','projectfiles.projectphase:id,name','projectstaffallocations.user:id,first_name,last_name','individualprojectphases.costingweek.costing_day','individualprojectphases.phase_labour.material_and_service:id,name')->where('projects.id',$request->id)->first()->makeHidden(['created_at','updated_at'])->toArray();
    }

    //WIliiam
    public function getAllProjects(){
         return Project::all()->makeHidden(['created_at','updated_at'])->toArray();
    }

    public function addProjectCosting(Request $r, Mailer $mail){
        // dd($r->all());
        Project::where('id',$r->project_id)->update(['project_costing' => 1]);
         // Prep for mail
        $p_name = $r->name;
        $name = \Auth::user()->first_name." ".\Auth::user()->last_name;
         $message = "$name has created a costing container for ". $p_name ." project.";
        
        //Get all roles having this permission
        $matched_roles = Subscription::with('role.users')->where('new_project_costing',1)->get();

        foreach ($matched_roles as $key => $value) 
        {
          // get all the users having that role
          $users = $value->role->users;
          foreach ($users as $key => $user) {

            //get user details (first_name.last_name and email)
            $email = $user->email;
            // dd($email);
            try{
              // dd($email);
               $mail->to($email)->send(new newProjectCosting( "Notification from Operations Manager",$message));
             } catch (\Swift_TransportException $e) {
                return $e;

            }
            
          }
          
        }
        return Project::where('id',$r->project_id)->first()->makeHidden(['created_at','updated_at'])->toArray();

    } 

    public function addVendorContract(Request $r){
        // dd($r->all());
        Project::where('id',$r->project_id)->update(['vendor_contract' => 1]);
        return Project::where('id',$r->project_id)->first()->makeHidden(['created_at','updated_at'])->toArray();

    }

    public function getReceivables(Request $request)
    {
        // dd($request->all());
        $columns = ['id','name'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;

        
        $query = Project::select(['id','name'])->with('client_payments','expense_records_amount','individualprojectphases.phase_material','individualprojectphases.phase_labour','individualprojectphases.expense_record')->orderBy($columns[$column], $dir);


        if($searchValue){
            $query->where('id', 'like', '%' . $searchValue . '%')
                    ->orWhere('name', 'like', '%' . $searchValue . '%')
                    ->orWhereHas('client_payments', function($q) use ($searchValue){
                        $q->where('SUM(amount) as amount',  'like', '%' . $searchValue . '%')
                          ->groupBy('project_id');
                    })
                    ->orWhereHas('expense_record', function($q) use ($searchValue){
                        $q->where('SUM(amount) as amount',  'like', '%' . $searchValue . '%')
                          ->groupBy('individual_project_phases_id');
                    })->get();
        }


        $projects = $query->paginate($length);
        // dd($projects);
        return ['data' => $projects, 'draw' => $request->draw];
    }

    public function search(Request $request){
        $query = Project::select('id','name')->with('individualprojectphases.projectphase:id,name')->where('name', 'like', '%' .$request->data . '%')->get();
        return $query;
    }
    
    public function showRequisition(Request $request)
    {
        // dd($request->all());
        $columns = ['id','name','department_id','project_date','type','active'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;

        
        $query = Project::select(['id','name','department_id','project_date','type','active','project_costing'])->with('department:id,name')->orderBy($columns[$column], $dir)->where('projects.purchase_requisition',1)->where('projects.active',1);


        if($searchValue){
            $query->where('id', 'like', '%' . $searchValue . '%')
                    ->orWhere('name', 'like', '%' . $searchValue . '%')
                    ->orWhere('project_date', 'like', '%' . $searchValue . '%')
                    ->orWhere('cost', 'like', '%' . $searchValue . '%')
                    ->orWhereHas('department', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->get();
        }


        $projects = $query->paginate($length);
        // dd($projects);
        return ['data' => $projects, 'draw' => $request->draw];
    }

    public function addPurchaseRequisition(Request $r){
        // dd($r->all());
        Project::where('id',$r->project_id)->update(['purchase_requisition' => 1]);
        return Project::where('id',$r->project_id)->first()->makeHidden(['created_at','updated_at'])->toArray();

    }

    // WIlliam
    public function getUserAllocations(Request $request)
    {
      
       $columns = ['id','name','project_date','staff','active'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;

  

        $query = Project::select('id', 'name','project_date','active')->with('projectstaffallocations.user:id,first_name,last_name')->orderBy($columns[$column], $dir)->where('project_date','>=',$request->start_date)->where('project_date','<=',$request->end_date)->whereHas('projectstaffallocations', function($q) use($request){
                        $q->whereHas('user', function($q) use($request) {
                            $q->where('id', $request->id);
                        });
                    });




        if($searchValue){

            $query->where('id', 'like', '%' . $searchValue . '%')
                    ->orWhere('departure_date', 'like', '%' .$searchValue . '%')
                    ->orWhere('return_date', 'like', '%' .$searchValue . '%')
                    ->orWhere('destination_address', 'like', '%' .$searchValue . '%')
                    ->orWhereHas('user', function($q) use ($searchValue){
                        $q->where('first_name',  'like', '%' . $searchValue . '%');
                        $q->orWhere('last_name',  'like', '%' . $searchValue . '%');
                    })->get();

            
        }

        $vehicle_requests = $query->paginate($length);

        return ['data' => $vehicle_requests, 'draw' => $request->draw];
        
    } 

}
