<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BankAccountTopup;
use App\ExpenseRecord;

class BankAccountTopupController extends Controller
{
    public function viewTopups(Request $request){
    	// dd($request->all());
    	$columns = ['id','amount'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

        //William
    	$query = BankAccountTopup::with('user:id,first_name,last_name','expense_records_amount','bank_account:id,account_number')->where('bank_account_id',$request->id)->orderBy($columns[$column], $dir);


    	if($searchValue){
    	    $query->where(function($query) use ($searchValue) {
    	        $query->where('amount', 'like', '%' . $searchValue . '%')
    	        ->orWhere('id', 'like', '%' .$searchValue . '%');
    	    })->orWhereHas('user', function($q) use ($searchValue){
                  $q->where('first_name',  'like', '%' . $searchValue . '%');
                  $q->orWhere('last_name',  'like', '%' . $searchValue . '%');
             })->get();
    	}


    	$topups = $query->paginate($length);

    	return ['data' => $topups, 'draw' => $request->draw];

    }

    //WIlliam delete

    public function topup(Request $r)
    {
        // dd($r->all());
        $bat = new BankAccountTopup;
        $bat->bank_account_id = $r->id;
        $bat->amount = $r->usedAmount;
        $bat->created_by = \Auth::user()->id;
        $bat->save();
        return "ok";
    }
}
