<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProjectPhaseActivity;
use App\ProjectPhase;
use App\MaterialAndService;

class ProjectPhaseActivityController extends Controller
{
    public function add(Request $r)
    {
    	$data = [];
    	$result = $r->data;

    	foreach ($result as $key => $value) {
    		if($value['projectphase'] == '' or $value['activity']==''){
    			return response(['error'=>"Fields can't be empty"], 404);
    		}
    		
        if (ProjectPhaseActivity::where('project_phase_id', '=', $value['projectphase'])->where('material_and_service_id', '=', $value['activity'])->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
           $pa = new ProjectPhaseActivity;
           $pa->project_phase_id = $value['projectphase'];
           $pa->material_and_service_id = $value['activity'];
           $pa->save();
           $data[] = $pa->where('id',$pa->id)->with('project_phase:id,name','material_labour:id,name')->get();

        }
       }
     return $data;
    }

    public function show(Request $request)
    {

    	$columns = ['id','project_phase_id','material_and_service_id'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

    	$query = ProjectPhaseActivity::select('id','project_phase_id','material_and_service_id')->with('project_phase:id,name','material_labour:id,name')->orderBy($columns[$column], $dir);

    	if($searchValue){
    	    $query->where('id', 'like', '%' . $searchValue . '%')
    	    		->orWhereHas('project_phase', function($q) use ($searchValue){
    	            	$q->where('name',  'like', '%' . $searchValue . '%');
    	        	})->orWhereHas('material_labour', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->get();
    	}


    	$colors = $query->paginate($length);

    	return ['data' => $colors, 'draw' => $request->draw];
    }



    public function edit(Request $request)
    {
    	
        if (ProjectPhaseActivity::where('project_phase_id', '=', $request->project_phase['id'])->where('material_and_service_id', '=', $request->material_labour['id'])->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
           $pa = ProjectPhaseActivity::find($request->id);
            $pa->project_phase_id = $request->project_phase['id'];
            $pa->material_and_service_id = $request->material_labour['id'];
            $pa->save();
        }

        $project_phase = ProjectPhase::find($request->project_phase['id']);
        $material_labour = MaterialAndService::find($request->material_labour['id']);

        return ['project_phase'=> $project_phase->makeHidden(['created_at','updated_at'])->toArray(),'material_labour'=> $material_labour->makeHidden(['created_at','updated_at'])->toArray()];
        
       
    }
}
