<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;

class CityController extends Controller
{
    public function get(){
        return City::all()->makeHidden(['created_at','updated_at'])->toArray();
    }
}
