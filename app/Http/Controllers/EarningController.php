<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Earning;

class EarningController extends Controller
{
   public function update(Request $r){

       foreach ($r->earnings as $key => $value) 
        {
          if($value['earns']['id'] == '' or $value['amount']==''){//Check if any phase field was left blank
          return response(['error'=>"No field can be empty"], 404);
          }
          
        }
       


       $earnings = $r->earnings;

       
        
        Earning::where('user_id',$r->user_id)->where('payroll_id',$r->payroll_id)->delete();//Clear all the record before reinserting
    	if(count($earnings) > 0){
        foreach ($earnings as $key => $value) {

                  // return $earnings[$key]['amount'];
                if(isset($earnings[$key]['id'])){
                
                 $earn = new Earning;
                  $earn->id = $value['id'];
                  $earn->earn_id = $value['earns']['id'];
                  $earn->amount = $value['amount'];
                  $earn->payroll_id = $value['payroll_id'];
                  $earn->user_id = $value['user_id'];
                  $earn->save();
                 
                }else{
                // return 'w';
                $earn1 = new Earning;
               	$earn1->payroll_id = $r->payroll_id;
                $earn1->earn_id = $value['earns']['id'];
               	$earn1->user_id = $r->user_id;
               	$earn1->amount = $value['amount'];
               	$earn1->save();
               }
                
               	 
            }
            return Earning::where('user_id',$r->user_id)->where('payroll_id',$r->payroll_id)->with('earns:id,name')->get()->makeHidden(['created_at','updated_at'])->toArray();
        }else{

        	return 0;
            // return "Done";
        }
       
        
      }
}
