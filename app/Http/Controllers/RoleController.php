<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Role;
use App\Permission;
use App\Subscription;

class RoleController extends Controller
{
    public function add(Request $r)
    {
    	$r->validate([
            'name' => 'required',
    	    'active' => 'required',
    	    
    	]);

    	
        if (Role::where('name', '=', $r->name)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
            $permission_id = Permission::max('id');
            $subscription_id = Subscription::max('id');
           $role = new Role;
           $role->name = $r->name;
           $role->active = $r->active;
           $role->permission_id = ++$permission_id;
           $role->subscription_id = ++$subscription_id;
           $role->save();


           $permission = new Permission;
           $permission->role_id = $role->id;
           $permission->save(); 

           $subscription = new Subscription;
           $subscription->role_id = $role->id;
           $subscription->save();

        }
    	return $role->makeHidden(['created_at','updated_at'])->toArray();
    }

    public function show(Request $request)
    {

    	$columns = ['id','name','active'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

    	$query = Role::select('id', 'name','active')->orderBy($columns[$column], $dir);


    	if($searchValue){
    	    $query->where(function($query) use ($searchValue) {
    	        $query->where('name', 'like', '%' . $searchValue . '%')
    	        ->orWhere('id', 'like', '%' .$searchValue . '%');
    	    });
    	}


    	$roles = $query->paginate($length);

    	return ['data' => $roles, 'draw' => $request->draw];
    }



    public function edit(Request $request)
    {
      if (Role::where('name', '=', $request->name)->where('active', '=', $request->active)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
        $role = Role::find($request->id);
        $role->name = $request->name;
        $role->active = $request->active;
        $role->save();

        }
       
        return $role;
    }

     public function get(){
        return Role::all()->makeHidden(['created_at','updated_at'])->toArray();
    }
}
