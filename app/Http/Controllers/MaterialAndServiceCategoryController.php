<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MaterialAndServiceCategory;

class MaterialAndServiceCategoryController extends Controller
{
     public function add(Request $r)
    {
    	$r->validate([
    	    'category' => 'required',
    	    
    	]);

    	// $earn = Earn::firstOrCreate(['name' => $r->name]);
        if (MaterialAndServiceCategory::where('name', '=', $r->category)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
           $category = new MaterialAndServiceCategory;
           $category->name = $r->category;
           $category->save();

        }
    	return $category->makeHidden(['created_at','updated_at'])->toArray();
    }

    public function show(Request $request)
    {

    	$columns = ['id','name'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

    	$query = MaterialAndServiceCategory::select('id', 'name')->orderBy($columns[$column], $dir);


    	if($searchValue){
    	    $query->where(function($query) use ($searchValue) {
    	        $query->where('name', 'like', '%' . $searchValue . '%')
    	        ->orWhere('id', 'like', '%' .$searchValue . '%');
    	    });
    	}


    	$categories = $query->paginate($length);

    	return ['data' => $categories, 'draw' => $request->draw];
    }

    public function edit(Request $request)
    {
        if (MaterialAndServiceCategory::where('name', '=', $request->name)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
        $category = MaterialAndServiceCategory::find($request->id);
        $category->name = $request->name;
        $category->save();
        return $category;
        }
    }

    public function get(){
         return MaterialAndServiceCategory::all()->makeHidden(['created_at','updated_at'])->toArray();
    }  


    public function getAll(Request $r){
      // dd($r->all());
      return MaterialAndServiceCategory::with('material_and_services')->get()->makeHidden(['created_at','updated_at'])->toArray();

    }
}
