<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subscription;

class SubscriptionController extends Controller
{
     public function get(Request $request){
    	$permissions = Subscription::where('role_id', $request->role_id)->with('role')->first();

    	return $permissions->makeHidden(['id','role_id','created_at','updated_at'])->toArray();
    	
    }

    public function edit(Request $request){
    	// return $request->data;

    	
    	Subscription::where('role_id', '=', $request->role_id)
			->update($request->data);

			return "success";
    }
}
