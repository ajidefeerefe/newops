<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmployeeImprestTopupWithdrawals;

class EmployeeImprestTopupWithdrawalsController extends Controller
{
    public function add(Request $r){

    	// return $r->all();

    	 $amount = str_replace(",", "",$r->amount);

    	 if((float)$amount > (float)$r->topup_amount){
    	 	 return response(['error'=>'Topup balance too low'], 404);
    	 }else{

	    	$withdraw = new EmployeeImprestTopupWithdrawals;
	    	$withdraw->project_id = $r->project;
	    	$withdraw->employee_imprest_id = $r->topup_id;
	    	$withdraw->project_phase_id = $r->project_phase;
            if($r->asset_type == 'materials'){
                 $withdraw->asset_model_id = $r->asset;
            }else if($r->asset_type == 'services'){
                $withdraw->material_and_service_id = $r->asset;
            }
            $withdraw->asset_type = $r->asset_type;
	    	$withdraw->withdraw_date = $r->date;
	    	$withdraw->amount = $amount;
	    	$withdraw->save();

	    	return $withdraw->where('id',$withdraw->id)->get()->makeHidden(['created_at','updated_at'])->toArray();
    	 }

    }

     public function show(Request $request)
    {

    	$columns = ['id','project_id','project_phase_id','material_and_service_id','amount','withdraw_date'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

  
            $query = EmployeeImprestTopupWithdrawals::where('employee_imprest_id',$request->id)->select('id','employee_imprest_id','project_id','project_phase_id','amount','material_and_service_id','withdraw_date','asset_model_id','asset_type')->with('project:id,name','projectPhase:id,name','material:id,name','asset.assetmanufacturer')->orderBy($columns[$column], $dir);
	// $query = EmployeeImprest::select('id','employee_id','user_type','created_by')->with('user:id,last_name,first_name','vendor:id,name','topups:id,amount','topup.imprest.withdraws:id,amount')->orderBy($columns[$column], $dir);




    	if($searchValue){

    	    $query->where('amount', 'like', '%' . $searchValue . '%')
                    ->orWhere('id', 'like', '%' .$searchValue . '%')
    	    		->orWhere('withdraw_date', 'like', '%' .$searchValue . '%')
    	    		->orWhereHas('project', function($q) use ($searchValue){
    	            	$q->where('name',  'like', '%' . $searchValue . '%');
    	        	})->orWhereHas('projectPhase', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->orWhereHas('material', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->get();

    	    
    	}



    	$asset_categories = $query->paginate($length);

    	return ['data' => $asset_categories, 'draw' => $request->draw];
    }

    public function imprestUser(Request $request){
        return EmployeeImprestTopupWithdrawals::where('employee_imprest_id',$request->id)->select('id','employee_imprest_id')->with('imprest','imprest.user','imprest.vendor')->first();
    }
}
