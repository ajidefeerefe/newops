<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmployeeImprest;
use App\User;
use App\Vendor;

class EmployeeImprestController extends Controller
{
     public function add(Request $r)
    {
    	
    	// $r->validate([
     //        'model' => 'required',
    	//     'active' => 'required',
    	//     'assetcategory' => 'required',
    	//     'assetmanufacturer' => 'required',

    	    
    	// ]);

    	
     //    if (EmployeeImprest::where('name', '=', $r->model)->where('asset_manufacturer_id',$r->assetmanufacturer)->where('asset_category_id',$r->assetcategory)->exists()) {
     //        return response(['error'=>'Data already exist'], 404);
     //    } else {
            
           $imprest = new EmployeeImprest;
           $imprest->employee_id = $r->user_id;
           $imprest->user_type = $r->user_type;
           $imprest->created_by = \Auth::user()->id;
           $imprest->save();

           if($r->user_type == 'user'){
              $user = User::find($r->user_id);
              $user->employee_imprest = 1;
              $user->save();
           }else{
             $vensor = Vendor::find($r->user_id);
              $vensor->employee_imprest = 1;
              $vensor->save();
           }

        
    	return $imprest->where('id',$imprest->id)->with('user:id,last_name,first_name','created_by:id,last_name,first_name','vendor:id,name','withdraws','topups')->get()->makeHidden(['created_at','updated_at'])->toArray();
    }

    public function show(Request $request)
    {

    	$columns = ['id','employee_id','user_type','created_by'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

  

    	$query = EmployeeImprest::select('id','employee_id','user_type','created_by')->with('user:id,last_name,first_name','created_by:id,last_name,first_name','vendor:id,name','withdraws','topups')->orderBy($columns[$column], $dir);
	// $query = EmployeeImprest::select('id','employee_id','user_type','created_by')->with('user:id,last_name,first_name','vendor:id,name','topups:id,amount','topup.imprest.withdraws:id,amount')->orderBy($columns[$column], $dir);




    	if($searchValue){

    	    $query->where('name', 'like', '%' . $searchValue . '%')
    	    		->orWhere('id', 'like', '%' .$searchValue . '%')
    	    		->orWhereHas('assetcategory', function($q) use ($searchValue){
    	            	$q->where('name',  'like', '%' . $searchValue . '%');
    	        	})->get();

    	    
    	}



    	$asset_categories = $query->paginate($length);

    	return ['data' => $asset_categories, 'draw' => $request->draw];
    }



  //   public function edit(Request $request)
  //   {
    	
  //     if (EmployeeImprest::where('name', '=', $request->name)->where('active', '=', $request->active)->where('asset_category_id', '=', $request->assetcategory['id'])->where('asset_manufacturer_id', '=', $request->assetmanufacturer['id'])->exists()) {
  //           return response(['error'=>'Data already exist'], 404);
  //       } else {
  //       $asset_category = EmployeeImprest::find($request->id);
  //       $asset_category->name = $request->name;
  //       $asset_category->active = $request->active;
  //       $asset_category->asset_category_id = $request->assetcategory['id'];
  //       $asset_category->asset_manufacturer_id = $request->assetmanufacturer['id'];
  //       $asset_category->save();

  //       }

		// $assetcategory = EmployeeImprest::find($request->assetcategory['id'])->makeHidden(['created_at','updated_at','user_id'])->toArray();
		// $assetmanufacturer = EmployeeImprest::find($request->assetmanufacturer['id'])->makeHidden(['created_at','updated_at','user_id'])->toArray();

      

  //       return ['assetcategory'=>$assetcategory,'assetmanufacturer'=>$assetmanufacturer];
       
        
  //   }


    // public function get(){
    //    return EmployeeImprest::where('active',1)->get()->makeHidden(['user_id','created_at','updated_at'])->toArray();
    // }

    // public function getVehicleModels(){
    //    $assetmodel = EmployeeImprest::where('asset_category_id',4)->get()->makeHidden(['user_id','created_at','updated_at'])->toArray();

    //    return $assetmodel;
    // }
}
