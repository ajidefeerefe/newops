<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MaterialAndServiceType;

class MaterialAndServiceTypeController extends Controller
{
    public function get()
    {
    	 return MaterialAndServiceType::all()->makeHidden(['created_at','updated_at'])->toArray();
    }
}
