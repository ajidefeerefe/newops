<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MaterialAndService;
use App\MaterialAndServiceCategory;
use App\MaterialAndServiceType;

class MaterialAndServiceController extends Controller
{
     public function add(Request $r)
    {
    	$r->validate([
            'name' => 'required',
    	    'type' => 'required',
    	    'category' => 'required',

    	    
    	]);

    	
        if (MaterialAndService::where('name', '=', $r->name)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
            
           $material_and_service = new MaterialAndService;
           $material_and_service->name = $r->name;
           $material_and_service->material_and_services_category_id = $r->category;
           $material_and_service->service_type_id = $r->type;
           $material_and_service->active = 1;
           $material_and_service->save();

        }
    	return $material_and_service->where('id',$material_and_service->id)->with('category:id,name','type:id,name')->get()->makeHidden(['created_at','updated_at'])->toArray();
    }

    public function show(Request $request)
    {

    	$columns = ['id','name','material_and_services_category_id','service_type_id'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

  


    	$query = MaterialAndService::select('id', 'name','material_and_services_category_id','service_type_id','active')->with('category:id,name','type:id,name')->orderBy($columns[$column], $dir);




    	if($searchValue){

    	    $query->where('name', 'like', '%' . $searchValue . '%')
    	    		->orWhere('id', 'like', '%' .$searchValue . '%')
    	    		->orWhereHas('category', function($q) use ($searchValue){
    	            	$q->where('name',  'like', '%' . $searchValue . '%');
    	        	})->orWhereHas('type', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->get();

    	    
    	}



    	$material_and_service = $query->paginate($length);

    	return ['data' => $material_and_service, 'draw' => $request->draw];
    }



    public function edit(Request $request)
    {
    	
      if (MaterialAndService::where('name', '=', $request->name)->where('material_and_services_category_id', '=', $request->category['id'])->where('service_type_id', '=', $request->type['id'])->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
        $material_and_service = MaterialAndService::find($request->id);
        $material_and_service->name = $request->name;
        $material_and_service->material_and_services_category_id = $request->category['id'];
        $material_and_service->service_type_id = $request->type['id'];
        $material_and_service->save();

        }

        $material_and_service_cat = MaterialAndServiceCategory::find($request->category['id']);
        $material_and_service_type = MaterialAndServiceType::find($request->type['id']);

        return ['type'=> $material_and_service_type->makeHidden(['created_at','updated_at'])->toArray(),'category'=> $material_and_service_cat->makeHidden(['created_at','updated_at'])->toArray()];
       
        
    }

    public function getS(){
        return MaterialAndService::where('service_type_id',2)->get()->makeHidden(['created_at','updated_at'])->toArray();
    }
    public function getM(){
        return MaterialAndService::where('service_type_id',1)->get()->makeHidden(['created_at','updated_at'])->toArray();
    }
    public function getAll(){
        return MaterialAndService::all()->makeHidden(['created_at','updated_at'])->toArray();
    }

    // public function getMaterial(Request $request){
    // $query = MaterialAndService::select('id', 'name')->with('',)->where('name', 'like', '%' .$request->data . '%')->where('service_type_id',1)->get();

    // return $query;
    //      // return MaterialAndServiceCategory::where('material_and_services_category_id',1)->makeHidden(['created_at','updated_at'])->toArray();
    // }  


    public function getService(Request $request){
    $query = MaterialAndService::select('id', 'name')->where('name', 'like', '%' .$request->data . '%')->where('service_type_id',2)->get();
    return $query;
         // return MaterialAndServiceCategory::where('material_and_services_category_id',2)->makeHidden(['created_at','updated_at'])->toArray();
    }

    
    public function categoryMaterials(Request $r){
        return MaterialAndService::where('material_and_services_category_id', $r->category_id)->where('active',1)->where('service_type_id',1)->get()->toArray();
    }
    public function categoryLabours(Request $r){
        return MaterialAndService::where('material_and_services_category_id', $r->category_id)->where('active',1)->where('service_type_id',2)->get()->toArray();
    }

    public function search(Request $request){
        $query = MaterialAndService::select('id','name')->where('name', 'like', '%' .$request->data . '%')->where('service_type_id',1)->get();
        return $query;
    }
}
