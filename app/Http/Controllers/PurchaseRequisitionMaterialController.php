<?php

/*

	StatusForPR - 0 == New - The PR is a totally fresh one and unattended to
	StatusForPR - 1 == Reviewed - Attended to (Either approved or declined)
	StatusForPR - 2 == Extra - Additional materials added to this PR

	Status - 0 == New
	Status - 1 == Approved
	Status - 2 == Declined
	Status - 3 == Delivered

	StatusForPR - The status that controls what status is displayed for the parent PR
	Status-  This controls the status seen for each individual material
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PurchaseRequisitionMaterial;
use App\ProjectMaterial;
use App\ExpenseRecord;
use App\Subscription;
use App\Mail\extraRequisitionMaterial;
use App\Mail\approvedRequisition;
use App\Mail\vendorAssignment;
use Illuminate\Mail\Mailer;
use DB;

class PurchaseRequisitionMaterialController extends Controller
{
    public function show(Request $request){//Used for the purchase requisition
	    // dd($request->all());
	    $columns = ['id','vendor_id','project_material_id','quantity','quantity_delivered','quantity_left','status'];

	    $length = $request->params['length'];
	    $column = $request->params['column'];
	    $dir = $request->params['dir'];
	    $searchValue = $request->params['search'];

	    
	    //WIlliam
	    $query = PurchaseRequisitionMaterial::with('vendor.vendorservice','project_material.material_and_service','expense_records_amount','purchase_requisition.user','purchase_requisition.project.projectstaffallocations.user')->where('purchase_requisition_id',$request->id)->orderBy($columns[$column], $dir);

	    if($searchValue)
	    {
	        $query->where('id', 'like', '%' . $searchValue . '%')
	        	  ->orWhere('status', 'like', '%' . $searchValue . '%')
	        	  ->orWhere('quantity', 'like', '%' . $searchValue . '%')
	        	  ->orWhere('quantity_delivered', 'like', '%' . $searchValue . '%')
	              ->orWhereHas('vendor', function($q) use ($searchValue){
	                  $q->where('name',  'like', '%' . $searchValue . '%');
	              })->orWhereHas('project_material', function($q) use ($searchValue){
	                  $q->whereHas('material_and_service', function($q) use ($searchValue){
	                      $q->where('name',  'like', '%' . $searchValue . '%');
	                  });
	              })->get();
	    }


	    $purchase_requisition_materials = $query->paginate($length);
	    // dd($projects);
	    return ['data' => $purchase_requisition_materials, 'draw' => $request->params['draw']];
	}

	// WIlliam
	public function add(Request $r, Mailer $mail){
		// dd($r->all());
		if($r->materials == [])
		{//Check if the data is null
		  return response(['error'=>"No attached material"], 404);
		} 
		$pr_id = $r->requisition_id;

		$container = '';
		if ($r->in_budget) {

	      foreach ($r->materials as $key => $value)
	      {

	        $pr_material = new PurchaseRequisitionMaterial;
	        $pr_material->purchase_requisition_id = $pr_id;
	        $pr_material->project_material_id = $value['project_material_id'];
	        $pr_material->quantity = $value['quantity'];
	        $pr_material->save();
	        //Prep for mail
	        $name = ucfirst($value['material_name']).' - ';
	        $qty = 'Quantity = '.$value['quantity'].';'." \r\n";
	        $container .=  $name . $qty;   

	      }

	    }
	    else
	    {
	      foreach ($r->materials as $key => $value)
	      {
	        //Create a project material for this material being requested for this particular phase
	        // dd($r->all());
	        $input = [
	          'individual_project_phases_id' => $r->individual_project_phases_id,        
	          'material_and_services_id' => $value['material_and_services_id'],
	          'quantity' => $value['quantity'],
	          'extra' => 1
	        ];
	        $p_id = ProjectMaterial::create($input)->id;

	        //Then Create a purchase requisition material record using the created project material id
	        $pr_material = new PurchaseRequisitionMaterial;
	        $pr_material->purchase_requisition_id = $pr_id;
	        $pr_material->project_material_id = $p_id;
	        $pr_material->quantity = $value['quantity'];
	        $pr_material->save();
	        //Prep for mail
	        $name = ucfirst($value['material_name']).' - ';
	        $qty = 'Quantity = '.$value['quantity'].';'." \r\n";
	        $container .=  $name . $qty; 

	      }
	    }
	    $requester = ucfirst(\Auth::user()->last_name)." ".ucfirst(\Auth::user()->first_name);
	    $extra = $r->in_budget ? "" : "This update contains materials not captured during the initial project costing.";

    	$matched_roles = Subscription::with('role.users')->where('requisition_updates',1)->get();
	    foreach ($matched_roles as $key => $value) {
	      // get all the users having that role
	      $users = $value->role->users;
	      foreach ($users as $key => $user) {

	        //get user details (first_name.last_name and email)
	        $email = $user->email;
	        // dd($email);
	        try{
	          // dd($email);
	           $mail->to($email)->send(new extraRequisitionMaterial("Notification from Operations Manager",$requester,$container,$pr_id,$r->project,$extra));
	         } catch (\Swift_TransportException $e) {
	            return $e;

	        }
	        
	      }
	      
	    }
	    return "Ok";
	}

	// William
	public function approve(Request $r, Mailer $mail){
	    // dd($r->all());

		PurchaseRequisitionMaterial::where('id', $r->id)->update(['status' => 1,'approved_by'=> \Auth::user()->id, 'status_for_pr' => 1]); //Approve Project Phase


		// Set mail values
		// Get the details of the material being approved
        $name = ucfirst($r->material['project_material']['material_and_service']['name']).' - ';
        $qty = 'Quantity = '.$r->material['quantity'].';';
        $material =  $name . $qty;
        $project = $r->project['name'];
        $approver = ucfirst(\Auth::user()->last_name)." ".ucfirst(\Auth::user()->first_name);

		$requester = ucfirst($r->user['last_name'])." ".ucfirst($r->user['first_name']);
	   	$allocated_staff = $r->staff_allocations;

	    $matched_roles = Subscription::with('role.users')->where('approve_requisition',1)->get();

	    // Send the requester a mail
	    $email = $r->user['email'];
	    $message = "Your purchase requisition with ID: $r->id on the ".ucfirst($project)." project with material";
	    try{
           $mail->to($email)->send(new approvedRequisition("Notification from Operations Manager",$message,$material,$approver));

         } catch (\Swift_TransportException $e) {
            return $e;

        }

        // Send all staff allocated to the project having this PR a mail
        $message = "A purchase requisition with ID: $r->id made by $requester on the ".ucfirst($project)." project with materials";
        foreach ($allocated_staff as $key => $value) {
	      	$user = $value['user'];

	        //get user details (first_name.last_name and email)
	        $email = $user['email'];
	        try{
           		$mail->to($email)->send(new approvedRequisition("Notification from Operations Manager",$message,$material,$approver));
	         } catch (\Swift_TransportException $e) {
	            return $e;

	        }
	        	      
	    }

	    // Send users with the right subscription an email
        $message = "A purchase requisition with ID: $r->id made by $requester on the ".ucfirst($project)." project with materials";
	    foreach ($matched_roles as $key => $value) {
	      // get all the users having that role
	      $users = $value->role->users;
	      foreach ($users as $key => $user) {

	        //get user details (first_name.last_name and email)
	        $email = $user->email;
	        // dd($email);
	        try{
	          // dd($email);
           		$mail->to($email)->send(new approvedRequisition("Notification from Operations Manager",$message,$material,$approver));
	          
	         } catch (\Swift_TransportException $e) {
	            return $e;

	        }
	        
	      }
	      
	    }
	    return "okay";
	}

	public function decline(Request $r){
	    // dd($r->all());
		PurchaseRequisitionMaterial::where('id', $r->id)->update(['status' => 2,'declined_by'=> \Auth::user()->id, 'status_for_pr' => 1]); //Approve Project Phase
	    return "okay";
	}

	public function delivered(Request $r){
	    // dd($r->all());
		PurchaseRequisitionMaterial::where('id', $r->id)->update(['status' => 3,'marked_delivered_by'=> \Auth::user()->id,'quantity_delivered' => DB::raw("quantity_delivered + $r->quantity"), 'status_for_pr' => 1]); //Mark delivered
	    return "okay";
	}

	public function add_payment(Request $r){
		// dd($r->all());
		$payment = new ExpenseRecord;
		$payment->bank_account_id = $r->bank;
		$payment->chart_of_account_id = $r->chart_of_account;
		$payment->date = $r->date;
		$payment->purchase_requisition_material_id = $r->material_id;
		$payment->project_id = $r->project_id;//Needed for reports Although it can still be gotten via relationships T\_( - - )_/T
		 $payment->created_by = \Auth::user()->id;
		$payment->amount = $r->usedcost;
		$payment->individual_project_phases_id = $r->individual_project_phases_id;
		$payment->save();
		return "";
	}

	public function assign_vendor(Request $r, Mailer $mail){
	    // dd($r->all());
		PurchaseRequisitionMaterial::where('id', $r->requisition_material_id)->update(['vendor_id' => $r->vendor,'current_vendor_price'=> $r->current_vendor_price]); //Assign a vendor

		// Set mail values
		// Get the details of the material being approved
        $name = ucfirst($r->data["material"]['project_material']['material_and_service']['name']).' - ';
        $qty = 'Quantity = '.$r->data["material"]['quantity'].';';
        $material =  $name . $qty;
        $project = $r->data["project"]['name'];
        $assigner = ucfirst(\Auth::user()->last_name)." ".ucfirst(\Auth::user()->first_name);
        $vendorName = $r->vendorName;

		$requester = ucfirst($r->data["user"]['last_name'])." ".ucfirst($r->data["user"]['first_name']);
	   	$allocated_staff = $r->data["staff_allocations"];

	    $matched_roles = Subscription::with('role.users')->where('approve_requisition',1)->get();

	    // Send the requester a mail
	    $email = $r->data["user"]['email'];
	    $vendorMsg = "$r->vendorName - Price = NGN ".number_format($r->current_vendor_price);
	    $message = "Has been assigned to your purchase requisition with ID: $r->requisition_material_id on the ".ucfirst($project)." project with material";
	    try{
           $mail->to($email)->send(new vendorAssignment("Notification from Operations Manager",$message,$material,$assigner,$vendorMsg));

         } catch (\Swift_TransportException $e) {
            $e;

        }

        // Send all staff allocated to the project having this PR a mail
        $message = "Has been assigned to the purchase requisition with ID: $r->requisition_material_id made by $requester on the ".ucfirst($project)." project with materials";
        foreach ($allocated_staff as $key => $value) {
	      	$user = $value['user'];

	        //get user details (first_name.last_name and email)
	        $email = $user['email'];
	        try{
           		$mail->to($email)->send(new vendorAssignment("Notification from Operations Manager",$message,$material,$assigner,$vendorMsg));
	         } catch (\Swift_TransportException $e) {
	            $e;

	        }
	        	      
	    }

	    // Send users with the right subscription an email
        $message = "A purchase requisition with ID: $r->requisition_material_id made by $requester on the ".ucfirst($project)." project with materials";
	    foreach ($matched_roles as $key => $value) {
	      // get all the users having that role
	      $users = $value->role->users;
	      foreach ($users as $key => $user) {

	        //get user details (first_name.last_name and email)
	        $email = $user->email;
	        // dd($email);
	        try{
	          // dd($email);
           		$mail->to($email)->send(new vendorAssignment("Notification from Operations Manager",$message,$material,$assigner,$vendorMsg));
	          
	         } catch (\Swift_TransportException $e) {
	            $e;

	        }
	        
	      }
	      
	    }
	    return "okay";
	}

	// William
	public function ProjectRequisitions(Request $request)
    {
        // dd(date('Y-m-d',strtotime($request->start_date)));
       $columns = ['purchase_requisition_id','date','amount'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;

  
        $query = PurchaseRequisitionMaterial::with('expense_records','expense_records_amount','vendor:id,name','project_material.costing_day.costing_week:id,start_date,end_date','project_material.material_and_service:id,name','project_material.individualprojectphase.projectphase:id,name','project_material.individualprojectphase.project:id,name,type')->orderBy($columns[$column], $dir)->where('created_at','>=',date('Y-m-d',strtotime($request->start_date)))->where('created_at','<=',date('Y-m-d',strtotime($request->end_date)))
        	->whereHas('project_material', function($q) use ($request){
                $q->whereHas('individualprojectphase', function($q) use ($request){
                    $q->whereHas('project', function($q) use ($request){
                        $q->where('id', $request->id);
                    });
                });
            });
                                    
        if($searchValue){

	            $expenses = PurchaseRequisitionMaterial::with('expense_records','expense_records_amount','vendor:id,name','project_material.costing_day.costing_week:id,start_date,end_date','project_material.material_and_service:id,name','project_material.individualprojectphase.projectphase:id,name','project_material.individualprojectphase.project:id,name,type')->orderBy($columns[$column], $dir)->where('created_at','>=',date('Y-m-d',strtotime($request->start_date)))->where('created_at','<=',date('Y-m-d',strtotime($request->end_date)))
		        	->whereHas('project_material', function($q) use ($request){
		                $q->whereHas('individualprojectphase', function($q) use ($request){
		                    $q->whereHas('project', function($q) use ($request){
		                        $q->where('id', $request->id);
		                    });
		                });
		            })->where(function ($q) use ($searchValue)
                {
                    $q->where('purchase_requisition_id', 'like', '%' .$searchValue . '%')
                    ->orWhereHas('expense_records_amount', function($q) use ($searchValue){
                        $q->havingRaw('SUM(amount) like ? ',  ['%'.$searchValue.'%'] );
                    })
                    ->orWhereHas('vendor', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })
                    ->orWhereHas('project_material', function($q) use ($searchValue){
                        $q->whereHas('material_and_service', function($q) use ($searchValue){
                            $q->where('name',  'like', '%' . $searchValue . '%');
                        });
                        $q->orWhereHas('individualprojectphase', function($q) use ($searchValue){
                            $q->whereHas('project', function($q) use ($searchValue){
	                            $q->where('name',  'like', '%' . $searchValue . '%');
	                        })
	                        ->orWhereHas('projectphase', function($q) use ($searchValue){
	                            $q->where('name',  'like', '%' . $searchValue . '%');
	                        });
                        });
                    });
                })
                ->orderBy('id','DESC');
            $response = $expenses->paginate($length);
              
        }else{
            $response = $query->paginate($length);
        }


        return ['data' => $response, 'draw' => $request->draw];
        
    }


    public function DateRequisitions(Request $request)
    {
        // dd(date('Y-m-d',strtotime($request->start_date)));
       $columns = ['purchase_requisition_id','date','amount'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;

  
        $query = PurchaseRequisitionMaterial::with('expense_records','expense_records_amount','vendor:id,name','project_material.costing_day.costing_week:id,start_date,end_date','project_material.material_and_service:id,name','project_material.individualprojectphase.projectphase:id,name','project_material.individualprojectphase.project:id,name,type')->orderBy($columns[$column], $dir)->where('created_at','>=',date('Y-m-d',strtotime($request->start_date)))->where('created_at','<=',date('Y-m-d',strtotime($request->end_date)));
                                    
        if($searchValue){

	        $expenses = PurchaseRequisitionMaterial::with('expense_records','expense_records_amount','vendor:id,name','project_material.costing_day.costing_week:id,start_date,end_date','project_material.material_and_service:id,name','project_material.individualprojectphase.projectphase:id,name','project_material.individualprojectphase.project:id,name,type')->orderBy($columns[$column], $dir)->where('created_at','>=',date('Y-m-d',strtotime($request->start_date)))->where('created_at','<=',date('Y-m-d',strtotime($request->end_date)))
	        	->where(function ($q) use ($searchValue)
	            {
	                $q->where('purchase_requisition_id', 'like', '%' .$searchValue . '%')
	                ->orWhereHas('expense_records_amount', function($q) use ($searchValue){
	                    $q->havingRaw('SUM(amount) like ? ',  ['%'.$searchValue.'%'] );
	                })
	                ->orWhereHas('vendor', function($q) use ($searchValue){
	                    $q->where('name',  'like', '%' . $searchValue . '%');
	                })
	                ->orWhereHas('project_material', function($q) use ($searchValue){
	                    $q->whereHas('material_and_service', function($q) use ($searchValue){
	                        $q->where('name',  'like', '%' . $searchValue . '%');
	                    });
	                    $q->orWhereHas('individualprojectphase', function($q) use ($searchValue){
	                        $q->whereHas('project', function($q) use ($searchValue){
	                            $q->where('name',  'like', '%' . $searchValue . '%');
	                        })
	                        ->orWhereHas('projectphase', function($q) use ($searchValue){
	                            $q->where('name',  'like', '%' . $searchValue . '%');
	                        });
	                    });
	                });
	            })
	        	->orderBy('id','DESC');
	    	$response = $expenses->paginate($length);
	      
	        }else{
	            $response = $query->paginate($length);
	        }
	        return ['data' => $response, 'draw' => $request->draw];
        
    } 

    public function MaterialRequisitions(Request $request)
    {
        // dd(date('Y-m-d m:i:s',strtotime($request->end_date)));
       $columns = ['purchase_requisition_id','date','amount'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;

  
        $query = PurchaseRequisitionMaterial::with('expense_records','expense_records_amount','vendor:id,name','project_material.costing_day.costing_week:id,start_date,end_date','project_material.material_and_service:id,name','project_material.individualprojectphase.projectphase:id,name','project_material.individualprojectphase.project:id,name,type')->orderBy($columns[$column], $dir)->where('created_at','>=',date('Y-m-d',strtotime($request->start_date)))->where('created_at','<=',date('Y-m-d',strtotime($request->end_date)))
        	->whereHas('project_material', function($q) use($request){
        		$q->whereHas('material_and_service', function($q) use ($request){
        			$q->where('id', $request->id);
        		});
        	});
                                    
        if($searchValue){

	        $expenses = PurchaseRequisitionMaterial::with('expense_records','expense_records_amount','vendor:id,name','project_material.costing_day.costing_week:id,start_date,end_date','project_material.material_and_service:id,name','project_material.individualprojectphase.projectphase:id,name','project_material.individualprojectphase.project:id,name,type')->orderBy($columns[$column], $dir)->where('created_at','>=',date('Y-m-d',strtotime($request->start_date)))->where('created_at','<=',date('Y-m-d',strtotime($request->end_date)))
	        	->whereHas('project_material', function($q) use($request){
	        		$q->whereHas('material_and_service', function($q) use ($request){
	        			$q->where('id', $request->id);
	        		});
	        	})
	        	->where(function ($q) use ($searchValue)
	            {
	                $q->where('purchase_requisition_id', 'like', '%' .$searchValue . '%')
	                ->orWhereHas('expense_records_amount', function($q) use ($searchValue){
	                    $q->havingRaw('SUM(amount) like ? ',  ['%'.$searchValue.'%'] );
	                })
	                ->orWhereHas('vendor', function($q) use ($searchValue){
	                    $q->where('name',  'like', '%' . $searchValue . '%');
	                })
	                ->orWhereHas('project_material', function($q) use ($searchValue){
	                    $q->whereHas('material_and_service', function($q) use ($searchValue){
	                        $q->where('name',  'like', '%' . $searchValue . '%');
	                    });
	                    $q->orWhereHas('individualprojectphase', function($q) use ($searchValue){
	                        $q->whereHas('project', function($q) use ($searchValue){
	                            $q->where('name',  'like', '%' . $searchValue . '%');
	                        })
	                        ->orWhereHas('projectphase', function($q) use ($searchValue){
	                            $q->where('name',  'like', '%' . $searchValue . '%');
	                        });
	                    });
	                });
	            })
	        	->orderBy('id','DESC');
	    	$response = $expenses->paginate($length);
	      
	        }else{
	            $response = $query->paginate($length);
	        }
	        return ['data' => $response, 'draw' => $request->draw];
        
    } 

}
