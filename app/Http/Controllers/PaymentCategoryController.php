<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PaymentCategory;

class PaymentCategoryController extends Controller
{
    public function add(Request $r)
    {
    	$r->validate([
    	    'name' => 'required',
    	    'description' => 'required',
    	]);

    	// $earn = Earn::firstOrCreate(['name' => $r->name]);
        if (PaymentCategory::where('name', '=', $r->name)->exists()) {
            return response(['error'=>'This Payment Category already exists'], 404);
        } else {
           $PaymentCategory = PaymentCategory::create($r->only(['name','description']));
        }
    	return $PaymentCategory->makeHidden(['created_at','updated_at'])->toArray();
    }

    public function show(Request $request)
    {

    	$columns = ['id','name','description'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

    	$query = PaymentCategory::select('id', 'name','description')->orderBy($columns[$column], $dir);


    	if($searchValue){
    	    $query->where(function($query) use ($searchValue) {
    	        $query->where('name', 'like', '%' . $searchValue . '%')
    	        ->orWhere('id', 'like', '%' .$searchValue . '%')
    	        ->orWhere('description', 'like', '%' .$searchValue . '%');
    	    });
    	}


    	$colors = $query->paginate($length);

    	return ['data' => $colors, 'draw' => $request->draw];
    }



    public function edit(Request $request)
    {
        if (PaymentCategory::where('name', '=', $request->name)->exists()) {
            return response(['error'=>'Payment Category already exist'], 404);
        } else {
           $color = PaymentCategory::find($request->id);
            $color->name = $request->name;
            $color->description = $request->description;
            $color->save();
        }
        
        return $color;
    }

    public function getAll()
    {
        return PaymentCategory::all()->makeHidden(['created_at','updated_at'])->toArray();

    }
}
