<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Deduction;

class DeductionController extends Controller
{
    public function update(Request $r){

       foreach ($r->deductions as $key => $value) 
        {
          if($value['deducts']['id'] == '' or $value['amount']==''){//Check if any phase field was left blank
          return response(['error'=>"No field can be empty"], 404);
          }
          
        }
       


       $deductions = $r->deductions;

       
        
        Deduction::where('user_id',$r->user_id)->where('payroll_id',$r->payroll_id)->delete();//Clear all the record before reinserting
    	if(count($deductions) > 0){
        foreach ($deductions as $key => $value) {

                  // return $earnings[$key]['amount'];
                if(isset($deductions[$key]['id'])){
                
                 $earn = new Deduction;
                  $earn->id = $value['id'];
                  $earn->deduct_id = $value['deducts']['id'];
                  $earn->amount = $value['amount'];
                  $earn->payroll_id = $value['payroll_id'];
                  $earn->user_id = $value['user_id'];
                  $earn->save();
                 
                }else{
                // return 'w';
                $earn1 = new Deduction;
               	$earn1->payroll_id = $r->payroll_id;
                $earn1->deduct_id = $value['deducts']['id'];
               	$earn1->user_id = $r->user_id;
               	$earn1->amount = $value['amount'];
               	$earn1->save();
               }
                
               	 
            }
            return Deduction::where('user_id',$r->user_id)->where('payroll_id',$r->payroll_id)->with('deducts:id,name')->get()->makeHidden(['created_at','updated_at'])->toArray();
        }else{

        	return 0;
            // return "Done";
        }
       
        
      }
}
