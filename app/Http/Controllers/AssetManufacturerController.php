<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AssetManufacturer;
use App\AssetCategory;

class AssetManufacturerController extends Controller
{
     public function add(Request $r)
    {
    	$r->validate([
            'name' => 'required',
    	    'active' => 'required',
    	    'assetcategory' => 'required',

    	    
    	]);

    	
        if (AssetManufacturer::where('name', '=', $r->name)->where('asset_category_id',$r->assetcategory)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
            
           $asset_manufacturer = new AssetManufacturer;
           $asset_manufacturer->name = $r->name;
           $asset_manufacturer->active = $r->active;
           $asset_manufacturer->user_id = \Auth::user()->id;
           $asset_manufacturer->asset_category_id = $r->assetcategory;
           
           $asset_manufacturer->save();

        }
    	return $asset_manufacturer->where('id',$asset_manufacturer->id)->with('assetcategory')->get()->makeHidden(['user_id','created_at','updated_at'])->toArray();
    }

    public function show(Request $request)
    {

    	$columns = ['id','name','asset_category_id'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

  

    	$query = AssetManufacturer::select('id', 'name','asset_category_id','active')->with(['assetcategory' => function($query) {
    	    $query->select('id','name');
    	}])->orderBy($columns[$column], $dir);




    	if($searchValue){

    	    $query->where('name', 'like', '%' . $searchValue . '%')
    	    		->orWhere('id', 'like', '%' .$searchValue . '%')
    	    		->orWhereHas('assetcategory', function($q) use ($searchValue){
    	            	$q->where('name',  'like', '%' . $searchValue . '%');
    	        	})->get();

    	    
    	}



    	$asset_categories = $query->paginate($length);

    	return ['data' => $asset_categories, 'draw' => $request->draw];
    }



    public function edit(Request $request)
    {
    	
      if (AssetManufacturer::where('name', '=', $request->name)->where('active', '=', $request->active)->where('asset_category_id', '=', $request->assetcategory['id'])->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
        $asset_category = AssetManufacturer::find($request->id);
        $asset_category->name = $request->name;
        $asset_category->active = $request->active;
        $asset_category->asset_category_id = $request->assetcategory['id'];
        $asset_category->save();

        }

        $assetcategory = AssetCategory::find($request->assetcategory['id']);

        return $assetcategory->makeHidden(['created_at','updated_at','user_id'])->toArray();
       
        
    }
    public function get(Request $request)
    {
        return AssetManufacturer::where('active',1)->get()->makeHidden(['user_id','created_at','updated_at'])->toArray();
    }

}
