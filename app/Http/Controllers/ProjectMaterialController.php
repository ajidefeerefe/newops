<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProjectMaterial;
use App\IndividualProjectPhases;
use App\CostingWeek;

class ProjectMaterialController extends Controller
{
    public function update(Request $r){

      // dd($r->all());
      if($r->materials == [])
      {//Check if the data is null
        return response(['error'=>"Nothing to update"], 404);
      } 

      $individual_project_phases_id = $r->materials[0]['individual_project_phases_id'];

      foreach ($r->materials as $key => $value) 
        {
          if($value['material_and_services_id']=='' or $value['quantity']=='' or $value['amount']==''){//Check if any phase field was left blank
          return response(['error'=>"No field can be empty"], 404);
          }
        }
       
        
      ProjectMaterial::where('individual_project_phases_id', '=', $value['individual_project_phases_id'])->where('extra',0)->delete();//Clear all the records captured in the budgetd excluding extra ones before reinserting
      foreach ($r->materials as $key => $value) {
        if($value['extra'] == 0){//Create this one if it was initially in the budget

          $p_material = new ProjectMaterial;
          $amount = str_replace(',', '', $value['amount']);//Remove the commas before inserting
          $p_material->id = $value['id'];
          $p_material->individual_project_phases_id = $value['individual_project_phases_id'];
          $p_material->material_and_services_id = $value['material_and_services_id'];
          $p_material->quantity = $value['quantity'];
          $p_material->amount = $amount;
          $p_material->save(); 
        }
          
      };
      IndividualProjectPhases::where('id', $individual_project_phases_id)->update(['status' => 5]); //Set the project as needing review 

      return ProjectMaterial::with('material_and_service:id,name')->where('individual_project_phases_id', '=', $value['individual_project_phases_id'])->get()->makeHidden(['created_at','updated_at'])->toArray();

      
      // return ProjectMaterial::with('projectphase:id,name')->get()->makeHidden(['created_at','updated_at'])->toArray();
  }

  public function add(Request $r){
    // dd($r);
    $individual_project_phases_id = $r->materials[0]['individual_project_phases_id'];
    foreach ($r->materials as $key => $value) 
        {
          if($value['material_and_services_id']=='' or $value['quantity']=='' or $value['amount']==''){//Check if any phase field was left blank
          return response(['error'=>"No field can be empty"], 404);
          }
          if (ProjectMaterial::where('individual_project_phases_id', '=', $value['individual_project_phases_id'])->where('material_and_services_id',$value['material_and_services_id'])->exists()) {
              return response(['error'=>'Duplicates Not Permitted'], 404);
          }
        }

        foreach ($r->materials as $key => $value) {

          $amount = str_replace(',', '', $value['amount']);//Remove the commas before inserting
          $p_material = new ProjectMaterial;
          $p_material->individual_project_phases_id = $value['individual_project_phases_id'];
          $p_material->material_and_services_id = $value['material_and_services_id'];
          $p_material->quantity = $value['quantity'];
          $p_material->amount = $amount;
          $p_material->save();        

         }

      IndividualProjectPhases::where('id', $individual_project_phases_id)->update(['status' => 5]); //Set the project as needing review 
      return ProjectMaterial::with('material_and_service:id,name')->where('individual_project_phases_id', '=', $value['individual_project_phases_id'])->get()->makeHidden(['created_at','updated_at'])->toArray();


  }

  public function delete(Request $r){
        // dd($r->all());

      ProjectMaterial::where('id',$r->id)->delete();

      IndividualProjectPhases::where('id', $r->individual_project_phases_id)->update(['status' => 5]); //Set the project as needing review 

      return ProjectMaterial::with('material_and_service:id,name')->where('individual_project_phases_id', '=', $r->individual_project_phases_id)->get()->makeHidden(['created_at','updated_at'])->toArray();

    }

    public function updateForOperationalProjects(Request $r){

      // dd($r->all());
      if($r->materials == [])
      {//Check if the data is null
        return response(['error'=>"Nothing to update"], 404);
      }
      

      foreach ($r->materials as $key => $value) 
        {
          if($value['material_and_services_id'] =='' or $value['costing_day_id'] =='' or $value['quantity']=='' or $value['amount']==''){//Check if any phase field was left blank
          return response(['error'=>"No field can be empty"], 404);
          }
        }
       
      $costing_week_id = $r->costing_week_id;
      $individual_project_phases_id = $r->materials[0]['individual_project_phases_id'];
      
      ProjectMaterial::where('individual_project_phases_id', '=', $individual_project_phases_id)->delete();//Clear all the record before reinserting

      foreach ($r->materials as $key => $value) {
          $p_material = new ProjectMaterial;
          $amount = str_replace(',', '', $value['amount']);//Remove the commas before inserting ;)
          $p_material->id = $value['id'];
          $p_material->costing_day_id = $value['costing_day_id'];
          $p_material->individual_project_phases_id = $value['individual_project_phases_id'];
          $p_material->material_and_services_id = $value['material_and_services_id'];
          $p_material->quantity = $value['quantity'];
          $p_material->amount = $amount;
          $p_material->save(); 
          };
      CostingWeek::where('id', $costing_week_id)->update(['status' => 5]); //Set the project as needing review 

      return CostingWeek::with('costing_day.project_material','costing_day.project_labour')->where('id',$costing_week_id)->get()->makeHidden(['created_at','updated_at'])->toArray();
      
      // return ProjectMaterial::with('projectphase:id,name')->get()->makeHidden(['created_at','updated_at'])->toArray();
  }


    public function addForOperationalProjects(Request $r){
    // dd($r);
    $costing_week_id = $r->materials[0]['costing_week_id'];

    foreach ($r->materials as $key => $value) 
        {
          if($value['costing_day_id']=='' or $value['material_and_services_id']=='' or $value['quantity']=='' or $value['amount']==''){//Check if any phase field was left blank
          return response(['error'=>"No field can be empty"], 404);
          }
          if (ProjectMaterial::where('costing_day_id', '=', $value['costing_day_id'])->where('material_and_services_id',$value['material_and_services_id'])->exists()) {
              return response(['error'=>'Duplicates Not Permitted'], 404);
          }
        }

        foreach ($r->materials as $key => $value) {

          $amount = str_replace(',', '', $value['amount']);//Remove the commas before inserting
          $p_material = new ProjectMaterial;
          $p_material->costing_day_id = $value['costing_day_id'];
          $p_material->individual_project_phases_id = $value['individual_project_phases_id'];
          $p_material->material_and_services_id = $value['material_and_services_id'];
          $p_material->quantity = $value['quantity'];
          $p_material->amount = $amount;
          $p_material->save();        

         }

        CostingWeek::where('id', $costing_week_id)->update(['status' => 5]); //Set the project as needing review 

       return CostingWeek::with('individualprojectphase.phase_material','costing_day.project_material','costing_day.project_labour')->where('id',$costing_week_id)->get()->makeHidden(['created_at','updated_at'])->toArray();

    }

    public function deleteForOperationalProjects(Request $r){
        // dd($r->all());

      ProjectMaterial::where('id',$r->id)->delete();

      CostingWeek::where('id', $r->costing_day_id)->update(['status' => 5]); //Set the project as needing review 

      return 'Okay';
    }

    

}
