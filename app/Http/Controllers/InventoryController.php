<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inventory;
class InventoryController extends Controller
{
      public function add(Request $r)
    {
    	
    	$r->validate([
            
    	    'location' => 'required',
    	    'material' => 'required',

    	    
    	]);

    	
        if (Inventory::where('inventory_location_id', '=', $r->location)->where('materrial_and_service_id',$r->material)->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
            
           $inventory = new Inventory;
           $inventory->inventory_location_id = $r->location;
           $inventory->materrial_and_service_id = $r->material;
           $inventory->save();

        }
    	return $inventory->where('id',$inventory->id)->with('topup','material:id,name','topup.withdraws','location:id,name')->get()->makeHidden(['created_at','updated_at'])->toArray();
    }

    public function show(Request $request)
    {

    	$columns = ['id','inventory_location_id','materrial_and_service_id'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

  

    	$query = Inventory::select('id', 'inventory_location_id','materrial_and_service_id')->with('topup','material:id,name','topup.withdraws','location:id,name')->orderBy($columns[$column], $dir);




    	if($searchValue){

    	    $query->where('id', 'like', '%' . $searchValue . '%')
    	    		->orWhereHas('material', function($q) use ($searchValue){
    	            	$q->where('name',  'like', '%' . $searchValue . '%');
    	        	})->orWhereHas('topup', function($q) use ($searchValue){
                        $q->where('quantity',  'like', '%' . $searchValue . '%');
                    })->get();

    	    
    	}



    	$asset_categories = $query->paginate($length);

    	return ['data' => $asset_categories, 'draw' => $request->draw];
    }



    public function edit(Request $request)
    {
    	
      if (AssetModel::where('name', '=', $request->name)->where('active', '=', $request->active)->where('asset_category_id', '=', $request->assetcategory['id'])->where('asset_manufacturer_id', '=', $request->assetmanufacturer['id'])->exists()) {
            return response(['error'=>'Data already exist'], 404);
        } else {
        $asset_category = AssetModel::find($request->id);
        $asset_category->name = $request->name;
        $asset_category->active = $request->active;
        $asset_category->asset_category_id = $request->assetcategory['id'];
        $asset_category->asset_manufacturer_id = $request->assetmanufacturer['id'];
        $asset_category->save();

        }

		$assetcategory = AssetCategory::find($request->assetcategory['id'])->makeHidden(['created_at','updated_at','user_id'])->toArray();
		$assetmanufacturer = AssetManufacturer::find($request->assetmanufacturer['id'])->makeHidden(['created_at','updated_at','user_id'])->toArray();

      

        return ['assetcategory'=>$assetcategory,'assetmanufacturer'=>$assetmanufacturer];
       
        
    }


    public function get(){
       return AssetModel::where('active',1)->get()->makeHidden(['user_id','created_at','updated_at'])->toArray();
    }

    public function getVehicleModels(){
       $assetmodel = AssetModel::where('asset_category_id',4)->get()->makeHidden(['user_id','created_at','updated_at'])->toArray();

       return $assetmodel;
    }
}
