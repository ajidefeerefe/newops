<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VendorContract;
use App\ProjectLabour;
use App\ContractAgreement;
use App\ContractMilestone;
use App\ContractDate;
use App\ExpenseRecord;
use App\Mail\approveContract;
use App\Mail\newContract;
use App\Mail\declineContract;
use App\Mail\editContract;

class VendorContractController extends Controller
{
    
     public function add(Request $r)
    {

        // return $r->costing_day_id['id'];
        // return $r->all();
      //gets the projectlabour if it already exist
     
     
    	//create new projectlabour if it isn't in the budget
        if(!$r->in_budget){
            $pl = new ProjectLabour;
            $pl->material_and_services_id = $r->labour;
            $pl->individual_project_phases_id = $r->project_phase;
            if($r->costing_day_id != null){
              $pl->costing_day_id = $r->costing_day_id['id'];
            }
            $pl->quantity = 1;
            $pl->amount = $r->total;
            $pl->extra = 1;
            $pl->save();

        }else{
           $pl_material_id = ProjectLabour::find( $r->labour)->material_and_services_id;
        }

        

    		// return $r->all();
    	 // $amount = str_replace(",", "",$r->amount);
            
           $vc = new VendorContract;
           $vc->vendor_id = $r->vendor;
           if($r->asset != ''){
            $vc->liquid_asset_id = $r->asset;
           }
           //get material_and_service_id if either newly crated or already existing on depending on the data coming from the frontend
           $vc->material_and_service_id = (!$r->in_budget ? $r->labour : $pl_material_id );
           $vc->project_id = $r->project_id;
           $vc->contractor = $r->contractor;
           $vc->individual_project_phase_id = $r->project_phase;
           $vc->project_labour_id = (!$r->in_budget ? $pl->id : $r->labour);
           $vc->amount = str_replace(",", "",$r->total);
           $vc->start_date = $r->start_date;
           $vc->end_date =  $r->end_date;
           $vc->created_by = \Auth::user()->id;
           $vc->notes = $r->notes;
           $vc->status = 0;
           $vc->save();

          foreach ($r->scopes as $key => $value) {
            if($value['description'] != null){
               $ca = new ContractAgreement;
               $ca->vendor_contract_id = $vc->id;
               $ca->name = $value['description'];
               $ca->save();
            }

           }

           foreach ($r->milestones as $key => $value) {
            if($value['description'] != null){
               $cm = new ContractMilestone;
               $cm->vendor_contract_id = $vc->id;
               $cm->name = $value['description'];
               $cm->amount = str_replace(",", "",$value['amount']);
               $cm->save();
            }
           } 

           foreach ($r->dates as $key => $value) {
            if($value['description'] != null){
               $cm = new ContractDate;
               $cm->vendor_contract_id = $vc->id;
               $cm->date = $value['description'];
               $cm->amount = str_replace(",", "",$value['amount']);
               $cm->save();
            }

           }

        return "success";
    	// return $topup->where('id',$topup->id)->with('withdraws')->get()->makeHidden(['created_at','updated_at'])->toArray();
    }

    public function show(Request $request)
    {
    	// return $request->all();

    	$columns = ['id','employee_id','topup_by','amount','topup_date'];

    	$length = $request->length;
    	$column = $request->column;
    	$dir = $request->dir;
    	$searchValue = $request->search;

  

    	$query = VendorContract::where('project_id',$request->id)->select('id','project_id','material_and_service_id','liquid_asset_id','vendor_id','amount','individual_project_phase_id','status','project_labour_id','contractor')->with('expenseRecords','vendor:id,name','material:id,name','project:id,name','individualprojectphase.phase_labour.material_and_service','individualprojectphase.projectphase','projectphase:id,name','asset.assetmodel.assetmanufacturer','projectlabour.material_and_service','agreements','milestones','contractor:id,first_name,last_name')->orderBy($columns[$column], $dir);
	// $query = EmployeeImprest::select('id','employee_id','user_type','created_by')->with('user:id,last_name,first_name','vendor:id,name','topups:id,amount','topup.imprest.withdraws:id,amount')->orderBy($columns[$column], $dir);




    	if($searchValue){

    	    $query->where('name', 'like', '%' . $searchValue . '%')
    	    		->orWhere('id', 'like', '%' .$searchValue . '%')
    	    		->orWhereHas('assetcategory', function($q) use ($searchValue){
    	            	$q->where('name',  'like', '%' . $searchValue . '%');
    	        	})->get();

    	    
    	}



    	$asset_categories = $query->paginate($length);

    	return ['data' => $asset_categories, 'draw' => $request->draw];
    }

    public function getContract(Request $r){
        return VendorContract::with('vendor:id,name','material:id,name','project:id,name','individualprojectphase','projectphase:id,name','asset.assetmodel.assetmanufacturer','agreements','milestones','dates','projectlabour','asset')->where('id',$r->id)->get()->makeHidden(['created_at','updated_at'])->toArray();
    }


    public function pay(Request $request){
        // dd( $request->data[0]['project']);
       $er = new ExpenseRecord;
       $er->project_id = $request->data[0]['project_id'];
       $er->vendor_id =  $request->data[0]['vendor_id'];
       $er->chart_of_account_id = $request->data[0]['chartofaccount'];
       $er->material_and_service_id = $request->data[0]['projectlabour']['material_and_services_id'];
       $er->bank_account_id = $request->data[0]['bankaccount_id'];
       $er->individual_project_phases_id = $request->data[0]['individual_project_phase_id'];
       $er->vendor_contract_id = $request->data[0]['id'];
       $er->amount = $request->data[0]['amount_paid'];
       $er->date = $request->data[0]['payment_date'];
       $er->vendor_guarantor = $request->data[0]['contractor'];
       $er->created_by = \Auth::user()->id;
       $er->save();

       return ExpenseRecord::where('id',$er->id)->get();
    }


    public function approveOrDecline(Request $request){
       return $request->all();
       // return $request->individualprojectphase['projectphase']['name'];
        $vc = VendorContract::find($request->id);
        $vc->status = $request->status;
        $vc->save();


        $project = $request->project['name'];
        $aggreements =  $request->agreements;
        $milestones =  $request->milestones;
        $contractor =  $request->contractor['first_name'].' '.$request->contractor['last_name'];
        $vendor =  $request->vendor['name'];
        $asset =  $request->asset;
        $service =  $request->material;
        $project_phase =  $request->individualprojectphase['projectphase']['name'];
        if($request->status == 1){
          $status = "Approved";
        }else{
          $status = "Declined";
        }
        $amount = $request->amount;




        $matched_roles = Subscription::with('role.users')->where('approve_contract',1)->get();

        foreach ($matched_roles as $key => $value) {
          // get all the users having that role
          $users = $value->role->users;
          foreach ($users as $key => $user) {

            //get user details (first_name.last_name and email)
            $email = $user->email;
            // dd($email);
            try{
              //send email
               $mail->to($email)->send(new approveContract( "Notification from Operations Manager",$project,$aggreements,$milestones,$contractor,$vendor,$asset,$service,$project_phase,$status,$amount));
             } catch (\Swift_TransportException $e) {
                return response(['error'=>$e], 404);

            }
            
          }
          
        }

        return $request->status;
    }


    public function showPayments(Request $request)
    {
        // return $request->all();

        $columns = ['id','project_id','vendor_id','individual_project_phases_id','material_and_service_id','amount','date','vendor_guarantor','created_by'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;

  

        $query = ExpenseRecord::where('vendor_contract_id',$request->id)->select('id','project_id','vendor_id','individual_project_phases_id','material_and_service_id','amount','date','vendor_guarantor','created_by')->with('user','vendor_guarantor','vendor:id,name','material_and_service:id,name','individual_project_phase.project','individual_project_phase.projectphase')->orderBy($columns[$column], $dir);
    // $query = EmployeeImprest::select('id','employee_id','user_type','created_by')->with('user:id,last_name,first_name','vendor:id,name','topups:id,amount','topup.imprest.withdraws:id,amount')->orderBy($columns[$column], $dir);




        if($searchValue){

            $query->where('id', 'like', '%' . $searchValue . '%')
                    ->orWhereHas('assetcategory', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->get();

            
        }



        $asset_categories = $query->paginate($length);

        return ['data' => $asset_categories, 'draw' => $request->draw];
    }



    public function DateRangeContracts(Request $request)//William
    {
        // dd(date('Y-m-d',strtotime($request->start_date)));
       $columns = ['id','project_id','vendor_id','material_and_service_id','amount','start_date'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;

  
        $query = VendorContract::select('id','project_id','material_and_service_id','liquid_asset_id','vendor_id','amount','individual_project_phase_id','status','project_labour_id','contractor')->with('expenseRecords','vendor:id,name','material:id,name','project:id,name','individualprojectphase.phase_labour.material_and_service','individualprojectphase.projectphase','projectphase:id,name','asset.assetmodel.assetmanufacturer','projectlabour.material_and_service')->orderBy($columns[$column], $dir)->where('start_date','>=',date('Y-m-d',strtotime($request->start_date)))->where('start_date','<=',date('Y-m-d',strtotime($request->end_date)));
                                    



        if($searchValue){

            $expenses = VendorContract::select('id','project_id','material_and_service_id','liquid_asset_id','vendor_id','amount','individual_project_phase_id','status','project_labour_id','contractor')->with('expenseRecords','vendor:id,name','material:id,name','project:id,name','individualprojectphase.phase_labour.material_and_service','individualprojectphase.projectphase','projectphase:id,name','asset.assetmodel.assetmanufacturer','projectlabour.material_and_service')->where('start_date','>=',date('Y-m-d',strtotime($request->start_date)))->where('start_date','<=',date('Y-m-d',strtotime($request->end_date)))
                ->where(function ($q) use ($searchValue)
                {
                    $q->where('id', 'like', '%' . $searchValue . '%')
                    ->orWhere('start_date', 'like', '%' .$searchValue . '%')
                    ->orWhere('amount', 'like', '%' .$searchValue . '%')
                    ->orWhereHas('material', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })
                    ->orWhereHas('project', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->orWhereHas('vendor', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->orWhereHas('expenseRecords', function($q) use ($searchValue){
                       $q->havingRaw('SUM(amount) like ? ',  ['%'.$searchValue.'%'] );
                    });
                })
                ->orderBy('id','DESC');
            $response = $expenses->paginate($length);
              
        }else{
            $response = $query->paginate($length);
        }


        return ['data' => $response, 'draw' => $request->draw];
        
    }


    public function ProjectContracts(Request $request)//William
    {
      
       $columns = ['id','project_id','vendor_id','material_and_service_id','amount','start_date'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;

  
        //Costing week isnt useful atm cos v1 doesnt clearly represent the range
        $query = VendorContract::select('id','project_id','material_and_service_id','liquid_asset_id','vendor_id','amount','individual_project_phase_id','status','project_labour_id','contractor')->with('expenseRecords','vendor:id,name','material:id,name','project:id,name','individualprojectphase.phase_labour.material_and_service','individualprojectphase.projectphase','projectphase:id,name','asset.assetmodel.assetmanufacturer','projectlabour.material_and_service')->orderBy($columns[$column], $dir)->where('start_date','>=',date('Y-m-d',strtotime($request->start_date)))->where('start_date','<=',date('Y-m-d',strtotime($request->end_date)))
                                ->where('project_id', $request->id);
                                    



        if($searchValue){

            $expenses = VendorContract::select('id','project_id','material_and_service_id','liquid_asset_id','vendor_id','amount','individual_project_phase_id','status','project_labour_id','contractor')->with('expenseRecords','vendor:id,name','material:id,name','project:id,name','individualprojectphase.phase_labour.material_and_service','individualprojectphase.projectphase','projectphase:id,name','asset.assetmodel.assetmanufacturer','projectlabour.material_and_service')->where('start_date','>=',date('Y-m-d',strtotime($request->start_date)))->where('start_date','<=',date('Y-m-d',strtotime($request->end_date)))
                ->where('project_id', $request->id)
                ->where(function ($q) use ($searchValue)
                {
                    $q->where('id', 'like', '%' . $searchValue . '%')
                    ->orWhere('start_date', 'like', '%' .$searchValue . '%')
                    ->orWhere('amount', 'like', '%' .$searchValue . '%')
                    ->orWhereHas('material', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })
                    ->orWhereHas('project', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->orWhereHas('vendor', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->orWhereHas('expenseRecords', function($q) use ($searchValue){
                       $q->havingRaw('SUM(amount) like ? ',  ['%'.$searchValue.'%'] );
                    });
                })
                ->orderBy('id','DESC');
            $response = $expenses->paginate($length);
              
        }else{
            $response = $query->paginate($length);
        }


        return ['data' => $response, 'draw' => $request->draw];
        
    }

    public function LabourContracts(Request $request)
    {
        // dd(date('Y-m-d',strtotime($request->start_date)));
       $columns = ['id','project_id','vendor_id','material_and_service_id','amount','start_date'];

        $length = $request->length;
        $column = $request->column;
        $dir = $request->dir;
        $searchValue = $request->search;

  
        $query = VendorContract::select('id','project_id','material_and_service_id','liquid_asset_id','vendor_id','amount','individual_project_phase_id','status','project_labour_id','contractor')->with('expenseRecords','vendor:id,name','material:id,name','project:id,name','individualprojectphase.phase_labour.material_and_service','individualprojectphase.projectphase','projectphase:id,name','asset.assetmodel.assetmanufacturer','projectlabour.material_and_service')->orderBy($columns[$column], $dir)->where('start_date','>=',date('Y-m-d',strtotime($request->start_date)))->where('start_date','<=',date('Y-m-d',strtotime($request->end_date)))->where('material_and_service_id',$request->id);
         
                                    
        if($searchValue){

          $expenses = VendorContract::select('id','project_id','material_and_service_id','liquid_asset_id','vendor_id','amount','individual_project_phase_id','status','project_labour_id','contractor')->with('expenseRecords','vendor:id,name','material:id,name','project:id,name','individualprojectphase.phase_labour.material_and_service','individualprojectphase.projectphase','projectphase:id,name','asset.assetmodel.assetmanufacturer','projectlabour.material_and_service')->orderBy($columns[$column], $dir)->where('start_date','>=',date('Y-m-d',strtotime($request->start_date)))->where('start_date','<=',date('Y-m-d',strtotime($request->end_date)))->where('material_and_service_id',$request->id)
            ->where(function ($q) use ($searchValue)
              {
                 $q->where('id', 'like', '%' . $searchValue . '%')
                    ->orWhere('start_date', 'like', '%' .$searchValue . '%')
                    ->orWhere('amount', 'like', '%' .$searchValue . '%')
                    ->orWhereHas('material', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })
                    ->orWhereHas('project', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->orWhereHas('vendor', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->orWhereHas('expenseRecords', function($q) use ($searchValue){
                       $q->havingRaw('SUM(amount) like ? ',  ['%'.$searchValue.'%'] );
                    });
              })
            ->orderBy('id','DESC');
        $response = $expenses->paginate($length);
        
          }else{
              $response = $query->paginate($length);
          }
          return ['data' => $response, 'draw' => $request->draw];
        
    } 
}
