<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use App\Project;

class UserController extends Controller
{
        public function add(Request $r)
    {
  
      $r->validate([
          'first_name' => 'required',
          'last_name' => 'required',
          'email' => 'required',
          'phone' => 'required',
          'department' => 'required',
          'role' => 'required',
          'position' => 'required',
          'active' => 'required',
          
      ]);

      // $earn = Earn::firstOrCreate(['name' => $r->name]);
        if (User::where('email', '=', $r->email)->exists()) {
            return response(['error'=>'User with email already exist'], 404);
        } else {
           $user = new User;
           $user->first_name = $r->first_name;
           $user->last_name = $r->last_name;
           $user->email = $r->email;
           $user->phone_no = $r->phone;
           $user->department_id = $r->department;
           $user->role_id = $r->role;
           $user->position_id = $r->position;
           $user->active = $r->active;
           $user->save();
        }
      // return $user->where('id',$user->id)->with('position:id,name','department:id,name')->get()->makeHidden(['salary_component','salary','role_id','relative_relationship','relative_phone_no','relative_name','position_id','pay_frequency','phone_no','national_id','driver_license','department_id','bank_id','account_no','created_at','updated_at'])->toArray();
    }

    public function show(Request $request)
    {

      $columns = ['id','first_name','last_name','department_id','position_id','active'];

      $length = $request->length;
      $column = $request->column;
      $dir = $request->dir;
      $searchValue = $request->search;

      
      $query = User::select('id', 'first_name','last_name','department_id','position_id','email','active')->with('position:id,name','department:id,name')->where('active',1)->orderBy($columns[$column], $dir);


      if($searchValue){
          $query->where('id', 'like', '%' . $searchValue . '%')
              ->orWhere('first_name', 'like', '%' . $searchValue . '%')
              ->orWhere('last_name', 'like', '%' . $searchValue . '%')
              ->orWhereHas('position', function($q) use ($searchValue){
                    $q->where('name',  'like', '%' . $searchValue . '%');
                })->orWhereHas('department', function($q) use ($searchValue){
                        $q->where('name',  'like', '%' . $searchValue . '%');
                    })->get();
      }


      $users = $query->paginate($length);

      return ['data' => $users, 'draw' => $request->draw];
    }



    public function edit(Request $r)
    {
      
           $user = User::find($r->id);
           $user->first_name = $r['first_name'];
           $user->last_name = $r['last_name'];
           $user->email = $r['email'];
           $user->phone_no = $r['phone_no'];
           $user->department_id = $r['department'];
           $user->role_id = $r['role'];
           $user->position_id = $r['position'];
           $user->active = $r['active'];
           $user->save();
        
        
        return $user;
    }
  public function edit_second(Request $r)
    {
      
           $user = User::find($r->id);
           $user->relative_name = $r->relative_name;
           $user->relative_relationship = $r->relative_relationship;
           $user->relative_phone_no = $r->relative_phone_no;
           if($r->alternative_phone_no != ""){
            $user->alternative_phone_no = $r->alternative_phone_no;
           }else{
              $user->alternative_phone_no = null;
           }
           $user->save();
        
        
        return $user;
    } 

   public function edit_third(Request $r)
    {
      
           $user = User::find($r->id);
           
           if($r->driver_license != ""){
            $user->driver_license = $r->driver_license;
           }else{
              $user->driver_license = null;
           }
           if($r->national_id != ""){
            $user->national_id = $r->national_id;
           }else{
              $user->national_id = null;
           }
           if($r->international_id != ""){
            $user->international_id = $r->international_id;
           }else{
              $user->international_id = null;
           }
           $user->save();
        
        
        return $user;
    }
public function edit_fourth(Request $r)
    {
      
           $user = User::find($r->id);
           
           if($r->bank_id != ""){
            $user->bank_id = $r->bank_id;
           }else{
              $user->bank_id = null;
           }
           if($r->account_no != ""){
            $user->account_no = $r->account_no;
           }else{
              $user->account_no = null;
           }
           if($r->account_type != ""){
            $user->account_type = $r->account_type;
           }else{
              $user->account_type = null;
           }
           $user->save();
        
        
        return $user;
    }
public function edit_fifth(Request $r)
    {
      
           $user = User::find($r->id);
           
           if($r->salary_component != ""){
            $user->salary_component = $r->salary_component;
           }else{
              $user->salary_component = null;
           }
           if($r->salary != ""){
            $user->salary = str_replace(",", "",$r->salary);
           }else{
              $user->salary = 0;
           }
           if($r->pay_frequency != ""){
            $user->pay_frequency = $r->pay_frequency;
           }else{
              $user->pay_frequency = null;
           }
           $user->save();
        
        
        return $user;
    }


    public function get(Request $request){
        return User::find($request->id)->makeHidden(['created_at','updated_at'])->toArray();
    }

     public function editLogin(Request $request){
        $user = User::find($request->id);
        $user->password = bcrypt($request->password);
        $user->active = $request->active;
        $user->save();

        return 'success';
    }



    public function getAll(Request $request){
        return User::select('id','first_name','last_name')->get()->makeHidden(['created_at','updated_at'])->toArray();
    } 

    public function getAllSalary(Request $request){
      $columns = ['id','first_name','last_name','salary'];

      $length = $request->length;
      $column = $request->column;
      $dir = $request->dir;
      $searchValue = $request->search;

      
      $query = User::select('id', 'first_name','last_name','salary')->with('deductions.deducts','earnings.earns','tax','bank:id,name')->orderBy($columns[$column], $dir);


      if($searchValue){
          $query->where('id', 'like', '%' . $searchValue . '%')
              ->orWhere('first_name', 'like', '%' . $searchValue . '%')
              ->orWhere('last_name', 'like', '%' . $searchValue . '%')
              ->orWhere('salary', 'like', '%' . $searchValue . '%')
              // ->orWhereHas('position', function($q) use ($searchValue){
              //       $q->where('name',  'like', '%' . $searchValue . '%');
              //   })->orWhereHas('department', function($q) use ($searchValue){
              //           $q->where('name',  'like', '%' . $searchValue . '%');
              //       })
              ->get();
      }


      $users = $query->paginate($length);

      return ['data' => $users, 'draw' => $request->draw];
        // return User::select('id','first_name','last_name','salary')->get()->makeHidden(['created_at','updated_at'])->toArray();
    } 

    public function getSalary(Request $request){
      return User::select('id', 'first_name','last_name','salary','bank_id','account_no')->with('deductions.deducts','earnings.earns','tax','bank:id,name')->where('id',$request->id)->get();
    }

    public function userPaye(Request $request){
        return User::select('id','first_name','last_name')->where('paye',0)->get();
    } 
    public function userImprest(Request $request){
        return User::select('id','first_name','last_name')->where('employee_imprest',0)->get();
    }

     public function search(Request $request)
    {
        
        $query = User::select('id','users.*',DB::raw('CONCAT(users.first_name, " ", users.last_name) AS name'))->where('first_name', 'like', '%' .$request->data . '%')->orWhere('last_name', 'like', '%' .$request->data . '%')->get();

        
        return $query;
       
        
    }
// Removed getallocations
}
