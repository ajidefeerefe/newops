<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProjectStaffAllocation;

class ProjectStaffAllocationController extends Controller
{
    public function add(Request $r){

        // dd($r->all());
        foreach ($r->staff as $key => $value) 
        {
          if($value['user_id'] == '' or $value['start_date']=='' or $value['end_date']=='')
            {//Check if any phase field was left blank
              return response(['error'=>"No field can be empty"], 404);
            }
        }
        ProjectStaffAllocation::where('project_id',$r->project_id)->delete();//Clear all the record before reinserting
        foreach ($r->staff as $key => $value)
        {
           	$p_staff = new ProjectStaffAllocation;
           	$p_staff->project_id = $r->project_id;
           	$p_staff->user_id = $value['user_id'];
           	$p_staff->start_date = $value['start_date'];
           	$p_staff->end_date = $value['end_date'];
           	$p_staff->save();
                
        }
        return "Done";
        // return ProjectStaffAllocationController::with('projectphase:id,name')->get()->makeHidden(['created_at','updated_at'])->toArray();
    }
}
