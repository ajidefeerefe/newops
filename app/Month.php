<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Month extends Model
{
    protected $fillable = ['name'];

    public function payperiod()
    {
         // return $this->hasMany('App\PayPeriod');
    }
}
