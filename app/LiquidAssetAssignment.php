<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiquidAssetAssignment extends Model
{
     protected $guarded = ['id'];

    public function user()
    {
    	return $this->belongsTo('App\User','assigned_to');
       
    } 
    public function client()
    {


    	return $this->belongsTo('App\Client','assigned_to');
    	// if($this->user_type == 'user'){
    	// 	return $this->belongsTo('App\User','assigned_to');
    	// }elseif ($this->user_type == 'client'){
    	// 	return $this->belongsTo('App\Client','assigned_to');
    	// }elseif ($this->user_type == 'vendor') {
    	// 	return $this->belongsTo('App\Vendor','assigned_to');
    	// }
    	
       
    }

    public function vendor()
    {
    	return $this->belongsTo('App\Vendor','assigned_to');
       
    } 
   
}
