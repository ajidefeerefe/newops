<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    public function expense_records(){
    	return $this->hasMany('App\ExpenseRecord');
    }

    public function expense_records_amount(){
    	
    	return $this->hasMany('App\ExpenseRecord')
    				->selectRaw('bank_account_id, SUM(amount) as amount')
    				->groupBy('bank_account_id');

    }

    public function bank(){
        return $this->belongsTo('App\Bank');
    }

    public function client_payments(){
    	return $this->hasMany('App\ClientPayment');
    }

    public function client_payments_amount(){
    	
    	return $this->hasMany('App\ClientPayment')
    				->selectRaw('bank_account_id, SUM(amount) as amount')
    				->groupBy('bank_account_id');

    }
     public function bank_account_topups_amount(){
        
        return $this->hasMany('App\BankAccountTopup')
                    ->selectRaw('bank_account_id, SUM(amount) as amount')
                    ->groupBy('bank_account_id');

    }
}
