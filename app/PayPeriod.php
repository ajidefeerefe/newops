<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayPeriod extends Model
{
    protected $fillable = ['year','month_id'];

    public function month()
    {
    	return $this->belongsTo('App\Month');
       
    }
}
