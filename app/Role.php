<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	protected $fillable = ['name','active','permission_id'];

    // public function user()
    // {
    //     // return $this->hasMany('App\User');
    //     return $this->belongsToMany('App\User','user__roles', 'role_id', 'user_id');
    // }
     public function users()
    {
        return $this->hasMany('App\User');
        // return $this->belongsToMany('App\User','user__roles', 'role_id', 'user_id');
    }

    public function permission()
    {
        return $this->hasOne('App\Permission','role_id');
    }


    public function subscription()
    {
        return $this->hasOne('App\Subscription');
    }
}
