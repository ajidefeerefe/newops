<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectFile extends Model
{
    public function project(){
    	return $this->belongsTo('App\Project','project_id');
    }

    public function projectphase(){
    	return $this->belongsTo('App\ProjectPhase','project_phase_id');
    }
}
