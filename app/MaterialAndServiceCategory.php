<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaterialAndServiceCategory extends Model
{
    protected $fillable = ['name'];

    public function material_and_services(){
    	return $this->hasMany('App\MaterialAndService','material_and_services_category_id');
    }

}
