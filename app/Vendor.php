<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $guarded = ['id'];

    public function category(){
    	return $this->belongsTo('App\VendorCategory','category_id');
    }

    public function country(){

    	return $this->belongsTo('App\Country','country_id');

    }

    public function state(){

    	return $this->belongsTo('App\State','state_id');

    }

    public function city(){

    	return $this->belongsTo('App\City','city_id');
    }

    public function bank(){
    	return $this->belongsTo('App\Bank','bank_id');
    }

    public function vendorservice(){
    	return $this->hasMany('App\VendorService');
    }
    public function purchase_requisition_materials(){
        return $this->hasMany('App\PurchaseRequisitionMaterial');
    }
}
