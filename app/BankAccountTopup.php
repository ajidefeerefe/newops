<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankAccountTopup extends Model
{
	protected $guarded = [];
	
    public function bank_account(){
    	return $this->belongsTo('App\BankAccount');
    }

    public function expense_records_amount(){
    	
    	return $this->hasMany('App\ExpenseRecord')
    				->selectRaw('bank_account_topup_id, SUM(amount) as amount')
    				->groupBy('bank_account_topup_id');

    }

    public function user(){
    	return $this->belongsTo('App\User','created_by');
    }
}
