<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeImprest extends Model
{
    protected $guarded = ['id'];

    public function user()
    {
    	return $this->belongsTo('App\User','employee_id');
       
    } 

    public function vendor()
    {
    	return $this->belongsTo('App\Vendor','employee_id');
       
    } 

    public function topups()
    {
    	return $this->hasMany('App\EmployeeImprestTopup','employee_imprest_id')
    				->selectRaw('employee_imprest_id, SUM(amount) as amount, id')
    				->groupBy('id','employee_imprest_id');
       
    } 

    public function withdraws()
    {
        return $this->hasMany('App\EmployeeImprestTopupWithdrawals','employee_imprest_id')
                    ->selectRaw('employee_imprest_id, SUM(amount) as amount')
                    ->groupBy('employee_imprest_id');
       
    } 

    public function created_by()
    {
    	return $this->belongsTo('App\User','created_by');
    				
       
    } 
}
