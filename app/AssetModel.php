<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetModel extends Model
{
    protected $fillable = ['user_id','name','active','asset_category_id','asset_manufacturer_id'];

    public function assetcategory()
    {
    	return $this->belongsTo('App\AssetCategory','asset_category_id');
       
    }

    public function assetmanufacturer()
    {
    	return $this->belongsTo('App\AssetManufacturer','asset_manufacturer_id');
       
    }
}
