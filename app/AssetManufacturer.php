<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetManufacturer extends Model
{
    protected $fillable = ['user_id','name','active','asset_category_id'];

    public function assetcategory()
    {
    	return $this->belongsTo('App\AssetCategory','asset_category_id');
       
    }
}
