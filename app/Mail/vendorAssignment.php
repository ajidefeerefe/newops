<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class vendorAssignment extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $title,$message,$material,$assigner,$vendorMsg;
    public function __construct($title,$message,$material,$assigner,$vendorMsg)
    {

        $this->assigner = $assigner;
        $this->title = $title;
        $this->message = $message;
        $this->material = $material;
        $this->vendorMsg = $vendorMsg;

    }

   
    public function build()
    {
        return $this->view('mails.finance.vendorAssignment',['title'=>$this->title,'assigner'=>$this->assigner,'message_x'=>$this->message,'material'=>$this->material,'vendorMsg'=>$this->vendorMsg]);
    }
}
