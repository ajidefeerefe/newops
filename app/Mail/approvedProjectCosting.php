<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class approvedProjectCosting extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $title,$note,$project, $extra;
    public function __construct($title,$note,$project,$extra)
    {
       
        $this->note = $note;
        $this->title = $title;
        $this->project = $project;
        $this->extra = $extra;
    }

   
    public function build()
    {
        return $this->view('mails.finance.approvedProjectCosting',['title'=>$this->title,'note'=>$this->note,'project'=>$this->project,'extra' => $this->extra]);
    }
}
