<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class approvedRequisition extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $title,$message,$material,$approver;
    public function __construct($title,$message,$material,$approver)
    {

        $this->approver = $approver;
        $this->title = $title;
        $this->message = $message;
        $this->material = $material;

    }

   
    public function build()
    {
        return $this->view('mails.finance.approvedRequisition',['title'=>$this->title,'approver'=>$this->approver,'message_x'=>$this->message,'material'=>$this->material]);
    }
}
