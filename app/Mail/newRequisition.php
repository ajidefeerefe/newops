<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class newRequisition extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $title,$requester,$date,$container,$needDate,$id,$project,$extra;
    public function __construct($title,$requester,$date,$container,$needDate,$id,$project,$extra)
    {
        $this->requester = $requester;
        $this->title = $title;
        $this->date = $date;
        $this->container = $container;
        $this->needDate = $needDate;
        $this->id = $id;
        $this->project = $project;
        $this->extra = $extra;
    }

   
    public function build()
    {
        return $this->view('mails.finance.newRequisition',['title'=>$this->title,'requester'=>$this->requester,'date'=>$this->date,'container'=>$this->container,'needDate'=>$this->needDate,'id'=>$this->id,'project'=>$this->project,'extra'=>$this->extra]);
    }
}
