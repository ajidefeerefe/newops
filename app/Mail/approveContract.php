<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class approveContract extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $title,$project,$aggreements,$milestones,$contractor,$vendor,$asset,$service,$project_phase,$status,$amount;
    
    public function __construct($title,$project,$aggreements,$milestones,$contractor,$vendor,$asset,$service,$project_phase,$status,$amount)
    {
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('view.name');
    }
}
