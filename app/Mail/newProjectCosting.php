<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class newProjectCosting extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $title,$note;
    public function __construct($title,$note)
    {
       
        $this->note = $note;
        $this->title = $title;
        
    }

   
    public function build()
    {
        return $this->view('mails.finance.newProjectCosting',['title'=>$this->title,'note'=>$this->note]);
    }
}
