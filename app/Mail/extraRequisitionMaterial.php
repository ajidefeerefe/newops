<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class extraRequisitionMaterial extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $title,$requester,$container,$id,$project,$extra;
    public function __construct($title,$requester,$container,$id,$project,$extra)
    {
        $this->requester = $requester;
        $this->title = $title;
        $this->container = $container;
        $this->id = $id;
        $this->project = $project;
        $this->extra = $extra;
    }

   
    public function build()
    {
        return $this->view('mails.finance.extraRequisitionMaterials',['title'=>$this->title,'requester'=>$this->requester,'container'=>$this->container,'id'=>$this->id,'project'=>$this->project,'extra'=>$this->extra]);
    }
}
