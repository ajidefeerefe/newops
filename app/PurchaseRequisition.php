<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseRequisition extends Model
{
    protected $guarded = [];

    public function user(){

    	return $this->belongsTo('App\User','user_id');

    }

    public function individual_project_phase(){

    	return $this->belongsTo('App\IndividualProjectPhases','individual_project_phases_id');

    }

    public function project_materials(){

    	return $this->belongsTo('App\ProjectMaterial');

    }

    public function expense_records(){
    	return $this->hasMany('App\ExpenseRecord');
    }

    public function expense_records_amount(){
    	
    	return $this->hasMany('App\ExpenseRecord')
    				->selectRaw('purchase_requisition_id, SUM(amount) as amount')
    				->groupBy('purchase_requisition_id');

    }

    public function purchase_requisition_materials(){
    	return $this->hasMany('App\PurchaseRequisitionMaterial');
    }

    public function project(){
        return $this->belongsTo('App\Project');
    }
}
