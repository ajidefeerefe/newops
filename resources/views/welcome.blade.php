<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Opsmanager</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="icon" href="{{asset('images/rytegate-fav.png')}}" type="image/x-icon" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <!-- Styles -->
        {{-- <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.css')}}"> --}}
        <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/layout.css')}}">
         <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/main_theme.css')}}">
       
        
        <link rel="stylesheet" type="text/css" href="{{asset('css/font-awesome.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/component.css')}}">
        {{-- <link rel="stylesheet" type="text/css" href="{{asset('css/waves.css')}}"> --}}
        {{-- <link rel="stylesheet" type="text/css" href="{{asset('css/animate.css')}}"> --}}
        {{-- <link rel="stylesheet" type="text/css" href="{{asset('css/spinkit.css')}}"> --}}
        {{-- <link rel="stylesheet" type="text/css" href="{{asset('css/unslider.css')}}"> --}}
        {{-- <link rel="stylesheet" type="text/css" href="{{asset('css/chartist.min.css')}}"> --}}
        {{-- <link rel="stylesheet" type="text/css" href="{{asset('css/emojione.sprites.css')}}"> --}}
        
    
    </head>
    <body class="theme-indigo light layout-fixed">
        <div id="app">
            <main-app/>
        </div>
        
        <script type="text/javascript" src="{{asset('js/all.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
        {{-- <script type="text/javascript" src="{{asset('js/layout.js')}}"></script> --}}
        {{-- <script type="text/javascript" src="{{asset('js/bootstrap.js')}}"></script> --}}
        {{-- <script type="text/javascript" src="{{asset('js/jquery.slimscroll.js')}}"></script> --}}
        {{-- <script type="text/javascript" src="{{asset('js/jquery.storageapi.js')}}"></script> --}}
        {{-- <script type="text/javascript" src="{{asset('js/screenfull.js')}}"></script> --}}
        {{-- <script type="text/javascript" src="{{asset('js/modernizr.custom.js')}}"></script> --}}
        {{-- <script type="text/javascript" src="{{asset('js/waves.js')}}"></script> --}}
        {{-- <script type="text/javascript" src="{{asset('js/emojione.min.js')}}"></script> --}}


        {{-- <script type="text/javascript" src="{{asset('js/masonry.pkgd.min.js')}}"></script> --}}
        {{-- <script type="text/javascript" src="{{asset('js/jquery.knob.min.js')}}"></script> --}}
        {{-- <script type="text/javascript" src="{{asset('js/jquery.sparkline.js')}}"></script> --}}
        {{-- <script type="text/javascript" src="{{asset('js/skycons.js')}}"></script> --}}
        
        {{-- <script type="text/javascript" src="{{asset('js/masonry.min.js')}}"></script> --}}
        {{-- <script type="text/javascript" src="{{asset('js/unslider-min.js')}}"></script> --}}

        {{-- <script type="text/javascript" src="{{asset('js/chartist.js')}}"></script> --}}
        {{-- <script type="text/javascript" src="{{asset('js/jquery-knob.js')}}"></script> --}}
        {{-- <script type="text/javascript" src="{{asset('js/index_2.js')}}"></script> --}}

        {{-- <script type="text/javascript" src="{{asset('js/demo.js')}}"></script> --}}
        


        
    </body>
</html>
