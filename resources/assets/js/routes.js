import store from './store';
const Home = () => import('./components/Home.vue');
const Login = () => import('./components/auth/Login.vue');
const Dashboard = () => import('./components/Dashboard.vue');
const Bank = () => import('./components/system-setup/Banks.vue');
const Earnings = () => import('./components/system-setup/Earnings.vue');
const Deductions = () => import('./components/system-setup/Deductions.vue');
const Colors = () => import('./components/system-setup/Colors.vue');
const Roles = () => import('./components/system-setup/UserRoles.vue');
const UserPermissions = () => import('./components/system-setup/UserPermissions.vue');
const UserNotifications = () => import('./components/system-setup/UserNotifications.vue');
const Departments = () => import('./components/system-setup/Departments.vue');
const ChartOfAccounts = () => import('./components/system-setup/ChartOfAccounts.vue');
const PayPeriods = () => import('./components/system-setup/PayPeriods.vue');
const AssetCategories = () => import('./components/system-setup/AssetCategories.vue');
const AssetManufacturers = () => import('./components/system-setup/AssetManufacturers.vue');
const AssetModels = () => import('./components/system-setup/AssetModels.vue');
const MaterialServicesCategories = () => import('./components/system-setup/MaterialServicesCategories.vue');
const MaterialServices = () => import('./components/system-setup/MaterialServices.vue');
const ProjectPhases = () => import('./components/system-setup/ProjectPhases.vue');
const ProjectPhasesActivities = () => import('./components/system-setup/ProjectPhasesActivities.vue');
const Positions = () => import('./components/system-setup/Positions.vue');
const VendorCategories = () => import('./components/system-setup/VendorCategories.vue');
const InventoryLocations = () => import('./components/system-setup/InventoryLocations.vue');
const ImprestCategories = () => import('./components/system-setup/ImprestCategories.vue');
const HighlevelProjectPhases = () => import('./components/system-setup/HighlevelProjectPhases.vue');
const UserProfiles = () => import('./components/user-administration/UserProfiles.vue');
const AddUser = () => import('./components/user-administration/AddUser.vue');
const EditUser = () => import('./components/user-administration/EditUser.vue');
const UserLogins = () => import('./components/user-administration/UserLogins.vue');
const LiquidAssets = () => import('./components/asset-manager/LiquidAssets.vue');
const ViewLiquidAssets = () => import('./components/asset-manager/ViewLiquidAssets.vue');
const ClientProfiles = () => import('./components/client-administration/ClientProfiles.vue');
const AddClient = () => import('./components/client-administration/AddClient.vue');
const EditClient = () => import('./components/client-administration/EditClient.vue');
const ServiceVendors = () => import('./components/vendor-administration/ServiceVendors.vue');
const AddVendor = () => import('./components/vendor-administration/AddVendor.vue');
const EditVendor = () => import('./components/vendor-administration/EditVendor.vue');
const VendorPricings = () => import('./components/vendor-administration/VendorPricings.vue');
const Projects = () => import('./components/project-administration/Projects.vue');
const EditProject = () => import('./components/project-administration/EditProject.vue');
const VehicleRequests = () => import('./components/carpool-manager/VehicleRequests.vue');
const VehicleAssignments = () => import('./components/carpool-manager/VehicleAssignments.vue');
const Costings = () => import('./components/finance-administration/project-costing/Costings.vue');
const CostingWeeks = () => import('./components/finance-administration/project-costing/CostingWeeks.vue');
const EditCosting = () => import('./components/finance-administration/project-costing/EditCosting.vue');
const EmployeeImprests = () => import('./components/finance-administration/employee-imprest/EmployeeImprests.vue');
const ImprestTopups = () => import('./components/finance-administration/employee-imprest/ImprestTopups.vue');
const TopupWithdrawals = () => import('./components/finance-administration/employee-imprest/TopupWithdrawals.vue');
const InventoryStores = () => import('./components/finance-administration/inventory-store/InventoryStores.vue');
const TopUps = () => import('./components/finance-administration/inventory-store/TopUps.vue');
const Taxes = () => import('./components/finance-administration/paye/Taxes.vue');
const Invoices = () => import('./components/finance-administration/invoice/Invoices.vue');
const PrintInvoice = () => import('./components/finance-administration/invoice/PrintInvoice.vue');
const ProjectReceivables = () => import('./components/finance-administration/account-receivables/ProjectReceivables.vue');
const ContractsCategories = () => import('./components/finance-administration/vendor-contract/ContractsCategories.vue');
const Contracts = () => import('./components/finance-administration/vendor-contract/Contracts.vue');
const AddContracts = () => import('./components/finance-administration/vendor-contract/AddContracts.vue');
const EditContract = () => import('./components/finance-administration/vendor-contract/EditContract.vue');
const ShowPayments = () => import('./components/finance-administration/vendor-contract/ShowPayments.vue');
const PrintContract = () => import('./components/finance-administration/vendor-contract/Print.vue');
const Payroll = () => import('./components/finance-administration/payroll/PayPeriods.vue');
const UsersPayroll = () => import('./components/finance-administration/payroll/Payrolls.vue');
const Payment = () => import('./components/finance-administration/payroll/Payment.vue');
const PrintPayment = () => import('./components/finance-administration/payroll/print.vue');
const PaymentCategories = () => import('./components/system-setup/PaymentCategories.vue');
const PurchaseRequisitions = () => import('./components/finance-administration/purchase-requisition/PurchaseRequisitions.vue');
const ViewRequisition = () => import('./components/finance-administration/purchase-requisition/ViewRequisitions.vue');
const ViewRequisitionMaterials = () => import('./components/finance-administration/purchase-requisition/ViewRequisitionMaterials.vue');
const BankAccounts = () => import('./components/finance-administration/bank-account/BankAccounts.vue');
const Archived = () => import('./components/finance-administration/archive/PayPeriods.vue');
const UsersSavedPayroll = () => import('./components/finance-administration/archive/Payrolls.vue');
const UpdatePayment = () => import('./components/finance-administration/archive/Payment.vue');
const PrintUpdatedPayment = () => import('./components/finance-administration/archive/print.vue');
const BankAccountTopups = () => import('./components/finance-administration/bank-account/BankAccountTopups.vue');
const BankAccountExpenses = () => import('./components/finance-administration/bank-account/ExpenseRecords.vue');
const StaffAllocations = () => import('./components/reports/StaffAllocations.vue');
const ExpenseByProjects = () => import('./components/reports/ExpenseByProjects.vue');
const ExpenseByDateRanges = () => import('./components/reports/ExpenseByDateRanges.vue');
const ExpenseByPhases = () => import('./components/reports/ExpenseByPhases.vue');
const ExpenseByProjectPhases = () => import('./components/reports/ExpenseByProjectPhases.vue');
const ExpenseByTypes = () => import('./components/reports/ExpenseByTypes.vue');
const RequisitionByProjects = () => import('./components/reports/RequisitionByProjects.vue');
const RequisitionByDateRanges = () => import('./components/reports/RequisitionByDateRanges.vue');
const RequisitionByMaterials = () => import('./components/reports/RequisitionByMaterials.vue');
const ContractByDateRanges = () => import('./components/reports/ContractByDateRanges.vue');
const ContractByProjects = () => import('./components/reports/ContractByProjects.vue');
const ContractByMaterials = () => import('./components/reports/ContractByMaterials.vue');
const ReportByChartOfAccounts = () => import('./components/reports/ReportByChartOfAccounts.vue');
export const routes = [
	{
		path: '/',
		component: Home,
		meta: {
			requiresAuth: true
		},
		children: [
			{
		      path: '',
		       name: 'dashboard',
		      component: Dashboard,
	      		
		    },
		   
		    {
		      path: 'system/banks',
		      name: 'banks',
		      component: Bank,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.banks ===1){
	             		next();
	             	}else{
	             		
	             		next(from.path);

	             	}
	             	
	           	}
		    },
		    {
		      path: 'system/earnings',
		      name: 'earnings',
		      component: Earnings,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.system_setup ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	}
		    },
		     {
		      path: 'system/deductions',
		      name: 'deductions',
		      component: Deductions,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.system_setup ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	}
		    },
		   {
		      path: 'system/colors',
		      name: 'colors',
		      component: Colors,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.color ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	}
		    },
		     {
		      path: 'system/user-roles',
		      name: 'user-roles',
		      component: Roles,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.user_role ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'system/user-roles/permissions/:id',
		      name: 'user-permission',
		      component: UserPermissions,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.user_permission ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'system/user-notification/subscription/:id',
		      name: 'user-subscriptions',
		      component: UserNotifications,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.user_permission ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'system/user-departments',
		      name: 'user-departments',
		      component: Departments,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.user_department ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'system/chartofaccounts',
		      name: 'chartofaccounts',
		      component: ChartOfAccounts,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.system_setup ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'system/pay-periods',
		      name: 'payperiods',
		      component: PayPeriods,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.system_setup ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
			{
		      path: 'system/asset-category',
		      name: 'assetcategory',
		      component: AssetCategories,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.asset_category ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'system/asset-manufacturer',
		      name: 'assetmanufacturer',
		      component: AssetManufacturers,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.asset_manufacturer ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'system/asset-model',
		      name: 'assetmodel',
		      component: AssetModels,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.asset_model ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'system/material-services-category',
		      name: 'material-services-category',
		      component: MaterialServicesCategories,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.system_setup ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'system/materials-and-services',
		      name: 'material-services',
		      component: MaterialServices,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.material_and_labour ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'system/project-phases',
		      name: 'projectphases',
		      component: ProjectPhases,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.project_phase_setup ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'system/project-phase-activities',
		      name: 'projectphaseactivities',
		      component: ProjectPhasesActivities,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.system_setup ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    }, 
		    {
		      path: 'system/vendor-category',
		      name: 'vendorcategory',
		      component: VendorCategories,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.vendor_category ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	}
	         
		    },
		    {
		      path: 'system/user-positions',
		      name: 'user-positions',
		      component: Positions,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.user_position ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'system/inventory-location',
		      name: 'inventorylocation',
		      component: InventoryLocations,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.inventory_location ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    // {
		    //   path: 'system/imprest-category',
		    //   name: 'imprestcategory',
		    //   component: ImprestCategories,
		    //   //checks if a user has permission to view a page, if not, redirect back
	     // 	  beforeEnter: (to, from, next) => {
	     //         	const currentUser = store.state.currentUser;
	     //         	if(currentUser.permissions.system_setup ===1){
	     //         		next();
	     //         	}else{
	     //         		next(from.path);
	     //         	}
	             	
	     //       	}
	         
		    // },
		    {
		      path: 'system/highlevel-project-phase',
		      name: 'highlevelprojectphases',
		      component: HighlevelProjectPhases,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.system_setup ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	}
	         
		    },
		    {
		      path: 'user-administration/users-profiles',
		      name: 'users-profiles',
		      component: UserProfiles,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.user_profile ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },  
		    {
		      path: 'user-administration/users-profiles/add-user',
		      name: 'add-user',
		      component: AddUser,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.user_profile ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'user-administration/users-profiles/edit-user/:id',
		      name: 'edit-user',
		      component: EditUser,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.user_profile === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'user-administration/users-login',
		      name: 'user-login',
		      component: UserLogins,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.user_login === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'asset-manager/liquid-assets',
		      name: 'liquid-assets',
		      component: LiquidAssets,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.asset_manager === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'asset-manager/liquid-assets/view/:id',
		      name: 'view-liquid-asset',
		      component: ViewLiquidAssets,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.asset_manager === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'client-administration/client-profiles',
		      name: 'client-profiles',
		      component: ClientProfiles,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.client_administration === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'client-administration/client-profiles/add-client',
		      name: 'add-client',
		      component: AddClient,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.view_client === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'client-administration/client-profiles/:id',
		      name: 'edit-client',
		      component: EditClient,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.view_client === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'vendor-administration/vendors',
		      name: 'service-vendors',
		      component: ServiceVendors,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.vendor_administration === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'vendor-administration/add',
		      name: 'add-vendor',
		      component: AddVendor,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.service_vendor === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'vendor-administration/view-edit/:id',
		      name: 'edit-vendor',
		      component: EditVendor,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.service_vendor === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'vendor-administration/pricing/',
		      name: 'vendor-pricing',
		      component: VendorPricings,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.vendor_pricing === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'project-administration/projects/',
		      name: 'projects',
		      component: Projects,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.projects === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'project-administration/project/:id',
		      name: 'edit-project',
		      component: EditProject,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.projects === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		     {
		      path: 'carpool-manager/vehicle-request',
		      name: 'vehicle-request',
		      component: VehicleRequests,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.vehicle_request === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'carpool-manager/vehicle-assignments',
		      name: 'vehicle-assignments',
		      component: VehicleAssignments,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.vehicle_request === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },

		    //WIlliam codes
//WIlliam codes

		    {
		      path: 'project-costing/costings',
		      name: 'project-costing',
		      component: Costings,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.project_costing === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },

		    {
		      path: 'project-costing/:id',
		      name: 'edit-costing',
		      component: EditCosting,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.project_costing === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },

		    //William
		    {
		      path: 'project-costing/weeks/:id',
		      name: 'costing-weeks',
		      component: CostingWeeks,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.project_costing === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'finance/personnel-imprest',
		      name: 'personnel-imprest',
		      component: EmployeeImprests,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.employee_imprest === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    }, 

		    {
		      path: 'finance/personnel-imprest/topups/:id',
		      name: 'view-topups',
		      component: ImprestTopups,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.employee_imprest === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    {
		      path: 'finance/personnel-imprest/topups/withdraws/:id',
		      name: 'view-topups-withdraws',
		      component: TopupWithdrawals,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.employee_imprest === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },

		    {
		      path: 'finance/inventory/store',
		      name: 'inventory-store',
		      component: InventoryStores,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.inventory_store === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    }, 

		    {
		      path: 'finance/inventory/topups/:id',
		      name: 'view-inventory-topups',
		      component: TopUps,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.inventory_store === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },

		    {
		      path: 'finance/taxes',
		      name: 'tax',
		      component: Taxes,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.payroll === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },

			{
		      path: 'finance/invoices',
		      name: 'invoice',
		      component: Invoices,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.view_invoices === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },

		    {
		      path: 'print/invoice/:id',
		      name: 'print-invoice',
		      component: PrintInvoice,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.view_invoices === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
		    
		     {
		      path: 'finance/account-receivables',
		      name: 'account-receivables',
		      component: ProjectReceivables,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.imprest_manager ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	}
	         
		    }, 

		    {
		      path: 'finance/vendor-contract/categories',
		      name: 'vendor-contract-categories',
		      component: ContractsCategories,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.contracts ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	}
	         
		    },
		     {
		      path: 'finance/vendor-contract/contracts/:id',
		      name: 'vendor-contracts',
		      component: Contracts,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.contracts === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
	         
		    },
			{
		      path: 'finance/vendor-contract/contracts/add/:id',
		      name: 'add-contracts',
		      component: AddContracts,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.contracts === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
				         
		    },
		    {
		      path: 'finance/vendor-contract/contracts/edit/:id',
		      name: 'edit-contract',
		      component: EditContract,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.contracts === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
				         
		    },
		     {
		      path: 'finance/vendor-contract/contract/payments/:id',
		      name: 'show-costing-payments',
		      component: ShowPayments,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.contracts === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
				         
		    },
	      {
	      path: 'finance/vendor-contract/print/contract/:id',
	      name: 'print-vendor-contract',
	      component: PrintContract,
	      //checks if a user has permission to view a page, if not, redirect back
     	  beforeEnter: (to, from, next) => {
             	const currentUser = store.state.currentUser;
             	if(currentUser.permissions.contracts === 1){
             		next();
             	}else{
             		next(from.path);
             	}
             	
           	},
				         
		    },
		    {
		      path: 'system/payment-category',
		      name: 'paymentcategory',
		      component: PaymentCategories,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.imprest_category ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	}
	         
		    },
		     {
		      path: 'finance/purchase-requisition',
		      name: 'purchase-requisition',
		      component: PurchaseRequisitions,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.finance_administration ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	}
	         
		    },
		    {
		      path: 'finance/purchase-requisition/:id',
		      name: 'view-requisition',
		      component: ViewRequisition,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.finance_administration ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	}
	         
		    },

		    {
		      path: 'finance/purchase-requisition/view/:id',
		      name: 'requisition-materials',
		      component: ViewRequisitionMaterials,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.finance_administration ===1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	}
	         
		    },

		    {
		      path: 'finance/payroll/pay-periods',
		      name: 'payroll',
		      component: Payroll,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.payroll === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
				         
		    },
		    {
		      path: 'finance/payroll/pay-period/:id',
		      name: 'view-users-payroll',
		      component: UsersPayroll,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.payroll === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
				         
		    },
		    {
		      path: 'finance/payroll/make-payment/:payperiod/:id',
		      name: 'view-payment',
		      component: Payment,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.payroll === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
				         
		    },
			{
		      path: 'finance/payroll/print-payment/:payperiod/:id',
		      name: 'print-payment',
		      component: PrintPayment,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.payroll === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
				         
		    },
		    {
		      path: 'finance/archive/pay-periods',
		      name: 'archive',
		      component: Archived,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.payroll === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
				         
		    },
		    {
		      path: 'finance/archive/pay-period/:id',
		      name: 'view-archive-payroll',
		      component: UsersSavedPayroll,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.payroll === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
				         
		    },
		    {
		      path: 'finance/archive/make-payment/:payperiod/:id',
		      name: 'view-updatedpayment',
		      component: UpdatePayment,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.payroll === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
				         
		    },
			 {
		      path: 'finance/archive/print-payment/:payperiod/:id',
		      name: 'print-updatedpayment',
		      component: PrintUpdatedPayment,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.payroll === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
				         
		    },
		    {
		      path: 'finance/bank-accounts',
		      name: 'bank-accounts',
		      component: BankAccounts,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.bank_account === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
				         
		    },
		     {
		      path: 'bank-accounts/topups/:id',
		      name: 'bank-account-topups',
		      component: BankAccountTopups,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.bank_account === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
				         
		    },

		    {
		      path: 'bank-accounts/expense-records/:id',
		      name: 'bank-account-expenses',
		      component: BankAccountExpenses,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.bank_account === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
				         
		    },

		     {
		      path: 'reports/staff-allocations',
		      name: 'staff-allocation-report',
		      component: StaffAllocations,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.report_by_staff_allocation === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
             	
           	},
				         
		    },

		    {
		      path: 'reports/expense-by-project',
		      name: 'expense-by-project-report',
		      component: ExpenseByProjects,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.expense_by_project === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
				         
		    },
		    {
		      path: 'reports/expense-by-date-range',
		      name: 'expense-by-date-range-report',
		      component: ExpenseByDateRanges,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.expense_by_date_range === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
				         
		    },
		    {
		      path: 'reports/expense-by-phase',
		      name: 'expense-by-phase-report',
		      component: ExpenseByPhases,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.expense_by_project_phase_cumulative === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
				         
		    },

		    {
		      path: 'reports/expense-by-project-phase',
		      name: 'expense-by-project-phase-report',
		      component: ExpenseByProjectPhases,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.expense_by_project_phase === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
				         
		    },

		    {
		      path: 'reports/expense-by-type',
		      name: 'expense-by-type-report',
		      component: ExpenseByTypes,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.expense_by_category === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
				         
		    },

		    {
		      path: 'reports/requisitions-by-project',
		      name: 'requisitions-by-project-report',
		      component: RequisitionByProjects,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.requisition_by_project === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
				         
		    },

		    {
		      path: 'reports/requisitions-by-date-range',
		      name: 'requisitions-by-date-report',
		      component: RequisitionByDateRanges,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.requisition_by_date_range === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
				         
		    },
		     {
		      path: 'reports/requisitions-by-material',
		      name: 'requisitions-by-material-report',
		      component: RequisitionByMaterials,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.requisition_by_material === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
				         
		    }, 
		    {
		      path: 'reports/contracts-by-date-range',
		      name: 'contract-by-date-report',
		      component: ContractByDateRanges,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.contracts_by_date_range === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
				         
		    },
		    {
		      path: 'reports/contracts-by-project',
		      name: 'contract-by-project-report',
		      component: ContractByProjects,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.contract_by_project === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
				         
		    },
			{
		      path: 'reports/contracts-by-labour',
		      name: 'contract-by-labour-report',
		      component: ContractByMaterials,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.contracts_by_labour === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
				         
		    },
		    {
		      path: 'reports/chart-of-account-report',
		      name: 'report-by-chart',
		      component: ReportByChartOfAccounts,
		      //checks if a user has permission to view a page, if not, redirect back
	     	  beforeEnter: (to, from, next) => {
	             	const currentUser = store.state.currentUser;
	             	if(currentUser.permissions.reports === 1){
	             		next();
	             	}else{
	             		next(from.path);
	             	}
	             	
	           	},
				         
		    },


		  ]
	},
	// {
	// 	path: '/header',
	// 	component: Header,
	// 	meta: {
	// 		requiresAuth: true
	// 	}
	// },
	{
		path: '/login',
		component: Login,
	
	}
];