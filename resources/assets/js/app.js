require('./bootstrap');

import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import StoreData from './store';
import {routes} from './routes';
import MainApp from './components/MainApp.vue';
import {initialize} from './helpers/general.js';
import Notifications from 'vue-notification';

import Datetime from 'vue-datetime';
// You need a specific loader for CSS files
import 'vue-datetime/dist/vue-datetime.css';
 

// import './components/assets/js/layout.js';
// Vue.component('project', require('./components/datatables/Project.vue'));

Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(Notifications);
Vue.use(Datetime);
Vue.mixin({
  computed: {
    unique () {
      return function (arr, key) {
        var output = []
        var usedKeys = {}
        for (var i = 0; i < arr.length; i++) {
          if (!usedKeys[arr[i][key]]) {
            usedKeys[arr[i][key]] = true
            output.push(arr[i])
          }
        }
      		// console.log(usedKeys);
        
        return output
      }
    }
  }
})
const store = new Vuex.Store(StoreData);

const router = new VueRouter({
	routes,
	mode: 'history'
});

initialize(store, router);


const app = new Vue({
    el: '#app',
    router,
    store,
    components: {
    	MainApp,
    }
});
