import {getLocalUser} from "./helpers/auth";

const user = getLocalUser();
export default{
	state: {
		currentUser: user,
		isLoggedIn: !!user,
		loading: false,
		auth_error: null,
		earnings: {},
		earnings_pag: {},
		banks: {},
		banks_pag: {},
		deductions: {},
		deductions_pag: {},
		colors: {},
		colors_pag: {},
		roles: {},
		roles_pag: {},
		departments: {},
		departments_pag: {},
		chartofaccounts: {},
		chartofaccounts_pag: {},
		payperiods: {},
		payperiods_pag: {},
		assetcategories: {},
		assetcategories_pag: {},
		assetmanufacturers: {},
		assetmanufacturers_pag: {},
		assetmodels: {},
		assetmodels_pag: {},
		materialServiceCategories: {},
		materialServiceCategories_pag: {},
		materialServices: {},
		materialServices_pag: {},
		projectPhases: {},
		projectPhases_pag: {},	
		projectPhaseActivities: {},
		projectPhaseActivities_pag: {},
		highLevelProjectPhases: {},
		highLevelProjectPhases_pag: {},
		inventoryLocations_pag: {},
		inventoryLocations: {},
		paymentCategories_pag: {},
		vendorCategories: {},
		vendorCategories_pag: {},
		paymentCategories: {},
		positions: {},
		positions_pag: {},
		users: [],
		users_pag: {},
		clients: {},
		clients_pag: {},
		liquidAssets: {},
		liquidAssets_pag: {},
		vendors: [],
		vendors_pag: {},
		vendorspricing: [],
		vendorspricing_pag: {},
		liquidAssetAssignments: {},
		liquidAssetAssignments_pag: {},
		projects:{},
		projects_pag:{},
		costedProjects:{},
		costedProjects_pag:{},
		vehicleRequests: {},
		vehicleRequests_pag: {},
		vehicleAssignments: {},
		vehicleAssignments_pag: {},
		employeeImprest: {},
		employeeImprest_pag: {},
		employeeImprestTopups: {},
		employeeImprestTopups_pag: {},
		inventoryTopups: {},
		inventoryTopups_pag: {},
		inventory: {},
		inventory_pag: {},
		invoice: [],
		invoice_pag: [],
		tax: [],
		tax_pag: [],
		employeeImprestTopupWithdraws: [],
		employeeImprestTopupWithdraws_pag: [],
		contractCategories:[],
        contractCategories_pag:[],
		contracts: [],
		contracts_pag: [],
		payrolls: [],
		payrolls_pag: [],
		generalPurchaseRequisitions:{},
		generalPurchaseRequisitions_pag:{},
		purchaseRequisitions:{},
		purchaseRequisitions_pag:{},
		bank_accounts: {},
		bank_accounts_pag: {},
		savedPayperiods:{},
		savedPayperiods_pag:{},
		contractPayments:{},
		contractPayments_pag:{},
		// permissions:{}
	},
	mutations: {
		login(state){
			state.loading = true;
			state.auth_error = null;
		},
		loginSuccess(state, payload){
			state.auth_error = null;
			state.isLoggedIn = true;
			state.loading = false;
			state.currentUser = Object.assign({}, payload.user, {token: payload.access_token},{permissions: payload.role.original[0].role.permission }, {time: payload.expires_in});
			localStorage.setItem("user", JSON.stringify(state.currentUser));
		},
		loginFailed(state, payload){
			state.loading = false;
			state.auth_error = payload.error;

		},
		logout(state){
			localStorage.removeItem("user");
			state.isLoggedIn = false;
			state.currentUser = null;
		},
		loadEarnings(state,payload){	
			state.earnings = payload.data.data;
			state.earnings_pag = payload.data;
		},
		loadBanks(state,payload){	
			state.banks = payload.data.data;
			state.banks_pag = payload.data;
		},
		loadDeductions(state,payload){	
			state.deductions = payload.data.data;
			state.deductions_pag = payload.data;
		},
		loadColors(state,payload){	
			state.colors = payload.data.data;
			state.colors_pag = payload.data;
		},
		loadRoles(state,payload){	
			state.roles = payload.data.data;
			state.roles_pag = payload.data;
		},
		loadDepartments(state,payload){	
			state.departments = payload.data.data;
			state.departments_pag = payload.data;
		},
		loadChartOfAccounts(state,payload){	
			state.chartofaccounts = payload.data.data;
			state.chartofaccounts_pag = payload.data;
		},
		loadPayPeriods(state,payload){	
			state.payperiods = payload.data.data;
			state.payperiods_pag = payload.data;
		},
		loadAssetCategories(state,payload){	
			state.assetcategories = payload.data.data;
			state.assetcategories_pag = payload.data;
		},
		loadAssetManufacturers(state,payload){	
			state.assetmanufacturers = payload.data.data;
			state.assetmanufacturers_pag = payload.data;
		},
		loadAssetModels(state,payload){	
			state.assetmodels = payload.data.data;
			state.assetmodels_pag = payload.data;
		},
		loadMaterialServiceCategory(state,payload){	
			state.materialServiceCategories = payload.data.data;
			state.materialServiceCategories_pag = payload.data;
		},
		loadMaterialService(state,payload){	
			state.materialServices = payload.data.data;
			state.materialServices_pag = payload.data;
		},
		loadProjectPhase(state,payload){	
			state.projectPhases = payload.data.data;
			state.projectPhases_pag = payload.data;
		},
		loadInventoryLocation(state,payload){	
			state.inventoryLocations = payload.data.data;
			state.inventoryLocations_pag = payload.data;
		},
		loadVendorCategory(state,payload){	
			state.vendorCategories = payload.data.data;
			state.vendorCategories_pag = payload.data;
		},
		loadPaymentCategory(state,payload){	
			state.paymentCategories = payload.data.data;
			state.paymentCategories_pag = payload.data;
		},
		loadHighLevelProjectPhase(state,payload){	
			state.highLevelProjectPhases = payload.data.data;
			state.highLevelProjectPhases_pag = payload.data;
		},
		loadProjectPhaseActivity(state,payload){	
			state.projectPhaseActivities = payload.data.data;
			state.projectPhaseActivities_pag = payload.data;
		},
		loadPosition(state,payload){	
			state.positions = payload.data.data;
			state.positions_pag = payload.data;
		},
		loadUser(state,payload){	
			state.users = payload.data.data;
			state.users_pag = payload.data;
		},
		loadClient(state,payload){	
			state.clients = payload.data.data;
			state.clients_pag = payload.data;
		},
		loadLiquidAsset(state,payload){	
			state.liquidAssets = payload.data.data;
			state.liquidAssets_pag = payload.data;
		},
		loadVendor(state,payload){	
			state.vendors = payload.data.data;
			state.vendors_pag = payload.data;
		},
		loadVendorPricing(state,payload){	
			state.vendorspricing = payload.data.data;
			state.vendorspricing_pag = payload.data;
		},
		loadLiquidAssetAssignment(state,payload){	
			state.liquidAssetAssignments = payload.data.data;
			state.liquidAssetAssignments_pag = payload.data;
		},
		loadProject(state,payload){	
			state.projects = payload.data.data;
			state.projects_pag = payload.data;
		},
		loadCostedProject(state,payload){	
			state.costedProjects = payload.data.data;
			state.costedProjects_pag = payload.data;
		},
		loadVehicleRequest(state,payload){	
			state.vehicleRequests = payload.data.data;
			state.vehicleRequests_pag = payload.data;
		},
		loadVehicleAssignment(state,payload){	
			state.vehicleAssignments = payload.data.data;
			state.vehicleAssignments_pag = payload.data;
		},
		loadEmployeeImprest(state,payload){	
			state.employeeImprest = payload.data.data;
			state.employeeImprest_pag = payload.data;
		},
		loadEmployeeImprestTopups(state,payload){	
			state.employeeImprestTopups = payload.data.data;
			state.employeeImprestTopups_pag = payload.data;
		},
		loadEmployeeImprestWithdraws(state,payload){	
			state.employeeImprestTopupWithdraws = payload.data.data;
			state.employeeImprestTopupWithdraws_pag = payload.data;
		},
		loadInventory(state,payload){	
			state.inventory = payload.data.data;
			state.inventory_pag = payload.data;
		},
		loadInvoice(state,payload){	
			state.invoice = payload.data.data;
			state.invoice_pag = payload.data;
		},
		loadTax(state,payload){	
			state.tax = payload.data.data;
			state.tax_pag = payload.data;
		},
		loadInventoryTopups(state,payload){	
			state.inventoryTopups = payload.data.data;
			state.inventoryTopups_pag = payload.data;
		},
		loadContractsCategory(state,payload){	
					state.contractCategories = payload.data.data;
					state.contractCategories_pag = payload.data;
				},
		loadContracts(state,payload){	
			state.contracts = payload.data.data;
			state.contracts_pag = payload.data;
		},
		loadPayrolls(state,payload){	
			state.payrolls = payload.data.data;
			state.payrolls_pag = payload.data;
		},
		loadGeneralPurchaseRequisition(state,payload){//load projects having requisition
			state.generalPurchaseRequisitions = payload.data.data;
			state.generalPurchaseRequisitions_pag = payload.data;
		},
		loadProjectPurchaseRequisition(state,payload){//Load the requisition of a particular project
			state.purchaseRequisitions = payload.data.data;
			state.purchaseRequisitions_pag = payload.data;
		},
		loadBankAccounts(state,payload){//Load Bank Accounts
			state.bank_accounts = payload.data.data;
			state.bank_accounts_pag = payload.data;
		},
		loadSavedPayPeriods(state,payload){//Load the requisition of a particular project
			state.savedPayperiods = payload.data.data;
			state.savedPayperiods_pag = payload.data;
		},
		loadContractPayments(state,payload){//Load the requisition of a particular project
			state.contractPayments = payload.data.data;
			state.contractPayments_pag = payload.data;
		},


	},
	getters: {
		isLoading(state){
			return state.isloading;
		},
		isLoggedIn(state){
			return state.isLoggedIn;
		},
		currentUser(state){
			return state.currentUser;
		},
		authError(state){
			return state.auth_error;
		},
		customers(state){
			return state.customer;
		}

	},
	actions: {
		login(context){
			context.commit("login")
		}
	}
};