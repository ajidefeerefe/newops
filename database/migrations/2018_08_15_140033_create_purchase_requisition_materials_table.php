<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseRequisitionMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_requisition_materials', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('purchase_requisition_id');
            $table->integer('project_material_id');
            // $table->string('amount');
            $table->integer('quantity');
            $table->integer('vendor_id')->default(0);
            $table->integer('status')->default(0);
            $table->integer('status_for_pr')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_requisition_materials');
    }
}
