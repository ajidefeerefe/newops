<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLiquidAssetAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('liquid_asset_assignments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('liquid_asset_id');
            $table->integer('assigned_to')->nullable();
            $table->string('user_type')->nullable();
            $table->tinyInteger('assignment_status')->nullable();
            $table->text('assginment_start')->nullable();
            $table->text('assginment_end')->nullable();
            $table->text('actual_return_date')->nullable();
            $table->text('assginment_note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('liquid_asset_assignments');
    }
}
