<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('project_date');
            $table->integer('department_id');
            $table->integer('created_by');
            $table->integer('coordinator')->default(0);
            $table->text('cost');
            $table->smallInteger('project_costing')->default(0);
            $table->smallInteger('vendor_contract')->default(0);
            $table->integer('type');
            $table->smallInteger('active')->default(1);
            // $table->integer('user_id'); We dont know the use of this one yet
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
