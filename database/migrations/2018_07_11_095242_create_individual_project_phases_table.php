<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndividualProjectPhasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('individual_project_phases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->integer('project_phase_id');
            $table->string('cost');
            $table->string('start_date');
            $table->string('end_date');
            $table->integer('status');
            $table->integer('approved_by')->default(0);
            $table->integer('declined_by')->default(0);
            $table->integer('canceled_by')->default(0);
            $table->integer('review_submitter')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('individual_project_phases');
    }
}
