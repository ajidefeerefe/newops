<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLiquidAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('liquid_assets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('asset_model_id');
            $table->integer('asset_manufacturer_id');
            $table->integer('asset_category_id');
            $table->string('serial_no');
            $table->integer('color_id')->nullable();
            $table->text('maintenance')->nullable();
            $table->tinyInteger('active')->default(1);
            $table->tinyInteger('retired')->default(0);
            $table->integer('assignment_id')->nullable();
            $table->text('purchase_amount')->default(0);
            $table->text('purchase_date')->nullable();
            $table->integer('vendor_id')->nullable();
            $table->string('location')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('liquid_assets');
    }
}
