<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpenseRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expense_records', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->integer('vendor_id')->nullable();
            $table->integer('chart_of_account_id')->nullable();
            $table->integer('material_and_service_id')->nullable();
            $table->integer('bank_account_id')->nullable();
            $table->integer('individual_project_phase_id');
            // $table->integer('purchase_requisition_id')->nullable();
            $table->integer('vendor_contract_id')->nullable();
            $table->string('amount')->nullable();
            $table->string('date')->nullable();
            $table->integer('vendor_guarantor')->nullable();
            $table->integer('created_by')->nullable();
            $table->string('notes')->nullable();
            $table->integer('approved')->nullable();
            $table->integer('pay_period_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expense_records');
    }
}
