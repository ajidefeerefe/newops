<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id');
            $table->tinyInteger('system_setup')->default(0);
            $table->tinyInteger('color')->default(0);
            $table->tinyInteger('user_role')->default(0);
            $table->tinyInteger('user_department')->default(0);
            $table->tinyInteger('user_position')->default(0);
            $table->tinyInteger('company_location')->default(0);
            $table->tinyInteger('asset_manufacturer')->default(0);
            $table->tinyInteger('asset_model')->default(0);
            $table->tinyInteger('asset_category')->default(0);
            $table->tinyInteger('asset_file_type')->default(0);
            $table->tinyInteger('vendor_category')->default(0);
            $table->tinyInteger('vendor_pricing')->default(0);
            $table->tinyInteger('contracts')->default(0);
            $table->tinyInteger('project_details')->default(0);
            $table->tinyInteger('project_phase')->default(0);
            $table->tinyInteger('client_administration')->default(0);
            $table->tinyInteger('bank_account')->default(0);
            $table->tinyInteger('imprest_category')->default(0);
            $table->tinyInteger('projects')->default(0);
            $table->tinyInteger('project_phase_setup')->default(0);
            $table->tinyInteger('budget_manager(administrator)')->default(0);
            $table->tinyInteger('user_administration')->default(0);
            $table->tinyInteger('user_profile')->default(0);
            $table->tinyInteger('user_permission')->default(0);
            $table->tinyInteger('user_login')->default(0);
            $table->tinyInteger('asset_manager')->default(0);
            $table->tinyInteger('vendor_administration')->default(0);
            $table->tinyInteger('service_vendor')->default(0);
            $table->tinyInteger('inventory_store')->default(0);
            $table->tinyInteger('purchase_administration')->default(0);
            $table->tinyInteger('project_file')->default(0);
            $table->tinyInteger('resource_allocation')->default(0);
            $table->tinyInteger('view_client')->default(0);
            $table->tinyInteger('payroll')->default(0);
            $table->tinyInteger('assets')->default(0);
            $table->tinyInteger('carpool_manager')->default(0);
            $table->tinyInteger('vehicle_request')->default(0);
            $table->tinyInteger('finance_administration')->default(0);
            $table->tinyInteger('project_costing')->default(0);
            $table->tinyInteger('imprest_manager')->default(0);
            $table->tinyInteger('financial_reports')->default(0);
            $table->tinyInteger('system_setup(finance)')->default(0);
            $table->tinyInteger('reports')->default(0);
            $table->tinyInteger('material_and_labour')->default(0);
            $table->tinyInteger('banks')->default(0);
            $table->tinyInteger('inventory_location')->default(0);
            $table->tinyInteger('phase_material')->default(0);
            $table->tinyInteger('view_invoices')->default(0);
            $table->tinyInteger('employee_imprest')->default(0);
            $table->tinyInteger('contracts_by_date_range')->default(0);
            $table->tinyInteger('report_by_staff_allocation')->default(0);
            $table->tinyInteger('expense_by_project_phase')->default(0);
            $table->tinyInteger('expense_by_project')->default(0);
            $table->tinyInteger('full_expenses')->default(0);
            $table->tinyInteger('expense_by_project_phase_cumulative')->default(0);
            $table->tinyInteger('expense_by_date_range')->default(0);
            $table->tinyInteger('expense_by_category')->default(0);
            $table->tinyInteger('contracts_by_labour')->default(0);
            $table->tinyInteger('contract_by_project')->default(0);
            $table->tinyInteger('requisition_by_project')->default(0);
            $table->tinyInteger('requisition_by_date_range')->default(0);
            $table->tinyInteger('requisition_by_material')->default(0);
            $table->tinyInteger('vendor_flagging')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissions');
    }
}
