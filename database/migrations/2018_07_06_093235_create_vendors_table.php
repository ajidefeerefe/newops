<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('category_id');
            $table->string('specialty');
            $table->string('phone');
            $table->string('email');
            $table->string('address');
            $table->string('address2');
            // $table->tinyInteger('active')->default(1);
            $table->tinyInteger('flagged')->default(1);//William
            $table->integer('rating');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('flagged_by')->nullable();
            $table->integer('unflagged_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->integer('country_id');
            $table->integer('state_id');
            $table->integer('city_id');
            $table->string('note');
            $table->integer('bank_id');
            $table->string('account_number');
            $table->string('account_name');
            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('employee_imprest')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
