<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->tinyInteger('role_id')->nullable();
            $table->tinyInteger('department_id')->nullable();
            $table->tinyInteger('position_id')->nullable();
            $table->string('phone_no')->nullable();
            $table->string('relative_name')->nullable();
            $table->string('relative_relationship')->nullable();
            $table->string('relative_phone_no')->nullable();
            $table->string('alternative_phone_no')->nullable();
            $table->string('driver_license')->nullable();
            $table->string('national_id')->nullable();
            $table->string('international_id')->nullable();
            $table->string('bank_id')->nullable();
            $table->string('account_no')->nullable();
            $table->integer('account_type')->nullable();
            $table->double('salary',8,2)->default(0);
            $table->string('pay_frequency')->nullable();
            $table->string('salary_component')->nullable();
            $table->tinyInteger('active')->nullable();
            $table->tinyInteger('paye')->default(0);
            $table->tinyInteger('employee_imprest')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
