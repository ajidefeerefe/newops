<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeImprestTopupWithdrawalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_imprest_topup_withdrawals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->integer('employee_imprest_topup_id');
            $table->integer('project_phase_id');
            $table->integer('material_and_service_id');
            $table->text('withdraw_date');
            $table->text('amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_imprest_topup_withdrawals');
    }
}
