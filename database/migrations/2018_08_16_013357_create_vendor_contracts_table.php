<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id');
            $table->integer('material_and_service_id')->nullable();
            $table->integer('liquid_asset_id')->nullable();
            $table->integer('project_id');
            $table->integer('individual_project_phase_id');
            $table->integer('project_labour_id');
            $table->text('amount');
            $table->text('start_date');
            $table->text('end_date');
            $table->integer('contractor');
            $table->integer('created_by');
            $table->text('notes');
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_contracts');
    }
}
